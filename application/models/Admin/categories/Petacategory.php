<?php

class Petacategory extends CI_Model
{

    // Global Variables
    public $tbl = 'tbl_petacategory';

    // get data from table set in datatable
    function getData($postData = null)
    {

        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (title like '%" . $searchValue . "%') ";
        }

        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by("petacategory_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();

        $data = array();
        foreach ($records as $record) {
            $main = $this->md->select_where('tbl_category', array('category_id' => $record->main));
            $sub = $this->md->select_where('tbl_subcategory', array('subcategory_id' => $record->parent));
            $data[] = array(
                "id" => $record->petacategory_id,
                "main" => ($main) ? $main[0]->title : '<mark>Main Category Removed!</mark>',
                "parent" => ($sub) ? $sub[0]->title : '<mark>Sub Category Removed!</mark>',
                "title" => $record->title,
                "edit" => '<a href="' . base_url('manage-petacategory/edit/' . $record->petacategory_id) . '"style="color: #003eff" data-toggle="tooltip" data-placement="bottom" data-original-title="Update Data"><i class="far fa-edit" ></i></a> &nbsp; &nbsp; ',
                "delete" => '<a id="deleteItem" data-itemid="' . $record->petacategory_id . '"  class="text-danger cursor-pointer" data-toggle="tooltip" data-placement="bottom"  data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></a>'
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}

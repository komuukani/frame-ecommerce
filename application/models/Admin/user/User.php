<?php

class User extends CI_Model {

    // Global Variables
    public $tbl = 'tbl_admin';

    // get data from table set in datatable
    function getData($postData = null) {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search 
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (email like '%" . $searchValue . "%' or admin_name like '%" . $searchValue . "%' or phone like '%" . $searchValue . "%' or address like '%" . $searchValue . "%') ";
        }

        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by("admin_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();

        $data = array();
        foreach ($records as $record) {
            if ($record->admin_id != 1) {   // do not show super admin
                $data[] = array(
                    "status" => ($record->status == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>',
                    "user_role_id" => (($record->user_role_id != 0 ) ? $this->md->getItemName('tbl_role', 'role_id', 'title', $record->user_role_id) : '<mark>Data not found!</mark>'),
                    "admin_name" => $record->admin_name,
                    "contact" => '<a href="mailto:' . $record->email . '"><i class="far fa-paper-plane"></i> ' . $record->email . '</a> <br/> '
                    . '<a href="tel:' . $record->phone . '"><i class="fas fa-headset"></i> ' . $record->phone . '</a>',
                    "address" => $record->address,
                    "path" => '<a href="' . base_url($record->path ? $record->path : FILENOTFOUND) . '" target="_blank" data-toggle="tooltip" data-placement="right" title="" data-original-title="Show Full Category"><img src="' . base_url($record->path ? $record->path : FILENOTFOUND) . '" width="40" height="40" style="object-fit:contain" /></a>',
                    "datetime" => date(($webData ? $webData[0]->date_format : 'd-M-Y'), strtotime($record->register_date)),
                    "action" => ($record->admin_id != 1) ? (($record->status == 1) ? '<button id="inactiveItem" data-userid="' . $record->admin_id . '" class="btn btn-sm btn-danger">Change to Inactive</button>' : '<button id="activeItem" data-userid="' . $record->admin_id . '" class="btn btn-sm btn-success">Change to Active</button>') : '-',
                    "edit" => ($record->admin_id != 1) ? ('<a href="' . base_url('manage-user/edit/' . $record->admin_id) . '" class="cursor-pointer btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" data-original-title="Update Data"><i class="far fa-edit" ></i></a> &nbsp; &nbsp; ') : '-',
                    "delete" => ($record->admin_id != 1) ? ('<button id="deleteItem" data-itemid="' . $record->admin_id . '"  class="cursor-pointer btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom"  data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></button>') : '-'
                );
            }
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}

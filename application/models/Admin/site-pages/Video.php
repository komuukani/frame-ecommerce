<?php

class Video extends CI_Model
{

    // Global Variables
    public $tbl = 'tbl_video';

    // get data from table set in datatable
    function getData($postData = null)
    {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search 
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (title like '%" . $searchValue . "%') ";
        }

        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by("video_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();

        $data = array();
        foreach ($records as $record) {
            $brand = $this->md->select_where('tbl_brand', array('brand_id' => $record->brand_id));
            $data[] = array(
                "status" => ($record->status == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>',
                "brand" => ($brand) ? $brand[0]->title : '<mark>Brand Removed!</mark>',
                "title" => $record->title,
                "video" => '<a href="' . ($record->video) . '" target="_blank" data-toggle="tooltip" data-placement="right" title="" data-original-title="Show Full Video">Show Video</a>',
                "datetime" => date(($webData ? $webData[0]->date_format : 'd-M-Y'), strtotime($record->entry_date)),
                "action" => (($record->status == 1) ? '<button id="inactiveItem" data-userid="' . $record->video_id . '" class="btn btn-sm btn-danger">Change to Inactive</button>' : '<button id="activeItem" data-userid="' . $record->video_id . '" class="btn btn-sm btn-success">Change to Active</button>'),
                "edit" => '<a href="' . base_url('manage-video/edit/' . $record->video_id) . '"style="color: #003eff" data-toggle="tooltip" data-placement="bottom" data-original-title="Update Data"><i class="far fa-edit" ></i></a> &nbsp; &nbsp; ',
                "delete" => '<a id="deleteItem" data-itemid="' . $record->video_id . '"  class="text-danger cursor-pointer" data-toggle="tooltip" data-placement="bottom"  data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></a>'
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}

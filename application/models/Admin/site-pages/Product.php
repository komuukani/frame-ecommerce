<?php

class Product extends CI_Model
{

    // Global Variables
    public $tbl = 'tbl_product';

    // get data from table set in datatable
    function getData($postData = null)
    {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search 
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (title like '%" . $searchValue . "%' or sku like '%" . $searchValue . "%' or description like '%" . $searchValue . "%') ";
        }

        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by("product_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();

        $data = array();
        foreach ($records as $record) {
            $totalStock = $this->md->my_query('SELECT sum(stock) as `totalStock` FROM `tbl_inventory` WHERE `product_id` = ' . $record->product_id)->result();
            $photos = array_filter(explode(",", $record->photos));
            $tempPhoto = (file_exists('admin_asset/allPhotos/' . substr($record->title, 3) . '.jpg') ? ('admin_asset/allPhotos/' . substr($record->title, 3) . '.jpg') : FILENOTFOUND);
            $data[] = array(
                "status" => ($record->status == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>',
                "category" => $this->md->getItemName('tbl_category', 'category_id', 'title', $record->category_id),
                "sku" => $record->sku,
                "title" => $record->title,
                "photo" => '<a href="' . base_url($photos ? ($photos[0] ? $photos[0] : $tempPhoto) : $tempPhoto) . '"><img src="' . base_url($photos ? ($photos[0] ? $photos[0] : $tempPhoto) : $tempPhoto) . '" width="30px" /></a>',
                "stock" => $totalStock ? ($totalStock[0]->totalStock ? $totalStock[0]->totalStock : 0) : 0,
                "datetime" => date(($webData ? $webData[0]->date_format : 'd-M-Y'), strtotime($record->entry_date)),
                "action" => (($record->status == 1) ? '<button id="inactiveItem" data-userid="' . $record->product_id . '" class="btn btn-sm btn-danger">Change to Inactive</button>' : '<button id="activeItem" data-userid="' . $record->product_id . '" class="btn btn-sm btn-success">Change to Active</button>'),
                "edit" => '<a href="' . base_url('manage-product/edit/' . $record->product_id) . '"style="color: #003eff" data-toggle="tooltip" data-placement="bottom" data-original-title="Update Data"><i class="far fa-edit" ></i></a> &nbsp; &nbsp; ',
                "delete" => '<a id="deleteItem" data-itemid="' . $record->product_id . '"  class="text-danger cursor-pointer" data-toggle="tooltip" data-placement="bottom"  data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></a>'
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}

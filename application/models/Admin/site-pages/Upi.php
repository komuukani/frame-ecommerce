<?php

class Upi extends CI_Model
{

    // Global Variables
    public $tbl = 'tbl_upi';

    // get data from table set in datatable
    function getData($postData = null)
    {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (bank_name like '%" . $searchValue . "%' or upi like '%" . $searchValue . "%' or account_name like '%" . $searchValue . "%' or account_number like '%" . $searchValue . "%') ";
        }

        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by("upi_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();

        $data = array();
        foreach ($records as $record) {
            $data[] = array(
                "upi" => $record->upi,
                "bank_name" => $record->bank_name,
                "account_name" => $record->account_name,
                "account_number" => $record->account_number,
                "ifsc_code" => $record->ifsc_code,
                "branch" => $record->branch,
                "path" => '<a href="' . base_url($record->path ? $record->path : FILENOTFOUND) . '" target="_blank" data-toggle="tooltip" data-placement="right" title="" data-original-title="Show Full Category"><img src="' . base_url($record->path ? $record->path : FILENOTFOUND) . '" width="40" height="40" style="object-fit:contain" /></a>',
                "action" => (($record->status == 1) ? '<button id="inactiveItem" data-userid="' . $record->upi_id . '" class="btn btn-sm btn-danger">Change to Inactive</button>' : '<button id="activeItem" data-userid="' . $record->upi_id . '" class="btn btn-sm btn-success">Change to Active</button>'),
                "edit" => '<a href="' . base_url('manage-upi/edit/' . $record->upi_id) . '"style="color: #003eff" data-toggle="tooltip" data-placement="bottom" data-original-title="Update Data"><i class="far fa-edit" ></i></a> &nbsp; &nbsp; ',
                "delete" => '<a id="deleteItem" data-itemid="' . $record->upi_id . '"  class="text-danger cursor-pointer" data-toggle="tooltip" data-placement="bottom"  data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></a>'
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}

<?php

class Delivered extends CI_Model
{

    // Global Variables
    public $tbl = 'tbl_bill';

    // get data from table set in datatable
    function getData($postData = null)
    {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value

        ## Custom search filter
        $searchFrom = (isset($postData['searchFrom']) ? $postData['searchFrom'] : '');
        $searchTo = (isset($postData['searchTo']) ? $postData['searchTo'] : '');

        ## Search
        $search_arr = array();
        $searchQuery = "";

        if ($searchValue != '') {
            $search_arr[] = " (fname like '%" . $searchValue . "%' or email like '%" . $searchValue . "%' or phone like'%" . $searchValue . "%' or order_id like'%" . $searchValue . "%' or city like'%" . $searchValue . "%' ) ";
        }

        $search_arr[] = "status = 'Dispatched' AND cancelStatus = 0";  // show only delivered records

        if ($searchFrom != '' && $searchTo != "") {
            if ($searchFrom == $searchTo) {
                $search_arr[] = " entry_date like '%" . $searchFrom . "%' ";
            } else {
                $search_arr[] = " entry_date BETWEEN '" . $searchFrom . "' AND '" . $searchTo . "' ";
            }
        }

        if (count($search_arr) > 0) {
            $searchQuery = implode(" and ", $search_arr);
        }


        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
//        $this->db->where("status", 0);
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
//        $this->db->where("status", 0);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
//        $this->db->where("status", 0);
        $this->db->order_by("bill_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();
        $data = array();
        foreach ($records as $record) {
            $data[] = array(
                "status" => "<span class='badge badge-dark'>" . $record->status . "</span>",
                "order_id" => $record->order_id,
                "name" => $record->fname,
                "email" => $record->email,
                "phone" => $record->phone,
                "city" => $record->city,
                "address" => $record->address,
                "notes" => $record->notes,
                "datetime" => date(($webData ? $webData[0]->date_format : 'd-M-Y'), strtotime($record->entry_date)),
                "invoice" => '<a href="' . base_url('generate-report/view/' . $record->order_id) . '" target="_blank" data-toggle="tooltip" data-placement="right" title="Show Invoice" data-original-title="Show Invoice" class="btn btn-primary btn-sm">Show Invoice</a>',
                "download" => '<a href="' . base_url('generate-report/download/' . $record->order_id) . '" target="_blank" data-toggle="tooltip" data-placement="right" title="Download Invoice" data-original-title="Download Invoice" class="btn btn-info btn-sm">Download</a>',
//                "delete" => '<a id="deleteItem" data-itemid="' . $record->bill_id . '" class="text-danger cursor-pointer" data-toggle="tooltip" data-placement="bottom" data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></a>'
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}
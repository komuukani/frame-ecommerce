<?php

class Coupon extends CI_Model
{

    // Global Variables
    public $tbl = 'tbl_coupon';

    // get data from table set in datatable
    function getData($postData = null)
    {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search 
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (coupon_code like '%" . $searchValue . "%' or coupon_description like '%" . $searchValue . "%' or coupon_percentage like '%" . $searchValue . "%' or expiry_date like '%" . $searchValue . "%' or entry_date like '%" . $searchValue . "%') ";
        }

        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by("coupon_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();

        $data = array();
        foreach ($records as $record) {
            $data[] = array(
//                "status" => ($record->status == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>',
                "coupon_code" => strtoupper($record->coupon_code),
                "coupon_description" => $record->coupon_description,
                "coupon_percentage" => ($record->coupon_percentage),
                "minimum_subtotal" => ($record->minimum_subtotal),
                "maximum_subtotal" => ($record->maximum_subtotal),
                "free_shipping" => $record->free_shipping,
                "expiry_date" => date(($webData ? $webData[0]->date_format : 'd-M-Y'), strtotime($record->expiry_date)),
                "entry_date" => date(($webData ? $webData[0]->date_format : 'd-M-Y'), strtotime($record->entry_date)),
//                "action" => (($record->status == 1) ? '<button id="inactiveItem" data-userid="' . $record->coupon_id . '" class="btn btn-sm btn-danger">Change to Inactive</button>' : '<button id="activeItem" data-userid="' . $record->coupon_id . '" class="btn btn-sm btn-success">Change to Active</button>'),
                "edit" => ('<a href="' . base_url('manage-coupon/edit/' . $record->coupon_id) . '" class="cursor-pointer btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" data-original-title="Update Data"><i class="far fa-edit" ></i></a> &nbsp; &nbsp; '),
                "delete" => ('<button id="deleteItem" data-itemid="' . $record->coupon_id . '"  class="cursor-pointer btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom"  data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></button>')
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }
}

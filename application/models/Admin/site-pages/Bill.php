<?php

class Bill extends CI_Model
{

    // Global Variables
    public $tbl = 'tbl_bill';

    // get data from table set in datatable
    function getData($postData = null)
    {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value

        ## Custom search filter
        $searchFrom = (isset($postData['searchFrom']) ? $postData['searchFrom'] : '');
        $searchTo = (isset($postData['searchTo']) ? $postData['searchTo'] : '');
        $billStatus = (isset($postData['billStatus']) ? $postData['billStatus'] : '');

        ## Search
        $search_arr = array();
        $searchQuery = "";

        if ($searchValue != '') {
            $search_arr[] = " (fname like '%" . $searchValue . "%' or email like '%" . $searchValue . "%' or phone like'%" . $searchValue . "%' or order_id like'%" . $searchValue . "%' or city like'%" . $searchValue . "%' ) ";
        }

        if ($billStatus != '') {
            if ($billStatus == 'all') {
                $search_arr[] = " cancelStatus = 0";
            } else {
                $search_arr[] = "status = '" . $billStatus . "' AND cancelStatus = 0";
            }
        }

        if ($searchFrom != '' && $searchTo != "") {
            if ($searchFrom == $searchTo) {
                $search_arr[] = " entry_date like '%" . $searchFrom . "%' ";
            } else {
                $search_arr[] = " entry_date BETWEEN '" . $searchFrom . "' AND '" . $searchTo . "' ";
            }
        }

        if (count($search_arr) > 0) {
            $searchQuery = implode(" and ", $search_arr);
        }


        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
//        $this->db->where("status", 0);
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
//        $this->db->where("status", 0);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
//        $this->db->where("status", 0);
        $this->db->order_by("bill_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();
        $data = array();
        foreach ($records as $record) {
            $products = '';
            $proItems = $this->md->select_where('tbl_transaction', array('bill_id' => $record->bill_id));
            if ($proItems) {
                foreach ($proItems as $proItem) {
                    $product = $this->md->select_where('tbl_product', array('product_id' => $proItem->product_id));
                    $products .= $product ? ($product[0]->title . ", ") : '';
                }
            }
            // Handling multiple images in shippedPhoto to display them in one line
            $photos = '';
            if ($record->shippedPhoto) {
                $photoArray = explode(',', $record->shippedPhoto); // Split by commas
                foreach ($photoArray as $photo) {
                    $trimmedPhoto = trim($photo);
                    $photos .= '<a href="javascript:void(0);" data-toggle="modal" data-target="#imagePreviewModal" data-src="' . $trimmedPhoto . '">
                        <img src="' . $trimmedPhoto . '" width="40" height="30" style="margin: 2px; display: inline-block; object-fit:contain;" />
                    </a>';
                }
            }

            $products = rtrim($products, ", ");
            $data[] = array(
                "status" => "<span class='badge badge-dark'>" . $record->status . "</span>",
                "order_id" => $record->order_id,
                "name" => $record->fname,
                "email" => $record->email,
                "phone" => $record->phone,
                "city" => $record->city,
                "address" => $record->address,
                "items" => $products,
                "notes" => $record->notes,
                "total" => $record->total,
                "photos" => '<div style="white-space: nowrap;">' . $photos . '</div>',
                "lr_no" => $record->lr_no,
                "date_of_lr" => $record->date_of_lr,
                "datetime" => date(($webData ? $webData[0]->date_format : 'd-M-Y'), strtotime($record->entry_date)),
                "statusUpdate" => '<button data-toggle="modal" id="billID" data-billid="' . $record->bill_id . '" data-target="#update_status" class="btn btn-warning btn-sm">Update Status</button>',
                "invoice" => '<a href="' . base_url('generate-report/view/' . $record->order_id) . '" target="_blank" data-toggle="tooltip" data-placement="right" title="Show Invoice" data-original-title="Show Invoice" class="btn btn-primary btn-sm">Show Invoice</a>',
                "download" => '<a href="' . base_url('generate-report/download/' . $record->order_id) . '" target="_blank" data-toggle="tooltip" data-placement="right" title="Download Invoice" data-original-title="Download Invoice" class="btn btn-info btn-sm">Download</a>',
                "delete" => '<a id="deleteItem" data-itemid="' . $record->bill_id . '" class="text-danger cursor-pointer" data-toggle="tooltip" data-placement="bottom" data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></a>'
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}
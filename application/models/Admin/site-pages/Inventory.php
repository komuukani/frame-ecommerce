<?php

class Inventory extends CI_Model
{
    // get data from table set in datatable
    function getData($postData = null)
    {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value


        ## Custom search filter
        $tableName = (isset($postData['tableName']) ? $postData['tableName'] : ''); // product / location
        $tbl = 'tbl_' . ($tableName ? $tableName : 'product');

        ## Search
        $search_arr = array();
        $searchQuery = "";

        if ($searchValue != '') {
            $search_arr[] = " (title like '%" . $searchValue . "%') ";
        }

        if (count($search_arr) > 0) {
            $searchQuery = implode(" and ", $search_arr);
        }


        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
//        $this->db->where("status", 0);
        $records = $this->db->get($tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
//        $this->db->where("status", 0);
        $records = $this->db->get($tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
//        $this->db->where("status", 0);
        if ($tableName == '' || $tableName == 'product') {
            $this->db->order_by("product_id", "desc");
        } else {
            $this->db->order_by("location_id", "desc");
        }
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($tbl)->result();

        $data = array();
        if ($tableName == '' || $tableName == 'product') {
            foreach ($records as $record) {
                $totalStock = $this->md->my_query('SELECT sum(stock) as `totalStock` FROM `tbl_inventory` WHERE `product_id` = ' . $record->product_id)->result();
                $data[] = array(
                    "category" => $this->md->getItemName('tbl_category', 'category_id', 'title', $record->category_id),
                    "sku" => ucfirst($record->sku),
                    "title" => ucfirst($record->title),
                    "stock" => $totalStock ? ($totalStock[0]->totalStock ? $totalStock[0]->totalStock : 0) : 0,
                    "check" => '<button data-toggle="modal" id="productID" data-table="product" data-type="check" data-proid="' . $record->product_id . '" data-target="#inventoryModal" class="btn btn-info btn-sm">Check Stock</button>',
                    "add" => '<button data-toggle="modal" id="productID" data-table="product" data-type="add" data-proid="' . $record->product_id . '" data-target="#inventoryModal" class="btn btn-success btn-sm">Add / Edit Stock</button>'
                );
            }
        } else {
            foreach ($records as $record) {
                $totalStock = $this->md->my_query('SELECT sum(stock) as `totalStock` FROM `tbl_inventory` WHERE `location_id` = ' . $record->location_id)->result();
                $data[] = array(
                    "category" => '',
                    "sku" => '',
                    "title" => ucfirst($record->title),
                    "stock" => $totalStock ? ($totalStock[0]->totalStock ? $totalStock[0]->totalStock : 0) : 0,
                    "check" => '<button data-toggle="modal" id="productID" data-table="location" data-type="check" data-proid="' . $record->location_id . '" data-target="#inventoryModal" class="btn btn-info btn-sm">Check Stock</button>',
                    "add" => '<button data-toggle="modal" id="productID" data-table="location" data-type="add" data-proid="' . $record->location_id . '" data-target="#inventoryModal" class="btn btn-success btn-sm">Add / Edit Stock</button>'
                );
            }
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}

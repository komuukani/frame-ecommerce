<?php

defined('BASEPATH') or exit('No direct script access allowed');
// Front Side Routes
$route['home/?(:num)?'] = 'Pages/index';
$route['contact'] = 'Pages/contact';
$route['aboutus'] = 'Pages/aboutus';
$route['policy'] = 'Pages/policy';
$route['terms'] = 'Pages/terms';
$route['quality-policy'] = 'Pages/qualityPolicy';
$route['pricing-policy'] = 'Pages/pricingPolicy';
$route['return-refund-policy'] = 'Pages/returnRefundPolicy';
$route['shipping-policy'] = 'Pages/shippingPolicy';
$route['faq'] = 'Pages/faq';
$route['collection/?(:any)?'] = 'Pages/collection/$2/';
$route['product/?(:any)?/?(:any)?'] = 'Pages/product/$2/$3';
$route['blog/?(:any)?'] = 'Pages/blog/$2';
$route['404'] = 'Install/page_not_found';


// User pages
$route['user-login'] = 'User/login';
$route['user-register'] = 'User/register';
$route['user-verify'] = 'User/verify';
$route['welcome'] = 'User/welcome';
$route['myorder'] = 'User/myorder';
$route['cart'] = 'User/cart';
$route['wishlist'] = 'User/wishlist';
$route['checkout'] = 'User/checkout';
$route['update-password'] = 'User/update_password';
$route['change-password'] = 'User/change_password';
$route['forgot-password'] = 'User/forgot_password';
$route['edit-profile'] = 'User/edit_profile';
$route['profile-success'] = 'User/profile_success';
$route['user-logout'] = 'User/logout';
$route['user-forgot-password'] = 'User/forgot_password';
$route['order-placed'] = 'User/success';

/* ==================================================
  >> ADMIN ROUTES START
  ================================================== */
// >> AUTHENTICATION ROUTES
$route['authorize'] = 'Admin/Login/index';
$route['admin-forgot-password'] = 'Admin/Login/forgot_password';
$route['admin-dashboard'] = 'Admin/Login/dashboard';
$route['admin-profile'] = 'Admin/Login/profile';
$route['admin-setting/?(:any)?'] = 'Admin/Login/setting/$2';
$route['admin-feature-setting'] = 'Admin/Login/feature_setting';
$route['control-panel/?(:any)?'] = 'Admin/Login/control_panel/$3';
$route['admin-logout'] = 'Admin/Login/logout';
// >> COMMON PAGES ROUTES  
$route['add-order/?(:any)?/?(:any)?'] = 'Admin/Pages/addOrder/$2/$3';
$route['manage-contact/?(:any)?'] = 'Admin/Pages/contact/$2';
$route['manage-email_subscriber/?(:any)?'] = 'Admin/Pages/email/$2';
$route['manage-feedback/?(:any)?'] = 'Admin/Pages/feedback/$2';
$route['manage-banner/?(:any)?/?(:any)?'] = 'Admin/Pages/banner/$2/$3';
$route['manage-gallery/?(:any)?/?(:any)?'] = 'Admin/Pages/gallery/$2/$3';
$route['manage-client/?(:any)?/?(:any)?'] = 'Admin/Pages/client/$2/$3';
$route['manage-aboutus/?(:any)?/?(:any)?'] = 'Admin/Pages/aboutus/$2/$3';
$route['manage-policy/?(:any)?/?(:any)?'] = 'Admin/Pages/policy/$2/$3';
$route['manage-terms/?(:any)?/?(:any)?'] = 'Admin/Pages/terms/$2/$3';
$route['manage-inquiry/?(:any)?/?(:any)?'] = 'Admin/Pages/inquiry/$2/$3';
$route['manage-review/?(:any)?/?(:any)?'] = 'Admin/Pages/review/$2/$3';
$route['manage-blog/?(:any)?/?(:any)?'] = 'Admin/Pages/blog/$2/$3';
$route['manage-team/?(:any)?/?(:any)?'] = 'Admin/Pages/team/$2/$3';
$route['manage-video/?(:any)?/?(:any)?'] = 'Admin/Pages/video/$2/$3';
$route['manage-seo/?(:any)?/?(:any)?'] = 'Admin/Pages/seo/$2/$3';
$route['manage-category/?(:any)?/?(:any)?'] = 'Admin/Pages/category/$2/$3';
$route['manage-subcategory/?(:any)?/?(:any)?'] = 'Admin/Pages/subcategory/$2/$3';
$route['manage-user/?(:any)?/?(:any)?'] = 'Admin/Pages/user/$2/$3';
$route['manage-role/?(:any)?/?(:any)?'] = 'Admin/Pages/role/$2/$3';
$route['manage-permission/?(:any)?/?(:any)?'] = 'Admin/Pages/permission/$2/$3';
$route['manage-mainmenu/?(:any)?/?(:any)?'] = 'Admin/Pages/mainmenu/$2/$3';
$route['manage-submenu/?(:any)?/?(:any)?'] = 'Admin/Pages/submenu/$2/$3';
$route['generate-report/?(:any)?/?(:any)?'] = 'Admin/Pages/generate_report/$2/$3';
$route['add-photos/?(:num)?'] = 'Admin/Pages/addPhotos/$2';
// >> REMOVE ROUTES
$route['remove/(:any)/(:any)/?(:any)?'] = 'Admin/Pages/delete/$2/$3/$4';
/* XXXXXXXXXXXXXXXX     << ADMIN ROUTES END    XXXXXXXXXXXXXXXX */

/* ==================================================
  >> DEFAULT ROUTES START
  ================================================== */
$route['404_override'] = 'Install/page_not_found';
$route['translate_uri_dashes'] = FALSE;
$route['default_controller'] = 'Pages';
/* XXXXXXXXXXXXXXXX     << DEFAULT ROUTES END    XXXXXXXXXXXXXXXX */


/* ==================================================
  >> MAIN MENU & SUB MENU FROM DATABASE START
  ================================================== */
require_once(BASEPATH . 'database/DB.php');
$db = &DB();
// Main Menu 
if (INSTALLER_SETTING['database']) :
    if ($db->table_exists('tbl_mainmenu')) :
        $query = $db->get('tbl_mainmenu');  // get all main menu
        $result = $query->result();
        foreach ($result as $row) :
            $route[$row->slug] = $row->url; // set dynamic main menu route
        endforeach;
        // Sub Menu
        $query = $db->get('tbl_submenu');   // get all sub menu
        $result = $query->result();
        foreach ($result as $row) :
            $route[$row->slug] = $row->url; // set dynamic main sub route
        endforeach;
    endif;
endif;
///* XXXXXXXXXXX   << MAIN MENU & SUB MENU FROM DATABASE ROUTES END  XXXXXXXXXX */

<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') or define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*  _________ My Custom Variables Start _________ */

$root = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'];
$root .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

// $whitelist = array('127.0.0.1', '::1');  // local IP addresses
// if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)):   // checking the local ip address
//     // online
//     defined('DATABASE') OR define('DATABASE', ''); // Database Name
//     defined('USERNAME') OR define('USERNAME', ''); // Username
//     defined('PASSWORD') OR define('PASSWORD', ''); // Password 
//     defined('ROOT_URL') OR define('ROOT_URL', $root); // WEBSITE MAIN URL
// else:
//     // offline - Localhost DB
//     defined('DATABASE') OR define('DATABASE', 'db_enzo_admin_demo'); // Database Name
//     defined('USERNAME') OR define('USERNAME', 'root'); // Username
//     defined('PASSWORD') OR define('PASSWORD', ''); // Password
//     defined('ROOT_URL') OR define('ROOT_URL', $root); // WEBSITE MAIN URL
// endif;

defined('WEBSITE_STATUS') or define('WEBSITE_STATUS', 'live'); // WEBSITE STATUS    [demo/live]
defined('DEVELOPER') or define('DEVELOPER', 'Nishant Thummar'); // Developer company
defined('WEBLINK') or define('WEBLINK', 'https://nishantthummar.in/'); // Company web link
defined('TABLE_PREFIX') or define('TABLE_PREFIX', 'tbl_'); // Table prefix
defined('DEFAULT_TIMEZONE') or define('DEFAULT_TIMEZONE', 'Asia/Kolkata'); // Default Datetime zone
defined('FILENOTFOUND') or define('FILENOTFOUND', 'admin_asset/img/no_photo.jpg'); // File not found 404 photo
defined('PER_PAGE') or define('PER_PAGE', 4);

// STRIPE APIS TEST
//defined('STRIPE_PUBLISHABLE_KEY') or define('STRIPE_PUBLISHABLE_KEY', 'pk_test_51O6iwbGc96af4g0YyNRTpyD9lGU7mQbidMb45Wm8BgtyiMKc6fOyAFGrn5qS3247SHtfSne6p21B32VyCcSleBGB00sWcchJah');
//defined('STRIPE_SECRET_KEY') or define('STRIPE_SECRET_KEY', 'sk_test_51O6iwbGc96af4g0YxjGAUl008tQqxoNKVkdKZLCvm3FqibjOpBgFe3LC3JW9xwParNo86kgHfRwOrNRD6jsd9pGK00e2FtA6oJ');

// STRIPE APIS LIVE
defined('STRIPE_PUBLISHABLE_KEY') or define('STRIPE_PUBLISHABLE_KEY', 'pk_live_51O6iwbGc96af4g0YMHB72ogTQbbWp3qgbbMUyc9Cy8UM9j2a1jJ8gsj46nsSHjH8ap5AWPiztqmQaxaf33UnojlA004BsyERXN');
defined('STRIPE_SECRET_KEY') or define('STRIPE_SECRET_KEY', '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');

defined('STRIPE_CURRENCY') or define('STRIPE_CURRENCY', 'NZD');


/*  _________ My Custom Variables END _________ */


/* ---- CI INSTALLER CONSTANT---- */
/* -----CI installer------- */

function getInstallerSettings()
{
    $dirArray = explode(DIRECTORY_SEPARATOR, __DIR__);
    unset($dirArray[count($dirArray) - 1]);
    $dir = implode(DIRECTORY_SEPARATOR, $dirArray);
    $file = $dir . "/controllers/installer.json";
    $myfile = fopen($file, "r") or die("Unable to open file!");
    $fileContent = fread($myfile, filesize($file));
    fclose($myfile);
    return json_decode($fileContent, true);
}

define('INSTALLER_SETTING', getInstallerSettings());

/*-----CI installer-------*/
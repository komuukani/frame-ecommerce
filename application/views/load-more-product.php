<?php
if ($allProducts) {
    foreach ($allProducts as $product) {
        //$category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
        $attributes = $this->md->select_where('tbl_product_attribute', array('product_id' => $product->product_id));

        $photos = array(FILENOTFOUND);
        if ($attributes) {
            $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
        }

        $url = base_url('product/' . $product->slug . "/" . $product->product_id);
        ?>
        <div class="product text-center mb-20">
            <?php
            if ($product->sale) {
                echo '<span class="saleBadge">Sale</span>';
            }
            ?>
            <figure
                class="product-media">
                <a href="<?php echo $url; ?>">
                    <img
                        src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                        alt="<?php echo $product->title; ?>"
                        title="<?php echo $product->title; ?>"
                        class="imgPreview"
                        style="width: 100%;height: 330px;object-fit: cover"
                    >
                    <?php
                    if (array_key_exists(1, $photos)) {
                        ?>
                        <img
                            src="<?php echo base_url($photos ? ($photos[1] ? $photos[1] : FILENOTFOUND) : FILENOTFOUND); ?>"
                            alt="<?php echo $product->title; ?>"
                            title="<?php echo $product->title; ?>"
                            style="width: 100%;height: 330px;object-fit: cover"
                        >
                        <?php
                    }
                    ?>
                </a>
                <div class="customShoppingCart">
                    <form method="post" action="<?php echo base_url('Pages/addToCart'); ?>"
                          name="addToCartForm">
                        <?php
                        $wh['product_id'] = $product->product_id;
                        $wh['product_attribute_id'] = $attributes ? $attributes[0]->product_attribute_id : '';
                        $wh['category_id'] = $product->category_id;
                        $wh['unique_id'] = $this->input->cookie('unique_id');
                        $exist = $this->md->select_where('tbl_cart', $wh);
                        ?>
                        <input type="hidden" name="quantity"
                               value="<?php echo $exist ? ($exist[0]->qty + 1) : 1; ?>"/>
                        <input type="hidden" name="productId"
                               value="<?php echo $product->product_id; ?>"/>
                        <input type="hidden" name="productAttributeId"
                               value="<?php echo $attributes ? $attributes[0]->product_attribute_id : ''; ?>"/>
                        <input type="hidden" name="categoryId"
                               value="<?php echo $product->category_id; ?>"/>
                        <input type="hidden" name="price"
                               value="<?php echo $product->sale ? $product->sale_price : $product->price; ?>"/>
                        <button name="addToCart" value="addToCart" type="submit" class="cartIcon">
                            <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg"
                                 fill-rule="evenodd" clip-rule="evenodd">
                                <path
                                    d="M13.5 18c-.828 0-1.5.672-1.5 1.5 0 .829.672 1.5 1.5 1.5s1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5zm-3.5 1.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5zm14-16.5l-.743 2h-1.929l-3.473 12h-13.239l-4.616-11h2.169l3.776 9h10.428l3.432-12h4.195zm-12 4h3v2h-3v3h-2v-3h-3v-2h3v-3h2v3z"/>
                            </svg>
                        </button>
                    </form>
                </div>
            </figure>
            <div class="product-details text-left p-10 pl-0">
                <h3 class="product-name font-16 font-weight-600 text-000">
                    <a href="<?php echo $url; ?>"><?php echo $product->title; ?></a>
                </h3>
                <div class="product-price">
                    <?php
                    if ($product->sale) {
                        ?>
                        <del
                            class="price d-xs-block text-999 font-weight-normal"><?php echo number_format($product->price); ?>
                            SAR
                        </del>
                        <span
                            class="ml-lg-2 ml-md-2 ml-xs-0 price font-weight-normal"><?php echo $product->sale_price ? number_format($product->sale_price) : $product->price; ?> SAR</span>
                        <?php
                    } else {
                        ?>
                        <span
                            class="price font-weight-normal"><?php echo number_format($product->price); ?> SAR</span>
                        <?php
                    }
                    ?>
                </div>
                <div class="product-variations justify-content-start">
                    <?php
                    $allattributes = $this->md->my_query('select * from `tbl_product_attribute` where `product_id` =' . $product->product_id)->result();
                    if ($allattributes) {
                        foreach ($allattributes as $key => $attribute) {
                            $photos = $attribute->product_photos ? explode(",", $attribute->product_photos) : array(FILENOTFOUND);
                            ?>
                            <a class="color <?php echo $key == 0 ? 'active' : ''; ?> mr-1 <?php echo $key >= 6 ? 'hiddenColor' : ''; ?>"
                               onclick="$(this).parent().parent().prev().find('.imgPreview').attr('src','<?php echo base_url($photos[0]); ?>');"
                               href="javascript:void(0)"
                               style="
                                   background-color: <?php echo $attribute->color; ?>;
                               <?php echo $attribute->color == 'white' ? 'border:1px solid #dfdfdf' : ''; ?>;
                               <?php echo $key >= 6 ? 'display:none' : 'display:block'; ?>
                                   "
                            ></a>
                            <?php
                        }
                        if (count($allattributes) >= 6) {
                            ?>
                            <i
                                class="fa fa-plus text-000 font-18 ml-1 showHiddenColor"></i>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
} else {
    echo 'empty';
}
?>
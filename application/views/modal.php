<?php
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$category = $this->md->select_limit_order('tbl_category', 100, 'position', 'asc');
$web_data = ($web_data) ? $web_data[0] : '';

$user = $this->session->userdata('email');
$userdata = $cartProduct = array();
if (isset($user)) :
    $userdata = $this->md->select_where('tbl_register', array('email' => $user));
    $cartProduct = $this->md->select_where('tbl_cart', array('register_id' => ($userdata ? $userdata[0]->register_id : '')));
else:
    $cartProduct = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));
endif;
?>

<!-- Cart Drawer -->
<div class="aside aside_right overflow-hidden cart-drawer" id="cartDrawer">
    <div class="aside-header d-flex align-items-center">
        <h3 class="text-uppercase fs-6 mb-0">SHOPPING BAG ( <span
                    class="cart-amount js-cart-items-count"><?php echo count($cartProduct); ?></span> )
        </h3>
        <button class="btn-close-lg js-close-aside btn-close-aside ms-auto"></button>
    </div><!-- /.aside-header -->

    <div class="aside-content cart-drawer-items-list">
        <?php
        if ($cartProduct) {
            foreach ($cartProduct as $cartitem) {
                $productData = $this->md->select_where('tbl_product', array('product_id' => $cartitem->product_id));
                if ($productData) {
                    $product = $productData[0];
                    $category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
                    $photos = array_filter(explode(",", $product->photos));
                    $url = base_url('product/' . $product->slug . "/" . $product->product_id);
                    $tempPhoto = (file_exists('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') ? ('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') : FILENOTFOUND);
                    ?>
                    <div class="cart-drawer-item d-flex position-relative">
                        <div class="position-relative">
                            <a href="<?php echo $url; ?>">
                                <img
                                        loading="lazy"
                                        class="cart-drawer-item__img"
                                        src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : $tempPhoto) : $tempPhoto); ?>"
                                        alt="<?php echo $product->title; ?>"
                                        title="<?php echo $product->title; ?>">
                            </a>
                        </div>
                        <div class="cart-drawer-item__info flex-grow-1">
                            <h6 class="cart-drawer-item__title fw-normal"><a
                                        href="<?php echo $url; ?>"><?php echo $product->title; ?></a></h6>
                            <p class="cart-drawer-item__option text-secondary">
                                Category: <?php echo $category ? $category[0]->title : '-'; ?> </p>
                            <p class="cart-drawer-item__option text-secondary">
                                Quantity: <?php echo $cartitem->qty; ?> </p>
                        </div>

                        <a href="<?php echo base_url('Pages/removeCartItem/' . $cartitem->cart_id); ?>"
                           class="btn-close-xs position-absolute top-0 end-0"></a>
                    </div><!-- /.cart-drawer-item d-flex -->
                    <hr class="cart-drawer-divider">
                    <?php
                }
            }
        }
        ?>
    </div><!-- /.aside-content -->

    <div class="cart-drawer-actions position-absolute start-0 bottom-0 w-100">
        <hr class="cart-drawer-divider">
        <a href="<?php echo base_url('cart'); ?>" class="btn btn-light mt-3 d-block">View Cart</a>
        <a href="<?php echo base_url('checkout'); ?>" class="btn btn-primary mt-3 d-block">Checkout</a>
    </div><!-- /.aside-content -->
</div><!-- /.aside -->

<!-- Go To Top -->
<div id="scrollTop" class="visually-hidden end-0"></div>

<!-- Page Overlay -->
<div class="page-overlay"></div><!-- /.page-overlay -->
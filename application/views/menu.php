<div class="cart-dropdown off-canvas">
    <div class="dropdown-box">
        <div class="canvas-header">
            <?php
            if ($cartProduct) {
                ?>
                <h4
                    class="canvas-title text-transform-capitalize font-weight-bold font-24">Your
                    Cart</h4>
                <?php
            }
            ?>
            <a href="javascript:void(0)" class="btn btn-dark btn-link btn-icon-right btn-close"
               onclick="$('.cartModal').hide();$('.cartModalBody').hide();$('body').css('overflow-y','auto');$('.cartModalBody').css('transform','translateX(350px)')"><i
                    class="d-icon-close"></i><span class="sr-only">Cart</span></a>
        </div>
        <?php
        if ($cartProduct) {
            ?>
            <div class="d-flex justify-content-between mt-20">
                <span class="font-10 text-transform-uppercase text-999 letter-spacing-2">Product</span>
                <span class="font-10 text-transform-uppercase text-999 letter-spacing-2">Total</span>
            </div>
            <?php
        }
        ?>
        <div class="products scrollable"
             style="<?php echo (!$cartProduct) ? 'height: 100% !important; max-height: 100% !important; display: flex; align-items: center; flex-direction: column; justify-content: center;' : ''; ?>">
            <?php
            $total = 0;
            if ($cartProduct) {
                foreach ($cartProduct as $cartitem) {
                    $productData = $this->md->select_where('tbl_product', array('product_id' => $cartitem->product_id));
                    if ($productData) {
                        $product = $productData[0];
                        $attributes = $this->md->select_where('tbl_product_attribute', array('product_attribute_id' => $cartitem->product_attribute_id));

                        $photos = array(FILENOTFOUND);
                        if ($attributes) {
                            $photos = $attributes[0]->product_photos ? explode(",", $attributes[0]->product_photos) : array(FILENOTFOUND);
                        }

                        $url = base_url('product/' . $product->slug . "/" . $product->product_id . "/?color=" . $attributes[0]->color);
                        $total = $total + $cartitem->netprice; // Net total
                        ?>
                        <div class="product product-cart">
                            <figure class="product-media">
                                <a href="<?php echo $url ?>">
                                    <img
                                        src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                        alt="<?php echo $product->title; ?>"
                                        title="<?php echo $product->title; ?>"
                                        style="width: 80px;height: 88px;object-fit: contain"
                                    >
                                </a>
                            </figure>
                            <div class="product-detail">
                                <a href="<?php echo $url; ?>"
                                   class="product-name font-weight-bold font-16 text-000 mb-0 letter-spacing-1"><?php echo $productData ? $productData[0]->title : ''; ?>
                                </a>
                                <div class="price-box">
                                    <!--                                                    <span class="product-quantity">-->
                                    <?php //echo $cartitem->qty; ?><!--</span>-->
                                    <span
                                        class="product-price text-999 font-14"><?php echo $cartitem->price ? number_format($cartitem->price) : ''; ?> SAR</span>
                                </div>
                                <form method="post" action="<?php echo base_url('Pages/updateQty'); ?>"
                                      class="mt-2"
                                      name="updateCartForm">
                                    <input type="hidden" name="cartId"
                                           value="<?php echo $cartitem->cart_id; ?>"/>
                                    <div class="input-group">
                                        <div class="qty-container">
                                            <button class="qty-btn-minus btn-light" type="submit"
                                                    onclick="$(this).parentsUntil('.product-quantity').submit();">
                                                <i
                                                    class="fa fa-minus"></i></button>
                                            <input type="text" name="quantity"
                                                   value="<?php echo $cartitem->qty; ?>"
                                                   onchange="$(this).parentsUntil('.product-quantity').submit();"
                                                   class="input-qty"/>
                                            <button class="qty-btn-plus btn-light" type="submit"
                                                    onclick="$(this).parentsUntil('.product-quantity').submit();">
                                                <i
                                                    class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    <a href="<?php echo base_url('Pages/removeCartItem/' . $cartitem->cart_id); ?>"
                                       class="btn btn-link btn-close"
                                       style="display: flex;height: 2.2rem;padding: 0;border: none">
                                        <i class="fas fa-trash-alt font-16"></i>
                                    </a>
                                </form>
                                <span
                                    style="position: absolute; right: 0;top: 0;    line-height: 18px;color:#000"
                                    class="product-price text-999 font-16"><?php echo $cartitem->netprice ? number_format($cartitem->netprice) : ''; ?> <br/>SAR</span>
                            </div>
                        </div>
                        <?php
                    }
                }
            } else {
                echo "<h4 class='text-center font-weight-bold'>Your cart is empty</h4>";
                echo '<a href="' . base_url('checkout') . '" class="checkoutBtn2 text-center d-block" style="width: 75%">Continue shopping</a>';
            }
            ?>
        </div>
        <?php
        if ($cartProduct) {
            ?>
            <div style="position: absolute;bottom: 10px;width: 93%;margin: auto;">
                <div class="cart-total border-bottom-none font-16" style="border-top: 1px solid #ddd">
                    <label class="font-weight-bold letter-spacing-1">Subtotal</label>
                    <span class="price font-20"><?php echo $total != 0 ? number_format($total) : 0; ?> SAR</span>
                </div>
                <div>
                    <p class="text-left font-weight-normal text-999 letter-spacing-1 font-14">VAT
                        included. Shipping cost
                        at
                        checkout</p>
                </div>

                <div class="cart-action">
                    <!--                                <a href="-->
                    <?php //echo base_url('cart'); ?><!--" class="btn btn-dark btn-link">View Cart</a>-->
                    <a href="<?php echo base_url('checkout'); ?>"
                       class="checkoutBtn2 text-center d-block">Check out</a>
                </div>
            </div>
            <?php
        }
        ?>

    </div>
</div>
<?php
echo $page_head;
?>
<body class="home">
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main mt-100 mt-xs-50">
        <section class="pt-50 pb-50">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="text-left">
                            <h2 class="font-58 font-weight-bold">Pricing Policy</h2>
                        </div>
                        <div>
                            <p style="font-size: 16.8px">
                                1. infiniti exhibits a customer-centric pricing system which is built on a value-based
                                pricing criteria.
                                <br/><br/>
                                2. We believe Furniture is a need not a luxury, thus its cost has to stay reasonable and
                                within everyone’s reach.
                                <br/><br/>
                                3. Value to customers is set among our priorities, by always striving to increase
                                customer benefits and decrease their costs.
                                <br/><br/>
                                4. Our prices are unparalleled in the market, in comparison to the fine quality standard
                                that we furnish.
                                <br/><br/>
                                5. Customers should not be subjected to overrated prices to buy any commodity or
                                service.
                                <br/><br/>
                                6. Customers’ purchases have to be factored into a gained value that is higher than
                                incurred cost.
                                <br/><br/>
                                7. We do not resort to competitor-based pricing, to avoid positioning our prices on the
                                high side similarly.
                                <br/><br/>
                                8. Customers are only charged for the cost of the value which they feel, see and get.
                                <br/><br/>
                                9. Other outlets may offer similar products at less price, but it comes with a major
                                discount in quality.
                                <br/><br/>
                                10. Our products’ prices are carefully studied to be maintained, yet we reserve the
                                right to increase/decrease prices without prior notice.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>
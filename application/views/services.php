<?php
echo $page_head;
$categories = $this->md->select('tbl_category');
?>
<body>
<div id="canvas">
    <div id="box_wrapper">
        <!-- template sections -->
        <?php echo $page_header; ?>
        <?php echo $page_breadcumb; ?>

        <section id="about" class="ls section_padding_top_150 pt-100"
                 style="background: url('images/pattern.jpg');background-attachment: fixed;background-position: center;background-size: contain">
            <div style="background: rgba(255,255,255,0.5)">
                <div class="container">

                    <div class="row flex-wrap v-center mb-100">
                        <div
                            class="col-xs-12 col-md-6 col-lg-6">
                            <img src="images/about.png"
                                 alt="" class="cover-image" width="100%"
                                 style="border:2px solid #dfdfdf;padding: 5px"
                                 title="">
                        </div>
                        <div
                            class="col-xs-12 col-md-6 col-lg-6">
                                <span
                                    class="above_heading highlight">Dhillon's Services</span>
                            <h2 class="section_header">Digital Marketing</h2>
                            <div class="toppadding_10"></div>
                            <ul class="list1 no-bullets greylinks underlined-links color3">
                                <li>
                                    <div class="media teaser media-icon-teaser">
                                        <div class="media-left media-middle">
                                            <div class="teaser_icon size_normal highlight3"><i
                                                    class="clr-service-copying"></i></div>
                                        </div>
                                        <div class="media-body media-middle"><span
                                                class="text-000">Social Media</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media teaser media-icon-teaser">
                                        <div class="media-left media-middle">
                                            <div class="teaser_icon size_normal highlight3"><i
                                                    class="clr-service-copying"></i></div>
                                        </div>
                                        <div class="media-body media-middle"><span
                                                class="text-000">Website Development</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <a href="<?php echo base_url('contact') ?>" class="theme_button color1">Inquiry
                                Now</a>
                        </div>
                    </div>

                    <div class="row flex-wrap v-center mb-100">
                        <div
                            class="col-xs-12 col-md-6 col-md-push-6 col-lg-push-6">
                            <img src="images/copy-center.png"
                                 alt="" class="cover-image" width="100%"
                                 style="border:2px solid #dfdfdf;padding: 5px"
                                 title="">
                        </div>
                        <div
                            class="col-xs-12 col-md-6 col-md-pull-6 col-lg-pull-6">
                                <span
                                    class="above_heading highlight">Dhillon's Services</span>
                            <h2 class="section_header">Other Services</h2>
                            <div class="toppadding_10"></div>
                            <ul class="list1 no-bullets greylinks underlined-links color3">
                                <li>
                                    <div class="media teaser media-icon-teaser">
                                        <div class="media-left media-middle">
                                            <div class="teaser_icon size_normal highlight3"><i
                                                    class="clr-service-copying"></i></div>
                                        </div>
                                        <div class="media-body media-middle"><span
                                                class="text-000">Installation</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media teaser media-icon-teaser">
                                        <div class="media-left media-middle">
                                            <div class="teaser_icon size_normal highlight3"><i
                                                    class="clr-service-copying"></i></div>
                                        </div>
                                        <div class="media-body media-middle"><span
                                                class="text-000">Mail Out</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media teaser media-icon-teaser">
                                        <div class="media-left media-middle">
                                            <div class="teaser_icon size_normal highlight3"><i
                                                    class="clr-service-copying"></i></div>
                                        </div>
                                        <div class="media-body media-middle"><span
                                                class="text-000">Graphic Design</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <a href="<?php echo base_url('contact') ?>" class="theme_button color1">Inquiry
                                Now</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <?php echo $page_footer; ?>
    </div>
</div>

<?php echo $page_footerscript; ?>
</body>
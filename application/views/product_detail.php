<?php
echo $page_head;
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
if (!$product_data) {
    redirect('404');
}
$web_data = ($web_data) ? $web_data[0] : '';
$category = $this->md->select('tbl_category');
$photos = array_filter(explode(",", $product_data[0]->photos));
$tempPhoto = 'admin_asset/allPhotos/' . substr($product_data[0]->title, 3) . '.jpg';

$pro_id = $product_data[0]->product_id;  // Product ID
$allProducts = $this->md->select_limit_order('tbl_product', 10, 'product_id', 'desc', array('status' => 1));
$user = $this->session->userdata('email');

$categoryData = $this->md->select_where('tbl_category', array('category_id' => $product_data[0]->category_id));
$subcategoryData = $this->md->select_where('tbl_subcategory', array('subcategory_id' => $product_data[0]->subcategory_id));

?>
<body>
<?php echo $page_header; ?>

<main class="mt-50">
    <div class="mb-md-1 pb-md-3"></div>
    <section class="product-single container">
        <div class="row">
            <div class="col-lg-6">
                <div class="product-single__media" data-media-type="horizontal-thumbnail">
                    <div class="product-single__image">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php
                                if (is_array($photos) && !empty($photos)) {
                                    foreach ($photos as $photo) {
                                        ?>
                                        <div class="swiper-slide product-single__image-item">
                                            <img loading="lazy" class="h-auto"
                                                 src="<?php echo base_url($photo ? $photo : $tempPhoto); ?>"
                                                 width="788" height="788" title="<?php echo $product_data[0]->title; ?>"
                                                 alt="<?php echo $product_data[0]->title; ?>">
                                            <a data-fancybox="gallery"
                                               href="<?php echo base_url($photo ? $photo : $tempPhoto); ?>"
                                               data-bs-toggle="tooltip" data-bs-placement="left" title="Zoom">
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <use href="#icon_zoom"/>
                                                </svg>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="swiper-slide product-single__image-item">
                                        <img loading="lazy" class="h-auto"
                                             src="<?php echo base_url(file_exists($tempPhoto) ? $tempPhoto : FILENOTFOUND); ?>"
                                             width="788" height="788" title="<?php echo $product_data[0]->title; ?>"
                                             alt="<?php echo $product_data[0]->title; ?>">
                                        <a data-fancybox="gallery"
                                           href="<?php echo base_url(file_exists($tempPhoto) ? $tempPhoto : FILENOTFOUND); ?>"
                                           data-bs-toggle="tooltip" data-bs-placement="left" title="Zoom">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <use href="#icon_zoom"/>
                                            </svg>
                                        </a>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="swiper-button-prev">
                                <svg width="7" height="11" viewBox="0 0 7 11" xmlns="http://www.w3.org/2000/svg">
                                    <use href="#icon_prev_sm"/>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg width="7" height="11" viewBox="0 0 7 11" xmlns="http://www.w3.org/2000/svg">
                                    <use href="#icon_next_sm"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="product-single__thumbnail">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php
                                foreach ($photos as $photo) {
                                    ?>
                                    <div class="swiper-slide product-single__image-item">
                                        <img loading="lazy" class="h-auto"
                                             src="<?php echo base_url($photo); ?>"
                                             title="<?php echo $product_data[0]->title; ?>"
                                             alt="<?php echo $product_data[0]->title; ?>">
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="p-2 text-right">
                    <a href="<?php echo base_url('collection'); ?>">Collection</a>
                    <span style="font-family: 'Comic Sans MS';"> &nbsp;  >  &nbsp;  </span>
                    <a href="<?php echo base_url('collection?id=') . ($categoryData ? ucfirst($categoryData[0]->category_id) : ''); ?>"><?php echo $categoryData ? ucfirst($categoryData[0]->title) : ''; ?></a>
                    <span style="font-family: 'Comic Sans MS';"> &nbsp;  >  &nbsp;  </span>
                    <a href="<?php echo base_url('collection?subid=') . ($subcategoryData ? ucfirst($subcategoryData[0]->subcategory_id) : ''); ?>"><?php echo $subcategoryData ? ucfirst($subcategoryData[0]->title) : ''; ?></a>
                </div>
                <h1 class="product-single__name"><?php echo $product_data[0]->title; ?></h1>
                <div class="product-single__meta-info mb-10">
                    <div class="meta-item">
                        <label>SKU:</label>
                        <span><?php echo strtoupper($product_data[0]->sku); ?></span>
                    </div>
                    <div class="meta-item">
                        <label>Categories:</label>
                        <span><?php echo $categoryData ? ucfirst($categoryData[0]->title) : ''; ?></span>
                    </div>
                </div>
                <div class="stock mb-10">
                    <label>AVAILIBILITY:</label>
                    <?php
                    $totalStock = $this->md->my_query('SELECT sum(stock) as `totalStock` FROM `tbl_inventory` WHERE `product_id` = ' . $product_data[0]->product_id)->result();
                    $availableStock = $totalStock ? ($totalStock[0]->totalStock ? $totalStock[0]->totalStock : 0) : 0;

                    // Get Temporary table stock and less it from available stock
                    $tempStock = $this->md->my_query('SELECT sum(stock) as `totalStock` FROM `tbl_temp` WHERE `product_id` = ' . $product_data[0]->product_id)->result();
                    $temporaryStock = $tempStock ? ($tempStock[0]->totalStock ? $tempStock[0]->totalStock : 0) : 0;

                    $finalStock = $availableStock - $temporaryStock;

                    if ($finalStock <= 5 && $finalStock >= 0) {
                        echo '<span class="border" style="background: red;height: 20px;width: 20px;border-radius: 20px;display: inline-block">&nbsp;</span>';
                    } elseif ($finalStock <= 15 && $finalStock >= 6) {
                        echo '<span class="border" style="background:yellow;height: 20px;width: 20px;border-radius: 20px;display: inline-block">&nbsp;</span>';
                    } elseif ($finalStock > 15) {
                        echo '<span style="background:green;height: 20px;width: 20px;border-radius: 20px;display: inline-block">&nbsp;</span>';
                    }
                    ?>
                </div>
                <div class="product-single__short-desc">
                    <p><?php echo $product_data[0]->short_description; ?></p>
                </div>
                <form action="<?php echo base_url('Pages/addToCart'); ?>"
                      name="addToCartForm" method="post">
                    <?php
                    $wh['product_id'] = $product_data[0]->product_id;
                    $wh['category_id'] = $product_data[0]->category_id;
                    $wh['unique_id'] = $this->input->cookie('unique_id');
                    $exist = $this->md->select_where('tbl_cart', $wh);
                    ?>
                    <input type="hidden" name="productId"
                           value="<?php echo $product_data[0]->product_id; ?>"/>
                    <input type="hidden" name="categoryId"
                           value="<?php echo $product_data[0]->category_id; ?>"/>
                    <div class="product-single__addtocart">
                        <div class="qty-control position-relative">
                            <input type="hidden" name="avaStock"
                                   class="avaStock"
                                   value="<?php echo $finalStock; ?>"/>
                            <input type="number" name="quantity" value="<?php echo $exist ? ($exist[0]->qty) : 1; ?>"
                                   min="1"
                                   max="<?php echo $finalStock; ?>"
                                   class="qty-control__number text-center">
                            <div class="qty-control__reduce">-</div>
                            <div class="qty-control__increase">+</div>
                        </div><!-- .qty-control -->
                        <button name="addToCart" value="addToCart" type="submit" class="btn btn-primary">Add to Cart
                        </button>
                        <?php
                        if (!isset($user)) {
                            ?>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#sizeGuide"
                               class="btn btn-success btn-addtocart">Enquiry Now
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                </form>
                <div class="product-single__addtolinks mt-20">
                    <a href="https://api.whatsapp.com/send?phone=<?php echo $web_data ? $web_data->whatsapp : ''; ?>&amp;text=Hello, I am interested in your product, %0a%0aPlease send more information about it. %0a%0aThanks!"
                       target="_blank"
                       class="">
                        <svg class="w-6 h-6 text-gray-800 dark:text-white vertical-sub" aria-hidden="true"
                             xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24">
                            <path fill="currentColor" fill-rule="evenodd"
                                  d="M12 4a8 8 0 0 0-6.895 12.06l.569.718-.697 2.359 2.32-.648.379.243A8 8 0 1 0 12 4ZM2 12C2 6.477 6.477 2 12 2s10 4.477 10 10-4.477 10-10 10a9.96 9.96 0 0 1-5.016-1.347l-4.948 1.382 1.426-4.829-.006-.007-.033-.055A9.958 9.958 0 0 1 2 12Z"
                                  clip-rule="evenodd"/>
                            <path fill="currentColor"
                                  d="M16.735 13.492c-.038-.018-1.497-.736-1.756-.83a1.008 1.008 0 0 0-.34-.075c-.196 0-.362.098-.49.291-.146.217-.587.732-.723.886-.018.02-.042.045-.057.045-.013 0-.239-.093-.307-.123-1.564-.68-2.751-2.313-2.914-2.589-.023-.04-.024-.057-.024-.057.005-.021.058-.074.085-.101.08-.079.166-.182.249-.283l.117-.14c.121-.14.175-.25.237-.375l.033-.066a.68.68 0 0 0-.02-.64c-.034-.069-.65-1.555-.715-1.711-.158-.377-.366-.552-.655-.552-.027 0 0 0-.112.005-.137.005-.883.104-1.213.311-.35.22-.94.924-.94 2.16 0 1.112.705 2.162 1.008 2.561l.041.06c1.161 1.695 2.608 2.951 4.074 3.537 1.412.564 2.081.63 2.461.63.16 0 .288-.013.4-.024l.072-.007c.488-.043 1.56-.599 1.804-1.276.192-.534.243-1.117.115-1.329-.088-.144-.239-.216-.43-.308Z"/>
                        </svg>
                    </a>
                    <share-button class="share-button">
                        <button
                                class="menu-link menu-link_us-s to-share border-0 bg-transparent d-flex align-items-center">
                            <svg width="16" height="19" viewBox="0 0 16 19" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <use href="#icon_sharing"/>
                            </svg>
                            <span>Share</span>
                        </button>
                        <details id="Details-share-template__main" class="m-1 xl:m-1.5" hidden="">
                            <summary class="btn-solid m-1 xl:m-1.5 pt-3.5 pb-3 px-5">+</summary>
                            <div id="Article-share-template__main"
                                 class="share-button__fallback flex items-center absolute top-full left-0 w-full px-2 py-4 bg-container shadow-theme border-t z-10">
                                <div class="field grow mr-4">
                                    <label class="field__label sr-only" for="url">Link</label>
                                    <input type="text" class="field__input w-full" id="url"
                                           value="https://uomo-crystal.myshopify.com/blogs/news/go-to-wellness-tips-for-mental-health"
                                           placeholder="Link" onclick="this.select();" readonly="">
                                </div>
                                <button class="share-button__copy no-js-hidden">
                                    <svg class="icon icon-clipboard inline-block mr-1" width="11" height="13"
                                         fill="none" xmlns="http://www.w3.org/2000/svg" aria-hidden="true"
                                         focusable="false" viewBox="0 0 11 13">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M2 1a1 1 0 011-1h7a1 1 0 011 1v9a1 1 0 01-1 1V1H2zM1 2a1 1 0 00-1 1v9a1 1 0 001 1h7a1 1 0 001-1V3a1 1 0 00-1-1H1zm0 10V3h7v9H1z"
                                              fill="currentColor"></path>
                                    </svg>
                                    <span class="sr-only">Copy link</span>
                                </button>
                            </div>
                        </details>
                    </share-button>
                    <script src="./js/details-disclosure.js" defer="defer"></script>
                    <script src="./js/share.js" defer="defer"></script>
                </div>

                <div class="product-single__details-tab mt-0">
                    <ul class="nav nav-tabs justify-content-start bg-F5F5F5 border p-2" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link nav-link_underscore active" id="tab-description-tab" data-bs-toggle="tab"
                               href="#tab-description" role="tab" aria-controls="tab-description" aria-selected="true">Description</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link nav-link_underscore" id="tab-additional-info-tab" data-bs-toggle="tab"
                               href="#tab-additional-info" role="tab" aria-controls="tab-additional-info"
                               aria-selected="false">Additional
                                Information</a>
                        </li>
                    </ul>
                    <div class="tab-content pt-20">
                        <div class="tab-pane fade show active" id="tab-description" role="tabpanel"
                             aria-labelledby="tab-description-tab">
                            <div class="product-single__description">
                                <?php echo $product_data[0]->description; ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-additional-info" role="tabpanel"
                             aria-labelledby="tab-additional-info-tab">
                            <div class="product-single__addtional-info">
                                <?php echo $product_data[0]->additional_info; ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="products-carousel container mt-100">
        <h2 class="h3 text-uppercase mb-4 pb-xl-2 mb-xl-4">Related <strong>Products</strong></h2>

        <div id="related_products" class="position-relative">
            <div class="swiper-container js-swiper-slider"
                 data-settings='{
            "autoplay": false,
            "slidesPerView": 4,
            "slidesPerGroup": 4,
            "effect": "none",
            "loop": true,
            "pagination": {
              "el": "#related_products .products-pagination",
              "type": "bullets",
              "clickable": true
            },
            "navigation": {
              "nextEl": "#related_products .products-carousel__next",
              "prevEl": "#related_products .products-carousel__prev"
            },
            "breakpoints": {
              "320": {
                "slidesPerView": 2,
                "slidesPerGroup": 2,
                "spaceBetween": 14
              },
              "768": {
                "slidesPerView": 3,
                "slidesPerGroup": 3,
                "spaceBetween": 24
              },
              "992": {
                "slidesPerView": 4,
                "slidesPerGroup": 4,
                "spaceBetween": 30
              }
            }
          }'>
                <div class="swiper-wrapper">
                    <?php
                    if ($allProducts) {
                        foreach ($allProducts as $product) {
                            $category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
                            $relatedphotos = array_filter(explode(",", $product->photos));
                            $url = base_url('product/' . $product->slug . "/" . $product->product_id);
                            $whatsappUrl = "https://wa.me/" . $web_data->whatsapp;
                            $tempPhoto = (file_exists('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') ? ('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') : FILENOTFOUND);
                            ?>
                            <div class="swiper-slide product-card">
                                <div class="pc__img-wrapper">
                                    <a href="<?php echo $url; ?>">
                                        <img
                                                loading="lazy"
                                                width="330"
                                                height="400"
                                                class="pc__img"
                                                src="<?php echo base_url($relatedphotos ? ($relatedphotos[1] ? $relatedphotos[1] : $tempPhoto) : $tempPhoto); ?>"
                                                alt="<?php echo $product->title; ?>"
                                                title="<?php echo $product->title; ?>">
                                        <?php
                                        if (array_key_exists(1, $relatedphotos)) {
                                            ?>
                                            <img
                                                    loading="lazy"
                                                    src="<?php echo base_url($relatedphotos ? ($relatedphotos[1] ? $relatedphotos[1] : $tempPhoto) : $tempPhoto); ?>"
                                                    alt="<?php echo $product->title; ?>"
                                                    title="<?php echo $product->title; ?>"
                                                    width="330"
                                                    height="400"
                                                    class="pc__img pc__img-second"
                                            >
                                            <?php
                                        }
                                        ?>
                                    </a>
                                    <form method="post" action="<?php echo base_url('Pages/addToCart'); ?>"
                                          name="addToCartForm">
                                        <?php
                                        $wh['product_id'] = $product->product_id;
                                        $wh['category_id'] = $product->category_id;
                                        $wh['unique_id'] = $this->input->cookie('unique_id');
                                        $exist = $this->md->select_where('tbl_cart', $wh);
                                        ?>
                                        <input type="hidden" name="quantity"
                                               value="<?php echo $exist ? ($exist[0]->qty + 1) : 1; ?>"/>
                                        <input type="hidden" name="productId"
                                               value="<?php echo $product->product_id; ?>"/>
                                        <input type="hidden" name="categoryId"
                                               value="<?php echo $product->category_id; ?>"/>
                                        <button
                                                name="addToCart" value="addToCart" type="submit"
                                                class="pc__atc btn anim_appear-bottom btn position-absolute border-0 text-uppercase fw-medium"
                                                data-aside="cartDrawer" title="Add To Cart">Add To Cart
                                        </button>
                                    </form>

                                </div>

                                <div class="pc__info position-relative">
                                    <p class="pc__category"><?php echo $category ? $category[0]->title : ''; ?></p>
                                    <h6 class="pc__title"><a
                                                href="<?php echo $url; ?>"><?php echo $product->title; ?></a>
                                    </h6>
                                    <a
                                            href="<?php echo $whatsappUrl; ?>"
                                            target="_blank"
                                            class="pc__btn-wl position-absolute top-0 end-0 bg-transparent border-0 js-add-wishlist"
                                            title="Inquiry on Whatsapp">
                                        <svg class="w-6 h-6 text-gray-800 dark:text-white"
                                             aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                                             width="24" height="24" fill="none" viewBox="0 0 24 24">
                                            <path fill="currentColor" fill-rule="evenodd"
                                                  d="M12 4a8 8 0 0 0-6.895 12.06l.569.718-.697 2.359 2.32-.648.379.243A8 8 0 1 0 12 4ZM2 12C2 6.477 6.477 2 12 2s10 4.477 10 10-4.477 10-10 10a9.96 9.96 0 0 1-5.016-1.347l-4.948 1.382 1.426-4.829-.006-.007-.033-.055A9.958 9.958 0 0 1 2 12Z"
                                                  clip-rule="evenodd"/>
                                            <path fill="currentColor"
                                                  d="M16.735 13.492c-.038-.018-1.497-.736-1.756-.83a1.008 1.008 0 0 0-.34-.075c-.196 0-.362.098-.49.291-.146.217-.587.732-.723.886-.018.02-.042.045-.057.045-.013 0-.239-.093-.307-.123-1.564-.68-2.751-2.313-2.914-2.589-.023-.04-.024-.057-.024-.057.005-.021.058-.074.085-.101.08-.079.166-.182.249-.283l.117-.14c.121-.14.175-.25.237-.375l.033-.066a.68.68 0 0 0-.02-.64c-.034-.069-.65-1.555-.715-1.711-.158-.377-.366-.552-.655-.552-.027 0 0 0-.112.005-.137.005-.883.104-1.213.311-.35.22-.94.924-.94 2.16 0 1.112.705 2.162 1.008 2.561l.041.06c1.161 1.695 2.608 2.951 4.074 3.537 1.412.564 2.081.63 2.461.63.16 0 .288-.013.4-.024l.072-.007c.488-.043 1.56-.599 1.804-1.276.192-.534.243-1.117.115-1.329-.088-.144-.239-.216-.43-.308Z"/>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        echo '<center>';
                        echo "<img src='" . base_url('assets/images/no-product.jpg') . "' style='width:40%' />";
                        echo '</center>';
                    }
                    ?>
                </div><!-- /.swiper-wrapper -->
            </div><!-- /.swiper-container js-swiper-slider -->

            <div
                    class="products-carousel__prev position-absolute top-50 d-flex align-items-center justify-content-center">
                <svg width="25" height="25" viewBox="0 0 25 25" xmlns="http://www.w3.org/2000/svg">
                    <use href="#icon_prev_md"/>
                </svg>
            </div><!-- /.products-carousel__prev -->
            <div
                    class="products-carousel__next position-absolute top-50 d-flex align-items-center justify-content-center">
                <svg width="25" height="25" viewBox="0 0 25 25" xmlns="http://www.w3.org/2000/svg">
                    <use href="#icon_next_md"/>
                </svg>
            </div><!-- /.products-carousel__next -->

            <div class="products-pagination mt-4 mb-5 d-flex align-items-center justify-content-center"></div>
            <!-- /.products-pagination -->
        </div><!-- /.position-relative -->

    </section><!-- /.products-carousel container -->
</main>

<div class="mb-5 pb-xl-5"></div>
<?php echo $page_footer; ?>

<!-- Sizeguide -->
<div class="modal fade" id="sizeGuide" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog size-guide">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add A Few Details About Your Requirements To Get Relevant Quotes</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">

                        <div class="mb-15">
                            <img loading="lazy" class="h-auto" src="<?php echo base_url($photos[0]); ?>"
                                 width="788" height="788" title="<?php echo $product_data[0]->title; ?>"
                                 alt="<?php echo $product_data[0]->title; ?>">
                        </div>

                        <h3><?php echo $product_data[0]->title; ?></h3>
                        <div class="product-single__meta-info">
                            <div class="meta-item">
                                <label>SKU:</label>
                                <span><?php echo strtoupper($product_data[0]->sku); ?></span>
                            </div>
                            <div class="meta-item">
                                <label>Categories:</label>
                                <span><?php echo $this->md->getItemName('tbl_category', 'category_id', 'title', $product_data[0]->category_id); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form class="contact-form" method="post">
                            <?php
                            if (isset($error)) {
                                ?>
                                <div class="alert alert-danger p-1">
                                    <?php echo $error; ?>
                                </div>
                                <?php
                            }
                            if (isset($success)) {
                                ?>
                                <div class="alert alert-success p-1">
                                    <?php echo $success; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-6 col-12 mb-3">
                                    <input type="text" class="form-control" placeholder="Enter Extra field"
                                           name="extra_field" style="display: none !important"/>
                                    <input type="text" class="form-control input-focus" name="fname" placeholder="Name"
                                           value="<?php
                                           if (set_value('fname') && !isset($success)) {
                                               echo set_value('fname');
                                           }
                                           ?>">
                                    <div class="error-text">
                                        <?php
                                        if (form_error('fname')) {
                                            echo form_error('fname');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12 mb-3">
                                    <input type="email" class="form-control input-focus" placeholder="Email *"
                                           name="email"
                                           value="<?php
                                           if (set_value('email') && !isset($success)) {
                                               echo set_value('email');
                                           }
                                           ?>">
                                    <div class="error-text">
                                        <?php
                                        if (form_error('email')) {
                                            echo form_error('email');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <input type="tel" class="form-control input-focus mb-4" placeholder="Phone number"
                                   name="phone"
                                   value="<?php
                                   if (set_value('phone') && !isset($success)) {
                                       echo set_value('phone');
                                   }
                                   ?>">
                            <div class="error-text">
                                <?php
                                if (form_error('phone')) {
                                    echo form_error('phone');
                                }
                                ?>
                            </div>
                            <textarea class="form-control mb-3 input-focus" placeholder="Comment" rows="7"
                                      name="message"><?php
                                if (set_value('message') && !isset($success)) {
                                    echo set_value('message');
                                }
                                ?></textarea>
                            <div class="error-text">
                                <?php
                                if (form_error('message')) {
                                    echo form_error('message');
                                }
                                ?>
                            </div>
                            <div class="col-md-12 mt-8 mb-15">
                                <?php
                                if ($web_data->captcha_visibility) :
                                    echo '<div class="g-recaptcha" data-sitekey="' . $web_data->captcha_site_key . '"></div>';
                                endif;
                                ?>
                            </div>

                            <button type="submit" value="send" name="send" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.size-guide position-fixed-->


<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>
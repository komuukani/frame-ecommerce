<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit"):
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form method="post" enctype="multipart/form-data" name="edit">
                                                    <div class="panel-body row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Title</label>
                                                                <input type="text" name="title"
                                                                       value="<?php echo $updata[0]->title; ?>"
                                                                       placeholder="Enter Category Title"
                                                                       class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('title')) {
                                                                        echo form_error('title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Position</label>
                                                            <select name="position"
                                                                    class="form-control <?php if (form_error('position')) { ?> is-invalid <?php } ?>">
                                                                <?php
                                                                for ($i = (count($category) + 1); $i >= 1; $i--) {
                                                                    echo "<option " . ($updata[0]->position == $i ? 'selected' : '') . " value='" . $i . "'>" . $i . "</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('position')) {
                                                                    echo form_error('position');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Select Photo <span
                                                                        style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span>
                                                                </label>
                                                                <div class="fileupload fileupload-new"
                                                                     data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <input type="file" id="file"
                                                                               onchange="readURL(this, 'blah');$('#updateStatus').val('yes');"
                                                                               name="category" class=""
                                                                               accept="image/*">
                                                                        <input type="hidden" id="updateStatus"
                                                                               name="updateStatus"/>
                                                                        <input type="hidden"
                                                                               value="<?php echo $updata[0]->path; ?>"
                                                                               name="oldPath"/>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                if ($updata[0]->path):
                                                                    ?>
                                                                    <img src="<?php echo base_url($updata[0]->path); ?>"
                                                                         class="mt-20 center-block" width="50"
                                                                         id="blah"/>
                                                                <?php
                                                                else:
                                                                    ?>
                                                                    <img class="mt-20 center-block" width="50"
                                                                         id="blah"/>
                                                                <?php
                                                                endif;
                                                                ?>
                                                                <p class="error-text file-error" style="display: none">
                                                                    Select a valid file!</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update Category
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>
                                                <!-- >> Import Excel/CSV Start
                                                         ================================================== -->
                                                <form name="import" method="post" enctype="multipart/form-data">
                                                    <div
                                                        class="panel-body row mb-30 bg-F4F4F4 text-000 border shadow p-20">
                                                        <div class="col-md-7">
                                                            <div>
                                                                <label class="control-label font-weight-bold vertical-sub mr-10">Import CSV or Excel File
                                                                    <span
                                                                        style="font-size: 13px" class="text-danger">*(Upload only .csv | .xlsx | .xls files.)</span>
                                                                </label>
                                                                <a href="<?php echo base_url('admin_asset/sample/main-category.xlsx') ?>" download="Main Category Import Sample"
                                                                   class="mt-1 badge badge-primary">Sample File Download
                                                                </a>
                                                                <div class="fileupload fileupload-new"
                                                                     data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <input type="file" required
                                                                               name="importedFile">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 text-center">
                                                            <button type="submit"
                                                                    class="mt-3 btn-hover-shine btn btn-shadow btn-info"
                                                                    value="import Data" name="importData">Import Data
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- << Import Excel/CSV End
                                                   ================================================== -->
                                                <hr/>
                                                <!-- >> Add Form Start
                                                  ================================================== -->
                                                <form name="add" method="post" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Title</label>
                                                                <input type="text" name="title" value="<?php
                                                                if (set_value('title') && !isset($success)) {
                                                                    echo set_value('title');
                                                                }
                                                                ?>" placeholder="Enter Category Title"
                                                                       class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('title')) {
                                                                        echo form_error('title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Position</label>
                                                            <select name="position"
                                                                    class="form-control <?php if (form_error('position')) { ?> is-invalid <?php } ?>">
                                                                <?php
                                                                for ($i = (count($category) + 1); $i >= 1; $i--) {
                                                                    echo "<option value='" . $i . "'>" . $i . "</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('position')) {
                                                                    echo form_error('position');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Select Photo <span
                                                                        style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span>
                                                                </label>
                                                                <div class="fileupload fileupload-new"
                                                                     data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <input type="file" id="file"
                                                                               onchange="readURL(this, 'blah');"
                                                                               name="category" class=""
                                                                               accept="image/*">
                                                                    </div>
                                                                </div>
                                                                <img src="" class="mt-20 center-block" width="50"
                                                                     id="blah"/>
                                                                <p class="error-text file-error" style="display: none">
                                                                    Select a valid file!</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Upload Category
                                                        </button>
                                                        <button
                                                            class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                            type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                   ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                            ================================================== -->
                        <?php
                        else:
                            if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="">
                                                <table class="table mb-none table-hover" id="categoryTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Main Category ID</th>
                                                        <th>Title</th>
                                                        <th>Position</th>
                                                        <th>Photo</th>
                                                        <?php
                                                        if ($permission['all'] || $permission['edit']):
                                                            echo '<th>Edit</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['delete']):
                                                            echo '<th>Delete</th>';
                                                        endif;
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else:
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <?php echo $page_footer; ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'id'},
        {data: 'title'},
        {data: 'position'},
        {data: 'path'},
        <?php
        if ($permission['all'] || $permission['edit']):
            echo '{data: "edit"},';
        endif;
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'path');
</script>
</body>
<?php
echo $page_head; //  Load Head Link and Scripts
$orderStatus = ['Inquired', 'Estimate', 'On Hold', 'Dispatch', 'Dispatched'];
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <?php
                if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                    ?>
                    <div class="section-body">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" id="billStatus"
                                               value="<?php echo $this->uri->segment(3) == 'OnHold' ? 'On Hold' : $this->uri->segment(3); ?>">
                                        <div class="display-flex justify-content-between mb-20" style="gap: 15px">
                                            <?php
                                            $totalAllData = count($this->md->select("tbl_bill"));
                                            ?>
                                            <button onclick="$('#billStatus').val('all');"
                                                    class="orderStatus btn btn-dark w-100 font-14 text-transform-uppercase letter-spacing-3"
                                                    data-status="">All
                                                <span class="counter"><?php echo $totalAllData; ?></span></button>
                                            <?php
                                            foreach ($orderStatus as $status) {
                                                $totalData = count($this->md->select_where("tbl_bill", array("status" => $status)));
                                                ?>
                                                <button onclick="$('#billStatus').val('<?php echo $status; ?>');"
                                                        class="orderStatus btn btn-dark w-100 font-14 text-transform-uppercase letter-spacing-3"
                                                        data-status="<?php echo str_replace(" ", "", $status); ?>"><?php echo $status; ?>
                                                    <span class="counter"><?php echo $totalData; ?></span></button>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">From Date</label>
                                            <input type="date" class="form-control" id="searchFrom"
                                                   placeholder="Select From Date">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">To Date </label>
                                            <input type="date" class="form-control" id="searchTo"
                                                   placeholder="Select To Date">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <button class="btn btn-success mt-30" id="dateWiseFilter">Date wise Filter
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <h4 id="counterTotal" class="mt-30 mt-xs-5 text-right font-30"></h4>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped mb-none" id="billTable">
                                        <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Order ID</th>
                                            <th>Name</th>
                                            <th class="none">Customer Email</th>
                                            <th>Phone</th>
                                            <th class="none">City</th>
                                            <th class="none">Address</th>
                                            <th class="none">Items</th>
                                            <th class="none">Order Notes</th>
                                            <th>Total</th>
                                            <th class="none">Photos</th>
                                            <th class="none">LR No.</th>
                                            <th class="none">LR Date</th>
                                            <th>Invoice Date</th>
                                            <th>Status</th>
                                            <th>Invoice</th>
                                            <th>Download</th>
                                            <?php
                                            if ($this->session->userdata('role') == 'admin') {
                                                if ($permission['all'] || $permission['delete']):
                                                    echo '<th>Delete</th>';
                                                endif;
                                            }
                                            ?>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                else:
                    $this->load->view('admin/common/access_denied');
                endif;
                ?>
            </section>
        </div>
        <!-- >> Main Content Start
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer   ?>
    </div>
</div>
<!-- Image Preview Modal -->
<div class="modal fade" id="imagePreviewModal" tabindex="-1" aria-labelledby="imagePreviewModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <img id="modalImage" src="" alt="Image Preview" style="width: 100%; height: auto;"/>
            </div>
        </div>
    </div>
</div>

<!-- >> Status Update Modal Start
    ================================================== -->
<div class="modal fade" tabindex="-1" role="dialog" id="update_status"
     data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" name="updateBillStatus_form" id="updateBillStatus_form"
                      enctype="multipart/form-data">
                    <div id="billData">

                    </div>
                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <?php
                if ($this->session->userdata('role') == 'admin') {
                    ?>
                    <button type="submit" name="update" value="assign" class="btn btn-primary"
                            form="updateBillStatus_form">Save changes
                    </button>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<!-- << Status Update Modal End
================================================== -->

<?php echo $page_footerscript;  //  Load Footer script  ?>
<script>
    $(document).ready(function () {
        // Event listener for image click
        $('#imagePreviewModal').on('show.bs.modal', function (event) {
            var imgLink = $(event.relatedTarget); // Link that triggered the modal
            var imgSrc = imgLink.data('src'); // Extract the image source from data-src attribute
            var modalImage = $('#modalImage'); // Image inside the modal

            // Update the src of the image in the modal
            modalImage.attr('src', imgSrc);
        });
    });

    // confirmation message for model on escape key
    $(document).ready(function () {
        $('#update_status').on('shown.bs.modal', function () {
            $(document).on('keyup', function (e) {
                if (e.key === "Escape") {
                    // Show confirmation dialog
                    if (confirm("Are you sure you want to close the modal? Any unsaved changes will be lost.")) {
                        $('#update_status').modal('hide');
                    }
                }
            });
        });

        $('#update_status').on('hidden.bs.modal', function () {
            // Remove the keyup event listener when the modal is closed
            $(document).off('keyup');
        });
    });

    // Restrict Enter key in model
    $(document).ready(function () {
        // Trigger the code only when the modal is shown
        $('#inventoryModal').on('shown.bs.modal', function () {
            // Immediately restrict the Enter key on the modal to prevent unintended closing
            $(document).on('keydown.preventClose', function (e) {
                if (e.key === "Enter") {
                    e.preventDefault(); // Prevent Enter from triggering default behavior
                }
            });

            // Restrict Enter key navigation within the form
            $('#updateBillStatus_form').on('keydown', 'input', function (e) {
                if (e.key === "Enter") {
                    e.preventDefault(); // Prevent form submission

                    // Get all focusable elements in the form
                    const focusable = $('#updateBillStatus_form').find('input, select, textarea').filter(':visible');
                    const index = focusable.index(this) + 1;

                    // Focus the next element if it exists, else wrap around to the first input
                    if (index < focusable.length) {
                        focusable.eq(index).focus();
                    } else {
                        focusable.eq(0).focus(); // Wrap around to the first input if end is reached
                    }
                }
            });
        });

        // Clean up the event when the modal is hidden
        $('#inventoryModal').on('hidden.bs.modal', function () {
            $(document).off('keydown.preventClose'); // Remove the Enter key restriction
            $('#updateBillStatus_form').off('keydown', 'input');
        });
    });

    $(document).ready(function () {
        // List column which will be display in the table
        $column = [
            {data: 'status'},
            {data: 'order_id'},
            {data: 'name'},
            {data: 'email'},
            {data: 'phone'},
            {data: 'city'},
            {data: 'address'},
            {data: 'items'},
            {data: 'notes'},
            {data: 'total'},
            {data: 'photos'},
            {data: 'lr_no'},
            {data: 'date_of_lr'},
            {data: 'datetime'},
            {data: 'statusUpdate'},
            {data: 'invoice'},
            {data: 'download'},
            <?php
            if ($this->session->userdata('role') == 'admin') {
                if ($permission['all'] || $permission['delete']):
                    echo '{data: "delete"},';
                endif;
            }
            ?>
        ];
        $tbl = '<?php echo $active_page; ?>';
        $url = "<?php echo base_url($current_page . '/getdata') ?>";
        $responsive = true;
        var userDataTable = $('#' + $tbl + 'Table').DataTable({
            dom: 'Bfrtlip', // enable Export buttons, per page, search, pagination
            'processing': true, // true/false
            'responsive': $responsive, // enable expand(+) mode (view more in table)
            'serverSide': true, // true/false
            'pageLength': 10, // Per page Item
            "lengthMenu": [[10, 25, 50, 500, 1000, 5000], [10, 25, 50, 500, 1000, 5000]],
            'serverMethod': 'post', // get/post
            'ajax': {
                'url': $url,
                'data': function (data) {
                    data.searchFrom = $('#searchFrom').val();
                    data.searcchTo = $('#searchTo').val();
                    data.billStatus = $('#billStatus').val();
                    data.cancelledOrderStatus = $('#cancelledOrderStatus').val();
                }
            },
            'columns': $column,
            "fnDrawCallback": function () {
                $('[data-toggle="tooltip"]').tooltip(); // enable tooltip in datatable
                var info = userDataTable.page.info();
                $('#counterTotal').html(info.recordsDisplay);
            },
            "columnDefs": [
                {"sortable": false, "targets": [0]}
            ]
        });
        $('#dateWiseFilter').click(function () {
            userDataTable.draw(); // From Date to Date Filter
        });
        $('#cancelledOrders').click(function () {
            userDataTable.draw(); // From Date to Date Filter
        });
        $('#billStatus').change(function () {
            userDataTable.draw(); // From Date to Date Filter
        });
        $('.orderStatus').click(function () {
            const status = $(this).data('status'); // Here you can get the current order status of button
            userDataTable.draw(); // Apply your filtering or drawing logic here
        });
        $(document).on("click", "#deleteItem", function () {
            const id = $(this).data('itemid'); // getting record ID
            actionOnDatatable('delete', $tbl, id, ''); // call delete item function
        }); // Delete Call
        $(document).on("click", "#billID", function () {
            const billid = $(this).data('billid'); // getting Bill ID
            var data = {billid: billid}; //Bill ID
            $('#billData').html('<h4>Loading...</h4>');
            var url = "<?php echo base_url('Admin/Pages/getBillData') ?>";
            jQuery.post(url, data, function (data) {
                jQuery('#billData').html(data);
            });
        });
    });
</script>
<style>
    .customTable {
        width: 100%;
    }

    .customTable tr th {
        background: #dfdfdf;
        color: #000;
        padding: 7px;
        border: 1px solid #999;
    }

    .customTable tr td {
        border: 1px solid #999;
        color: #000;
        padding: 7px;
    }

    .customTable input {
        width: 90px;
    }

    .counter {
        background-color: green;
        color: #fff;
        width: 40px;
        height: 40px;
        display: inline-block;
        border-radius: 40px;
        text-align: center;
        line-height: 40px;
        font-size: 18px;
    }
</style>
</body>
<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>  
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header  ?>  
            <?php echo $page_sidebar; //  Load Sidebar  ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb ?> 
                    <?php
                    if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                        ?>
                        <div class="section-body">  
                            <form method="post" name="sendmail" id="sendmail">
                                <div class="card"> 
                                    <div class="card-body"> 
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-striped mb-none" id="email_subscriberTable">
                                                        <thead>
                                                            <tr>  
                                                                <th>
                                                                    <div class="custom-checkbox custom-control">
                                                                        <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all" name="all_email" value="all">
                                                                        <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                                                                    </div>
                                                                </th>
                                                                <th>Email</th> 
                                                                <th>Date</th>
                                                                <?php
                                                                if ($permission['all'] || $permission['delete']):
                                                                    echo '<th>Delete</th>';
                                                                endif;
                                                                ?>   
                                                            </tr>
                                                        </thead> 
                                                    </table>
                                                </div> 
                                            </div>
                                            <div class="col-md-6"> 
                                                <div class="form-group">
                                                    <label class="control-label">Mail Subject</label>
                                                    <input type="text" name="subject" value="<?php
                                                    if (set_value('subject') && !isset($success)) {
                                                        echo set_value('subject');
                                                    }
                                                    ?>" placeholder="Enter Mail Subject" class="form-control <?php if (form_error('subject')) { ?>  is-invalid  <?php } ?>">
                                                    <div class="error-text">
                                                        <?php
                                                        if (form_error('subject')) {
                                                            echo form_error('subject');
                                                        }
                                                        ?>
                                                    </div>
                                                </div>  
                                                <div class="form-group">
                                                    <label class="control-label">Enter Message of mail in your format</label>
                                                    <textarea name="message" class="summernote"><?php
                                                        if (set_value('message') && !isset($success)) {
                                                            echo set_value('message');
                                                        }
                                                        ?></textarea> 
                                                    <div class="error-text">
                                                        <?php
                                                        echo form_error('message');
                                                        ?>
                                                    </div> 
                                                </div>  
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="card-footer bg-whitesmoke text-md-right pt-10 pb-10">
                                        <button type="submit" form="sendmail" class="mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="send_mail">Send Mail</button>
                                        <button class="btn btn-secondary" type="reset">Reset</button>
                                    </div>
                                </div> 
                            </form>
                        </div>
                        <?php
                    else:
                        $this->load->view('admin/common/access_denied');
                    endif;
                    ?>
                </section>
            </div> 
            <!-- >> Main Content Start
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer   ?> 
        </div>
    </div>
    <?php
    $alert_data['success'] = $success;
    $alert_data['error'] = $error;
    $this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
    ?> 
    <script>
        // List column which will be display in the table
        const column = [
            {data: 'check'},
            {data: 'email'},
            {data: 'datetime'},
<?php
if ($permission['all'] || $permission['delete']):
    echo '{data: "delete"},';
endif;
?>
        ];
        // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE] 
        getDataTable('<?php echo 'email_subscriber'; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column);
    </script> 
</body> 

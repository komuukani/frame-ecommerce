<?php echo $page_head; //  Load Head Link and Scripts       ?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <?php
                if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                    ?>
                    <div class="section-body">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">From Date</label>
                                            <input type="date" class="form-control" id="searchFrom"
                                                   placeholder="Select From Date">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">To Date </label>
                                            <input type="date" class="form-control" id="searchTo"
                                                   placeholder="Select To Date">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <button class="btn btn-success mt-30" id="dateWiseFilter">Date wise Filter
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <h4 id="counterTotal" class="mt-30 mt-xs-5 text-right font-30"></h4>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped mb-none" id="billTable">
                                        <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Order ID</th>
                                            <th>Name</th>
                                            <th class="none">Customer Email</th>
                                            <th>Phone</th>
                                            <th class="none">City</th>
                                            <th class="none">Address</th>
                                            <th class="none">Order Notes</th>
                                            <th>Invoice Date</th>
                                            <th>Invoice</th>
                                            <th>Download</th>
                                            <?php
                                            if ($permission['all'] || $permission['delete']):
//                                                echo '<th>Delete</th>';
                                            endif;
                                            ?>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                else:
                    $this->load->view('admin/common/access_denied');
                endif;
                ?>
            </section>
        </div>
        <!-- >> Main Content Start
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer   ?>
    </div>
</div>

<?php echo $page_footerscript; //  Load Footer script  ?>
<script>
    $(document).ready(function () {
        // List column which will be display in the table
        $column = [
            {data: 'status'},
            {data: 'order_id'},
            {data: 'name'},
            {data: 'email'},
            {data: 'phone'},
            {data: 'city'},
            {data: 'address'},
            {data: 'notes'},
            {data: 'datetime'},
            {data: 'invoice'},
            {data: 'download'},
            <?php
            if ($permission['all'] || $permission['delete']):
//                echo '{data: "delete"},';
            endif;
            ?>
        ];
        $tbl = '<?php echo "bill"; ?>';
        $url = "<?php echo base_url($current_page . '/getdata') ?>";
        $responsive = true;
        var userDataTable = $('#' + $tbl + 'Table').DataTable({
            dom: 'Bfrtlip', // enable Export buttons, per page, search, pagination
            'processing': true, // true/false
            'responsive': $responsive, // enable expand(+) mode (view more in table)
            'serverSide': true, // true/false
            'pageLength': 10, // Per page Item
            "lengthMenu": [[10, 25, 50, 500, 1000, 5000], [10, 25, 50, 500, 1000, 5000]],
            'serverMethod': 'post', // get/post
            'ajax': {
                'url': $url,
                'data': function (data) {
                    data.searchFrom = $('#searchFrom').val();
                    data.searchTo = $('#searchTo').val();
                }
            },
            'columns': $column,
            "fnDrawCallback": function () {
                $('[data-toggle="tooltip"]').tooltip(); // enable tooltip in datatable
                var info = userDataTable.page.info();
                $('#counterTotal').html(info.recordsDisplay);
            },
            "columnDefs": [
                {"sortable": false, "targets": [0]}
            ]
        });
        $('#dateWiseFilter').click(function () {
            userDataTable.draw();   // From Date to Date Filter
        });
        $(document).on("click", "#deleteItem", function () {
            const id = $(this).data('itemid'); // getting record ID
            actionOnDatatable('delete', $tbl, id, '');  // call delete item function
        }); // Delete Call
    });

</script>
</body>
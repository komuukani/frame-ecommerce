<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$fields = $this->md->select('tbl_petacategory');    // get all category
//$locations = $this->md->select('tbl_location');    // get all location
$sizeFields = $this->md->my_query('SELECT * FROM tbl_size ORDER BY size ASC')->result();    // get all Size
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit"):
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form method="post" name="edit" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label">Product SKU</label>
                                                            <input type="text" name="sku"
                                                                   value="<?php
                                                                   if (set_value('sku') && !isset($success)) {
                                                                       echo set_value('sku');
                                                                   } else {
                                                                       echo $updata[0]->sku;
                                                                   }
                                                                   ?>" placeholder="Enter Product SKU"
                                                                   class="form-control <?php if (form_error('sku')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('sku')) {
                                                                    echo form_error('sku');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Peta Category</label>
                                                                <select name="petacategory_id"
                                                                        id="peta_category"
                                                                        class="form-control select2 text-capitalize <?php if (form_error('petacategory_id')) { ?> is-invalid <?php } ?>">
                                                                    <option value="">Select Peta Category</option>
                                                                    <?php
                                                                    if (!empty($fields)):
                                                                        foreach ($fields as $fields_val):
                                                                            ?>
                                                                            <option
                                                                                <?php echo $fields_val->petacategory_id == $updata[0]->petacategory_id ? 'selected' : ''; ?>
                                                                                value="<?php echo $fields_val->petacategory_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('petacategory_id')) {
                                                                        echo form_error('petacategory_id');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" id="categoryData">
                                                            <?php
                                                            $val = $updata[0]->petacategory_id;   // Peta ID
                                                            $petacategory = $this->md->select_where('tbl_petacategory', array('petacategory_id' => $val));
                                                            if (!empty($petacategory)) {
                                                                $subcategory = $this->md->select_where('tbl_subcategory', array('subcategory_id' => $petacategory[0]->parent));
                                                                if (!empty($subcategory)) {
                                                                    $category = $this->md->select_where('tbl_category', array('category_id' => $subcategory[0]->parent));
                                                                    if (!empty($category)) {
                                                                        ?>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label">Sub
                                                                                        Category</label>
                                                                                    <select name="subcategory_id"
                                                                                            class="form-control select2 text-capitalize <?php if (form_error('subcategory_id')) { ?> is-invalid <?php } ?>">
                                                                                        <option
                                                                                            value="<?php echo $subcategory[0]->subcategory_id; ?>"><?php echo $subcategory[0]->title; ?></option>
                                                                                    </select>
                                                                                    <div class="error-text">
                                                                                        <?php
                                                                                        if (form_error('subcategory_id')) {
                                                                                            echo form_error('subcategory_id');
                                                                                        }
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label">Category</label>
                                                                                    <select name="category_id"
                                                                                            class="form-control select2 text-capitalize <?php if (form_error('category_id')) { ?> is-invalid <?php } ?>">
                                                                                        <option
                                                                                            value="<?php echo $category[0]->category_id; ?>"><?php echo $category[0]->title; ?></option>
                                                                                    </select>
                                                                                    <div class="error-text">
                                                                                        <?php
                                                                                        if (form_error('category_id')) {
                                                                                            echo form_error('category_id');
                                                                                        }
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </div>

                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Product Title</label>
                                                            <input type="text" name="title" value="<?php
                                                            if (set_value('title') && !isset($success)) {
                                                                echo set_value('title');
                                                            } else {
                                                                echo $updata[0]->title;
                                                            }
                                                            ?>" placeholder="Enter Product Title"
                                                                   id="productTitle"
                                                                   class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('title')) {
                                                                    echo form_error('title');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
<!--                                                        <div class="form-group col-md-3">-->
<!--                                                            <label class="control-label">Product Stock </label>-->
<!--                                                            <input type="number" step="any" name="stock"-->
<!--                                                                   value="--><?php
//                                                                   if (set_value('stock') && !isset($success)) {
//                                                                       echo set_value('stock');
//                                                                   } else {
//                                                                       echo $updata[0]->stock;
//                                                                   }
//                                                                   ?><!--" placeholder="Enter Product Stock"-->
<!--                                                                   class="form-control --><?php //if (form_error('stock')) { ?><!-- is-invalid --><?php //} ?><!--">-->
<!--                                                            <div class="error-text">-->
<!--                                                                --><?php
//                                                                if (form_error('stock')) {
//                                                                    echo form_error('stock');
//                                                                }
//                                                                ?>
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                        <div class="col-md-3">-->
<!--                                                            <div class="form-group">-->
<!--                                                                <label class="control-label">Product location </label>-->
<!--                                                                <select name="location"-->
<!--                                                                        class="form-control select2 text-capitalize --><?php //if (form_error('location')) { ?><!-- is-invalid --><?php //} ?><!--">-->
<!--                                                                    <option value="">Select Location</option>-->
<!--                                                                    --><?php
//                                                                    if (!empty($locations)):
//                                                                        foreach ($locations as $location):
//                                                                            ?>
<!--                                                                            <option-->
<!--                                                                                --><?php //echo $location->title == $updata[0]->location ? 'selected' : ''; ?>
<!--                                                                                value="--><?php //echo $location->title; ?><!--">--><?php //echo $location->title; ?><!--</option>-->
<!--                                                                        --><?php
//                                                                        endforeach;
//                                                                    endif;
//                                                                    ?>
<!--                                                                </select>-->
<!--                                                                <div class="error-text">-->
<!--                                                                    --><?php
//                                                                    if (form_error('location')) {
//                                                                        echo form_error('location');
//                                                                    }
//                                                                    ?>
<!--                                                                </div>-->
<!--                                                            </div>-->
<!--                                                        </div>-->

                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product Rate</label>
                                                            <input type="number" step="any" name="rate" value="<?php
                                                            if (set_value('rate') && !isset($success)) {
                                                                echo set_value('rate');
                                                            } else {
                                                                echo $updata[0]->rate;
                                                            }
                                                            ?>" placeholder="Enter Product Rate"
                                                                   class="form-control <?php if (form_error('rate')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('rate')) {
                                                                    echo form_error('rate');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product Stick</label>
                                                            <input type="number" step="any" name="stick" value="<?php
                                                            if (set_value('stick') && !isset($success)) {
                                                                echo set_value('stick');
                                                            } else {
                                                                echo $updata[0]->stick;
                                                            }
                                                            ?>" placeholder="Enter Product Stick"
                                                                   class="form-control <?php if (form_error('stick')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('stick')) {
                                                                    echo form_error('stick');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product FT</label>
                                                            <input type="number" step="any" name="ft" value="<?php
                                                            if (set_value('ft') && !isset($success)) {
                                                                echo set_value('ft');
                                                            } else {
                                                                echo $updata[0]->ft;
                                                            }
                                                            ?>" placeholder="Enter Product FT"
                                                                   class="form-control <?php if (form_error('ft')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('ft')) {
                                                                    echo form_error('ft');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Description</label>
                                                            <textarea name="description"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('description');
                                                                } else {
                                                                    echo $updata[0]->description;
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('description')) {
                                                                    echo form_error('description');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Additional Info</label>
                                                            <textarea name="additional_info"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('additional_info');
                                                                } else {
                                                                    echo $updata[0]->additional_info;
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('additional_info')) {
                                                                    echo form_error('additional_info');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Select Product Photos <span
                                                                        class="font-12 text-info">*(Upload only .jpg | .jpeg | .png files with less than 1MB size and dimension should be 1280*1350.)</span>
                                                                </label>
                                                                <div class="fileupload fileupload-new"
                                                                     data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <input type="file" multiple="" id="file"
                                                                               onchange="imagesPreview(this, '#img_preview');$('#updateStatus').val('yes');"
                                                                               name="product_photos[]" class=""
                                                                               accept="image/*">
                                                                        <input type="hidden" id="updateStatus"
                                                                               name="updateStatus"/>
                                                                        <input type="hidden"
                                                                               value="<?php echo $updata[0]->photos; ?>"
                                                                               name="oldPath"/>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <?php
                                                                    $allphotos = $updata[0]->photos ? explode(",", $updata[0]->photos) : [];
                                                                    if (!empty($allphotos)) :
                                                                        $pp = 0;    // photo index
                                                                        foreach ($allphotos as $photo) :
                                                                            ?>
                                                                            <div class="col-md-3">
                                                                                <img
                                                                                    src="<?php echo base_url($photo); ?>"
                                                                                    class="mt-20 center-block"
                                                                                    style="width: 100%;height: 100px;object-fit: contain"/>
                                                                                <a class="btn btn-danger btn-sm"
                                                                                   href="<?php echo base_url('Admin/Pages/removePhoto/product/' . $updata[0]->product_id . '/' . $pp); ?>">Remove</a>
                                                                            </div>
                                                                            <?php
                                                                            $pp++;
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </div>
                                                                <div id="img_preview"></div>
                                                                <p class="error-text file-error" style="display: none">
                                                                    Select a valid file!</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="featured"
                                                                        <?php echo $updata[0]->featured == 1 ? 'checked' : ''; ?>
                                                                           value="1"
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Featured Product
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update Product
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>
                                                <!-- >> Import Excel/CSV Start
                                                             ================================================== -->
                                                <form name="import" method="post" enctype="multipart/form-data">
                                                    <div
                                                        class="panel-body row mb-30 bg-F4F4F4 text-000 border shadow p-20">
                                                        <div class="col-md-7">
                                                            <div>
                                                                <label class="control-label font-weight-bold vertical-sub mr-10">Import CSV or Excel File
                                                                    <span
                                                                        style="font-size: 13px" class="text-danger">*(Upload only .csv | .xlsx | .xls files.)</span>
                                                                </label>
                                                                <a href="<?php echo base_url('admin_asset/sample/products.xlsx') ?>" download="Product Import Sample"
                                                                   class="mt-1 badge badge-primary">Sample File Download
                                                                </a>
                                                                <div class="fileupload fileupload-new"
                                                                     data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <input type="file" required
                                                                               name="importedFile">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 text-center">
                                                            <button type="submit"
                                                                    class="mt-3 btn-hover-shine btn btn-shadow btn-info"
                                                                    value="import Data" name="importData">Import Data
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- << Import Excel/CSV End
                                                   ================================================== -->
                                                <hr/>
                                                <!-- >> Add Form Start
                                                ================================================== -->
                                                <form method="post" name="add" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label">Product SKU</label>
                                                            <input type="text" name="sku"
                                                                   value="<?php
                                                                   if (set_value('sku') && !isset($success)) {
                                                                       echo set_value('sku');
                                                                   }
                                                                   ?>" placeholder="Enter Product SKU"
                                                                   class="form-control <?php if (form_error('sku')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('sku')) {
                                                                    echo form_error('sku');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Peta Category</label>
                                                                <select name="petacategory_id"
                                                                        id="peta_category"
                                                                        class="form-control select2 text-capitalize <?php if (form_error('petacategory_id')) { ?> is-invalid <?php } ?>">
                                                                    <option value="">Select Peta Category</option>
                                                                    <?php
                                                                    if (!empty($fields)):
                                                                        foreach ($fields as $fields_val):
                                                                            ?>
                                                                            <option
                                                                                value="<?php echo $fields_val->petacategory_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('petacategory_id')) {
                                                                        echo form_error('petacategory_id');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" id="categoryData">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Sub
                                                                            Category</label>
                                                                        <select name="subcategory_id"
                                                                                id="sub_category"
                                                                                class="form-control select2 text-capitalize <?php if (form_error('subcategory_id')) { ?> is-invalid <?php } ?>">
                                                                            <option value="">Select Sub Category
                                                                            </option>
                                                                        </select>
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('subcategory_id')) {
                                                                                echo form_error('subcategory_id');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Category</label>
                                                                        <select name="category_id"
                                                                                id="parentCategory"
                                                                                class="form-control select2 text-capitalize <?php if (form_error('category_id')) { ?> is-invalid <?php } ?>">
                                                                            <option value="">Select Category</option>
                                                                            <?php
                                                                            if (!empty($fields)):
                                                                                foreach ($fields as $fields_val):
                                                                                    ?>
                                                                                    <option
                                                                                        value="<?php echo $fields_val->category_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                                <?php
                                                                                endforeach;
                                                                            endif;
                                                                            ?>
                                                                        </select>
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('category_id')) {
                                                                                echo form_error('category_id');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Product Title</label>
                                                            <input type="text" name="title" value="<?php
                                                            if (set_value('title') && !isset($success)) {
                                                                echo set_value('title');
                                                            }
                                                            ?>" placeholder="Enter Product Title"
                                                                   id="productTitle"
                                                                   class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('title')) {
                                                                    echo form_error('title');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
<!--                                                        <div class="form-group col-md-3">-->
<!--                                                            <label class="control-label">Product Stock </label>-->
<!--                                                            <input type="number" step="any" name="stock"-->
<!--                                                                   value="--><?php
//                                                                   if (set_value('stock') && !isset($success)) {
//                                                                       echo set_value('stock');
//                                                                   }
//                                                                   ?><!--" placeholder="Enter Product Stock"-->
<!--                                                                   class="form-control --><?php //if (form_error('stock')) { ?><!-- is-invalid --><?php //} ?><!--">-->
<!--                                                            <div class="error-text">-->
<!--                                                                --><?php
//                                                                if (form_error('stock')) {
//                                                                    echo form_error('stock');
//                                                                }
//                                                                ?>
<!--                                                            </div>-->
<!--                                                        </div>-->
<!--                                                        <div class="col-md-3">-->
<!--                                                            <div class="form-group">-->
<!--                                                                <label class="control-label">Product location </label>-->
<!--                                                                <select name="location"-->
<!--                                                                        class="form-control select2 text-capitalize --><?php //if (form_error('location')) { ?><!-- is-invalid --><?php //} ?><!--">-->
<!--                                                                    <option value="">Select Location</option>-->
<!--                                                                    --><?php
//                                                                    if (!empty($locations)):
//                                                                        foreach ($locations as $location):
//                                                                            ?>
<!--                                                                            <option-->
<!--                                                                                value="--><?php //echo $location->title; ?><!--">--><?php //echo $location->title; ?><!--</option>-->
<!--                                                                        --><?php
//                                                                        endforeach;
//                                                                    endif;
//                                                                    ?>
<!--                                                                </select>-->
<!--                                                                <div class="error-text">-->
<!--                                                                    --><?php
//                                                                    if (form_error('location')) {
//                                                                        echo form_error('location');
//                                                                    }
//                                                                    ?>
<!--                                                                </div>-->
<!--                                                            </div>-->
<!--                                                        </div>-->

                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product Rate</label>
                                                            <input type="number" step="any" name="rate" value="<?php
                                                            if (set_value('rate') && !isset($success)) {
                                                                echo set_value('rate');
                                                            }
                                                            ?>" placeholder="Enter Product Rate"
                                                                   class="form-control <?php if (form_error('rate')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('rate')) {
                                                                    echo form_error('rate');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product Stick</label>
                                                            <input type="number" step="any" name="stick" value="<?php
                                                            if (set_value('stick') && !isset($success)) {
                                                                echo set_value('stick');
                                                            }
                                                            ?>" placeholder="Enter Product Stick"
                                                                   class="form-control <?php if (form_error('stick')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('stick')) {
                                                                    echo form_error('stick');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product FT</label>
                                                            <input type="number" step="any" name="ft" value="<?php
                                                            if (set_value('ft') && !isset($success)) {
                                                                echo set_value('ft');
                                                            }
                                                            ?>" placeholder="Enter Product FT"
                                                                   class="form-control <?php if (form_error('ft')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('ft')) {
                                                                    echo form_error('ft');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>


                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Description</label>
                                                            <textarea name="description"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('description');
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('description')) {
                                                                    echo form_error('description');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Additional Info</label>
                                                            <textarea name="additional_info"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('additional_info');
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('additional_info')) {
                                                                    echo form_error('additional_info');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Select Product Photos <span
                                                                        class="font-12 text-info">*(Upload only .jpg | .jpeg | .png files with less than 1MB size and dimension should be 1280*1350.)</span>
                                                                </label>
                                                                <div class="fileupload fileupload-new"
                                                                     data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <input type="file" id="file" multiple=""
                                                                               onchange="imagesPreview(this, '#img_preview');"
                                                                               name="product_photos[]" class=""
                                                                               accept="image/*">
                                                                    </div>
                                                                </div>
                                                                <div id="img_preview"></div>
                                                                <p class="error-text file-error" style="display: none">
                                                                    Select a valid file!</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="featured"
                                                                           value="1"
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Featured Product
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Add Product
                                                        </button>
                                                        <button
                                                            class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                            type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                            ================================================== -->
                        <?php
                        else:
                            if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table mb-none table-hover" id="productTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Status</th>
                                                        <th>Category</th>
                                                        <th>SKU</th>
                                                        <th>Title</th>
                                                        <th>Photo</th>
                                                        <th>Stock</th>
                                                        <th class="none">Entry Date</th>
                                                        <?php
                                                        if ($permission['all'] || $permission['status']):
                                                            echo '<th>Action</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['edit']):
                                                            echo '<th>Edit</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['delete']):
                                                            echo '<th>Delete</th>';
                                                        endif;
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else:
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer  ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'status'},
        {data: 'category'},
        {data: 'sku'},
        {data: 'title'},
        {data: 'photo'},
        {data: 'stock'},
        {data: 'datetime'},
        <?php
        if ($permission['all'] || $permission['status']):
            echo '{data: "action"},';
        endif;
        if ($permission['all'] || $permission['edit']):
            echo '{data: "edit"},';
        endif;
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'photos', true);

    // Product Title convert into slug (Replace space with dash)
    jQuery(document).on("keyup blur", "#productTitle", function () {
        let str = $('#productTitle').val();
        jQuery('#productSlug').val('Loading...');
        var data = {
            str: str
        };
        var url = "<?php echo base_url('Admin/Pages/getSlug'); ?>";
        jQuery.post(url, data, function (data) {
            jQuery('#productSlug').val(data);
        });
    });
</script>
</body>  
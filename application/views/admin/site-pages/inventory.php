<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$fields = $this->md->select('tbl_petacategory');    // get all category
$locations = $this->md->select('tbl_location');    // get all location
$sizeFields = $this->md->my_query('SELECT * FROM tbl_size ORDER BY size ASC')->result();    // get all Size
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit"):
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form method="post" name="edit" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label">Product SKU</label>
                                                            <input type="text" name="sku"
                                                                   value="<?php
                                                                   if (set_value('sku') && !isset($success)) {
                                                                       echo set_value('sku');
                                                                   } else {
                                                                       echo $updata[0]->sku;
                                                                   }
                                                                   ?>" placeholder="Enter Product SKU"
                                                                   class="form-control <?php if (form_error('sku')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('sku')) {
                                                                    echo form_error('sku');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Peta Category</label>
                                                                <select name="petacategory_id"
                                                                        id="peta_category"
                                                                        class="form-control select2 text-capitalize <?php if (form_error('petacategory_id')) { ?> is-invalid <?php } ?>">
                                                                    <option value="">Select Peta Category</option>
                                                                    <?php
                                                                    if (!empty($fields)):
                                                                        foreach ($fields as $fields_val):
                                                                            ?>
                                                                            <option
                                                                                <?php echo $fields_val->petacategory_id == $updata[0]->petacategory_id ? 'selected' : ''; ?>
                                                                                    value="<?php echo $fields_val->petacategory_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('petacategory_id')) {
                                                                        echo form_error('petacategory_id');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" id="categoryData">
                                                            <?php
                                                            $val = $updata[0]->petacategory_id;   // Peta ID
                                                            $petacategory = $this->md->select_where('tbl_petacategory', array('petacategory_id' => $val));
                                                            if (!empty($petacategory)) {
                                                                $subcategory = $this->md->select_where('tbl_subcategory', array('subcategory_id' => $petacategory[0]->parent));
                                                                if (!empty($subcategory)) {
                                                                    $category = $this->md->select_where('tbl_category', array('category_id' => $subcategory[0]->parent));
                                                                    if (!empty($category)) {
                                                                        ?>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label">Sub
                                                                                        Category</label>
                                                                                    <select name="subcategory_id"
                                                                                            class="form-control text-capitalize <?php if (form_error('subcategory_id')) { ?> is-invalid <?php } ?>">
                                                                                        <option
                                                                                                value="<?php echo $subcategory[0]->subcategory_id; ?>"><?php echo $subcategory[0]->title; ?></option>
                                                                                    </select>
                                                                                    <div class="error-text">
                                                                                        <?php
                                                                                        if (form_error('subcategory_id')) {
                                                                                            echo form_error('subcategory_id');
                                                                                        }
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label">Category</label>
                                                                                    <select name="category_id"
                                                                                            class="form-control text-capitalize <?php if (form_error('category_id')) { ?> is-invalid <?php } ?>">
                                                                                        <option
                                                                                                value="<?php echo $category[0]->category_id; ?>"><?php echo $category[0]->title; ?></option>
                                                                                    </select>
                                                                                    <div class="error-text">
                                                                                        <?php
                                                                                        if (form_error('category_id')) {
                                                                                            echo form_error('category_id');
                                                                                        }
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Title</label>
                                                            <input type="text" name="title" value="<?php
                                                            if (set_value('title') && !isset($success)) {
                                                                echo set_value('title');
                                                            } else {
                                                                echo $updata[0]->title;
                                                            }
                                                            ?>" placeholder="Enter Product Title"
                                                                   id="productTitle"
                                                                   class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('title')) {
                                                                    echo form_error('title');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label">Product Stock </label>
                                                            <input type="number" step="any" name="stock"
                                                                   value="<?php
                                                                   if (set_value('stock') && !isset($success)) {
                                                                       echo set_value('stock');
                                                                   } else {
                                                                       echo $updata[0]->stock;
                                                                   }
                                                                   ?>" placeholder="Enter Product Stock"
                                                                   class="form-control <?php if (form_error('stock')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('stock')) {
                                                                    echo form_error('stock');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Product location </label>
                                                                <select name="location"
                                                                        class="form-control select2 text-capitalize <?php if (form_error('location')) { ?> is-invalid <?php } ?>">
                                                                    <option value="">Select Location</option>
                                                                    <?php
                                                                    if (!empty($locations)):
                                                                        foreach ($locations as $location):
                                                                            ?>
                                                                            <option
                                                                                <?php echo $location->title == $updata[0]->location ? 'selected' : ''; ?>
                                                                                    value="<?php echo $location->title; ?>"><?php echo $location->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('location')) {
                                                                        echo form_error('location');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product Rate</label>
                                                            <input type="number" step="any" name="rate" value="<?php
                                                            if (set_value('rate') && !isset($success)) {
                                                                echo set_value('rate');
                                                            } else {
                                                                echo $updata[0]->rate;
                                                            }
                                                            ?>" placeholder="Enter Product Rate"
                                                                   class="form-control <?php if (form_error('rate')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('rate')) {
                                                                    echo form_error('rate');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product Stick</label>
                                                            <input type="number" step="any" name="stick" value="<?php
                                                            if (set_value('stick') && !isset($success)) {
                                                                echo set_value('stick');
                                                            } else {
                                                                echo $updata[0]->stick;
                                                            }
                                                            ?>" placeholder="Enter Product Stick"
                                                                   class="form-control <?php if (form_error('stick')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('stick')) {
                                                                    echo form_error('stick');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product FT</label>
                                                            <input type="number" step="any" name="ft" value="<?php
                                                            if (set_value('ft') && !isset($success)) {
                                                                echo set_value('ft');
                                                            } else {
                                                                echo $updata[0]->ft;
                                                            }
                                                            ?>" placeholder="Enter Product FT"
                                                                   class="form-control <?php if (form_error('ft')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('ft')) {
                                                                    echo form_error('ft');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Description</label>
                                                            <textarea name="description"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('description');
                                                                } else {
                                                                    echo $updata[0]->description;
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('description')) {
                                                                    echo form_error('description');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Additional Info</label>
                                                            <textarea name="additional_info"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('additional_info');
                                                                } else {
                                                                    echo $updata[0]->additional_info;
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('additional_info')) {
                                                                    echo form_error('additional_info');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Select Product Photos <span
                                                                            class="font-12 text-info">*(Upload only .jpg | .jpeg | .png files with less than 1MB size and dimension should be 1280*1350.)</span>
                                                                </label>
                                                                <div class="fileupload fileupload-new"
                                                                     data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <input type="file" multiple="" id="file"
                                                                               onchange="imagesPreview(this, '#img_preview');$('#updateStatus').val('yes');"
                                                                               name="product_photos[]" class=""
                                                                               accept="image/*">
                                                                        <input type="hidden" id="updateStatus"
                                                                               name="updateStatus"/>
                                                                        <input type="hidden"
                                                                               value="<?php echo $updata[0]->photos; ?>"
                                                                               name="oldPath"/>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <?php
                                                                    $allphotos = $updata[0]->photos ? explode(",", $updata[0]->photos) : [];
                                                                    if (!empty($allphotos)) :
                                                                        $pp = 0;    // photo index
                                                                        foreach ($allphotos as $photo) :
                                                                            ?>
                                                                            <div class="col-md-3">
                                                                                <img
                                                                                        src="<?php echo base_url($photo); ?>"
                                                                                        class="mt-20 center-block"
                                                                                        style="width: 100%;height: 100px;object-fit: contain"/>
                                                                                <a class="btn btn-danger btn-sm"
                                                                                   href="<?php echo base_url('Admin/Pages/removePhoto/product/' . $updata[0]->product_id . '/' . $pp); ?>">Remove</a>
                                                                            </div>
                                                                            <?php
                                                                            $pp++;
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </div>
                                                                <div id="img_preview"></div>
                                                                <p class="error-text file-error" style="display: none">
                                                                    Select a valid file!</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="featured"
                                                                        <?php echo $updata[0]->featured == 1 ? 'checked' : ''; ?>
                                                                           value="1"
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Featured Product
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update Product
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>
                                                <!-- >> Add Form Start
                                                ================================================== -->
                                                <form method="post" name="add" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label">Product SKU</label>
                                                            <input type="text" name="sku"
                                                                   value="<?php
                                                                   if (set_value('sku') && !isset($success)) {
                                                                       echo set_value('sku');
                                                                   }
                                                                   ?>" placeholder="Enter Product SKU"
                                                                   class="form-control <?php if (form_error('sku')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('sku')) {
                                                                    echo form_error('sku');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Peta Category</label>
                                                                <select name="petacategory_id"
                                                                        id="peta_category"
                                                                        class="form-control select2 text-capitalize <?php if (form_error('petacategory_id')) { ?> is-invalid <?php } ?>">
                                                                    <option value="">Select Peta Category</option>
                                                                    <?php
                                                                    if (!empty($fields)):
                                                                        foreach ($fields as $fields_val):
                                                                            ?>
                                                                            <option
                                                                                    value="<?php echo $fields_val->petacategory_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('petacategory_id')) {
                                                                        echo form_error('petacategory_id');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" id="categoryData">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Sub
                                                                            Category</label>
                                                                        <select name="subcategory_id"
                                                                                id="sub_category"
                                                                                class="form-control select2 text-capitalize <?php if (form_error('subcategory_id')) { ?> is-invalid <?php } ?>">
                                                                            <option value="">Select Sub Category
                                                                            </option>
                                                                        </select>
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('subcategory_id')) {
                                                                                echo form_error('subcategory_id');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Category</label>
                                                                        <select name="category_id"
                                                                                id="parentCategory"
                                                                                class="form-control select2 text-capitalize <?php if (form_error('category_id')) { ?> is-invalid <?php } ?>">
                                                                            <option value="">Select Category</option>
                                                                            <?php
                                                                            if (!empty($fields)):
                                                                                foreach ($fields as $fields_val):
                                                                                    ?>
                                                                                    <option
                                                                                            value="<?php echo $fields_val->category_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                                <?php
                                                                                endforeach;
                                                                            endif;
                                                                            ?>
                                                                        </select>
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('category_id')) {
                                                                                echo form_error('category_id');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Title</label>
                                                            <input type="text" name="title" value="<?php
                                                            if (set_value('title') && !isset($success)) {
                                                                echo set_value('title');
                                                            }
                                                            ?>" placeholder="Enter Product Title"
                                                                   id="productTitle"
                                                                   class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('title')) {
                                                                    echo form_error('title');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label">Product Stock </label>
                                                            <input type="number" step="any" name="stock"
                                                                   value="<?php
                                                                   if (set_value('stock') && !isset($success)) {
                                                                       echo set_value('stock');
                                                                   }
                                                                   ?>" placeholder="Enter Product Stock"
                                                                   class="form-control <?php if (form_error('stock')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('stock')) {
                                                                    echo form_error('stock');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="control-label">Product location </label>
                                                                <select name="location"
                                                                        class="form-control select2 text-capitalize <?php if (form_error('location')) { ?> is-invalid <?php } ?>">
                                                                    <option value="">Select Location</option>
                                                                    <?php
                                                                    if (!empty($locations)):
                                                                        foreach ($locations as $location):
                                                                            ?>
                                                                            <option
                                                                                    value="<?php echo $location->title; ?>"><?php echo $location->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('location')) {
                                                                        echo form_error('location');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product Rate</label>
                                                            <input type="number" step="any" name="rate" value="<?php
                                                            if (set_value('rate') && !isset($success)) {
                                                                echo set_value('rate');
                                                            }
                                                            ?>" placeholder="Enter Product Rate"
                                                                   class="form-control <?php if (form_error('rate')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('rate')) {
                                                                    echo form_error('rate');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product Stick</label>
                                                            <input type="number" step="any" name="stick" value="<?php
                                                            if (set_value('stick') && !isset($success)) {
                                                                echo set_value('stick');
                                                            }
                                                            ?>" placeholder="Enter Product Stick"
                                                                   class="form-control <?php if (form_error('stick')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('stick')) {
                                                                    echo form_error('stick');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product FT</label>
                                                            <input type="number" step="any" name="ft" value="<?php
                                                            if (set_value('ft') && !isset($success)) {
                                                                echo set_value('ft');
                                                            }
                                                            ?>" placeholder="Enter Product FT"
                                                                   class="form-control <?php if (form_error('ft')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('ft')) {
                                                                    echo form_error('ft');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>


                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Description</label>
                                                            <textarea name="description"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('description');
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('description')) {
                                                                    echo form_error('description');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Additional Info</label>
                                                            <textarea name="additional_info"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('additional_info');
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('additional_info')) {
                                                                    echo form_error('additional_info');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Select Product Photos <span
                                                                            class="font-12 text-info">*(Upload only .jpg | .jpeg | .png files with less than 1MB size and dimension should be 1280*1350.)</span>
                                                                </label>
                                                                <div class="fileupload fileupload-new"
                                                                     data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <input type="file" id="file" multiple=""
                                                                               onchange="imagesPreview(this, '#img_preview');"
                                                                               name="product_photos[]" class=""
                                                                               accept="image/*">
                                                                    </div>
                                                                </div>
                                                                <div id="img_preview"></div>
                                                                <p class="error-text file-error" style="display: none">
                                                                    Select a valid file!</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="featured"
                                                                           value="1"
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Featured Product
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Add Product
                                                        </button>
                                                        <button
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                                type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                            ================================================== -->
                        <?php
                        else:
                            if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Select Table</label>
                                                        <select class="form-control select2" id="tableName">
                                                            <option value="product">Product</option>
                                                            <option value="location">Location</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table mb-none table-hover" id="inventoryTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Category</th>
                                                        <th>SKU</th>
                                                        <th>Title</th>
                                                        <th>Available Stock</th>
                                                        <th>Check Stock</th>
                                                        <th>Add/Edit Stock</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else:
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer  ?>
    </div>
</div>
<!-- >> Permission Modal Start
   ================================================== -->
<div class="modal fade" role="dialog" id="inventoryModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Manage Inventory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="inventory_area">

            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- << Permission Modal End
================================================== -->
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>

<script>
    // confirmation message for model on escape key
    $(document).ready(function () {
        $('#inventoryModal').on('shown.bs.modal', function () {
            $(document).on('keyup', function (e) {
                if (e.key === "Escape") {
                    // Show confirmation dialog
                    if (confirm("Are you sure you want to close the modal? Any unsaved changes will be lost.")) {
                        $('#inventoryModal').modal('hide');
                    }
                }
            });
        });

        $('#inventoryModal').on('hidden.bs.modal', function () {
            // Remove the keyup event listener when the modal is closed
            $(document).off('keyup');
        });
    });

    // Restrict Enter key in model
    $(document).ready(function () {
        // Trigger the code only when the modal is shown
        $('#inventoryModal').on('shown.bs.modal', function () {
            // Immediately restrict the Enter key on the modal to prevent unintended closing
            $(document).on('keydown.preventClose', function (e) {
                if (e.key === "Enter") {
                    e.preventDefault(); // Prevent Enter from triggering default behavior
                }
            });

            // Restrict Enter key navigation within the form
            $('#inventory_form').on('keydown', 'input', function (e) {
                if (e.key === "Enter") {
                    e.preventDefault(); // Prevent form submission

                    // Get all focusable elements in the form
                    const focusable = $('#inventory_form').find('input, select, textarea').filter(':visible');
                    const index = focusable.index(this) + 1;

                    // Focus the next element if it exists, else wrap around to the first input
                    if (index < focusable.length) {
                        focusable.eq(index).focus();
                    } else {
                        focusable.eq(0).focus(); // Wrap around to the first input if end is reached
                    }
                }
            });
        });

        // Clean up the event when the modal is hidden
        $('#inventoryModal').on('hidden.bs.modal', function () {
            $(document).off('keydown.preventClose'); // Remove the Enter key restriction
            $('#inventory_form').off('keydown', 'input');
        });
    });


    $(document).ready(function () {
        // List column which will be display in the table
        $column = [
            {data: 'category'},
            {data: 'sku'},
            {data: 'title'},
            {data: 'stock'},
            {data: 'check'},
            {data: 'add'}
        ];
        $tbl = '<?php echo "inventory"; ?>';
        $url = "<?php echo base_url($current_page . '/getdata') ?>";
        $responsive = false;
        var userDataTable = $('#' + $tbl + 'Table').DataTable({
            dom: 'Bfrtlip', // enable Export buttons, per page, search, pagination
            'processing': true, // true/false
            'responsive': $responsive, // enable expand(+) mode (view more in table)
            'serverSide': true, // true/false
            'pageLength': 10, // Per page Item
            "lengthMenu": [[10, 25, 50, 500, 1000, 5000], [10, 25, 50, 500, 1000, 5000]],
            'serverMethod': 'post', // get/post
            'ajax': {
                'url': $url,
                'data': function (data) {
                    data.tableName = $('#tableName').val();
                }
            },
            'columns': $column,
            "fnDrawCallback": function () {
                $('[data-toggle="tooltip"]').tooltip(); // enable tooltip in datatable
                var info = userDataTable.page.info();
                //$('#counterTotal').html(info.recordsDisplay);
            },
            "columnDefs": [
                {"sortable": false, "targets": [0]}
            ]
        });
        $('#tableName').change(function () {
            userDataTable.draw();   // From Date to Date Filter
        });
        $(document).on("click", "#productID", function () {
            const proid = $(this).data('proid'); // getting proid ID
            const type = $(this).data('type'); // add / edit/ show
            const table = $(this).data('table'); // product / location
            var data = {proid: proid, type: type, table: table};    //proid ID
            $('#inventory_area').html('<h4>Loading...</h4>');
            var url = "Admin/Pages/getProductStockData";
            jQuery.post(url, data, function (data) {
                jQuery('#inventory_area').html(data);
                $("#inventory_area select").select2({
                    dropdownParent: $("#inventoryModal")
                });
            });
        });
    });
</script>

<style>
    .addContent .addMore, .addContent .removeLast {
        position: absolute;
        right: 0;
        top: 30px;
    }
</style>
</body>
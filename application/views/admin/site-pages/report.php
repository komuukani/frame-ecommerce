<?php
$web_data = $this->md->select('tbl_web_data')[0];
$upi = $this->md->select_where('tbl_upi', array('status' => 1));
?>
<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title>Invoice No.: <?php echo $invoiceData ? $invoiceData[0]->order_id : '' ?> </title>
    <base href="<?php echo base_url('assets/'); ?>"/>
    <!-- Favicon Icon -->
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link href="<?php echo base_url('admin_asset/'); ?>css/responsive.css" rel="stylesheet">
    <!-- Report CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('admin_asset/'); ?>/css/report.css"/>
</head>
<body>
<!-- Define header and footer blocks before your content -->
<!--<header>-->
<!--    Dronachryas-->
<!--</header>-->

<footer>
    <div class="page-counter">
        Invoice No.: <?php echo $invoiceData ? $invoiceData[0]->order_id : '' ?>
    </div>
    <div class="clearfix"></div>
</footer>

<!-- Wrap the content of your PDF inside a main tag -->
<main>
    <!-- Page - 1 -->
    <div style="page-break-after: never;" class="page-1">
        <div align="center">
            <h1 class="text-transform-uppercase letter-spacing-3 font-weight-100">
                || <?php echo $web_data ? $web_data->project_name : ''; ?> ||</h1>
            <p class="">
                Phone Number: <?php echo $web_data ? $web_data->phone : ''; ?>
                <br/>
                <?php echo $web_data ? $web_data->address : ''; ?>
            </p>
        </div>
        <table cellspacing="0" cellpadding="10">
            <tr style="border:1px solid #000;">
                <td>
                    <b class="subheading">Bill To</b>
                    <b>Name: </b><?php echo $invoiceData ? ucfirst($invoiceData[0]->fname) : '' ?><br/>
                    <b>Email: </b><?php echo $invoiceData ? $invoiceData[0]->email : 'not mentioned!' ?> <br/>
                    <b>Phone: </b><?php echo $invoiceData ? $invoiceData[0]->phone : 'not mentioned!' ?><br/>
                    <b>Address: </b><?php echo $invoiceData ? ucfirst($invoiceData[0]->address) : 'not mentioned!' ?>
                </td>
                <td style="border:1px solid #000;width: 50%">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="strong">Order ID#</td>
                            <td>:</td>
                            <td><?php echo $invoiceData ? $invoiceData[0]->order_id : '' ?></td>
                        </tr>
                        <tr>
                            <td class="strong">Invoice Date</td>
                            <td>:</td>
                            <td><?php echo $invoiceData ? (date('d-M-Y', strtotime($invoiceData[0]->entry_date))) : '' ?></td>
                        </tr>
                        <tr>
                            <td class="strong">LR Number</td>
                            <td>:</td>
                            <td><?php echo $invoiceData ? $invoiceData[0]->lr_no : '' ?></td>
                        </tr>
                        <tr>
                            <td class="strong">Date of LR</td>
                            <td>:</td>
                            <td><?php echo $invoiceData ? (date('d-M-Y', strtotime($invoiceData[0]->date_of_lr))) : '' ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border:1px solid #000;">
                <td colspan="2">
                    <table border="1" bordercolor="#f5f5f5" cellspacing="0" cellpadding="3" width="100%">
                        <tr>
                            <th>Frame No</th>
                            <th>Box</th>
                            <th>Stick</th>
                            <th>FT</th>
                            <th>Rate</th>
                            <th>Amount</th>
                        </tr>
                        <?php
                        $totalBoxes = 0;
                        $items = $this->md->select_where('tbl_transaction', array('bill_id' => $invoiceData[0]->bill_id));
                        if ($items) {
                            foreach ($items as $item) {
                                $product = $this->md->select_where('tbl_product', array('product_id' => $item->product_id));
                                $totalBoxes += $item->qty;
                                ?>
                                <tr class="text-center">
                                    <td><?php echo($product ? $product[0]->title : ''); ?> </td>
                                    <td><?php echo $item->qty; ?></td>
                                    <td><?php echo $item->stick; ?></td>
                                    <td><?php echo $item->ft; ?></td>
                                    <td><?php echo number_format($item->rate, 2); ?></td>
                                    <td><?php echo number_format($item->amount, 2); ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr>
                            <td align="right">Total Boxes</td>
                            <td
                                    align="center"><?php echo $totalBoxes; ?></td>
                            <td colspan="3" align="right">Cartage</td>
                            <td align="center"><?php echo number_format(($invoiceData ? $invoiceData[0]->cartage : 0), 2); ?></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="right">GST</td>
                            <td align="center"><?php echo number_format(($invoiceData ? $invoiceData[0]->gst : 0), 2); ?></td>
                        </tr>
                        <tr class="font-16">
                            <td colspan="5" align="right">Total</td>
                            <td align="center"><?php echo number_format(($invoiceData ? $invoiceData[0]->total : 0), 2); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border:1px solid #000;">
                <td align="left" colspan="2">
                    <p class="p-0 m-0 text-left">
                        <b>Tr. Booking
                            Destination:</b> <?php echo($invoiceData ? ucfirst($invoiceData[0]->booking_destination) : ''); ?>
                    </p>
                    <p class="p-0 m-0 text-left">
                        <b> Name of
                            Transport:</b> <?php echo($invoiceData ? ucfirst($invoiceData[0]->name_transport) : ''); ?>
                    </p>
                    <p class="p-0 m-0 text-left">
                        <b>Name of Sales
                            Person:</b> <?php echo($invoiceData ? ucfirst($invoiceData[0]->name_sales_person) : ''); ?>
                    </p>
                    <p class="p-0 m-0 text-left">
                        <b>Drop
                            Location:</b> <?php echo($invoiceData ? ucfirst($invoiceData[0]->drop_location) : ''); ?>
                    </p>
                </td>
            </tr>
            <tr style="border:1px solid #000;">
                <td align="left">
                    <h3 class="p-0 m-0 text-center">
                        NEFT/RTGS: <?php echo number_format(($invoiceData ? $invoiceData[0]->neft_rtgs : 0), 2); ?></h3>
                    <ul style="list-style: none;text-transform: uppercase;border-top:1px solid #dfdfdf;margin-top: 5px;padding-top: 5px;">
                        <li><b>Bank Name: </b> Union Bank</li>
                        <li><b>Account Name: </b> P J Enterprise</li>
                        <li><b>Account Number: </b> 6316546464646</li>
                        <li><b>IFSC Code: </b> ASU23466</li>
                        <li><b>Branch: </b> Baroda Branch</li>
                    </ul>
                </td>
                <td align="center" style="border:1px solid #000;width: 50%">
                    <img src="<?php echo base_url('assets/images/upi.png'); ?>" alt="Upi"
                         width="120px"/>
                </td>
            </tr>
            <?php
            if ($upi) {
                ?>
                <tr style="border:1px solid #000;">
                    <td align="left">
                        <h3 class="p-0 m-0 text-center">
                            CASH: <?php echo number_format(($invoiceData ? $invoiceData[0]->cash : 0), 2); ?></h3>
                        <ul style="list-style: none;text-transform: uppercase;border-top:1px solid #dfdfdf;margin-top: 5px;padding-top: 5px;">
                            <li><b>Bank Name: </b> <?php echo $upi ? $upi[0]->bank_name : ''; ?></li>
                            <li><b>Account Name: </b> <?php echo $upi ? $upi[0]->account_name : ''; ?></li>
                            <li><b>Account Number: </b> <?php echo $upi ? $upi[0]->account_number : ''; ?></li>
                            <li><b>IFSC Code: </b> <?php echo $upi ? $upi[0]->ifsc_code : ''; ?></li>
                            <li><b>Branch: </b> <?php echo $upi ? $upi[0]->branch : ''; ?></li>
                        </ul>
                    </td>
                    <td align="center" style="border:1px solid #000;width: 50%">
                        <img src="<?php echo $upi ? base_url($upi[0]->path) : base_url(FILENOTFOUND); ?>" alt="Upi"
                             width="120px"/>
                    </td>
                </tr>
                <?php
            }
            ?>
            <tr style="border:1px solid #000;">
                <td align="center">
                    <i>Thanks for your business!</i>
                    <Br/><Br/><Br/>
                </td>
                <td align="center" style="border:1px solid #000;width: 50%">
                    <i> Authorized Signature</i>
                    <Br/><Br/><Br/>
                </td>
            </tr>
        </table>
    </div>

</main>
</body>
</html>
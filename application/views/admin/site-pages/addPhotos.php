<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <div class="row">
                        <!-- >> ADD/EDIT Data Start
                        ================================================== -->
                        <div class="col-md-12">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <?php
                                    if (isset($productData)):
                                        ?>
                                        <!-- >> Edit Form Start
                                        ================================================== -->
                                        <form method="post" name="edit" enctype="multipart/form-data">
                                            <h4><?php echo $productData ? $productData[0]->title : ''; ?></h4>
<!--                                            <p class="mb-0">-->
<!--                                                <b>SKU:</b> --><?php //echo $productData ? $productData[0]->sku : ''; ?><!--</p>-->
                                            <p class="mb-0">
                                                <b>Price:</b> <?php echo $productData ? ($productData[0]->price ? number_format($productData[0]->price) : '') : ''; ?>
                                                SAR</p>
                                            <hr/>

                                            <div class="panel-body row">
                                                <div class="col-md-12">
                                                    <div class="addContent">
                                                        <?php
                                                        $productId = $productData[0]->product_id;
                                                        $attributes = $this->md->select_where('tbl_product_attribute', array('product_id' => $productId));
                                                        if ($attributes) {
                                                            foreach ($attributes as $key => $attribute) {
                                                                ?>
                                                                <div class="row item bg-F5F5F5 mb-10">
                                                                    <div class="form-group col-md-2">
                                                                        <label class="control-label">Product
                                                                            Color</label>
                                                                        <h5 class="mb-0 font-18">
                                                                            <span
                                                                                class="colorBox"
                                                                                style="background-color: <?php echo $attribute->color; ?>"></span>
                                                                            <?php echo ucfirst($attribute->color); ?>
                                                                        </h5>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        <div class="form-group mb-1">
                                                                            <label class="control-label">Select
                                                                                Product Photos <span
                                                                                    class="font-12 text-info">*(Upload only .jpg | .jpeg | .png files with less than 1MB size.)</span>
                                                                            </label>
                                                                            <div class="fileupload fileupload-new">
                                                                                <div class="input-append">
                                                                                    <input type="file" id="file"
                                                                                           multiple=""
                                                                                           onchange="imagesPreview(this);$(this).next().val('yes');"
                                                                                           name="product_photos[<?php echo $attribute->product_attribute_id; ?>][]"
                                                                                           accept="image/*">
                                                                                    <input type="hidden"
                                                                                           name="updateStatus[<?php echo $attribute->product_attribute_id; ?>]"/>
                                                                                    <input type="hidden"
                                                                                           value="<?php echo $attribute->product_photos; ?>"
                                                                                           name="oldPath[<?php echo $attribute->product_attribute_id; ?>]"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="img_preview"></div>
                                                                            <div class="row">
                                                                                <?php
                                                                                $allphotos = $attribute->product_photos ? explode(",", $attribute->product_photos) : [];
                                                                                if (!empty($allphotos)) :
                                                                                    foreach ($allphotos as $key => $photo) :
                                                                                        ?>
                                                                                        <div class="col-md-3">
                                                                                            <img
                                                                                                src="<?php echo base_url($photo); ?>"
                                                                                                class="mt-20 center-block"
                                                                                                style="width: 100%;height: 100px;object-fit: contain"/>
                                                                                            <a class="btn btn-danger btn-sm"
                                                                                               href="<?php echo base_url('Admin/Pages/removeProductPhoto/' . $attribute->product_attribute_id . '/' . $key); ?>">Remove</a>
                                                                                        </div>
                                                                                    <?php
                                                                                    endforeach;
                                                                                endif;
                                                                                ?>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <footer class="panel-footer">
                                                <button type="submit"
                                                        class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                        value="send" name="addPhotos">Add Photos
                                                </button>
                                                <a href="<?php echo base_url('manage-product/show'); ?>"
                                                   class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Go
                                                    Back</a>
                                            </footer>
                                        </form>
                                        <!-- << Edit Form End
                                        ================================================== -->
                                    <?php
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <!-- << ADD/EDIT Data END
                        ================================================== -->
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer  ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
</body>
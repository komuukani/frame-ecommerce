<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$products = $this->md->select_where('tbl_product', array('status' => 1));    // get all products
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="section-title mt-0"><?php echo ucfirst($page_type) . " Order Data"; ?></h2>
                        </div>
                    </div>
                    <div class="row">
                        <!-- >> ADD/EDIT Data Start
                        ================================================== -->
                        <div class="col-md-12">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <?php
                                    if (isset($updata)):
                                        ?>
                                        <!-- >> Edit Form Start
                                        ================================================== -->
                                        <form method="post" name="edit" enctype="multipart/form-data">
                                            <div class="panel-body row">
                                                <div class="col-md-12">
                                                    <h5 class="bg-DFDFDF text-transform-uppercase font-18 letter-spacing-4 text-000 p-2">
                                                        <i
                                                                class="fa fa-shopping-cart"></i> Order Details:</h5>
                                                    <div class="addContent">
                                                        <?php
                                                        $transactions = $this->md->select_where('tbl_transaction', array('bill_id' => $updata[0]->bill_id));
                                                        if ($transactions) {
                                                            foreach ($transactions as $key => $transaction) {
                                                                ?>
                                                                <div class="row item">
                                                                    <div class="form-group col-md-4">
                                                                        <label
                                                                                class="control-label">Products</label>
                                                                        <select name="products[]"
                                                                                required
                                                                                class="form-control select2 text-capitalize">
                                                                            <option value="">Select Products
                                                                            </option>
                                                                            <?php
                                                                            if (!empty($products)):
                                                                                foreach ($products as $product):
                                                                                    ?>
                                                                                    <option
                                                                                        <?php echo $transaction->product_id == $product->product_id ? 'selected' : ''; ?>
                                                                                            value="<?php echo $product->product_id; ?>"><?php echo $product->title; ?></option>
                                                                                <?php
                                                                                endforeach;
                                                                            endif;
                                                                            ?>
                                                                        </select>
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('products')) {
                                                                                echo form_error('products');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group col-md-4">
                                                                        <label
                                                                                class="control-label">Quantity</label>
                                                                        <input type="number" step="any" name="qty[]"
                                                                               placeholder="Enter Product Quantity"
                                                                               value="<?php echo $transaction->qty; ?>"
                                                                               class="form-control <?php if (form_error('qty')) { ?> is-invalid <?php } ?>">
                                                                        <div class="error-text">
                                                                            <?php
                                                                            if (form_error('qty')) {
                                                                                echo form_error('qty');
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                    echo ($key > 0) ? '<button title="Remove Item" class="btn btn-md btn-danger removeLast" type="button"><i class="fa fa-minus"></i></button>' : '<button title="Add Item" class="btn btn-md btn-primary addMore" type="button"><i class="fa fa-plus"></i></button>';
                                                                    ?>
                                                                </div>
                                                                <?php
                                                            }
                                                        } else {
                                                            ?>
                                                            <div class="row item">
                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label">Products</label>
                                                                    <select name="products[]"
                                                                            required
                                                                            class="form-control text-capitalize">
                                                                        <option value="">Select Products</option>
                                                                        <?php
                                                                        if (!empty($products)):
                                                                            foreach ($products as $product):
                                                                                ?>
                                                                                <option
                                                                                        value="<?php echo $product->product_id; ?>"><?php echo $product->title; ?></option>
                                                                            <?php
                                                                            endforeach;
                                                                        endif;
                                                                        ?>
                                                                    </select>
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('products')) {
                                                                            echo form_error('products');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label">Quantity</label>
                                                                    <input type="number" step="any" name="qty[]"
                                                                           placeholder="Enter Product Quantity"
                                                                           class="form-control <?php if (form_error('qty')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('qty')) {
                                                                            echo form_error('qty');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                                <button title="Add Item"
                                                                        class="btn btn-md btn-primary addMore"
                                                                        type="button"><i
                                                                            class="fa fa-plus"></i></button>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <h5 class="bg-DFDFDF text-transform-uppercase font-18 letter-spacing-4 text-000 p-2">
                                                        <i
                                                                class="fa fa-user"></i> Customer Details:</h5>
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Customer Name</label>
                                                            <input type="text" name="fname" value="<?php
                                                            if (set_value('fname') && !isset($success)) {
                                                                echo set_value('fname');
                                                            } else {
                                                                echo $updata[0]->fname;
                                                            }
                                                            ?>" placeholder="Enter Customer Name"
                                                                   class="form-control <?php if (form_error('fname')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('fname')) {
                                                                    echo form_error('fname');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Customer Email</label>
                                                            <input type="email" name="email" value="<?php
                                                            if (set_value('email') && !isset($success)) {
                                                                echo set_value('email');
                                                            } else {
                                                                echo $updata[0]->email;
                                                            }
                                                            ?>" placeholder="Enter Customer Email"
                                                                   class="form-control <?php if (form_error('email')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('email')) {
                                                                    echo form_error('email');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Customer Phone</label>
                                                            <input type="text" name="phone" value="<?php
                                                            if (set_value('phone') && !isset($success)) {
                                                                echo set_value('phone');
                                                            } else {
                                                                echo $updata[0]->phone;
                                                            }
                                                            ?>" placeholder="Enter Customer Phone"
                                                                   class="form-control <?php if (form_error('phone')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('phone')) {
                                                                    echo form_error('phone');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">City</label>
                                                            <input type="text" name="city"
                                                                   value="<?php
                                                                   if (set_value('city') && !isset($success)) {
                                                                       echo set_value('city');
                                                                   } else {
                                                                       echo $updata[0]->city;
                                                                   }
                                                                   ?>" placeholder="Enter Customer city"
                                                                   class="form-control <?php if (form_error('city')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('city')) {
                                                                    echo form_error('city');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Postcode</label>
                                                            <input type="text" name="postcode"
                                                                   value="<?php
                                                                   if (set_value('postcode') && !isset($success)) {
                                                                       echo set_value('postcode');
                                                                   } else {
                                                                       echo $updata[0]->postal_code;
                                                                   }
                                                                   ?>" placeholder="Enter Customer Postcode"
                                                                   class="form-control <?php if (form_error('postcode')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('postcode')) {
                                                                    echo form_error('postcode');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Address</label>
                                                            <input type="text" name="address"
                                                                   value="<?php
                                                                   if (set_value('address') && !isset($success)) {
                                                                       echo set_value('address');
                                                                   } else {
                                                                       echo $updata[0]->address;
                                                                   }
                                                                   ?>" placeholder="Enter Customer Address"
                                                                   class="form-control <?php if (form_error('address')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('address')) {
                                                                    echo form_error('address');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Booking Destination</label>
                                                            <input type="text" name="booking_destination"
                                                                   value="<?php
                                                                   if (set_value('booking_destination') && !isset($success)) {
                                                                       echo set_value('booking_destination');
                                                                   } else {
                                                                       echo $updata[0]->booking_destination;
                                                                   }
                                                                   ?>" placeholder="Enter Booking Destination"
                                                                   class="form-control <?php if (form_error('booking_destination')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('booking_destination')) {
                                                                    echo form_error('booking_destination');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Name of Transport</label>
                                                            <input type="text" name="name_transport"
                                                                   value="<?php
                                                                   if (set_value('name_transport') && !isset($success)) {
                                                                       echo set_value('name_transport');
                                                                   } else {
                                                                       echo $updata[0]->name_transport;
                                                                   }
                                                                   ?>" placeholder="Enter Name of Transport"
                                                                   class="form-control <?php if (form_error('name_transport')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('name_transport')) {
                                                                    echo form_error('name_transport');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Drop Location</label>
                                                            <input type="text" name="drop_location"
                                                                   value="<?php
                                                                   if (set_value('drop_location') && !isset($success)) {
                                                                       echo set_value('drop_location');
                                                                   } else {
                                                                       echo $updata[0]->drop_location;
                                                                   }
                                                                   ?>" placeholder="Enter Drop Location"
                                                                   class="form-control <?php if (form_error('drop_location')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('drop_location')) {
                                                                    echo form_error('drop_location');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Name of Sales
                                                                Person</label>
                                                            <input type="text" name="name_sales_person"
                                                                   value="<?php
                                                                   if (set_value('name_sales_person') && !isset($success)) {
                                                                       echo set_value('name_sales_person');
                                                                   } else {
                                                                       echo $updata[0]->name_sales_person;
                                                                   }
                                                                   ?>" placeholder="Enter Name of Sales Person"
                                                                   class="form-control <?php if (form_error('name_sales_person')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('name_sales_person')) {
                                                                    echo form_error('name_sales_person');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Order Notes</label>
                                                            <textarea name="notes"
                                                                      placeholder="Enter Order Notes"
                                                                      style="min-height: 80px"
                                                                      class="form-control <?php if (form_error('notes')) { ?> is-invalid <?php } ?>"><?php
                                                                if (set_value('notes') && !isset($success)) {
                                                                    echo set_value('notes');
                                                                } else {
                                                                    echo $updata[0]->notes;
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('notes')) {
                                                                    echo form_error('notes');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <footer class="panel-footer">
                                                <button type="submit"
                                                        class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                        value="send" name="update">Update Order
                                                </button>
                                                <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                   class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                            </footer>
                                        </form>
                                        <!-- << Edit Form End
                                        ================================================== -->
                                    <?php
                                    else:
                                        ?>
                                        <!-- >> Add Form Start
                                        ================================================== -->
                                        <form method="post" name="add" enctype="multipart/form-data">
                                            <div class="panel-body row">
                                                <div class="col-md-12">
                                                    <h5 class="bg-DFDFDF text-transform-uppercase font-18 letter-spacing-4 text-000 p-2">
                                                        <i
                                                                class="fa fa-shopping-cart"></i> Order Details:</h5>
                                                    <div class="addContent">
                                                        <div class="row item">
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">Products</label>
                                                                <select name="products[]"
                                                                        required
                                                                        class="form-control select2 text-capitalize">
                                                                    <option value="">Select Products</option>
                                                                    <?php
                                                                    if (!empty($products)):
                                                                        foreach ($products as $product):
                                                                            ?>
                                                                            <option
                                                                                    value="<?php echo $product->product_id; ?>"><?php echo $product->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('products')) {
                                                                        echo form_error('products');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">Quantity</label>
                                                                <input type="number" step="any" name="qty[]"
                                                                       placeholder="Enter Product Quantity"
                                                                       class="form-control <?php if (form_error('qty')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('qty')) {
                                                                        echo form_error('qty');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <button title="Add Item"
                                                                    class="btn btn-md btn-primary addMore"
                                                                    type="button"><i
                                                                        class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <h5 class="bg-DFDFDF text-transform-uppercase font-18 letter-spacing-4 text-000 p-2">
                                                        <i
                                                                class="fa fa-user"></i> Customer Details:</h5>
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Customer Name</label>
                                                            <input type="text" name="fname" value="<?php
                                                            if (set_value('fname') && !isset($success)) {
                                                                echo set_value('fname');
                                                            }
                                                            ?>" placeholder="Enter Customer Name"
                                                                   class="form-control <?php if (form_error('fname')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('fname')) {
                                                                    echo form_error('fname');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Customer Email</label>
                                                            <input type="email" name="email" value="<?php
                                                            if (set_value('email') && !isset($success)) {
                                                                echo set_value('email');
                                                            }
                                                            ?>" placeholder="Enter Customer Email"
                                                                   class="form-control <?php if (form_error('email')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('email')) {
                                                                    echo form_error('email');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Customer Phone</label>
                                                            <input type="text" name="phone" value="<?php
                                                            if (set_value('phone') && !isset($success)) {
                                                                echo set_value('phone');
                                                            }
                                                            ?>" placeholder="Enter Customer Phone"
                                                                   class="form-control <?php if (form_error('phone')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('phone')) {
                                                                    echo form_error('phone');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">City</label>
                                                            <input type="text" name="city"
                                                                   value="<?php
                                                                   if (set_value('city') && !isset($success)) {
                                                                       echo set_value('city');
                                                                   }
                                                                   ?>" placeholder="Enter Customer city"
                                                                   class="form-control <?php if (form_error('city')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('city')) {
                                                                    echo form_error('city');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Postcode</label>
                                                            <input type="text" name="postcode"
                                                                   value="<?php
                                                                   if (set_value('postcode') && !isset($success)) {
                                                                       echo set_value('postcode');
                                                                   }
                                                                   ?>" placeholder="Enter Customer Postcode"
                                                                   class="form-control <?php if (form_error('postcode')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('postcode')) {
                                                                    echo form_error('postcode');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Address</label>
                                                            <input type="text" name="address"
                                                                   value="<?php
                                                                   if (set_value('address') && !isset($success)) {
                                                                       echo set_value('address');
                                                                   }
                                                                   ?>" placeholder="Enter Customer Address"
                                                                   class="form-control <?php if (form_error('address')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('address')) {
                                                                    echo form_error('address');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Booking Destination</label>
                                                            <input type="text" name="booking_destination"
                                                                   value="<?php
                                                                   if (set_value('booking_destination') && !isset($success)) {
                                                                       echo set_value('booking_destination');
                                                                   }
                                                                   ?>" placeholder="Enter Booking Destination"
                                                                   class="form-control <?php if (form_error('booking_destination')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('booking_destination')) {
                                                                    echo form_error('booking_destination');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Name of Transport</label>
                                                            <input type="text" name="name_transport"
                                                                   value="<?php
                                                                   if (set_value('name_transport') && !isset($success)) {
                                                                       echo set_value('name_transport');
                                                                   }
                                                                   ?>" placeholder="Enter Name of Transport"
                                                                   class="form-control <?php if (form_error('name_transport')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('name_transport')) {
                                                                    echo form_error('name_transport');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Drop Location</label>
                                                            <input type="text" name="drop_location"
                                                                   value="<?php
                                                                   if (set_value('drop_location') && !isset($success)) {
                                                                       echo set_value('drop_location');
                                                                   }
                                                                   ?>" placeholder="Enter Drop Location"
                                                                   class="form-control <?php if (form_error('drop_location')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('drop_location')) {
                                                                    echo form_error('drop_location');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Name of Sales
                                                                Person</label>
                                                            <input type="text" name="name_sales_person"
                                                                   value="<?php
                                                                   if (set_value('name_sales_person') && !isset($success)) {
                                                                       echo set_value('name_sales_person');
                                                                   }
                                                                   ?>" placeholder="Enter Name of Sales Person"
                                                                   class="form-control <?php if (form_error('name_sales_person')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('name_sales_person')) {
                                                                    echo form_error('name_sales_person');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Order Notes</label>
                                                            <textarea name="notes"
                                                                      placeholder="Enter Order Notes"
                                                                      style="min-height: 80px"
                                                                      class="form-control <?php if (form_error('notes')) { ?> is-invalid <?php } ?>"><?php
                                                                if (set_value('notes') && !isset($success)) {
                                                                    echo set_value('notes');
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('notes')) {
                                                                    echo form_error('notes');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <footer class="panel-footer">
                                                <button type="submit"
                                                        class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                        value="send" name="add">Add Order
                                                </button>
                                                <button
                                                        class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                        type="reset">Reset Form
                                                </button>
                                            </footer>
                                        </form>
                                        <!-- << Add Form End
                                        ================================================== -->
                                    <?php
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <!-- << ADD/EDIT Data END
                        ================================================== -->
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer  ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>

<style>
    .customTable {
        width: 100%;
    }

    .customTable tr th {
        background: #dfdfdf;
        color: #000;
        padding: 7px;
        border: 1px solid #999;
    }

    .customTable tr td {
        border: 1px solid #999;
        color: #000;
        padding: 7px;
    }

    .customTable input {
        width: 90px;
    }
</style>
</body>
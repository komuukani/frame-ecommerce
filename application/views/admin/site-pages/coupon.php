<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$category_data = $this->md->select('tbl_category');
$product_data = $this->md->select('tbl_product');
$control_data = $this->md->select_where('tbl_control', array('control_id' => 1));
$fields = $this->md->select('tbl_role');    // get all role   
?>

<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header
        ?>
        <?php echo $page_sidebar; //  Load Sidebar
        ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb
                ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)
                    ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit") :
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)) :
                                            if ($permission['all'] || $permission['edit']) :  // If coupon has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form method="post" name="edit" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="col-md-12">
                                                            <h5 class="mb-10 border-bottom p-5 bg-F5F5F5">Add Coupon
                                                                Info</h5>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label"><code>*</code>Coupon
                                                                Code</label>
                                                            <input type="text" name="coupon_code"
                                                                   value="<?php echo $updata[0]->coupon_code; ?>"
                                                                   placeholder="Enter Coupon Code"
                                                                   class="form-control text-uppercase <?php if (form_error('coupon_code')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('coupon_code')) {
                                                                    echo form_error('coupon_code');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label"><code>*</code>Coupon
                                                                Percentage(%)</label>
                                                            <input type="number" name="coupon_percentage"
                                                                   value="<?php echo $updata[0]->coupon_percentage; ?>"
                                                                   placeholder="Enter Coupon Percentage"
                                                                   class="form-control <?php if (form_error('coupon_percentage')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('coupon_percentage')) {
                                                                    echo form_error('coupon_percentage');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label"><code>*</code>Minimum Bill
                                                                Amount </label>
                                                            <input type="number" name="minimum_subtotal"
                                                                   value="<?php echo $updata[0]->minimum_subtotal; ?>"
                                                                   placeholder="Enter Minimum Bill Amount"
                                                                   class="form-control <?php if (form_error('minimum_subtotal')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('minimum_subtotal')) {
                                                                    echo form_error('minimum_subtotal');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label"><code>*</code>Maximum Bill
                                                                Amount </label>
                                                            <input type="number" name="maximum_subtotal"
                                                                   value="<?php echo $updata[0]->maximum_subtotal; ?>"
                                                                   placeholder="Enter Maximum Bill Amount"
                                                                   class="form-control <?php if (form_error('maximum_subtotal')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('maximum_subtotal')) {
                                                                    echo form_error('maximum_subtotal');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label"><code>*</code>Select
                                                                Products</label>
                                                            <select name="product[]" multiple id="product"
                                                                    class="form-control select2 text-capitalize <?php if (form_error('product')) { ?> is-invalid <?php } ?>">
                                                                <?php
                                                                if (!empty($product_data)) :
                                                                    $exis_pro = $updata[0]->product ? json_decode($updata[0]->product) : '';
                                                                    foreach ($product_data as $product_val) :
                                                                        ?>
                                                                        <option <?php echo !empty($exis_pro) && (in_array($product_val->product_id, $exis_pro)) ? 'selected' : ''; ?>
                                                                            value="<?php echo $product_val->product_id; ?>"><?php echo ucfirst($product_val->title); ?></option>
                                                                    <?php
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                            </select>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('product')) {
                                                                    echo form_error('product');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <!--                                                        <div class="form-group col-md-6">-->
                                                        <!--                                                            <label class="control-label"><code>*</code>Category</label>-->
                                                        <!--                                                            <select name="category[]" multiple id="category"-->
                                                        <!--                                                                    class="form-control select2 text-capitalize -->
                                                        <?php //if (form_error('category')) {
                                                        ?><!-- is-invalid --><?php //}
                                                        ?><!--">-->
                                                        <!--                                                                --><?php
                                                        //                                                                if (!empty($category_data)) :
                                                        //                                                                    $exis_cate = $updata[0]->category ? json_decode($updata[0]->category) : '';
                                                        //                                                                    foreach ($category_data as $category_val) :
                                                        //
                                                        ?>
                                                        <!--                                                                        <option --><?php //echo !empty($exis_cate) && (in_array($category_val->category_id, $exis_cate)) ? 'selected' : '';
                                                        ?>
                                                        <!--                                                                            value="-->
                                                        <?php //echo $category_val->category_id;
                                                        ?><!--">--><?php //echo ucfirst($category_val->title);
                                                        ?><!--</option>-->
                                                        <!--                                                                    --><?php
                                                        //                                                                    endforeach;
                                                        //                                                                endif;
                                                        //
                                                        ?>
                                                        <!--                                                            </select>-->
                                                        <!--                                                            <div class="error-text">-->
                                                        <!--                                                                --><?php
                                                        //                                                                if (form_error('category')) {
                                                        //                                                                    echo form_error('category');
                                                        //                                                                }
                                                        //
                                                        ?>
                                                        <!--                                                            </div>-->
                                                        <!--                                                        </div>-->
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label">Free Shipping Coupon?</label>
                                                            <Br/>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="free_shipping_yes" value="yes"
                                                                       name="free_shipping"
                                                                    <?php echo(($updata[0]->free_shipping == 'yes') ? 'checked' : ''); ?>
                                                                       class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                       for="free_shipping_yes">Yes</label>
                                                            </div>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="free_shipping_no" value="no"
                                                                       name="free_shipping" <?php echo(($updata[0]->free_shipping != 'yes') ? 'checked' : ''); ?>
                                                                       class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                       for="free_shipping_no">No</label>
                                                            </div>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('free_shipping')) {
                                                                    echo form_error('free_shipping');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label">Coupon Status</label> <Br/>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="active_coupon"
                                                                       value="1" <?php echo(($updata[0]->status) ? 'checked' : ''); ?>
                                                                       name="status" class="custom-control-input">
                                                                <label class="custom-control-label" for="active_coupon">Active</label>
                                                            </div>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="inactive_coupon"
                                                                       value="0" <?php echo((!$updata[0]->status) ? 'checked' : ''); ?>
                                                                       name="status" class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                       for="inactive_coupon">Inactive</label>
                                                            </div>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('status')) {
                                                                    echo form_error('status');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label"><code>*</code>Expiry
                                                                Date</label>
                                                            <input type="date" name="expiry_date"
                                                                   value="<?php echo $updata[0]->expiry_date; ?>"
                                                                   class="form-control <?php if (form_error('expiry_date')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('expiry_date')) {
                                                                    echo form_error('expiry_date');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-9">
                                                            <label class="control-label">Coupon Info</label>
                                                            <input type="text" name="coupon_description"
                                                                   value="<?php echo $updata[0]->coupon_description; ?>"
                                                                   placeholder="Enter Coupon Info"
                                                                   class="form-control <?php if (form_error('coupon_description')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('coupon_description')) {
                                                                    echo form_error('coupon_description');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update Coupon
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else :
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else :
                                            if ($permission['all'] || $permission['write']) :  // If coupon has write/add permission
                                                ?>
                                                <!-- >> Add Form Start
                                                ================================================== -->
                                                <form method="post" name="add" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="col-md-12">
                                                            <h5 class="mb-10 border-bottom p-5 bg-F5F5F5">Add Coupon
                                                                Info</h5>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label"><code>*</code>Coupon
                                                                Code</label>
                                                            <input type="text" name="coupon_code"
                                                                   value="<?php
                                                                   if (set_value('coupon_code') && !isset($success)) {
                                                                       echo set_value('coupon_code');
                                                                   }
                                                                   ?>" placeholder="Enter Coupon Code"
                                                                   class="form-control text-uppercase <?php if (form_error('coupon_code')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('coupon_code')) {
                                                                    echo form_error('coupon_code');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label"><code>*</code>Coupon
                                                                Percentage(%)</label>
                                                            <input type="number" name="coupon_percentage" value="<?php
                                                            if (set_value('coupon_percentage') && !isset($success)) {
                                                                echo set_value('coupon_percentage');
                                                            }
                                                            ?>" placeholder="Enter Coupon Percentage"
                                                                   class="form-control <?php if (form_error('coupon_percentage')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('coupon_percentage')) {
                                                                    echo form_error('coupon_percentage');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label"><code>*</code>Minimum Bill
                                                                Amount</label>
                                                            <input type="number" name="minimum_subtotal"
                                                                   value="<?php
                                                                   if (set_value('minimum_subtotal') && !isset($success)) {
                                                                       echo set_value('minimum_subtotal');
                                                                   }
                                                                   ?>" placeholder="Enter Minimum Bill Amount "
                                                                   class="form-control <?php if (form_error('minimum_subtotal')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('minimum_subtotal')) {
                                                                    echo form_error('minimum_subtotal');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label"><code>*</code>Maximum Bill
                                                                Amount </label>
                                                            <input type="number" name="maximum_subtotal"
                                                                   value="<?php
                                                                   if (set_value('maximum_subtotal') && !isset($success)) {
                                                                       echo set_value('maximum_subtotal');
                                                                   }
                                                                   ?>" placeholder="Enter Maximum Bill Amount"
                                                                   class="form-control <?php if (form_error('maximum_subtotal')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('maximum_subtotal')) {
                                                                    echo form_error('maximum_subtotal');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label"><code>*</code>Select
                                                                Products</label>
                                                            <select name="product[]" multiple id="product"
                                                                    class="form-control select2 text-capitalize <?php if (form_error('product')) { ?> is-invalid <?php } ?>">
                                                                <?php
                                                                if (!empty($product_data)) :
                                                                    foreach ($product_data as $product_val) :
                                                                        ?>
                                                                        <option
                                                                            value="<?php echo $product_val->product_id; ?>"><?php echo ucfirst($product_val->title); ?></option>
                                                                    <?php
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                            </select>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('product')) {
                                                                    echo form_error('product');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <!--                                                        <div class="form-group col-md-6">-->
                                                        <!--                                                            <label class="control-label"><code>*</code>Select-->
                                                        <!--                                                                Category</label>-->
                                                        <!--                                                            <select name="category[]" multiple id="category"-->
                                                        <!--                                                                    class="form-control select2 text-capitalize -->
                                                        <?php //if (form_error('category')) {
                                                        ?><!-- is-invalid --><?php //}
                                                        ?><!--">-->
                                                        <!--                                                                --><?php
                                                        //                                                                if (!empty($category_data)) :
                                                        //                                                                    foreach ($category_data as $category_val) :
                                                        //
                                                        ?>
                                                        <!--                                                                        <option-->
                                                        <!--                                                                            value="-->
                                                        <?php //echo $category_val->category_id;
                                                        ?><!--">--><?php //echo ucfirst($category_val->title);
                                                        ?><!--</option>-->
                                                        <!--                                                                    --><?php
                                                        //                                                                    endforeach;
                                                        //                                                                endif;
                                                        //
                                                        ?>
                                                        <!--                                                            </select>-->
                                                        <!--                                                            <div class="error-text">-->
                                                        <!--                                                                --><?php
                                                        //                                                                if (form_error('category')) {
                                                        //                                                                    echo form_error('category');
                                                        //                                                                }
                                                        //
                                                        ?>
                                                        <!--                                                            </div>-->
                                                        <!--                                                        </div>-->
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label">Free Shipping Coupon?</label>
                                                            <Br/>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="free_shipping_yes" value="yes"
                                                                       name="free_shipping"
                                                                       class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                       for="free_shipping_yes">Yes</label>
                                                            </div>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="free_shipping_no" value="no"
                                                                       name="free_shipping" checked
                                                                       class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                       for="free_shipping_no">No</label>
                                                            </div>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('free_shipping')) {
                                                                    echo form_error('free_shipping');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label">Coupon Status</label> <Br/>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="active_coupon" value="1"
                                                                       name="status" checked=""
                                                                       class="custom-control-input">
                                                                <label class="custom-control-label" for="active_coupon">Active</label>
                                                            </div>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="inactive_coupon" value="0"
                                                                       name="status" class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                       for="inactive_coupon">Inactive</label>
                                                            </div>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('status')) {
                                                                    echo form_error('status');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label class="control-label"><code>*</code>Expiry
                                                                Date</label>
                                                            <input type="date" name="expiry_date" value="<?php
                                                            if (set_value('expiry_date') && !isset($success)) {
                                                                echo set_value('expiry_date');
                                                            }
                                                            ?>"
                                                                   class="form-control <?php if (form_error('expiry_date')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('expiry_date')) {
                                                                    echo form_error('expiry_date');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-9">
                                                            <label class="control-label">Coupon Info (optional)</label>
                                                            <input type="text" name="coupon_description" value="<?php
                                                            if (set_value('coupon_description') && !isset($success)) {
                                                                echo set_value('coupon_description');
                                                            }
                                                            ?>" placeholder="Enter Coupon Info"
                                                                   class="form-control <?php if (form_error('coupon_description')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('coupon_description')) {
                                                                    echo form_error('coupon_description');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Add Coupon
                                                        </button>
                                                        <button
                                                            class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                            type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                            <?php
                                            else :
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                            ================================================== -->
                        <?php
                        else :
                            if ($permission['all'] || $permission['read']) :  // If coupon has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="">
                                                <table class="table mb-none table-hover" id="couponTable">
                                                    <thead>
                                                    <tr>
                                                        <!--                                                        <th>Status</th>-->
                                                        <th>Code</th>
                                                        <th>Description</th>
                                                        <th>Percentage</th>
                                                        <th class="none">Minimum Subtotal</th>
                                                        <th class="none">Maximum Subtotal</th>
                                                        <th>Free Shipping</th>
                                                        <th class="none">Expiry Date</th>
                                                        <th class="none">Entry Date</th>
                                                        <?php
                                                        //                                                        if ($permission['all'] || $permission['status']) :
                                                        //                                                            echo '<th>Action</th>';
                                                        //                                                        endif;
                                                        if ($permission['all'] || $permission['edit']) :
                                                            echo '<th>Edit</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['delete']) :
                                                            echo '<th>Delete</th>';
                                                        endif;
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else :
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer
        ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
<script>
    // List column which will be display in the table
    const column = [
        // {
        //     data: 'status'
        // },
        {
            data: 'coupon_code'
        },
        {
            data: 'coupon_description'
        },
        {
            data: 'coupon_percentage'
        },
        {
            data: 'minimum_subtotal'
        },
        {
            data: 'maximum_subtotal'
        },
        {
            data: 'free_shipping'
        },
        {
            data: 'expiry_date'
        },
        {
            data: 'entry_date'
        },
        <?php
        //        if ($permission['all'] || $permission['status']) :
        //            echo '{data: "action"},';
        //        endif;
        if ($permission['all'] || $permission['edit']) :
            echo '{data: "edit"},';
        endif;
        if ($permission['all'] || $permission['delete']) :
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE, ENABLE_EXPAND_MODE(+) ]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'photos', true);
</script>
</body>
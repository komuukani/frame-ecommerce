<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit"):
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form method="post" name="edit" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Upi</label>
                                                            <input type="text" name="upi" value="<?php
                                                            if (set_value('upi') && !isset($success)) {
                                                                echo set_value('upi');
                                                            } else {
                                                                echo $updata[0]->upi;
                                                            }
                                                            ?>" placeholder="Enter Upi"
                                                                   class="form-control <?php if (form_error('upi')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('upi')) {
                                                                    echo form_error('upi');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Bank Name</label>
                                                            <input type="text" name="bank_name" value="<?php
                                                            if (set_value('bank_name') && !isset($success)) {
                                                                echo set_value('bank_name');
                                                            } else {
                                                                echo $updata[0]->bank_name;
                                                            }
                                                            ?>" placeholder="Enter Bank Name"
                                                                   class="form-control <?php if (form_error('bank_name')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('bank_name')) {
                                                                    echo form_error('bank_name');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Account Name</label>
                                                            <input type="text" name="account_name" value="<?php
                                                            if (set_value('account_name') && !isset($success)) {
                                                                echo set_value('account_name');
                                                            } else {
                                                                echo $updata[0]->account_name;
                                                            }
                                                            ?>" placeholder="Enter Account Name"
                                                                   class="form-control <?php if (form_error('account_name')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('account_name')) {
                                                                    echo form_error('account_name');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Account Number</label>
                                                            <input type="text" name="account_number" value="<?php
                                                            if (set_value('account_number') && !isset($success)) {
                                                                echo set_value('account_number');
                                                            } else {
                                                                echo $updata[0]->account_number;
                                                            }
                                                            ?>" placeholder="Enter Account Number"
                                                                   class="form-control <?php if (form_error('account_number')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('account_number')) {
                                                                    echo form_error('account_number');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">IFSC Code</label>
                                                            <input type="text" name="ifsc_code" value="<?php
                                                            if (set_value('ifsc_code') && !isset($success)) {
                                                                echo set_value('ifsc_code');
                                                            } else {
                                                                echo $updata[0]->ifsc_code;
                                                            }
                                                            ?>" placeholder="Enter IFSC Code"
                                                                   class="form-control <?php if (form_error('ifsc_code')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('ifsc_code')) {
                                                                    echo form_error('ifsc_code');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Branch</label>
                                                            <input type="text" name="branch" value="<?php
                                                            if (set_value('branch') && !isset($success)) {
                                                                echo set_value('branch');
                                                            } else {
                                                                echo $updata[0]->branch;
                                                            }
                                                            ?>" placeholder="Enter Branch"
                                                                   class="form-control <?php if (form_error('branch')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('branch')) {
                                                                    echo form_error('branch');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Select Photo <span
                                                                    style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span>
                                                            </label>
                                                            <div class="fileupload fileupload-new"
                                                                 data-provides="fileupload">
                                                                <div class="input-append">
                                                                    <input type="file" id="file"
                                                                           onchange="readURL(this, 'blah');$('#updateStatus').val('yes');"
                                                                           name="upi" class="" accept="image/*">
                                                                    <input type="hidden" id="updateStatus"
                                                                           name="updateStatus"/>
                                                                    <input type="hidden"
                                                                           value="<?php echo $updata[0]->path; ?>"
                                                                           name="oldPath"/>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ($updata[0]->path):
                                                                ?>
                                                                <img src="<?php echo base_url($updata[0]->path); ?>"
                                                                     class="mt-20 center-block" width="50" id="blah"/>
                                                            <?php
                                                            else:
                                                                ?>
                                                                <img class="mt-20 center-block" width="50" id="blah"/>
                                                            <?php
                                                            endif;
                                                            ?>
                                                            <p class="error-text file-error" style="display: none">
                                                                Select a valid file!</p>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update UPI
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>
                                                <!-- >> Add Form Start
                                                ================================================== -->
                                                <form method="post" name="add" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Upi</label>
                                                            <input type="text" name="upi" value="<?php
                                                            if (set_value('upi') && !isset($success)) {
                                                                echo set_value('upi');
                                                            }
                                                            ?>" placeholder="Enter Upi"
                                                                   class="form-control <?php if (form_error('upi')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('upi')) {
                                                                    echo form_error('upi');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Bank Name</label>
                                                            <input type="text" name="bank_name" value="<?php
                                                            if (set_value('bank_name') && !isset($success)) {
                                                                echo set_value('bank_name');
                                                            }
                                                            ?>" placeholder="Enter Bank Name"
                                                                   class="form-control <?php if (form_error('bank_name')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('bank_name')) {
                                                                    echo form_error('bank_name');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Account Name</label>
                                                            <input type="text" name="account_name" value="<?php
                                                            if (set_value('account_name') && !isset($success)) {
                                                                echo set_value('account_name');
                                                            }
                                                            ?>" placeholder="Enter Account Name"
                                                                   class="form-control <?php if (form_error('account_name')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('account_name')) {
                                                                    echo form_error('account_name');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Account Number</label>
                                                            <input type="text" name="account_number" value="<?php
                                                            if (set_value('account_number') && !isset($success)) {
                                                                echo set_value('account_number');
                                                            }
                                                            ?>" placeholder="Enter Account Number"
                                                                   class="form-control <?php if (form_error('account_number')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('account_number')) {
                                                                    echo form_error('account_number');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">IFSC Code</label>
                                                            <input type="text" name="ifsc_code" value="<?php
                                                            if (set_value('ifsc_code') && !isset($success)) {
                                                                echo set_value('ifsc_code');
                                                            }
                                                            ?>" placeholder="Enter IFSC Code"
                                                                   class="form-control <?php if (form_error('ifsc_code')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('ifsc_code')) {
                                                                    echo form_error('ifsc_code');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Branch</label>
                                                            <input type="text" name="branch" value="<?php
                                                            if (set_value('branch') && !isset($success)) {
                                                                echo set_value('branch');
                                                            }
                                                            ?>" placeholder="Enter Branch"
                                                                   class="form-control <?php if (form_error('branch')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('branch')) {
                                                                    echo form_error('branch');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Select Photo <span
                                                                    style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span>
                                                            </label>
                                                            <div class="fileupload fileupload-new"
                                                                 data-provides="fileupload">
                                                                <div class="input-append">
                                                                    <input type="file" id="file"
                                                                           onchange="readURL(this, 'blah');"
                                                                           name="upi" class="" accept="image/*">
                                                                </div>
                                                            </div>
                                                            <img src="" class="mt-20 center-block" width="50"
                                                                 id="blah"/>
                                                            <p class="error-text file-error" style="display: none">
                                                                Select a valid file!</p>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Add UPI
                                                        </button>
                                                        <button
                                                            class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                            type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                            ================================================== -->
                        <?php
                        else:
                            if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table mb-none table-hover" id="upiTable">
                                                    <thead>
                                                    <tr>
                                                        <th>UPI</th>
                                                        <th>Bank Name</th>
                                                        <th>Account Name</th>
                                                        <th class="none">Account Number</th>
                                                        <th>IFSC Code</th>
                                                        <th class="none">Branch</th>
                                                        <th>Photo</th>
                                                        <?php
                                                        if ($permission['all'] || $permission['status']):
                                                            echo '<th>Action</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['edit']):
                                                            echo '<th>Edit</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['delete']):
                                                            echo '<th>Delete</th>';
                                                        endif;
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else:
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer  ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'upi'},
        {data: 'bank_name'},
        {data: 'account_name'},
        {data: 'account_number'},
        {data: 'ifsc_code'},
        {data: 'branch'},
        {data: 'path'},
        <?php
        if ($permission['all'] || $permission['status']):
            echo '{data: "action"},';
        endif;
        if ($permission['all'] || $permission['edit']):
            echo '{data: "edit"},';
        endif;
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'path', true);
</script>
</body>
<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit"):
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form method="post" enctype="multipart/form-data" name="edit">
                                                    <div class="panel-body row">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="control-label">Size</label>
                                                                <input type="text" name="size"
                                                                       value="<?php echo $updata[0]->size; ?>"
                                                                       placeholder="Enter Size Size"
                                                                       class="form-control <?php if (form_error('size')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('size')) {
                                                                        echo form_error('size');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Measurement</label> <Br/>
                                                                <div
                                                                    class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="time_format_2"
                                                                           value="gm" <?php echo ($updata[0]->measurement == 'gm') ? 'checked' : '' ?>
                                                                           name="measurement"
                                                                           checked
                                                                           class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                           for="time_format_2">GM</label>
                                                                </div>
                                                                <div
                                                                    class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="time_format_1"
                                                                           value="kg" <?php echo ($updata[0]->measurement == 'kg') ? 'checked' : '' ?>
                                                                           name="measurement"
                                                                           class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                           for="time_format_1">KG</label>
                                                                </div>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('measurement')) {
                                                                        echo form_error('measurement');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update Size
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>
                                                <!-- >> Add Form Start
                                                  ================================================== -->
                                                <form name="add" method="post" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="control-label">Size</label>
                                                                <input type="text" name="size"
                                                                       value="<?php
                                                                       if (set_value('size') && !isset($success)) {
                                                                           echo set_value('size');
                                                                       }
                                                                       ?>" placeholder="Enter Size"
                                                                       class="form-control <?php if (form_error('size')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('size')) {
                                                                        echo form_error('size');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Measurement</label> <Br/>
                                                                <div
                                                                    class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="time_format_2"
                                                                           value="gm"
                                                                           name="measurement"
                                                                           checked
                                                                           class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                           for="time_format_2">GM</label>
                                                                </div>
                                                                <div
                                                                    class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="time_format_1"
                                                                           value="kg"
                                                                           name="measurement"
                                                                           class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                           for="time_format_1">KG</label>
                                                                </div>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('measurement')) {
                                                                        echo form_error('measurement');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Add Size
                                                        </button>
                                                        <button
                                                            class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                            type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                   ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                            ================================================== -->
                        <?php
                        else:
                            if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="">
                                                <table class="table mb-none table-hover" id="sizeTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Size</th>
                                                        <th>Measurement</th>
                                                        <?php
                                                        if ($permission['all'] || $permission['edit']):
                                                            echo '<th>Edit</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['delete']):
                                                            echo '<th>Delete</th>';
                                                        endif;
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else:
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <?php echo $page_footer; ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'size'},
        {data: 'measurement'},
        <?php
        if ($permission['all'] || $permission['edit']):
            echo '{data: "edit"},';
        endif;
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column);
</script>
</body>
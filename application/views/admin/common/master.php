<?php
// Getting All field of tbl_control table and list it in a submenu
$control_data = $this->md->select_where('tbl_control', array('control_id' => 1));
$fields = $this->db->list_fields('tbl_control');
$mainmenu = $this->md->my_query('SELECT * FROM `tbl_mainmenu` WHERE `status` = 1 ORDER BY `position`')->result();
$web_data = ($web_data) ? $web_data[0] : '';
$accessGranted = true;
$admin_menu = [];
if ($admin_data):
    if ($admin_data[0]->admin_id != 1):
        $accessGranted = false;
        $admin_menu = $this->md->select_where('tbl_permission_assign', array('role_id' => $admin_data[0]->user_role_id, 'type' => 'page'));   // get current user's permisison   
    endif;
endif;
?>
<!doctype html>
<html lang="en"
      data-theme="<?php echo ($admin_data) ? ($admin_data[0]->theme ? $admin_data[0]->theme : 'theme-1') : 'theme-1' ?>">
<?php ob_start(); ?>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="keyword"
          content="<?php echo(($web_data) ? $web_data->meta_keyword : 'Admin Panel - ADMINISTRATION'); ?>">
    <meta name="description"
          content="<?php echo(($web_data) ? $web_data->meta_desc : 'Admin Panel - ADMINISTRATION'); ?>">
    <meta name="author" content="<?php echo DEVELOPER; ?>">
    <title><?php echo $page_title . " - " . (($web_data) ? $web_data->project_name : 'ADMINISTRATION'); ?></title>
    <base href="<?php echo base_url(); ?>">
    <!-- >> Favicon Icon Start
    ================================================== -->
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <!-- << Favicon Icon End
    ================================================== -->
    <!-- >> Style Sheet Start
    ================================================== -->
    <!-- Bootstrap v4.1.3 -->
    <link href="admin_asset/modules/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- Font Awesome v5.5.0 -->
    <link href="admin_asset/modules/fontawesome/css/all.min.css" rel="stylesheet"/>
    <!-- Summer note Editor -->
    <link href="admin_asset/css/summernote.min.css" rel="stylesheet"/>
    <!-- Responsive Multimedia Styles -->
    <link href="admin_asset/css/responsive.css" rel="stylesheet"/>
    <!-- Toast Message Styles -->
    <link href="admin_asset/modules/izitoast/css/iziToast.min.css" rel="stylesheet">
    <!-- Select2 Styles -->
    <link rel="stylesheet" href="admin_asset/modules/select2/dist/css/select2.min.css">
    <?php if ($datatables): ?>
        <!--Data tables-->
        <link rel="stylesheet" href="admin_asset/modules/datatables/datatables.min.css">
        <link rel="stylesheet"
              href="admin_asset/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="admin_asset/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
    <?php endif; ?>
    <!-- Template CSS -->
    <link rel="stylesheet" href="admin_asset/css/style.css">
    <link rel="stylesheet" href="admin_asset/css/custom.css">
    <link rel="stylesheet" href="admin_asset/css/components.css">
    <!-- << Style Sheet end
    ================================================== -->
</head>
<?php $page_head = ob_get_clean(); ?>
<?php ob_start(); ?>
<!-- >> BREADCRUMB START
   ================================================== -->
<div class="section-header">
    <h1><i class="<?php echo $breadcrumb_icon; ?>"></i><?php echo $page_title; ?></h1>
    <?php
    if ($page_title != 'Dashboard'):
        ?>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="<?php echo base_url('admin-dashboard'); ?>">Dashboard</a></div>
            <div class="breadcrumb-item"><?php echo $page_title; ?></div>
        </div>
    <?php
    endif;
    ?>
</div>
<!-- << BREADCRUMB END
   ================================================== -->
<?php $page_breadcrumb = ob_get_clean(); ?>
<?php ob_start(); ?>
<!-- >> HEADER START
   ================================================== -->
<nav class="navbar navbar-expand-lg main-navbar">
    <form class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <li><a href="javascript:void(0)" data-toggle="sidebar" class="nav-link nav-link-lg"><i
                        class="fas fa-bars"></i></a></li>
            <li><a href="javascript:void(0);" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i
                        class="fas fa-search"></i></a></li>
        </ul>
        <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
            <div class="search-backdrop"></div>
            <div class="search-result">
                <div class="search-header">
                    Search History
                </div>
                <div class="search-item">
                    <a href="<?php echo base_url('admin-dashboard'); ?>">Admin Dashboard</a>
                    <a href="javascript:void(0);" class="search-close"><i class="fas fa-times"></i></a>
                </div> <!-- Repeat this portion -->
                <div class="search-header">Projects</div>
                <div class="search-item">
                    <a href="<?php echo base_url('admin-profile'); ?>">
                        <div class="search-icon bg-success text-white mr-3">
                            <i class="fas fa-user"></i>
                        </div>
                        Admin Profile
                    </a>
                    <a href="<?php echo base_url('admin-feature-setting'); ?>">
                        <div class="search-icon bg-warning text-white mr-3">
                            <i class="fas fa-cog"></i>
                        </div>
                        Profile Setting
                    </a>
                </div>
                <div class="search-item">
                    <a href="<?php echo base_url('admin-logout'); ?>">
                        <div class="search-icon bg-danger text-white mr-3">
                            <i class="fas fa-power-off"></i>
                        </div>
                        Logout
                    </a>
                </div>
            </div>
        </div>
    </form>
    <ul class="navbar-nav navbar-right">
        <li class="dropdown">
            <a href="javascript:void(0)" data-toggle="dropdown"
               class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <img alt="<?php echo($admin_data[0]->admin_name ? $admin_data[0]->admin_name : 'Admin Profile'); ?>"
                     src="<?php echo base_url($admin_data[0]->path ? $admin_data[0]->path : FILENOTFOUND); ?>"
                     style="width: 40px;height: 40px;object-fit: cover"
                     class="rounded-circle mr-1">
                <div class="d-sm-none d-lg-inline-block text-FFF">
                    Hi, <?php echo($admin_data[0]->admin_name ? $admin_data[0]->admin_name : 'Admin'); ?>
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-title"><?php echo(($web_data) ? $web_data->project_name : 'Admin Panel'); ?></div>
                <a href="<?php echo base_url('admin-dashboard'); ?>" class="dropdown-item has-icon">
                    <i class="fas fa-fire"></i> Dashboard
                </a>
                <a href="<?php echo base_url('admin-profile'); ?>" class="dropdown-item has-icon">
                    <i class="fas fa-user"></i> Profile
                </a>
                <a href="<?php echo base_url('admin-feature-setting'); ?>" class="dropdown-item has-icon">
                    <i class="fas fa-cogs"></i> Settings
                </a>
                <div class="dropdown-divider"></div>
                <a href="<?php echo base_url('admin-logout'); ?>" class="dropdown-item font-11">Last Login
                    :<?php echo date(($web_data ? $web_data->date_format : 'd-M-Y') . " " . ($web_data ? $web_data->time_format : 'H') . ':i', strtotime($web_data ? $admin_data[0]->lastseen : '')); ?></a>
                <a href="<?php echo base_url('admin-logout'); ?>" class="dropdown-item has-icon text-danger">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
<!-- << HEADER END
   ================================================== -->
<?php $page_header = ob_get_clean(); ?>
<?php ob_start(); ?>
<!-- >> SIDEBAR START
   ================================================== -->
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="<?php echo base_url('admin-dashboard'); ?>"><?php echo($web_data ? $web_data->project_name : 'Admin Panel'); ?></a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?php echo base_url('admin-dashboard'); ?>"><?php echo substr(($web_data ? $web_data->project_name : 'Admin Panel'), 0, 2); ?></a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="<?php echo $active_page == "dashboard" ? 'active' : ''; ?>">
                <a class="nav-link"
                   href="<?php echo base_url('admin-dashboard'); ?>"><i
                        class="fas fa-fire"></i> <span>Dashboard</span></a>
            </li>
            <li class="<?php echo $active_page == "addOrder" ? 'active' : ''; ?>">
                <a class="nav-link bg-info text-FFF"
                   href="<?php echo base_url('add-order'); ?>"><i
                        class="fas fa-plus"></i> <span>POS</span></a>
            </li>
            <!--            <li class="menu-header">Site Pages</li>-->
            <?php
            $per_id = array();
            foreach ($admin_menu as $ext_per) {
                $per = json_decode($ext_per->permission_id);
                foreach ($per as $per_val):
                    $per_id[] = $per_val;
                endforeach;
            }
            $allowed_menu = array_unique($per_id);
            sort($allowed_menu);    // Sort Array
            if ($accessGranted):
                ?>
                <li class="nav-item dropdown <?php echo (in_array($active_page, $fields)) ? 'active' : ''; ?>">
                    <a href="javascript:void(0)" class="nav-link has-dropdown" data-toggle="dropdown"><i
                            class="fas fa-columns"></i>
                        <span>Site Pages</span></a>
                    <ul class="dropdown-menu">
                        <?php
                        foreach ($fields as $fields_val):
                            if ($fields_val != 'control_id' && $control_data[0]->$fields_val):
                                if ($fields_val != 'category' && $fields_val != "subcategory") {
                                    ?>
                                    <li class="<?php echo ($active_page == $fields_val) ? 'active' : ''; ?>">
                                        <a
                                            class="nav-link"
                                            href="<?php echo base_url('manage-' . strtolower($fields_val) . '/show'); ?>">
                                            <?php echo ucfirst(str_replace("_", " ", $fields_val)); ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            endif;
                        endforeach;
                        ?>
                    </ul>
                </li>
                <?php
                if (!empty($mainmenu)):
                    echo '<li class="menu-header">Starter</li>';
                    foreach ($mainmenu as $menu_item):
                        $submenu = $this->md->my_query('SELECT * FROM `tbl_submenu` WHERE `status` = 1 AND `mainmenu_id` =' . $menu_item->mainmenu_id . ' ORDER BY `position`')->result();
                        ?>
                    <li <?php echo(!empty($submenu) ? 'class="nav-item dropdown"' : ''); ?> >
                        <a href="<?php echo $menu_item->slug; ?>" <?php echo(!empty($submenu) ? 'class="nav-link has-dropdown" data-toggle="dropdown"' : 'class="nav-link"'); ?>><i
                                class="<?php echo $menu_item->icon; ?>"></i>
                            <span><?php echo ucfirst($menu_item->title); ?></span></a>
                        <?php
                        if (!empty($submenu)):
                            echo '<ul class="dropdown-menu">';
                            foreach ($submenu as $sub_item):
                                ?>
                                <li><a class="nav-link"
                                       href="<?php echo $sub_item->slug; ?>"><?php echo ucfirst($sub_item->title); ?></a>
                                </li>
                            <?php
                            endforeach;
                            echo '</ul>';
                        endif;
                        ?>
                        </li>
                    <?php
                    endforeach;
                endif;
            else:
                // Main Menu and Sub menu from DB
                if (!empty($mainmenu)):
                    echo '<li class="menu-header">Starter</li>';
                    foreach ($mainmenu as $menu_item):
                        $exist_prmsn = $this->md->select_where('tbl_permission', array('feature' => $menu_item->controller));
                        if ($exist_prmsn) {
                            if (in_array($exist_prmsn[0]->permission_id, $allowed_menu)) {
                                $submenu = $this->md->my_query('SELECT * FROM `tbl_submenu` WHERE `status` = 1 AND `mainmenu_id` =' . $menu_item->mainmenu_id . ' ORDER BY `position`')->result();
                                ?>
                            <li <?php echo(!empty($submenu) ? 'class="nav-item dropdown"' : ''); ?> >
                                <a href="<?php echo $menu_item->slug; ?>" <?php echo(!empty($submenu) ? 'class="nav-link has-dropdown" data-toggle="dropdown"' : 'class="nav-link"'); ?>><i
                                        class="<?php echo $menu_item->icon; ?>"></i>
                                    <span><?php echo ucfirst($menu_item->title); ?></span></a>
                                <?php
                                if (!empty($submenu)):
                                    echo '<ul class="dropdown-menu">';
                                    foreach ($submenu as $sub_item):
                                        ?>
                                        <li><a class="nav-link"
                                               href="<?php echo $sub_item->slug; ?>"><?php echo ucfirst($sub_item->title); ?></a>
                                        </li>
                                    <?php
                                    endforeach;
                                    echo '</ul>';
                                endif;
                                ?>
                                </li>
                                <?php
                            }
                        }
                    endforeach;
                endif;
                if ($admin_data[0]->user_role_id != 12 && $admin_data[0]->user_role_id != 13) {
                    if (!empty($allowed_menu)):
                        ?>
                        <li class="nav-item dropdown <?php echo (in_array($active_page, $fields)) ? 'active' : ''; ?>">
                            <a href="javascript:void(0)" class="nav-link has-dropdown" data-toggle="dropdown"><i
                                    class="fas fa-columns"></i>
                                <span>Site Pages</span></a>
                            <ul class="dropdown-menu">
                                <?php
                                foreach ($allowed_menu as $per_val):
                                    $exist_prmsn = $this->md->select_where('tbl_permission', array('permission_id' => $per_val));
                                    if ($exist_prmsn[0]->menu == 'site pages'):
                                        if ($exist_prmsn[0]->feature == 'category' || $exist_prmsn[0]->feature == 'subcategory') {
                                            ?>
                                            <!-- Hide Category & SubCategory from the site pages menu -->
                                            <?php
                                        } else {
                                            ?>
                                            <li><a class="nav-link"
                                                   href="<?php echo base_url('manage-' . strtolower($exist_prmsn ? $exist_prmsn[0]->feature : '') . '/show'); ?>"><?php echo ucfirst($exist_prmsn ? $exist_prmsn[0]->feature : ''); ?></a>
                                            </li>
                                            <?php
                                        }
                                    endif;
                                endforeach;
                                ?>
                            </ul>
                        </li>
                    <?php
                    endif;
                }
            endif;
            ?>
            <li class="menu-header">Settings</li>
            <li class="<?php echo $active_page == "profile" ? 'active' : ''; ?>"><a class="nav-link"
                                                                                    href="<?php echo base_url('admin-profile'); ?>"><i
                        class="fas fa-user"></i> <span>Profile</span></a></li>
            <li class="<?php echo $active_page == "feature_setting" ? 'active' : ''; ?>"><a class="nav-link"
                                                                                            href="<?php echo base_url('admin-feature-setting'); ?>"><i
                        class="fas fa-pencil-ruler"></i> <span>Settings</span></a></li>
            <?php
            if ($accessGranted):
                ?>
                <li class="<?php echo $active_page == "control_panel" ? 'active' : ''; ?>"><a class="nav-link"
                                                                                              href="<?php echo base_url('control-panel'); ?>"><i
                            class="fas fa-cogs"></i> <span>Control Panel</span></a></li>
            <?php
            endif;
            ?>
        </ul>
        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            <a href="<?php echo base_url('admin-logout'); ?>" class="btn btn-danger btn-lg btn-block btn-icon-split">
                <i class="fas fa-sign-out-alt"></i> Logout
            </a>
        </div>
    </aside>
</div>
<!-- << SIDEBAR END
   ================================================== -->
<?php $page_sidebar = ob_get_clean(); ?>
<?php ob_start(); ?>
<!-- >> FOOTER START
   ================================================== -->
<footer class="main-footer">
    <div class="footer-left">
        All rights reserved
        ©<?php echo($web_data ? $web_data->project_name : 'Admin Panel'); ?> <?php echo date('Y'); ?>
    </div>
    <div class="footer-right">
        2.3.0
    </div>
</footer>
<!-- << FOOTER END
   ================================================== -->
<?php $page_footer = ob_get_clean(); ?>
<?php ob_start(); ?>
<!-- >> FOOTER SCRIPT START
   ================================================== -->
<!-- General JS Scripts -->
<script src="admin_asset/modules/jquery.min.js"></script>
<script src="admin_asset/modules/popper.js"></script>
<script src="admin_asset/modules/tooltip.js"></script>
<script src="admin_asset/modules/bootstrap/js/bootstrap.min.js"></script>
<script src="admin_asset/modules/nicescroll/jquery.nicescroll.min.js"></script>
<script src="admin_asset/modules/moment.min.js"></script>
<script src="admin_asset/js/stisla.js"></script>
<script src="admin_asset/js/summernote.min.js"></script>
<!-- Select2 Script -->
<script src="admin_asset/modules/select2/dist/js/select2.full.min.js"></script>
<!-- Enable Advanced Form Script -->
<script src="admin_asset/js/page/forms-advanced-forms.js"></script>
<!-- Toast Message JS Libraies -->
<script src="admin_asset/modules/izitoast/js/iziToast.min.js"></script>
<script src="admin_asset/js/page/modules-toastr.js"></script>
<!-- JS Libraies -->
<?php if ($datatables): ?>
    <!-- Datatable JS Libraies -->
    <script src="admin_asset/modules/datatables/datatables.min.js"></script>
    <script src="admin_asset/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="admin_asset/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
    <script src="admin_asset/modules/jquery-ui/jquery-ui.min.js"></script>
    <!-- Page Specific JS File -->
    <script src="admin_asset/js/page/modules-datatables.js"></script>
<?php endif; ?>
<!-- SweetAlert JS Libraies -->
<script src="admin_asset/modules/sweetalert/sweetalert.min.js"></script>
<script src="admin_asset/js/page/modules-sweetalert.js"></script>
<!-- Template JS File -->
<script src="admin_asset/js/scripts.js"></script>
<script src="admin_asset/js/custom.js"></script>
<script src="admin_asset/js/theme.js"></script>
<script>
    function showFieldStatusWise(val) {
        (val !== 'Inquired') ? ($('#billInfo').show()) : ($('#billInfo').hide());
        (val === 'On Hold' ? $('#cancelOrderBtn').show() : $('#cancelOrderBtn').hide());
        (val === 'Dispatch') ? ($('#shippedFileUpload').show()) : ($('#shippedFileUpload').hide());
        (val === 'Dispatch') ? ($('#no_of_bundles').show()) : ($('#no_of_bundles').hide());
        (val === 'Dispatch') ? ($('#lr_no').show()) : ($('#lr_no').hide());
        (val === 'Dispatch') ? ($('#date_of_lr').show()) : ($('#date_of_lr').hide());
        (val === 'Estimate') ? $('#billInfo input').removeAttr("readonly", "readonly") : $('#billInfo input').attr("readonly", "readonly");
    }

    /*--------------------
     GET SUB CATEGORY FROM MAIN CATEGORY
     ---------------------*/
    $(document).on("change", "#parentCategory", function () {
        let cateId = $(this).val(); // get main category id
        jQuery('#sub_category').html('<option>Loading...</option>');
        var data = {
            val: cateId
        };
        var url = "<?php echo base_url('Admin/Pages/get_category'); ?>";
        jQuery.post(url, data, function (data) {
            jQuery('#sub_category').html(data);
        });
    });
    /*--------------------
    GET PETA CATEGORY FROM SUB CATEGORY
    ---------------------*/
    $(document).on("change", "#sub_category", function () {
        let cateId = $(this).val(); // get main category id
        jQuery('#peta_category').html('<option>Loading...</option>');
        var data = {
            val: cateId
        };
        var url = "<?php echo base_url('Admin/Pages/get_petacategory'); ?>";
        jQuery.post(url, data, function (data) {
            jQuery('#peta_category').html(data);
        });
    });
    /*--------------------
       GET Main & Sub CATEGORY FROM peta CATEGORY
       ---------------------*/
    $(document).on("change", "#peta_category", function () {
        let cateId = $(this).val(); // get peta category id
        jQuery('#categoryData').html('<option>Loading...</option>');
        var data = {
            val: cateId
        };
        var url = "<?php echo base_url('Admin/Pages/get_category_data'); ?>";
        jQuery.post(url, data, function (data) {
            jQuery('#categoryData').html(data);
            $('#categoryData select').select2();
        });
    });
    /*--------------------
       CALCULATE BOX STICK FT RATE & AMOUNT
       ---------------------*/
    $(document).on("change", "#neft_rtgs", function () {
        let totalAmount = parseFloat($('.totalAmount').val());
        let neft_rtgs = parseFloat($(this).val());

        if (totalAmount > neft_rtgs) {
            $('#calCash').val((totalAmount - neft_rtgs).toFixed(2));
        }
    });

    $(document).on("change", "#calCash", function () {
        let totalAmount = parseFloat($('.totalAmount').val());
        let calCash = parseFloat($(this).val());

        if (totalAmount > calCash) {
            $('#neft_rtgs').val((totalAmount - calCash).toFixed(2));
        }
    });

    $(document).on("change", "#gstStatus", function () {
        let gstStatus = $(this).val();
        if (gstStatus === "Percentage") {
            $('.gstDefaultAmount').hide();
            $('.gstPercentage').show();
        } else {
            $('.gstDefaultAmount').show();
            $('.gstPercentage').hide();
        }
    });

    $(document).on("change", "#calBox,#calRate,.cartageDefaultAmount,.gstDefaultAmount,#gstStatus", function () {
        let calBox = parseFloat($(this).parentsUntil('.calculation').find('#calBox').val());
        let calStick = parseFloat($(this).parentsUntil('.calculation').find('#calStick').val());
        let calFt = parseFloat($(this).parentsUntil('.calculation').find('#calFt').val());
        let calRate = parseFloat($(this).parentsUntil('.calculation').find('#calRate').val());
        let cartageDefaultAmount = parseFloat($('.cartageDefaultAmount').val());
        let gstDefaultAmount = parseFloat($('.gstDefaultAmount').val());
        let gstStatus = ($('#gstStatus').val());

        let calAmount = $(this).parentsUntil('.calculation').find('#calAmount');

        calAmount.val(calBox * calStick * calFt * calRate);

        // Calculate total of each item
        let totalBox = totalStick = totalFt = totalRate = totalAmount = 0;
        $('.customTable #calBox').each(function () {
            totalBox += Number($(this).val());
        });
        $('.customTable #calStick').each(function () {
            totalStick += Number($(this).val());
        });
        $('.customTable #calFt').each(function () {
            totalFt += Number($(this).val());
        });
        $('.customTable #calRate').each(function () {
            totalRate += Number($(this).val());
        });
        $('.customTable #calAmount').each(function () {
            totalAmount += Number($(this).val());
        });

        let cartage = totalBox * cartageDefaultAmount;
        var gst = 0;
        if (gstStatus === "Percentage") {
            gst = (totalAmount * 18) / 100;
        } else {
            gst = (totalBox * gstDefaultAmount);
        }
        let total = totalAmount + gst + (totalBox * cartageDefaultAmount);

        $('.totalBox').html(totalBox);
        $('#totalStick').html(totalStick);
        $('#totalFt').html(totalFt);
        $('#totalRate').html(totalRate);
        //$('#totalAmount').html(totalAmount);

        $('.cartageAmount').val(cartage.toFixed(2));
        $('.gstAmount').val(gst.toFixed(2));
        $('.totalAmount').val(total.toFixed(2));

    });

</script>
<!-- >> FOOTER SCRIPT END
   ================================================== -->
<?php $page_footerscript = ob_get_clean(); ?>
<?php
$this->load->view($page, array(
    'page_title' => $page_title,
    'page_head' => $page_head,
    'page_breadcrumb' => ($breadcrumb) ? $page_breadcrumb : '',
    'page_header' => ($header) ? $page_header : '',
    'page_sidebar' => ($sidebar) ? $page_sidebar : '',
    'page_footer' => $page_footer,
    'page_footerscript' => $page_footerscript
));
?>
<style>
    .customTable input[readonly] {
        background-color: transparent;
        opacity: 1;
        border: none;
    }
</style>
</html>
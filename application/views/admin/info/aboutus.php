<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>  
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header  ?>  
            <?php echo $page_sidebar; //  Load Sidebar  ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb ?> 
                    <div class="section-body">
                        <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                        <div class="row">
                            <?php
                            if ($page_type == "add" || $page_type == "edit"):
                                ?> 
                                <!-- >> ADD/EDIT Data Start
                                ================================================== -->
                                <div class="col-12 col-md-12 col-lg-12"> 
                                    <div class="card">   
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form method="post" name="edit"> 
                                                    <div class="card-body">
                                                        <div class="form-group mb-15">
                                                            <label class="control-label">Enter About us info in your format</label> 
                                                            <textarea class="summernote" name="about"><?php echo $updata[0]->about; ?></textarea> 
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('about')) {
                                                                    echo form_error('about');
                                                                }
                                                                ?>
                                                            </div> 
                                                        </div>   
                                                    </div>
                                                    <footer class="card-footer">
                                                        <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="update">Update About us</button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" >Cancel</a> 
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                                <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>  
                                                <!-- >> Add Form Start
                                                ================================================== -->
                                                <form method="post" name="add"> 
                                                    <div class="card-body">
                                                        <div class="form-group mb-15">
                                                            <label class="control-label">Enter About us info in your format</label> 
                                                            <textarea name="about" class="summernote"></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('about')) {
                                                                    echo form_error('about');
                                                                }
                                                                ?>
                                                            </div> 
                                                        </div>   
                                                    </div>
                                                    <footer class="card-footer">
                                                        <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="add">Add About us</button> 
                                                        <button class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" type="reset">Reset Form</button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                                <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?> 
                                    </div>
                                </div>
                                <!-- << ADD/EDIT Data END
                                ================================================== -->
                                <?php
                            else:
                                if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                    ?> 
                                    <!-- >> Table Data Start
                                    ================================================== -->
                                    <div class="col-12 col-md-12 col-lg-12"> 
                                        <div class="card">
                                            <div class="card-body"> 
                                                <div class="">
                                                    <table class="table mb-none table-hover" id="aboutusTable">
                                                        <thead>
                                                            <tr> 
                                                                <th>Title</th> 
                                                                <?php
                                                                if ($permission['all'] || $permission['edit']):
                                                                    echo '<th>Edit</th>';
                                                                endif;
                                                                if ($permission['all'] || $permission['delete']):
                                                                    echo '<th>Delete</th>';
                                                                endif;
                                                                ?>   
                                                            </tr>
                                                        </thead> 
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <!-- << Table Data End
                                    ================================================== -->
                                    <?php
                                else:
                                    $this->load->view('admin/common/access_denied');
                                endif;
                            endif;
                            ?>
                        </div>
                    </div>
                </section>
            </div> 
            <!-- << Main Content End
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer   ?> 
        </div>
    </div> 
    <?php
    $alert_data['success'] = $success;
    $alert_data['error'] = $error;
    $this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
    ?> 
    <script>
        // List column which will be display in the table
        const column = [
            {data: 'about'},
<?php
if ($permission['all'] || $permission['edit']):
    echo '{data: "edit"},';
endif;
if ($permission['all'] || $permission['delete']):
    echo '{data: "delete"},';
endif;
?>
        ];
        // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE] 
        getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column);
    </script>
</body> 
<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$submenu = count($submenu);   // Count data of submenu table  
$fields = $this->md->select('tbl_mainmenu');    // get all category
?>
<body>
<link rel="stylesheet" href="admin_asset/modules/bootstrap-iconpicker/css/bootstrap-iconpicker.min.css">
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar     ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb    ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)     ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit"):
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form id="frmEdit" name="update" method="post">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Parent Menu</label>
                                                            <select name="parent"
                                                                    class="form-control select2 text-capitalize <?php if (form_error('parent')) { ?> is-invalid <?php } ?>">
                                                                <option value="">Select Parent Menu</option>
                                                                <?php
                                                                if (!empty($fields)):
                                                                    foreach ($fields as $fields_val):
                                                                        ?>
                                                                        <option <?php echo ($updata[0]->mainmenu_id == $fields_val->mainmenu_id) ? 'selected' : ''; ?>
                                                                            value="<?php echo $fields_val->mainmenu_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                    <?php
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                            </select>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('parent')) {
                                                                    echo form_error('parent');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label"><code>*</code>Title</label>
                                                            <input type="text" name="title"
                                                                   value="<?php echo $updata[0]->title; ?>"
                                                                   placeholder="Enter Menu Title"
                                                                   class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('title')) {
                                                                    echo form_error('title');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label
                                                                class="control-label"><code>*</code>Controller</label>
                                                            <input type="text" name="controller"
                                                                   value="<?php echo $updata[0]->controller; ?>"
                                                                   placeholder="Enter Menu Controller"
                                                                   class="form-control <?php if (form_error('controller')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('controller')) {
                                                                    echo form_error('controller');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label"><code>*</code>Slug</label>
                                                            <input type="text" name="slug"
                                                                   value="<?php echo $updata[0]->slug; ?>"
                                                                   placeholder="Enter Menu Slug"
                                                                   class="form-control <?php if (form_error('slug')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('slug')) {
                                                                    echo form_error('slug');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label"><code>*</code>Page URL</label>
                                                            <input type="text" name="url"
                                                                   value="<?php echo $updata[0]->url; ?>"
                                                                   placeholder="Enter Page URL"
                                                                   class="form-control <?php if (form_error('url')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('url')) {
                                                                    echo form_error('url');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label class="control-label">Position</label>
                                                            <select name="position"
                                                                    class="form-control select2 text-capitalize <?php if (form_error('position')) { ?> is-invalid <?php } ?>">
                                                                <?php
                                                                if ($submenu != 0):
                                                                    for ($i = ($submenu + 1); $i >= 1; $i--):
                                                                        echo '<option ' . (($i == $updata[0]->position) ? 'selected' : '') . '  value="' . $i . '">' . $i . '</option>';
                                                                    endfor;
                                                                else:
                                                                    echo '<option value="1">1</option>';
                                                                endif;
                                                                ?>
                                                            </select>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('position')) {
                                                                    echo form_error('position');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label class="control-label">Page Icon</label> <br/>
                                                            <button data-icon="<?php echo $updata[0]->icon; ?>"
                                                                    type="button" id="myEditor_icon"
                                                                    class="btn btn-outline-primary <?php if (form_error('icon')) { ?> is-invalid <?php } ?>">
                                                                Select Icon
                                                            </button>
                                                            <input type="hidden" value="<?php echo $updata[0]->icon; ?>"
                                                                   name="icon" class="item-menu">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('icon')) {
                                                                    echo form_error('icon');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Page Status</label> <Br/>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="active_page" value="1"
                                                                       name="status" <?php echo $updata[0]->status ? 'checked' : ''; ?>
                                                                       class="custom-control-input">
                                                                <label class="custom-control-label" for="active_page">Active</label>
                                                            </div>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="inactive_page" value="0"
                                                                       name="status" <?php echo $updata[0]->status ? '' : 'checked'; ?>
                                                                       class="custom-control-input">
                                                                <label class="custom-control-label" for="inactive_page">Inactive</label>
                                                            </div>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('status')) {
                                                                    echo form_error('status');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update Submenu
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>
                                                <!-- >> Add Form Start
                                                 ================================================== -->
                                                <form id="frmEdit" name="add" method="post">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label"><code>*</code>Parent
                                                                Menu</label>
                                                            <select name="parent" id="setSubmenuPosition"
                                                                    class="form-control select2 text-capitalize <?php if (form_error('parent')) { ?> is-invalid <?php } ?>">
                                                                <option value="">Select Parent Menu</option>
                                                                <?php
                                                                if (!empty($fields)):
                                                                    foreach ($fields as $fields_val):
                                                                        ?>
                                                                        <option
                                                                            value="<?php echo $fields_val->mainmenu_id; ?>"><?php echo ucfirst($fields_val->title); ?></option>
                                                                    <?php
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                            </select>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('parent')) {
                                                                    echo form_error('parent');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label"><code>*</code>Title</label>
                                                            <input type="text" name="title" value="<?php
                                                            if (set_value('title') && !isset($success)) {
                                                                echo set_value('title');
                                                            }
                                                            ?>" placeholder="Enter Menu Title"
                                                                   class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('title')) {
                                                                    echo form_error('title');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label
                                                                class="control-label"><code>*</code>Controller</label>
                                                            <input type="text" name="controller" value="<?php
                                                            if (set_value('controller') && !isset($success)) {
                                                                echo set_value('controller');
                                                            }
                                                            ?>" placeholder="Enter Menu Controller"
                                                                   class="form-control <?php if (form_error('controller')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('controller')) {
                                                                    echo form_error('controller');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label"><code>*</code>Slug</label>
                                                            <input type="text" name="slug" value="<?php
                                                            if (set_value('slug') && !isset($success)) {
                                                                echo set_value('slug');
                                                            }
                                                            ?>" placeholder="Enter Menu Slug"
                                                                   class="form-control <?php if (form_error('slug')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('slug')) {
                                                                    echo form_error('slug');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label"><code>*</code>Page URL</label>
                                                            <input type="text" name="url" value="<?php
                                                            if (set_value('url') && !isset($success)) {
                                                                echo set_value('url');
                                                            }
                                                            ?>" placeholder="Enter Page URL"
                                                                   class="form-control <?php if (form_error('url')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('url')) {
                                                                    echo form_error('url');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label class="control-label">Position</label>
                                                            <select name="position"
                                                                    class="form-control select2 text-capitalize <?php if (form_error('position')) { ?> is-invalid <?php } ?>">
                                                                <?php
                                                                if ($submenu != 0):
                                                                    for ($i = ($submenu + 1); $i >= 1; $i--):
                                                                        echo '<option value="' . $i . '">' . $i . '</option>';
                                                                    endfor;
                                                                else:
                                                                    echo '<option value="1">1</option>';
                                                                endif;
                                                                ?>
                                                            </select>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('position')) {
                                                                    echo form_error('position');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label class="control-label">Page Icon</label> <br/>
                                                            <button type="button" id="myEditor_icon"
                                                                    class="btn btn-outline-primary <?php if (form_error('icon')) { ?> is-invalid <?php } ?>">
                                                                Select Icon
                                                            </button>
                                                            <input type="hidden" name="icon" class="item-menu">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('icon')) {
                                                                    echo form_error('icon');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Page Status</label> <Br/>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="active_page" value="1"
                                                                       name="status" checked=""
                                                                       class="custom-control-input">
                                                                <label class="custom-control-label" for="active_page">Active</label>
                                                            </div>
                                                            <div
                                                                class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="inactive_page" value="0"
                                                                       name="status" class="custom-control-input">
                                                                <label class="custom-control-label" for="inactive_page">Inactive</label>
                                                            </div>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('status')) {
                                                                    echo form_error('status');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Add Submenu
                                                        </button>
                                                        <button
                                                            class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                            type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                           ================================================== -->
                        <?php
                        else:
                            if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="">
                                                <table class="table mb-none table-hover" id="submenuTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Status</th>
                                                        <th>Parent</th>
                                                        <th>Title</th>
                                                        <th>Slug</th>
                                                        <th>Position</th>
                                                        <th>Icon</th>
                                                        <th>URL</th>
                                                        <?php
                                                        if ($permission['all'] || $permission['edit']):
                                                            echo '<th>Edit</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['delete']):
                                                            echo '<th>Delete</th>';
                                                        endif;
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else:
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer     ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
<script src='admin_asset/js/jquery-menu-editor.js'></script>
<script type="text/javascript"
        src="admin_asset/modules/bootstrap-iconpicker/js/iconset/fontawesome5-3-1.min.js"></script>
<script type="text/javascript" src="admin_asset/modules/bootstrap-iconpicker/js/bootstrap-iconpicker.min.js"></script>
<script>
    jQuery(document).ready(function () {
        /* =============== DEMO =============== */
        // icon picker options
        var iconPickerOptions = {searchText: "User...", labelHeader: "{0}/{1}"};
        var editor = new MenuEditor('myEditor', {iconPicker: iconPickerOptions});
        editor.setForm($('#frmEdit'));
        /* ====================================== */
    });
</script>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'status'},
        {data: 'parent'},
        {data: 'title'},
        {data: 'slug'},
        {data: 'position'},
        {data: 'icon'},
        {data: 'url'},
        <?php
        if ($permission['all'] || $permission['edit']):
            echo '{data: "edit"},';
        endif;
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column);
</script>
</body>
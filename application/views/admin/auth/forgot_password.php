<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$web_data = ($web_data) ? $web_data[0] : '';
?> 
<body style="background-image:url('https://picsum.photos/1920/1080')!important;background-size: cover">
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                        <div class="login-brand">
                            <?php
                            if ($web_data->logo):
                                echo '<img src = "' . base_url($web_data->logo) . '" class="shadow-light" alt="' . $website_title . '" title = "' . $website_title . '" style="width: 200px;padding: 10px;">';
                            else:
                                echo "<h1 class='text-FFF font-30'>" . ($web_data ? $web_data->project_name : 'ADMINISTRATION') . "</h1>";
                            endif;
                            ?>  
                        </div>
                        <div class="card card-primary">
                            <div class="card-header"><h4>Recover Password</h4></div> 
                            <div class="card-body"> 
                                <form method="POST" class="needs-validation" novalidate="">
                                    <div class="form-group mb-15">
                                        <label for="email">Email</label>
                                        <input id="email" type="email" class="form-control <?php echo (form_error('username')) ? 'error-field' : ''; ?>" name="username" tabindex="1" autofocus value="<?php
                                        if (set_value('username') && !isset($success)) {
                                            echo set_value('username');
                                        }
                                        ?>"> 
                                        <div class="error-text">
                                            <?php
                                            if (form_error('username')) {
                                                echo form_error('username');
                                            }
                                            ?>
                                        </div> 
                                    </div>  
                                    <div class="form-group"> 
                                        <button type="submit" name="send" value="send" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                            Recover Now <i class="fa fa-chevron-right font-10"></i>
                                        </button>
                                        <a href="<?php echo base_url('authorize') ?>" class="text-small mt-15 display-block">
                                            Back to Login
                                        </a>
                                    </div>
                                </form> 
                            </div>
                        </div> 
                        <div class="simple-footer font-12">
                            ©<?php echo ($web_data ? $web_data->project_name : 'Admin Panel'); ?> Developed by <a href="<?php echo $web_link; ?>" target="_new"><?php echo $developer; ?></a> <?php echo date('Y'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div> 
    <?php
    $alert_data['success'] = $success;
    $alert_data['error'] = $error; 
    $this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
    ?>
</body> 

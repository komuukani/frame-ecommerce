<?php
echo $page_head;  //  Load Head Link and Scripts    
$control_data = $this->md->select_where('tbl_control', array('control_id' => 1));
$fields = $this->db->list_fields('tbl_control');
$superAdmin = true;
if ($admin_data):
    if ($admin_data[0]->admin_id != 1):
        $superAdmin = false;
        $admin_menu = $this->md->select_where('tbl_permission_assign', array('role_id' => $admin_data[0]->user_role_id, 'type' => 'page'));   // get current user's permisison   
    endif;
endif;
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar    ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb   ?>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?php echo base_url('manage-bill/show'); ?>">
                            <div class="card card-statistic-1">
                                <div class="card-icon bg-primary">
                                    <i class="fas fa-columns"></i>
                                </div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4 class="text-000">
                                            Total Purchases
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        echo count($this->md->select("tbl_bill"));
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?php echo base_url('manage-bill/show/Inquired'); ?>">
                            <div class="card card-statistic-1">
                                <div class="card-icon bg-primary">
                                    <i class="fas fa-columns"></i>
                                </div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4 class="text-000">
                                            Inquired Orders
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        echo count($this->md->select_where("tbl_bill", array("status" => "Inquired", "cancelStatus" => 0)));
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?php echo base_url('manage-bill/show/Estimate'); ?>">
                            <div class="card card-statistic-1">
                                <div class="card-icon bg-primary">
                                    <i class="fas fa-columns"></i>
                                </div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4 class="text-000">
                                            Estimate Orders
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        echo count($this->md->select_where("tbl_bill", array("status" => "Estimate", "cancelStatus" => 0)));
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?php echo base_url('manage-bill/show/OnHold'); ?>">
                            <div class="card card-statistic-1">
                                <div class="card-icon bg-primary">
                                    <i class="fas fa-columns"></i>
                                </div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4 class="text-000">
                                            On Hold Orders
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        echo count($this->md->select_where("tbl_bill", array("status" => "On Hold", "cancelStatus" => 0)));
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?php echo base_url('manage-bill/show/Dispatch'); ?>">
                            <div class="card card-statistic-1">
                                <div class="card-icon bg-primary">
                                    <i class="fas fa-columns"></i>
                                </div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4 class="text-000">
                                            Ready to Dispatch Orders
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        echo count($this->md->select_where("tbl_bill", array("status" => "Dispatch", "cancelStatus" => 0)));
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?php echo base_url('manage-bill/show/Dispatched'); ?>">
                            <div class="card card-statistic-1">
                                <div class="card-icon bg-primary">
                                    <i class="fas fa-columns"></i>
                                </div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4 class="text-000">
                                            Dispatched Orders
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        echo count($this->md->select_where("tbl_bill", array("status" => "Dispatched", "cancelStatus" => 0)));
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?php echo base_url('manage-cancelled/show'); ?>">
                            <div class="card card-statistic-1">
                                <div class="card-icon bg-primary">
                                    <i class="fas fa-columns"></i>
                                </div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4 class="text-000">
                                            Cancelled Orders
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        echo count($this->md->select_where("tbl_bill", array("cancelStatus" => 1)));
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer    ?>
    </div>
</div>
<?php echo $page_footerscript; // Load Footer script  ?>
</body> 
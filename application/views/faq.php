<?php
echo $page_head;
?>
<body class="home">
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main mt-100 mt-xs-50">
        <section class="pt-50 pb-50">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xs-8">
                        <div class="text-left">
                            <h2 class="font-58 font-weight-bold">FAQ</h2>
                        </div>
                        <div class="panel-group">
                            <?php
                            if (empty($faq)) :
                                echo "Sorry, content not available";
                            else :
                                foreach ($faq as $key => $faq_data) {
                                    ?>
                                    <div>
                                        <div>
                                            <h4 style="font-size: 16.8px"
                                                class="font-weight-bold"><?php echo ($key + 1) . ". " . ucfirst($faq_data->question); ?></h4>
                                        </div>
                                        <div
                                            class="panel-collapse">
                                            <div class="panel-body">
                                                <p style="font-size: 16.8px"><?php echo $faq_data->answer; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>
<?php
echo $page_head;
?>
<body class="home">
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main mt-100 mt-xs-50">
        <section class="pt-50 pb-50">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="text-left">
                            <h2 class="font-58 font-weight-bold">Return & Refund Policy</h2>
                        </div>
                        <div>
                            <p style="font-size: 16.8px">
                                1. infiniti is keen to secure customer satisfaction in all the stages of the customer
                                journey, including after purchase.
                                <br/><br/>
                                2. All purchases are quality controlled carefully before and during delivery to ensure
                                your order reaches you in a perfect condition.
                                <br/><br/>
                                3. Our operational error rate does not exceed 1% based on a systematic ecosystem tha
                                does not have a room for guesses and/or unexpected actions.
                                <br/><br/>
                                4. If you receive your order with a factory and/or delivery fault, you have to inform us
                                immediately, during the time of the delivery, not later.
                                <br/><br/>
                                5. We take full responsibility to return any unexpected faulty product, if you reported
                                it during the timeframe for reporting an item.
                                <br/><br/>
                                6. In the unexpected event of making a delivery for a wrong and/or faulty product, we
                                take prompt measures to replace it within the shortest time.
                                <br/><br/>
                                7. You do not pay any extra fees, in the case of returning a faulty order and
                                re-shipping the right order.
                                <br/><br/>
                                8. We reserve our right not to accept a customer dispute, if not reported during the
                                reporting timeframe (which is during the time of delivery).
                                <br/><br/>
                                9. We accept cancelling orders and/or refunding them, under one condition only, which
                                happens in the event of an unexpected delay of more than 3 days in the scheduled
                                delivery.
                                <br/><br/>
                                10. You take full responsibility to correctly check the dimensions of your order and be
                                sure it fits your space.
                                <br/><br/>
                                11. You realize that sometimes a fabric color in a picture, may, and may not, look
                                slightly different in reality, which is normally caused by the type of lightning in the
                                surrounding, whereas similar cases can't be deemed for return.
                                <br/><br/>
                                12. We trust our products to live years in perfect condition, but you can still
                                understand that we do not stand responsible for damages that are caused by natural wear
                                and tear of heavy usage.
                                <br/><br/>
                                13. You have to carefully read and view our product specs to ensure complete awareness
                                and understanding, as we do not accept returns and cancelations if you made an order by
                                mistake.
                                <br/><br/>
                                14. Returns are accepted as per condition no. 4 stated above only.
                                <br/><br/>
                                15. Refunds as per condition No. 9 mentioned above are to be made onto the original mode
                                of payment and will be processed within 10 to 45 days depends on the issuing bank.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>
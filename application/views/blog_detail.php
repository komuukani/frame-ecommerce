<?php
echo $page_head;
$web_data = ($web_data) ? $web_data[0] : '';
$blogs = $this->md->select_limit_order('tbl_blog', 5, 'blog_id', 'desc');
?>
<body>
<?php
echo $page_head;
?>
<body>
<?php echo $page_header; ?>

<main id="content" class="wrapper layout-page">
    <?php echo $page_breadcumb; ?>

    <section class="pt-10 pb-16 pb-lg-18">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="position-sticky top-0">
                        <aside class="primary-sidebar mt-12 mt-lg-0 bg-F9F9F9 pe-xl-9 me-xl-2 border p-5">
                            <div class="widget widget-post">
                                <h4 class="widget-title fs-5 mb-6">Recent posts</h4>
                                <ul class="list-unstyled mb-0 row gy-7 gx-0">
                                    <?php
                                    if (!empty($blogs)) {
                                        foreach ($blogs as $blog) {
                                            $url = base_url('blog/' . strtolower($blog->slug));
                                            ?>
                                            <li class="col-12">
                                                <div class="card border-0 flex-row">
                                                    <figure class="flex-shrink-0 mb-0 me-7">
                                                        <a href="<?php echo $url; ?>" class="d-block"
                                                           title="<?php echo $blog->title; ?>">
                                                            <img data-src="<?php echo base_url($blog->path); ?>"
                                                                 class="img-fluid lazy-image"
                                                                 alt="<?php echo $blog->title; ?>" width="60"
                                                                 height="80"
                                                                 src="<?php echo base_url($blog->path); ?>">
                                                        </a>
                                                    </figure>
                                                    <div class="card-body p-0">
                                                        <h5 class="card-text fw-semibold ls-1 text-uppercase fs-13px mb-3 text-body text-primary-hover">
                                                            <a class="text-decoration-none text-reset"
                                                               href="<?php echo $url; ?>"
                                                               title="Skin care"><?php echo date('d-M-Y', strtotime($blog->blogdate)); ?></a>
                                                        </h5>
                                                        <h4 class="card-title mb-0 text-body-emphasis fs-15px lh-base text-primary-hover">
                                                            <a class="text-decoration-none text-reset"
                                                               href="<?php echo $url; ?>"
                                                               title="<?php echo $blog->title; ?>"><?php echo $blog->title; ?></a>
                                                        </h4>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                            <div class="widget widget-instagram">
                                <div class="d-flex align-items-baseline justify-content-between">
                                    <h4 class="widget-title fs-5 mb-6">Share Post</h4>
                                </div>
                                <div class="sharethis-inline-share-buttons"></div>
                            </div>
                        </aside>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class=" text-center mb-13">
                        <h2 class=" px-6 text-body-emphasis border-0 fw-500 mb-4 fs-3 ">
                            <?php echo $blog_data ? $blog_data[0]->title : ''; ?>
                        </h2>
                        <ul class="list-inline fs-15px fw-semibold letter-spacing-01 d-flex justify-content-center align-items-center">
                            <li>
                                <i class="fa fa-user"></i>
                            </li>
                            <li class="border-end px-6 text-body-emphasis border-0 text-body">
                                By
                                <a href="<?php echo base_url('blog'); ?>">Admin</a>
                            </li>
                            <li class="list-inline-item px-6"><?php echo date('d-M-Y', strtotime($blog_data ? $blog_data[0]->blogdate : '')); ?></li>
                        </ul>
                    </div>
                    <div class="post-content">
                        <img src="<?php echo base_url($blog_data ? $blog_data[0]->path : FILENOTFOUND); ?>"
                             alt="<?php echo $blog_data ? $blog_data[0]->title : ''; ?>"
                             title="<?php echo $blog_data ? $blog_data[0]->title : ''; ?>"
                             style="width: 100%;"
                             class="mb-12 img-fluid"/>

                        <div class="mb-6 description">
                            <?php echo $blog_data ? $blog_data[0]->description : ''; ?>
                            <div style="clear:both"></div>
                        </div>
                        <div style="clear:both"></div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</main>


<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<script type="text/javascript"
        src="https://platform-api.sharethis.com/js/sharethis.js#property=642c03d2faaa470019ff1ae6&product=inline-share-buttons&source=platform"
        async="async"></script>
<style>
    .description table {
        width: 100% !important;
        border: 1px solid #dfdfdf;
    }

    .description table tr td {
        border: 1px solid #dfdfdf;
    }

    .description {
        width: 100% !important;
        overflow: auto;
    }
</style>
</body>
<?php
echo $page_head;
?>
<body class="home">
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main mt-100 mt-xs-50">
        <section class="pt-50 pb-50">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="text-left">
                            <h2 class="font-58 font-weight-bold">Quality Policy</h2>
                        </div>
                        <div>
                            <p style="font-size: 16.8px">
                                1. infiniti is committed to a fine quality standard that governs ever single action we
                                do, always, and at all times.
                                <br/><br/>
                                2. Quality control and assurance is a day-to-day top priority that is undertaken
                                strictly by us.
                                <br/><br/>
                                3. We are determined to manufacture and supply furniture products that can definitely
                                gain customer satisfaction
                                <br/><br/>
                                4. Our products are designed and produced to be safe, reliable, and durable, to preserve
                                its quality and shape over frequent usage, and longer than others.
                                <br/><br/>
                                5. Products are manufactured from finest raw material i.e. double-layer fabric, hardwood
                                beech wood, and durable long-life foam that do not shrink after couple of months.
                                <br/><br/>
                                6. We cherish details from a holistic perspective down to the smallest and deepest
                                details in every product.
                                <br/><br/>
                                7. Service excellence is one of our cornerstones to make sure our customers are always
                                happy in their customer experience.
                                <br/><br/>
                                8. Customer-centric we are and remain so, whereas the voice of customer (VOC) continues
                                to be our key driver to meet our custumer's ambitions successfully.
                                <br/><br/>
                                9.Customers are welcomed to voice out any idea and/or suggestion that could help us to
                                better meet their ambitions and expectations
                                <br/><br/>
                                10. We realize the importance of ongoing improvement, thus we regularly invest in
                                research and development.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>
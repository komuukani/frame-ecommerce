<?php
echo $page_head;
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>
<body>
<?php echo $page_header; ?>

<main>
    <div class="mb-4 pb-4"></div>
    <section class="lookbook container">
        <h2 class="page-title">All Products</h2>
    </section>
    <section class="lookbook-collection">
        <div class="container">
            <?php
            if ($products) {
                ?>
                <section class="products-grid container">
                    <div class="row">
                        <?php
                        if (empty($products)) {
                            echo '<div class="alert alert-warning col-md-12">Sorry, Product not available!</div>';
                        } else {
                            foreach ($products as $product) {
                                $category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
                                $photos = explode(",", $product->photos);
                                $url = base_url('product/' . $product->slug . "/" . $product->product_id);
                                $whatsappUrl = "https://wa.me/" . $web_data[0]->whatsapp;
                                ?>
                                <div class="col-6 col-md-4 col-lg-3">
                                    <div class="product-card mb-3 mb-md-4 mb-xxl-5">
                                        <div class="pc__img-wrapper">
                                            <a href="<?php echo $url; ?>">
                                                <img
                                                    loading="lazy"
                                                    width="330"
                                                    height="400"
                                                    class="pc__img"
                                                    src="<?php echo base_url($photos ? $photos[0] : FILENOTFOUND); ?>"
                                                    alt="<?php echo $product->title; ?>"
                                                    title="<?php echo $product->title; ?>">
                                                <?php
                                                if (array_key_exists(1, $photos)) {
                                                    ?>
                                                    <img
                                                        loading="lazy"
                                                        src="<?php echo base_url($photos ? ($photos[1] ? $photos[1] : FILENOTFOUND) : FILENOTFOUND); ?>"
                                                        alt="<?php echo $product->title; ?>"
                                                        title="<?php echo $product->title; ?>"
                                                        width="330"
                                                        height="400"
                                                        class="pc__img pc__img-second"
                                                    >
                                                    <?php
                                                }
                                                ?>
                                            </a>
                                            <form method="post"
                                                  action="<?php echo base_url('Pages/addToCart'); ?>"
                                                  name="addToCartForm">
                                                <?php
                                                $wh['product_id'] = $product->product_id;
                                                $wh['category_id'] = $product->category_id;
                                                $wh['unique_id'] = $this->input->cookie('unique_id');
                                                $exist = $this->md->select_where('tbl_cart', $wh);
                                                ?>
                                                <input type="hidden" name="quantity"
                                                       value="<?php echo $exist ? ($exist[0]->qty + 1) : 1; ?>"/>
                                                <input type="hidden" name="productId"
                                                       value="<?php echo $product->product_id; ?>"/>
                                                <input type="hidden" name="categoryId"
                                                       value="<?php echo $product->category_id; ?>"/>
                                                <button
                                                    name="addToCart" value="addToCart" type="submit"
                                                    class="pc__atc btn anim_appear-bottom btn position-absolute border-0 text-uppercase fw-medium"
                                                    data-aside="cartDrawer" title="Add To Cart">Add To
                                                    Cart
                                                </button>
                                            </form>

                                        </div>

                                        <div class="pc__info position-relative">
                                            <p class="pc__category"><?php echo $category ? $category[0]->title : ''; ?></p>
                                            <h6 class="pc__title"><a
                                                    href="<?php echo $url; ?>"><?php echo $product->title; ?></a>
                                            </h6>
                                            <a
                                                href="<?php echo $whatsappUrl; ?>"
                                                target="_blank"
                                                class="pc__btn-wl position-absolute top-0 end-0 bg-transparent border-0 js-add-wishlist"
                                                title="Inquiry on Whatsapp">
                                                <svg class="w-6 h-6 text-gray-800 dark:text-white"
                                                     aria-hidden="true"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     width="24" height="24" fill="none"
                                                     viewBox="0 0 24 24">
                                                    <path fill="currentColor" fill-rule="evenodd"
                                                          d="M12 4a8 8 0 0 0-6.895 12.06l.569.718-.697 2.359 2.32-.648.379.243A8 8 0 1 0 12 4ZM2 12C2 6.477 6.477 2 12 2s10 4.477 10 10-4.477 10-10 10a9.96 9.96 0 0 1-5.016-1.347l-4.948 1.382 1.426-4.829-.006-.007-.033-.055A9.958 9.958 0 0 1 2 12Z"
                                                          clip-rule="evenodd"/>
                                                    <path fill="currentColor"
                                                          d="M16.735 13.492c-.038-.018-1.497-.736-1.756-.83a1.008 1.008 0 0 0-.34-.075c-.196 0-.362.098-.49.291-.146.217-.587.732-.723.886-.018.02-.042.045-.057.045-.013 0-.239-.093-.307-.123-1.564-.68-2.751-2.313-2.914-2.589-.023-.04-.024-.057-.024-.057.005-.021.058-.074.085-.101.08-.079.166-.182.249-.283l.117-.14c.121-.14.175-.25.237-.375l.033-.066a.68.68 0 0 0-.02-.64c-.034-.069-.65-1.555-.715-1.711-.158-.377-.366-.552-.655-.552-.027 0 0 0-.112.005-.137.005-.883.104-1.213.311-.35.22-.94.924-.94 2.16 0 1.112.705 2.162 1.008 2.561l.041.06c1.161 1.695 2.608 2.951 4.074 3.537 1.412.564 2.081.63 2.461.63.16 0 .288-.013.4-.024l.072-.007c.488-.043 1.56-.599 1.804-1.276.192-.534.243-1.117.115-1.329-.088-.144-.239-.216-.43-.308Z"/>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div><!-- /.row -->
                </section><!-- /.Products-grid -->
                <?php
            }
            ?>
        </div>
    </section>
</main>


<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>
<?php
$web_data = $this->md->select('tbl_web_data')[0];
$banner = $this->md->my_query('SELECT * FROM `tbl_banner` ORDER BY `position` asc')->result();
?>
<section class="swiper-container js-swiper-slider slideshow full-width_padding"
         data-settings='{
        "autoplay": {
          "delay": 5000
        },
        "slidesPerView": 1,
        "effect": "fade",
        "loop": true,
        "pagination": {
          "el": ".slideshow-pagination",
          "type": "bullets",
          "clickable": true
        }
      }'>
    <div class="swiper-wrapper">
        <?php
        if (!empty($banner)) {
            foreach ($banner as $banner_data) {
                ?>
                <div class="swiper-slide full-width_border border-1" style="border-color: #f5e6e0;">
                    <div class="overflow-hidden position-relative h-100">
                        <div class="slideshow-bg" style="background-color: #f5e6e0;">
                            <img loading="lazy" src="<?php echo base_url($banner_data->path); ?>" width="1761"
                                 height="778"
                                 alt="Pattern" class="slideshow-bg__img object-fit-cover">
                        </div>
                        <!-- <p class="slideshow_markup font-special text-uppercase position-absolute end-0 bottom-0">Summer</p> -->
                        <div class="slideshow-character position-absolute bottom-0 pos_right-center">

                            <div class="character_markup">
                                <p class="text-uppercase font-sofia fw-bold animate animate_fade animate_rtl animate_delay-10">
                                    FRAMES</p>
                            </div>
                        </div>
                        <div class="slideshow-text container position-absolute start-50 top-50 translate-middle">
                            <h6 class="text_dash text-uppercase text-red fs-base fw-medium animate animate_fade animate_btt animate_delay-3">
                                <?php echo $banner_data->subtitle; ?></h6>
                            <h2 class="text-uppercase h1 fw-normal mb-0 animate animate_fade animate_btt animate_delay-5">
                                <?php echo $banner_data->title; ?></h2>
                            <a href="<?php echo base_url('product'); ?>"
                               class="btn-link btn-link_lg default-underline text-uppercase fw-medium animate animate_fade animate_btt animate_delay-7">Discover
                                More</a>
                        </div>
                    </div>
                </div><!-- /.slideshow-item -->
                <?php
            }
        }
        ?>
    </div><!-- /.slideshow-wrapper js-swiper-slider -->

    <div class="container">
        <div class="slideshow-pagination d-flex align-items-center position-absolute bottom-0 mb-5"></div>
        <!-- /.products-pagination -->
    </div><!-- /.container -->

    <div
        class="slideshow-social-follow d-none d-xxl-block position-absolute top-50 start-0 translate-middle-y text-center">
        <ul class="social-links list-unstyled mb-0 text-secondary">
            <li>
                <a href="<?php echo $web_data ? $web_data->facebook : ''; ?>" class="footer__social-link d-block">
                    <svg class="svg-icon svg-icon_facebook" width="9" height="15" viewBox="0 0 9 15"
                         xmlns="http://www.w3.org/2000/svg">
                        <use href="#icon_facebook"/>
                    </svg>
                </a>
            </li>
            <li>
                <a href="<?php echo $web_data ? $web_data->twitter : ''; ?>" class="footer__social-link d-block">
                    <svg class="svg-icon svg-icon_twitter" width="14" height="13" viewBox="0 0 14 13"
                         xmlns="http://www.w3.org/2000/svg">
                        <use href="#icon_twitter"/>
                    </svg>
                </a>
            </li>
            <li>
                <a href="<?php echo $web_data ? $web_data->instagram : ''; ?>" class="footer__social-link d-block">
                    <svg class="svg-icon svg-icon_instagram" width="14" height="13" viewBox="0 0 14 13"
                         xmlns="http://www.w3.org/2000/svg">
                        <use href="#icon_instagram"/>
                    </svg>
                </a>
            </li>
        </ul><!-- /.social-links list-unstyled mb-0 text-secondary -->
        <span
            class="slideshow-social-follow__title d-block mt-5 text-uppercase fw-medium text-secondary">Follow Us</span>
    </div><!-- /.slideshow-social-follow -->
    <a href="#section-collections-grid_masonry"
       class="slideshow-scroll d-none d-xxl-block position-absolute end-0 bottom-0 text_dash text-uppercase fw-medium">Scroll</a>
</section><!-- /.slideshow -->



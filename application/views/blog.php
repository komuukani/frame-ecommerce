<?php
echo $page_head;
?>
<body>

<?php echo $page_header; ?>

<main id="content" class="wrapper layout-page">
    <?php echo $page_breadcumb; ?>

    <section class="page-title z-index-2 position-relative">
        <div class="text-center py-13">
            <div class="container">
                <h2 class="mb-0">Blog</h2></div>
        </div>
    </section>
    <div class="container mb-lg-18 mb-15 pb-3">
        <div class="row gy-50px">
            <?php
            if (!empty($blog)) {
                foreach ($blog as $blog_data) {
                    $url = base_url('blog/' . strtolower($blog_data->slug));
                    ?>
                    <div class="col-md-6 col-lg-4">
                        <article class="card card-post-grid-3 bg-transparent border-0" data-animate="fadeInUp">
                            <figure class="card-img-top mb-8 position-relative"><a href="<?php echo $url; ?>"
                                                                                   class="hover-shine hover-zoom-in d-block"
                                                                                   title="<?php echo $blog_data->title; ?>">
                                    <img data-src="<?php echo base_url($blog_data->path); ?>"
                                         class="img-fluid lazy-image w-100"
                                         style="height:400px;object-fit: cover"
                                         src="<?php echo base_url($blog_data->path); ?>"
                                         alt="<?php echo $blog_data->title; ?>">
                                </a></figure>
                            <div class="card-body p-0">
                                <ul class="post-meta list-inline lh-1 d-flex flex-wrap fs-13px text-uppercase ls-1 fw-semibold m-0">
                                    <li class="border-start list-inline-item"><?php echo date('d-M-Y', strtotime($blog_data->blogdate)); ?></li>
                                </ul>
                                <h4 class="card-title lh-base mt-5 pt-2 mb-0">
                                    <a class="text-decoration-none"
                                       href="<?php echo $url; ?>"
                                       title="<?php echo $blog_data->title; ?>"><?php echo $blog_data->title; ?></a>
                                </h4>
                            </div>
                        </article>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</main>


<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>
<?php echo $page_head; ?>
<body>
<?php echo $page_header; ?>
<main id="content" class="wrapper layout-page">
    <?php
    //$this->load->view('user/profile_header');
    ?>
    <div class="contact-form section-padding">
        <div class="container-xl">
            <div class="row mt-50 mb-40">
                <div class="col-md-3">
                    <?php
                    $this->load->view('user/sidebar');
                    ?>
                </div>
                <div class="col-md-9">
                    <div class="contact-form p-30">
                        <form method="post" novalidate="">
                            <div class="row">
                                <div class="col-md-8">
                                    <h6 class="sub-title font-20 fw-500 text-uppercase">Update Your Password</h6>
                                    <hr class="mt-3 mb-4"/>
                                    <div class="single-personal-info mb-4">
                                        <label class="lbl">Current Password</label>
                                        <input type="password" class="form-control " name="current_password"
                                               placeholder="Enter Current Password" autocomplete="off"/>
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('current_password')) {
                                                echo form_error('current_password');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="single-personal-info mb-4">
                                        <label class="lbl">New Password</label>
                                        <input type="password" class="form-control " name="new_password"
                                               placeholder="Enter New Password" autocomplete="off"/>
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('new_password')) {
                                                echo form_error('new_password');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="single-personal-info mb-4">
                                        <label class="lbl">Confirm Password</label>
                                        <input type="password" class="form-control " name="con_password"
                                               placeholder="Enter Confirm Password" autocomplete="off"/>
                                        <div class="error-text p-0 sdfsfsdm-0">
                                            <?php
                                            if (form_error('con_password')) {
                                                echo form_error('con_password');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <button type="submit" value="send" name="update"
                                            class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary px-11">
                                        Update now <i class="far fa-chevron-double-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php echo $page_footer; ?>
<?php
echo $page_footerscript;
if (isset($err)) {
    ?>
    <script>
        $.notify('<?php echo $err; ?>', 'error');
    </script>
    <?php
}
?>
</body>  

<?php
echo $page_head;
$admin_data = $this->md->select('tbl_web_data')[0];
$user = $this->session->userdata('email');
$ip = $this->input->cookie('unique_id');
if ($user != "") {
    $wh['email'] = $user;
} else {
    $wh['unique_id'] = $ip;
}
$product = $this->md->select_where('tbl_wishlist', $wh);
?>

<body>
<?php echo $page_header; ?>
<main id="content" class="wrapper layout-page">
    <?php echo $page_breadcumb; ?>

    <section class="container container-xxl mb-13 mb-lg-15" data-animated-id="2">
        <div class="text-center"><h2 class="mb-13">Wishlist</h2></div>
        <form class="table-responsive-md">
            <table class="table" style="min-width: 710px">
                <tbody>
                <?php
                if (empty($product)) {
                    echo '<div class="alert alert-warning col-md-12 p-2">Sorry, Product not available!</div>';
                } else {
                    foreach ($product as $pro_data) {
                        $product_data = $this->md->select_where('tbl_product', array('product_id' => $pro_data->product_id));
                        if ($product_data) :
                            $product_data = $product_data[0];
                            $cateData = $this->md->select_where('tbl_category', array('category_id' => $product_data->category_id));   // Get Category Data
                            $url = base_url('product/' . ($cateData[0]->slug) . '/' . strtolower($product_data->slug));
                            $img = explode(",", $product_data->photos);
                            ?>
                            <tr class="border">
                                <th scope="row" class=" ps-xl-10 py-6 d-flex align-items-center border-0">
                                    <a href="javascript:void(0)"
                                       data-productid="<?php echo $product_data->product_id; ?>"
                                       class="add-to-wishlist d-block text-muted fw-lighter" data-wishlist="true"><i
                                            class="fas fa-times"></i></a>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-6 me-7">
                                            <a href="<?php echo $url; ?>"><img class="img-fluid loaded"
                                                                               title="<?php echo $product_data->title; ?>"
                                                                               src="<?php echo base_url(($img) ? $img[0] : FILENOTFOUND); ?>"
                                                                               alt="<?php echo $product_data->title; ?>"
                                                                               style="width: 100px;height: 100px;object-fit: cover">
                                            </a>
                                        </div>
                                        <div>
                                            <p class=" text-body-emphasis fw-semibold mb-5"><a
                                                    href="<?php echo $url; ?>"><?php echo $product_data->title; ?></a>
                                            </p>
                                            <p class="fw-bold fs-14px mb-4 text-body-emphasis">
                                               <span class="fs-18px text-body-emphasis fw-bold">
                            <ins class="text-decoration-none">
                                            <?php
                                            if ($product_data->standard_price == '') {
                                                if ($product_data->standard_price_50gm == '') {
                                                    if ($product_data->standard_price_100gm != '') {
                                                        echo "$" . number_format($product_data->standard_price_100gm, 2);
                                                    }
                                                } else {
                                                    echo "$" . number_format($product_data->standard_price_50gm, 2);
                                                }
                                            } else {
                                                echo "$" . number_format($product_data->standard_price, 2);
                                            }
                                            ?>
                                        </ins>
                                        <label class="ml-10 mr-10">—</label>
                                        <ins class="text-decoration-none">
                                            <?php
                                            if ($product_data->refill_price == '') {
                                                if ($product_data->refill_price_50gm == '') {
                                                    if ($product_data->refill_price_100gm != '') {
                                                        echo "$" . number_format($product_data->refill_price_100gm, 2);
                                                    }
                                                } else {
                                                    echo "$" . number_format($product_data->refill_price_50gm, 2);
                                                }
                                            } else {
                                                echo "$" . number_format($product_data->refill_price, 2);
                                            }
                                            ?>
                                        </ins>
                        </span>
                                            </p>
                                            <p class="mb-0 text-000 fw-normal">
                                                Category: <?php echo $this->md->getItemName('tbl_category', 'category_id', 'title', $product_data->category_id); ?></p>
                                        </div>
                                    </div>
                                </th>
                                <td class=" align-middle text-end pe-10">
                                    <span class="me-6">In stock</span>
                                </td>
                            </tr>
                        <?php
                        endif;
                    }
                }
                ?>
                <tr>
                    <td colspan="2" class="border-0 text-center py-8 px-0">
                        <a href="<?php echo base_url('product') ?>"
                           class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary text-light">
                            Continue Shopping
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </section>
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>
<?php
echo $page_head;
?>

<body>
<?php echo $page_header; ?>
<main id="content" class="wrapper layout-page">
    <?php
    //$this->load->view('user/profile_header');
    ?>
    <div class="contact-form section-padding">
        <div class="container-xl">
            <div class="row mt-50 mb-40">
                <div class="col-md-3">
                    <?php
                    $this->load->view('user/sidebar');
                    ?>
                </div>
                <div class="col-md-9">
                    <div class="contact-form p-30">
                        <form method="post" novalidate="" enctype="multipart/form-data">
                            <?php
                            if (isset($error)) {
                                ?>
                                <div class="alert alert-danger p-1">
                                    <?php echo $error; ?>
                                </div>
                                <?php
                            }
                            if (isset($success)) {
                                ?>
                                <div class="alert alert-success p-1">
                                    <?php echo $success; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <h6 class="sub-title font-20 fw-500 text-uppercase">Update Your Personal
                                        Information</h6>
                                    <hr class="mt-3 mb-4"/>
                                    <div class="row mb-4">
                                        <div class="col-md-6">
                                            <div class="single-personal-info">
                                                <label class="lbl">Full Name </label>
                                                <input type="text" class="form-control " name="fname"
                                                       value="<?php echo $user_info[0]->fname; ?>"
                                                       placeholder="Enter Full Name" autocomplete="off"/>
                                                <div class="error-text p-0 m-0">
                                                    <?php
                                                    if (form_error('fname')) {
                                                        echo form_error('fname');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="single-personal-info">
                                                <label class="lbl">Gender </label>
                                                <div class="mt-2">
                                                    <label for="male">
                                                        <input id="male"
                                                               type="radio" <?php echo($user_info[0]->gender == 'male' ? 'checked' : ''); ?>
                                                               style="width: auto" name="gender" value="male"/> Male
                                                    </label>&nbsp; &nbsp;
                                                    <label for="female">
                                                        <input type="radio"
                                                               id="female" <?php echo($user_info[0]->gender == 'female' ? 'checked' : ''); ?>
                                                               style="width: auto" name="gender" value="female"/> Female
                                                    </label>
                                                    <label for="other">
                                                        <input type="radio"
                                                               id="other" <?php echo($user_info[0]->gender == 'other' ? 'checked' : ''); ?>
                                                               style="width: auto" name="gender" value="other"/> Other
                                                    </label>
                                                </div>
                                                <div class="error-text p-0 m-0">
                                                    <?php
                                                    if (form_error('gender')) {
                                                        echo form_error('gender');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-md-6">
                                            <div class="single-personal-info">
                                                <label class="lbl">Phone Number</label>
                                                <input type="number" class="form-control " name="phone"
                                                       value="<?php echo $user_info[0]->phone; ?>"
                                                       placeholder="Enter Phone Number"
                                                       oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                       maxlength="12" autocomplete="off"/>
                                                <div class="error-text p-0 m-0">
                                                    <?php
                                                    if (form_error('phone')) {
                                                        echo form_error('phone');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="single-personal-info">
                                                <label class="lbl">Email </label>
                                                <input type="email" class="form-control " name="email"
                                                       value="<?php echo $user_info[0]->email; ?>"
                                                       placeholder="Enter Email" autocomplete="off"/>
                                                <div class="error-text p-0 m-0">
                                                    <?php
                                                    if (form_error('email')) {
                                                        echo form_error('email');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-md-4">
                                            <div class="single-personal-info">
                                                <label class="lbl">Country </label>
                                                <input type="text" class="form-control " name="country"
                                                       value="<?php echo $user_info[0]->country; ?>"
                                                       placeholder="Enter Country" autocomplete="off"/>
                                                <div class="error-text p-0 m-0">
                                                    <?php
                                                    if (form_error('country')) {
                                                        echo form_error('country');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="single-personal-info">
                                                <label class="lbl">State </label>
                                                <input type="text" class="form-control " name="state"
                                                       value="<?php echo $user_info[0]->state; ?>"
                                                       placeholder="Enter State" autocomplete="off"/>
                                                <div class="error-text p-0 m-0">
                                                    <?php
                                                    if (form_error('state')) {
                                                        echo form_error('state');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="single-personal-info">
                                                <label class="lbl">City </label>
                                                <input type="text" class="form-control " name="city"
                                                       value="<?php echo $user_info[0]->city; ?>"
                                                       placeholder="Enter City" autocomplete="off"/>
                                                <div class="error-text p-0 m-0">
                                                    <?php
                                                    if (form_error('city')) {
                                                        echo form_error('city');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mt-4">
                                            <div class="single-personal-info">
                                                <label class="lbl">Address </label>
                                                <textarea name="address" class="form-control "
                                                          placeholder="Enter Address"><?php echo $user_info[0]->address; ?></textarea>
                                                <div class="error-text p-0 m-0">
                                                    <?php
                                                    if (form_error('address')) {
                                                        echo form_error('address');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mt-4">
                                            <div class="single-personal-info">
                                                <label class="lbl">Postal Code </label>
                                                <input type="number" class="form-control " name="postal_code"
                                                       value="<?php echo $user_info[0]->postal_code; ?>"
                                                       placeholder="Enter Postal Code" autocomplete="off"/>
                                                <div class="error-text p-0 m-0">
                                                    <?php
                                                    if (form_error('postal_code')) {
                                                        echo form_error('postal_code');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mt-4">
                                            <div class="single-personal-info">
                                                <label class="lbl">Profile Photo </label>
                                                <input type="file"
                                                       onchange="readURL(this, 'blah');$('#updateStatus').val('yes');"
                                                       name="customer" accept="image/*"/>
                                                <input type="hidden" id="updateStatus" name="updateStatus"/>
                                                <input type="hidden" value="<?php echo $user_info[0]->path; ?>"
                                                       name="oldPath"/>
                                                <img
                                                    src="<?php echo ($user_info[0]->path) ? base_url($user_info[0]->path) : base_url(FILENOTFOUND); ?>"
                                                    class="mt-2 center-block" width="60" id="blah"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" value="send" name="save"
                                            class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary px-11">
                                        Save Changes <i class="far fa-chevron-double-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<?php
if (isset($err)) {
    ?>
    <script>
        $.notify('<?php echo $err; ?>', 'error');
    </script>
    <?php
}
?>
</body>
<?php
echo $page_head;
?>
<body>
<?php echo $page_header; ?>

<main>
    <div class="mb-4 pb-4"></div>
    <section class="login-register container">
        <h2>Recover Password</h2>
        <p class="fs-16 mb-7">Enter your registered email address to recover your password!</p>
        <div class="mt-5 login-form">
            <form name="login-form" method="post" class="needs-validation" novalidate>
                <?php
                if (isset($error)) {
                    ?>
                    <div class="alert alert-danger p-1 mb-5">
                        <?php echo $error; ?>
                    </div>
                    <?php
                }
                if (isset($success)) {
                    ?>
                    <div class="alert alert-success p-1 mb-5">
                        <?php echo $success; ?>
                    </div>
                    <?php
                }
                ?>
                <div class="form-floating mb-3">
                    <input name="username" type="email" class="form-control form-control_gray"
                           value="<?php
                           if (set_value('username') && !isset($success)) {
                               echo set_value('username');
                           }
                           ?>"
                           id="customerNameEmailInput1" placeholder="Email address *" required>
                    <label for="customerNameEmailInput1">Email address *</label>
                    <div class="error-text p-0 m-0">
                        <?php
                        if (form_error('username')) {
                            echo form_error('username');
                        }
                        ?>
                    </div>
                </div>

                <div class="pb-3"></div>

                <div class="d-flex align-items-center mb-3 pb-2">
                    <a href="<?php echo base_url('user-login') ?>" class="btn-text ms-auto">Back to Login</a>
                </div>

                <div class="mb-3">
                    <?php
                    if ($web_data[0]->captcha_visibility):
                        echo '<div class="g-recaptcha" data-sitekey="' . $web_data[0]->captcha_site_key . '"></div>';
                    endif;
                    ?>
                </div>

                <button name="forgot" value="send" class="btn btn-primary w-100 text-uppercase" type="submit">Submit
                </button>
            </form>
        </div>
    </section>
</main>

<div class="mb-5 pb-xl-5"></div>

<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>  
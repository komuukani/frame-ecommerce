<?php
echo $page_head;
?>

<body class="body-wrapper">
<?php echo $page_header; ?>
<main id="content">
    <?php
    //$this->load->view('user/profile_header');
    ?>
    <div class="contact-form section-padding">
        <div class="container-xl">
            <div class="row mt-50 mb-80">
                <div class="col-md-3">
                    <?php
                    $this->load->view('user/sidebar');
                    ?>
                </div>
                <div class="col-md-9">
                    <div class="contact-form p-30">
                        <div class="mt-2 mb-20">
                            <h6 class="sub-title font-20 fw-500 text-uppercase">My order</h6>
                            <hr class="mt-3 mb-3"/>
                        </div>
                        <table class="table table-bordered table-hover">
                            <tr class="text-center bg-DFDFDF text-000">
                                <th>Order ID</th>
                                <th>Status</th>
                                <th>Order Date</th>
                                <th>Invoice</th>
                            </tr>
                            <?php
                            if (!empty($orders)) {
                                foreach ($orders as $transaction_data) {
                                    ?>
                                    <tr class="font-light">
                                        <td align="center"><?php echo $transaction_data->order_id; ?></td>
                                        <td align="center"><?php echo $transaction_data->status; ?></td>
                                        <td align="center"><?php echo $transaction_data->entry_date; ?></td>
                                        <td align="center">
                                            <a class="bg-000 text-FFF shadow pl-20 pr-20 pt-2 pb-2 border-radius-10"
                                               target="_blank"
                                               href="<?php echo base_url('generate-report/view/' . $transaction_data->order_id); ?>">View</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="4" class="text-center text-000">
                                        Sorry, You don't have any order yet!
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<style>
    table td, table th {
        padding: 10px !important;
        border: 1px solid #dfdfdf !important;
    }

    table tr:last-child td {
        border-bottom: 1px solid #000 !important;
    }
</style>
</body>
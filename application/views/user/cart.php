<?php
echo $page_head;

$user = $this->session->userdata('email');
$userdata = $cartProduct = array();
if (isset($user)) :
    $userdata = $this->md->select_where('tbl_register', array('email' => $user));
    $cartProduct = $this->md->select_where('tbl_cart', array('register_id' => ($userdata ? $userdata[0]->register_id : '')));
else:
    $cartProduct = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));
endif;

$products = $this->md->select_limit_order('tbl_product', 8, 'product_id', 'desc', array('featured' => 1, 'status' => 1));
?>

<body>
<?php echo $page_header; ?>
<main>
    <div class="mb-4 pb-4"></div>
    <section class="shop-checkout container mb-100">
        <h2 class="page-title">Cart</h2>
        <div class="checkout-steps">
            <a href="<?php echo base_url('cart'); ?>" class="checkout-steps__item active">
                <span class="checkout-steps__item-number">01</span>
                <span class="checkout-steps__item-title">
            <span>Shopping Bag</span>
            <em>Manage Your Items List</em>
          </span>
            </a>
            <a href="<?php echo base_url('checkout'); ?>" class="checkout-steps__item">
                <span class="checkout-steps__item-number">02</span>
                <span class="checkout-steps__item-title">
            <span>Shipping and Checkout</span>
            <em>Checkout Your Items List</em>
          </span>
            </a>
            <a href="javascript:void(0);" class="checkout-steps__item">
                <span class="checkout-steps__item-number">03</span>
                <span class="checkout-steps__item-title">
            <span>Confirmation</span>
            <em>Review And Submit Your Order</em>
          </span>
            </a>
        </div>
        <div class="shopping-cart">
            <div class="cart-table__wrapper">
                <table class="cart-table">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th></th>
                        <th>Quantity</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($cartProduct) {
                        foreach ($cartProduct as $cartitem) {
                            $productData = $this->md->select_where('tbl_product', array('product_id' => $cartitem->product_id));
                            if ($productData) {
                                $product = $productData[0];
                                $category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
                                $photos = array_filter(explode(",", $product->photos));
                                $url = base_url('product/' . $product->slug . "/" . $product->product_id);
                                $tempPhoto = (file_exists('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') ? ('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') : FILENOTFOUND);
                                ?>
                                <tr>
                                    <td>
                                        <div class="shopping-cart__product-item">
                                            <a href="<?php echo $url; ?>">
                                                <img
                                                        loading="lazy"
                                                        class="cart-drawer-item__img"
                                                        src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : $tempPhoto) : $tempPhoto); ?>"
                                                        alt="<?php echo $product->title; ?>"
                                                        title="<?php echo $product->title; ?>"/>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="shopping-cart__product-item__detail">
                                            <h4><a href="<?php echo $url; ?>"><?php echo $product->title; ?></a></h4>
                                            <ul class="shopping-cart__product-item__options">
                                                <li>Category: <?php echo $category ? $category[0]->title : '-'; ?> </li>
                                                <li>Quantity: <?php echo $cartitem->qty; ?></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="qty-control position-relative">
                                            <form method="post" action="<?php echo base_url('Pages/updateQty'); ?>"
                                                  name="updateCartForm">
                                                <input type="hidden" name="cartId"
                                                       value="<?php echo $cartitem->cart_id; ?>"/>
                                                <input type="number" name="quantity"
                                                       value="<?php echo $cartitem->qty; ?>" min="1"
                                                       onchange="$(this).parentsUntil('.qty-control').submit();"
                                                       class="qty-control__number text-center">
                                                <div class="qty-control__reduce">-
                                                </div>
                                                <div class="qty-control__increase">+
                                                </div>
                                            </form>
                                        </div><!-- .qty-control -->
                                    </td>
                                    <td>
                                        <a href="<?php echo base_url('Pages/removeCartItem/' . $cartitem->cart_id); ?>"
                                           class="remove-cart">
                                            <svg width="10" height="10" viewBox="0 0 10 10" fill="#767676"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                        d="M0.259435 8.85506L9.11449 0L10 0.885506L1.14494 9.74056L0.259435 8.85506Z"/>
                                                <path
                                                        d="M0.885506 0.0889838L9.74057 8.94404L8.85506 9.82955L0 0.97449L0.885506 0.0889838Z"/>
                                            </svg>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-right mt-20">
            <div class="button-wrapper container">
                <a href="<?php echo base_url('checkout'); ?>" class="btn btn-primary btn-checkout">PROCEED TO
                    CHECKOUT</a>
            </div>
        </div>
    </section>
</main>

<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>
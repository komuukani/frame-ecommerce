<?php
echo $page_head;
?>
<body>
<?php echo $page_header; ?>
<main id="content" class="wrapper layout-page">
    <div class="contact-form section-padding pb-50">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="contact-form p-50 text-center">
                        <svg class="text-gray-800 dark:text-white" aria-hidden="true"
                             xmlns="http://www.w3.org/2000/svg" width="80" height="80" fill="none"
                             viewBox="0 0 24 24">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                  stroke-width="2"
                                  d="m8.032 12 1.984 1.984 4.96-4.96m4.55 5.272.893-.893a1.984 1.984 0 0 0 0-2.806l-.893-.893a1.984 1.984 0 0 1-.581-1.403V7.04a1.984 1.984 0 0 0-1.984-1.984h-1.262a1.983 1.983 0 0 1-1.403-.581l-.893-.893a1.984 1.984 0 0 0-2.806 0l-.893.893a1.984 1.984 0 0 1-1.403.581H7.04A1.984 1.984 0 0 0 5.055 7.04v1.262c0 .527-.209 1.031-.581 1.403l-.893.893a1.984 1.984 0 0 0 0 2.806l.893.893c.372.372.581.876.581 1.403v1.262a1.984 1.984 0 0 0 1.984 1.984h1.262c.527 0 1.031.209 1.403.581l.893.893a1.984 1.984 0 0 0 2.806 0l.893-.893a1.985 1.985 0 0 1 1.403-.581h1.262a1.984 1.984 0 0 0 1.984-1.984V15.7c0-.527.209-1.031.581-1.403Z"/>
                        </svg>

                        <h3 class="text-center mt-4">Order Placed Successfully!</h3>
                        <p class="text-center font-18 mt-3">Thank you for your order! <br/>
                            <i> In case of any discrepancy please contact <a href="mailto:support@skintolove.co.nz">support@skintolove.co.nz</a>.</i>
                        </p>
                        <a class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary px-11 mr-20"
                           href="<?php echo base_url('collection'); ?>">Shop Now</a>
                        <?php
                        if ($this->session->userdata('email')) {
                            ?>
                            <a class="btn btn-primary btn-hover-bg-dark btn-hover-border-primary px-11"
                               href="<?php echo base_url('myorder'); ?>">View Order</a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>


<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>
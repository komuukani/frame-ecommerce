<?php
echo $page_head;
?>
<body>
<?php echo $page_header; ?>

<main>
    <div class="mb-4 pb-4"></div>
    <section class="login-register container">
        <h2>Sign in</h2>
        <p class="fs-16 mb-7">Don't have an account yet? <a
                href="<?php echo base_url('user-register'); ?>"
                class="text-secondary border-bottom text-decoration-none">Sign up</a> for free</p>
        <div class="mt-5 login-form">
            <form name="login-form" method="post" class="needs-validation" novalidate>
                <?php
                if (isset($error)) {
                    ?>
                    <div class="alert alert-danger p-1 mb-5">
                        <?php echo $error; ?>
                    </div>
                    <?php
                }
                if (isset($success)) {
                    ?>
                    <div class="alert alert-success p-1 mb-5">
                        <?php echo $success; ?>
                    </div>
                    <?php
                }
                ?>
                <div class="form-floating mb-3">
                    <input name="username" type="email" class="form-control form-control_gray"
                           value="<?php
                           if (set_value('username') && !isset($success)) {
                               echo set_value('username');
                           }
                           ?>"
                           id="customerNameEmailInput1" placeholder="Email address *" required>
                    <label for="customerNameEmailInput1">Email address *</label>
                    <div class="error-text p-0 m-0">
                        <?php
                        if (form_error('username')) {
                            echo form_error('username');
                        }
                        ?>
                    </div>
                </div>

                <div class="pb-3"></div>

                <div class="form-floating mb-3">
                    <input name="password" type="password" class="form-control form-control_gray"
                           value="<?php
                           if (set_value('password') && !isset($success)) {
                               echo set_value('password');
                           }
                           ?>"
                           id="customerPasswodInput" placeholder="Password *" required>
                    <label for="customerPasswodInput">Password *</label>
                    <div class="error-text p-0 m-0">
                        <?php
                        if (form_error('password')) {
                            echo form_error('password');
                        }
                        ?>
                    </div>
                </div>

                <div class="d-flex align-items-center mb-3 pb-2">
                    <a href="<?php echo base_url('forgot-password') ?>" class="btn-text ms-auto">Lost password?</a>
                </div>

                <div class="mb-3">
                    <?php
                    if ($web_data[0]->captcha_visibility):
                        echo '<div class="g-recaptcha" data-sitekey="' . $web_data[0]->captcha_site_key . '"></div>';
                    endif;
                    ?>
                </div>

                <button name="login" value="send" class="btn btn-primary w-100 text-uppercase" type="submit">Log In
                </button>

                <div class="customer-option mt-4 text-center">
                    <span class="text-secondary">No account yet?</span>
                    <a href="<?php echo base_url('user-register') ?>" class="btn-text js-show-register">Create
                        Account</a>
                </div>
            </form>
        </div>
    </section>
</main>

<div class="mb-5 pb-xl-5"></div>

<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>  
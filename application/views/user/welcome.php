<?php
echo $page_head;
?>

<body>
<?php echo $page_header; ?>
<main>
    <?php
    //$this->load->view('user/profile_header');
    ?>
    <div class="contact-form section-padding">
        <div class="container-xl">
            <div class="row mt-50 mb-80">
                <div class="col-md-3">
                    <?php
                    $this->load->view('user/sidebar');
                    ?>
                </div>
                <div class="col-md-9">
                    <div class="contact-form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="p-30 bg-profile-info p-xs-10 text-capitalize font-18">
                                    <div class="row">
                                        <div class="col-md-3 col-5"><label class="lbl">Full Name</label></div>
                                        <div class="col-md-1 col-1"> :</div>
                                        <div
                                            class="col-md-8 col-6"> <?php echo($user_info ? $user_info[0]->fname : '<code>Name not found!</code>'); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-5"><label class="lbl">Email Address</label></div>
                                        <div class="col-md-1 col-1"> :</div>
                                        <div
                                            class="col-md-8 col-6"> <?php echo($user_info ? $user_info[0]->email : '<code>Email Address not found!</code>'); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-5"><label class="lbl">Phone Number</label></div>
                                        <div class="col-md-1 col-1"> :</div>
                                        <div
                                            class="col-md-8 col-6"> <?php echo($user_info ? $user_info[0]->phone : '<code>Phone Number not found!</code>'); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-5"><label class="lbl">Gender</label></div>
                                        <div class="col-md-1 col-1"> :</div>
                                        <div
                                            class="col-md-8 col-6"> <?php echo($user_info ? $user_info[0]->gender : '<code>Gender not found!</code>'); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-5"><label class="lbl">City</label></div>
                                        <div class="col-md-1 col-1"> :</div>
                                        <div
                                            class="col-md-8 col-6"> <?php echo($user_info ? $user_info[0]->city : '<code>Address not found!</code>'); ?></div>
                                        <!-- <div class="col-md-8 col-6"> <?php //echo ($user_info ? ($this->md->getItemName('tbl_city', 'id', 'name', $user_info[0]->city)) : '<code>City not found!</code>');
                                        ?></div> -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-5"><label class="lbl">State</label></div>
                                        <div class="col-md-1 col-1"> :</div>
                                        <div
                                            class="col-md-8 col-6"> <?php echo($user_info ? $user_info[0]->state : '<code>Address not found!</code>'); ?></div>
                                        <!-- <div class="col-md-8 col-6"> <?php //echo ($user_info ? ($this->md->getItemName('tbl_state', 'id', 'name', $user_info[0]->state)) : '<code>State not found!</code>');
                                        ?></div> -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-5"><label class="lbl">Address</label></div>
                                        <div class="col-md-1 col-1"> :</div>
                                        <div
                                            class="col-md-8 col-6"> <?php echo($user_info ? $user_info[0]->address : '<code>Address not found!</code>'); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-5"><label class="lbl">Postal Code</label></div>
                                        <div class="col-md-1 col-1"> :</div>
                                        <div
                                            class="col-md-8 col-6"> <?php echo($user_info ? $user_info[0]->postal_code : '<code>Postal Code not found!</code>'); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-5"><label class="lbl">Registered Date</label></div>
                                        <div class="col-md-1 col-1"> :</div>
                                        <div
                                            class="col-md-8 col-6"> <?php echo($user_info ? $user_info[0]->register_date : '<code>Registered Date not found!</code>'); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>
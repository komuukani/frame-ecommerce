<?php
$user = $this->session->userdata('register_id');
if (isset($user)):
    $userdata = $this->md->select_where('tbl_register', array('register_id' => $user));
endif;
?>

<div class="breadcrumbstwo">
    <div class="custom-shape-divider-bottom-1674915130">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path d="M598.97 114.72L0 0 0 120 1200 120 1200 0 598.97 114.72z" class="shape-fill"></path>
        </svg>
    </div>
    <div class="img-area" align="center">
        <div class="inner-area">
            <img src="<?php echo base_url($user_info[0]->path ? $user_info[0]->path : FILENOTFOUND); ?>" id="blah"/>
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-inner text-center">
                        <div class="breadcrumbs-title">
                            <?php
                            //                            if ($user_info[0]->status) {
                            ?>
                            <h3 class="text-theme font-24 text-transform-uppercase">
                                Welcome! <?php echo $user_info[0]->fname; ?></h3>
                            <?php
                            //                            } else {
                            ?>
                            <!--<h3 class="text-danger font-20 text-transform-uppercase"><i class="fas fa-exclamation-triangle"></i> You Haven't verified your Mobile/Email! <a class="text-decoration-underline text-000" href="<?php echo base_url('user-verify') ?>">Verify Now!</a></h3>-->
                            <?php
                            //                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
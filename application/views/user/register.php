<?php
echo $page_head;
?>
<body>
<?php echo $page_header; ?>


<main>
    <div class="mb-4 pb-4"></div>
    <section class="login-register container">
        <h2>Sign up</h2>
        <p class="fs-16 mb-7">Already have an account? <a href="<?php echo base_url('user-login'); ?>"
                                                          class="text-secondary border-bottom text-decoration-none">Sign
                in</a> for free</p>
        <div class="mt-5 login-form">
            <form name="register-form" method="post" class="needs-validation" novalidate>
                <?php
                if (isset($error)) {
                    ?>
                    <div class="alert alert-danger p-1 mb-5">
                        <?php echo $error; ?>
                    </div>
                    <?php
                }
                if (isset($success)) {
                    ?>
                    <div class="alert alert-success p-1 mb-5">
                        <?php echo $success; ?>
                    </div>
                    <?php
                }
                ?>
                <div class="form-floating mb-3">
                    <input name="fname" type="text" class="form-control form-control_gray"
                           value="<?php
                           if (set_value('fname') && !isset($success)) {
                               echo set_value('fname');
                           }
                           ?>"
                           id="fname" placeholder="Full Name *" required>
                    <label for="fname">Full Name *</label>
                    <div class="error-text p-0 m-0">
                        <?php
                        if (form_error('fname')) {
                            echo form_error('fname');
                        }
                        ?>
                    </div>
                </div>

                <div class="pb-3"></div>

                <div class="form-floating mb-3">
                    <input name="email" type="email" class="form-control form-control_gray"
                           value="<?php
                           if (set_value('email') && !isset($success)) {
                               echo set_value('email');
                           }
                           ?>"
                           id="customerNameEmailInput1" placeholder="Email address *" required>
                    <label for="customerNameEmailInput1">Email address *</label>
                    <div class="error-text p-0 m-0">
                        <?php
                        if (form_error('email')) {
                            echo form_error('email');
                        }
                        ?>
                    </div>
                </div>

                <div class="pb-3"></div>

                <div class="form-floating mb-3">
                    <input name="phone" type="number" class="form-control form-control_gray"
                           oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                           maxlength="12"
                           value="<?php
                           if (set_value('phone') && !isset($success)) {
                               echo set_value('phone');
                           }
                           ?>"
                           id="phone" placeholder="Phone *" required>
                    <label for="phone">Phone *</label>
                    <div class="error-text p-0 m-0">
                        <?php
                        if (form_error('phone')) {
                            echo form_error('phone');
                        }
                        ?>
                    </div>
                </div>

                <div class="pb-3"></div>

                <div class="form-floating mb-3">
                    <input name="password" type="password" class="form-control form-control_gray"
                           value="<?php
                           if (set_value('password') && !isset($success)) {
                               echo set_value('password');
                           }
                           ?>"
                           id="customerPasswodInput" placeholder="Password *" required>
                    <label for="customerPasswodInput">Password *</label>
                    <div class="error-text p-0 m-0">
                        <?php
                        if (form_error('password')) {
                            echo form_error('password');
                        }
                        ?>
                    </div>
                </div>

                <div class="d-flex align-items-center mb-3 pb-2">
                    <a href="<?php echo base_url('forgot-password') ?>" class="btn-text ms-auto">Lost password?</a>
                </div>

                <div class="mb-3">
                    <?php
                    if ($web_data[0]->captcha_visibility):
                        echo '<div class="g-recaptcha" data-sitekey="' . $web_data[0]->captcha_site_key . '"></div>';
                    endif;
                    ?>
                </div>

                <button name="register" value="send" class="btn btn-primary w-100 text-uppercase" type="submit">Register
                </button>

                <div class="customer-option mt-4 text-center">
                    <span class="text-secondary">Already have an Account?</span>
                    <a href="<?php echo base_url('user-login') ?>" class="btn-text js-show-register">Login
                        Now</a>
                </div>
            </form>
        </div>
    </section>
</main>
<div class="mb-5 pb-xl-5"></div>

<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>  
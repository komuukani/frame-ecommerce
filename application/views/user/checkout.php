<?php
echo $page_head;
$web_data = ($web_data) ? $web_data[0] : '';

$user = $this->session->userdata('email');
$userdata = $cartProduct = array();
if (isset($user)) :
    $userdata = $this->md->select_where('tbl_register', array('email' => $user));
    $cartProduct = $this->md->select_where('tbl_cart', array('register_id' => ($userdata ? $userdata[0]->register_id : '')));
else:
    $cartProduct = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));
endif;
?>
<body>
<?php echo $page_header; ?>
<main>
    <div class="mb-4 pb-4"></div>
    <section class="shop-checkout container mb-100">
        <h2 class="page-title">Shipping and Checkout</h2>

        <div class="checkout-steps">
            <a href="<?php echo base_url('cart'); ?>" class="checkout-steps__item active">
                <span class="checkout-steps__item-number">01</span>
                <span class="checkout-steps__item-title">
            <span>Shopping Bag</span>
            <em>Manage Your Items List</em>
          </span>
            </a>
            <a href="<?php echo base_url('checkout'); ?>" class="checkout-steps__item active">
                <span class="checkout-steps__item-number">02</span>
                <span class="checkout-steps__item-title">
            <span>Shipping and Checkout</span>
            <em>Checkout Your Items List</em>
          </span>
            </a>
            <a href="javascript:void(0);" class="checkout-steps__item">
                <span class="checkout-steps__item-number">03</span>
                <span class="checkout-steps__item-title">
            <span>Confirmation</span>
            <em>Review And Submit Your Order</em>
          </span>
            </a>
        </div>

        <div class="checkout-form">
            <div class="billing-info__wrapper">
                <h4>BILLING DETAILS</h4>
                <?php
                if (isset($user) && !empty($userdata)) {
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-floating my-3">
                                <input type="text" class="form-control" id="fname"
                                       name="fname"
                                       required
                                       value="<?php echo $userdata[0]->fname; ?>"
                                       placeholder="First Name">
                                <label for="fname">First Name</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating my-3">
                                <input type="text" class="form-control" id="checkout_phone" placeholder="Phone *"
                                       name="phone"
                                       readonly
                                       value="<?php echo $userdata[0]->phone; ?>">
                                <label for="checkout_phone">Phone *</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating my-3">
                                <input type="email" class="form-control" id="checkout_email" placeholder="Your Mail *"
                                       name="email"
                                       readonly
                                       value="<?php echo $userdata[0]->email; ?>"/>
                                <label for="checkout_email">Your Mail *</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-floating my-3">
                                <input type="text" class="form-control" id="city" placeholder="Town / City *"
                                       name="city"
                                       required
                                       value="<?php echo $userdata[0]->city; ?>">
                                <label for="city">Town / City *</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-floating my-3">
                                <input type="text" class="form-control" id="postal_code"
                                       name="postcode"
                                       required
                                       value="<?php echo $userdata[0]->postal_code; ?>"
                                       placeholder="Postcode / ZIP *">
                                <label for="postal_code">Postcode / ZIP *</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-floating mt-3 mb-3">
                                <input type="text" class="form-control" id="address"
                                       name="address"
                                       required
                                       value="<?php echo $userdata[0]->address; ?>"
                                       placeholder="Street Address *">
                                <label for="address">Street Address *</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-floating mt-3 mb-3">
                                <input type="text" class="form-control" id="booking_destination"
                                       name="booking_destination"
                                       required
                                       value="<?php echo $userdata[0]->address; ?>"
                                       placeholder="Booking Destination *">
                                <label for="booking_destination">Booking Destination *</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating mt-3 mb-3">
                                <input type="text" class="form-control" id="name_transport"
                                       name="name_transport"
                                       required
                                       placeholder="Name Transport *">
                                <label for="name_transport">Name of Transport *</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating mt-3 mb-3">
                                <input type="text" class="form-control" id="drop_location"
                                       name="drop_location"
                                       required
                                       placeholder="Drop Location *">
                                <label for="drop_location">Drop Location *</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating mt-3 mb-3">
                                <input type="text" class="form-control" id="name_sales_person"
                                       name="name_sales_person"
                                       required
                                       placeholder="Name of Sales Person *">
                                <label for="name_sales_person">Name of Sales Person *</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mt-3">
                            <textarea class="form-control form-control_gray" placeholder="Order Notes (optional)"
                                      id="notes"
                                      name="notes"
                                      cols="30" rows="8"></textarea>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mt-20">
                                <h2>Sign in</h2>
                                <p class="fs-16 mb-7">Don't have an account yet? <a
                                            href="<?php echo base_url('user-register'); ?>"
                                            class="text-secondary border-bottom text-decoration-none">Sign up</a> for
                                    free
                                </p>
                                <div class="mt-5 login-form">
                                    <form name="login-form" method="post" class="needs-validation" novalidate>
                                        <?php
                                        if (isset($error)) {
                                            ?>
                                            <div class="alert alert-danger p-1 mb-5">
                                                <?php echo $error; ?>
                                            </div>
                                            <?php
                                        }
                                        if (isset($success)) {
                                            ?>
                                            <div class="alert alert-success p-1 mb-5">
                                                <?php echo $success; ?>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="form-floating mb-3">
                                            <input name="username" type="email" class="form-control form-control_gray"
                                                   value="<?php
                                                   if (set_value('username') && !isset($success)) {
                                                       echo set_value('username');
                                                   }
                                                   ?>"
                                                   id="customerNameEmailInput1" placeholder="Email address *" required>
                                            <label for="customerNameEmailInput1">Email address *</label>
                                            <div class="error-text p-0 m-0">
                                                <?php
                                                if (form_error('username')) {
                                                    echo form_error('username');
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <div class="pb-3"></div>

                                        <div class="form-floating mb-3">
                                            <input name="password" type="password"
                                                   class="form-control form-control_gray"
                                                   value="<?php
                                                   if (set_value('password') && !isset($success)) {
                                                       echo set_value('password');
                                                   }
                                                   ?>"
                                                   id="customerPasswodInput" placeholder="Password *" required>
                                            <label for="customerPasswodInput">Password *</label>
                                            <div class="error-text p-0 m-0">
                                                <?php
                                                if (form_error('password')) {
                                                    echo form_error('password');
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <div class="d-flex align-items-center mb-3 pb-2">
                                            <a href="<?php echo base_url('forgot-password') ?>"
                                               class="btn-text ms-auto">Lost
                                                password?</a>
                                        </div>

                                        <div class="mb-3">
                                            <?php
                                            if ($web_data->captcha_visibility):
                                                echo '<div class="g-recaptcha" data-sitekey="' . $web_data->captcha_site_key . '"></div>';
                                            endif;
                                            ?>
                                        </div>

                                        <button name="login" value="send" class="btn btn-primary text-uppercase"
                                                type="submit">Log In
                                        </button>

                                        <div class="customer-option mt-4 text-center">
                                            <span class="text-secondary">No account yet?</span>
                                            <a href="<?php echo base_url('user-register') ?>"
                                               class="btn-text js-show-register">Create
                                                Account</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>

            <div class="checkout__totals-wrapper">
                <div class="sticky-content">
                    <div class="checkout__totals">
                        <h3>Your Order</h3>
                        <table class="checkout-cart-items">
                            <thead>
                            <tr>
                                <th>PRODUCT</th>
                                <th>QTY</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if ($cartProduct) {
                                foreach ($cartProduct as $cartitem) {
                                    $productData = $this->md->select_where('tbl_product', array('product_id' => $cartitem->product_id));
                                    if ($productData) {
                                        $product = $productData[0];
                                        $category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
                                        $photos = array_filter(explode(",", $product->photos));
                                        $url = base_url('product/' . $product->slug . "/" . $product->product_id);
                                        $tempPhoto = (file_exists('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') ? ('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') : FILENOTFOUND);
                                        ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo $url; ?>"
                                                   class="flex align-content-center">
                                                    <img src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : $tempPhoto) : $tempPhoto); ?>"
                                                         alt="<?php echo $product->title; ?>"
                                                         title="<?php echo $product->title; ?>"
                                                         width="70px"
                                                         class="mr-30"/>
                                                    <?php echo $product->title; ?>
                                                </a>
                                            </td>
                                            <td>
                                                <?php echo $cartitem->qty; ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    if (isset($user)) {
                        ?>
                        <button class="btn btn-primary w-100 text-transform-uppercase" id="addOrder" type="button">Send
                            Inquiry
                        </button>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>

<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>
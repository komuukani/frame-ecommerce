<?php
$user = $this->session->userdata('register_id');
if (isset($user)):
    $userdata = $this->md->select_where('tbl_register', array('register_id' => $user));
endif;
?>
<div class="sidemenu">
    <div class="">
        <ul class="p-0">
            <li class="<?php echo ($subpage == 'welcome') ? 'active' : ''; ?>">
                <a href="<?php echo base_url('welcome') ?>">
                    <i class="fas fa-home mr-5"></i> Dashboard
                </a>
            </li>
            <li class="<?php echo ($subpage == 'myorder') ? 'active' : ''; ?>">
                <a href="<?php echo base_url('myorder') ?>">
                    <i class="fa fa-check-double mr-5"></i> My Order
                </a>
            </li>
            <!--            <li class="--><?php //echo ($subpage == 'wishlist') ? 'active' : ''; ?><!--">-->
            <!--                <a href="--><?php //echo base_url('wishlist') ?><!--">-->
            <!--                    <i class="fa fa-heart mr-5"></i> Wishlist-->
            <!--                </a>-->
            <!--            </li>-->
            <li class="<?php echo ($subpage == 'edit_profile') ? 'active' : ''; ?>">
                <a href="<?php echo base_url('edit-profile') ?>">
                    <i class="fa fa-edit mr-5"></i> Update Profile
                </a>
            </li>
            <li class="<?php echo ($subpage == 'change_password') ? 'active' : ''; ?>">
                <a href="<?php echo base_url('change-password') ?>">
                    <i class="fa fa-lock mr-5"></i> Update Password
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('user-logout') ?>">
                    <i class="fa fa-power-off mr-5"></i> Logout
                </a>
            </li>
        </ul>
    </div>
</div> 
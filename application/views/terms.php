<?php
echo $page_head;
?>
<body>

<?php echo $page_header; ?>

<main>
    <div class="mb-4 pb-4"></div>
    <section class="about-us container">
        <div class="mw-930">
            <h2 class="page-title">TERMS & CONDITION</h2>
        </div>
        <div class="about-us__content pb-5 mb-5">
            <div class="mw-930">
                <p class="fs-6 fw-medium mb-4">
                    <?php
                    if (empty($terms)) :
                        echo "Sorry, content not available";
                    else :
                        foreach ($terms as $key => $terms_data) {
                            echo $terms_data->terms;
                        }
                    endif;
                    ?>
                </p>
            </div>
        </div>
    </section>
</main>

<div class="mb-5 pb-xl-5"></div>

<?php echo $page_footer; ?>

<?php echo $page_footerscript; ?>
</body>
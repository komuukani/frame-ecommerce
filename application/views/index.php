<?php
echo $page_head;
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$blogs = $this->md->select_limit_order('tbl_blog', 3, 'blog_id', 'desc');
$featuredProducts = $this->md->select_limit_order('tbl_product', 12, 'product_id', 'desc', array('featured' => 1, 'status' => 1));
$saleProducts = $this->md->select_limit_order('tbl_product', 12, 'product_id', 'desc', array('sale' => 1, 'status' => 1));
$category = $this->md->select_limit_order('tbl_category', 100, 'position', 'desc');
$reviews = $this->md->select('tbl_review');
$web_data = ($web_data) ? $web_data[0] : '';
$active_page = $this->uri->segment(1) ? $this->uri->segment(1) : 'index';
?>
<body>
<?php echo $page_header; ?>
<main>

    <?php $this->load->view('slider'); ?>
    <div class="mb-3 pb-3 mb-md-4 pb-md-4 mb-xl-5 pb-xl-5"></div>
    <div class="pb-1"></div>

    <!--    All product with category-->

    <section class="lookbook-collection">
        <div class="container">
            <?php
            if ($category) {
                ?>
                <div class="row">
                    <?php
                    foreach ($category as $item) {
                        $url = base_url('collection?id=' . $item->category_id);
                        $totalProduct = $this->md->select_where('tbl_product', array('category_id' => $item->category_id));
                        ?>
                        <div class="col-lg-4">
                            <div class="mb-30 border p-1 bg-DFDFDF">
                                <a href="<?php echo $url; ?>"
                                   class="border-dark lookbook-collection__item position-relative flex-grow-1 effect border-plus">
                                    <div class="lookbook-collection__item-image">
                                        <img loading="lazy"
                                             src="<?php echo base_url($item->path ? $item->path : FILENOTFOUND); ?>"
                                             title="<?php echo $item->title; ?>"
                                             alt="<?php echo $item->title; ?>">
                                    </div>
                                </a>
                                <div class="bg-F5F5F5 p-3">
                                    <p class="text-uppercase mb-1">
                                        TOTAL PRODUCTS: <?php echo $totalProduct ? count($totalProduct) : 0; ?></p>
                                    <h3><a href="<?php echo $url; ?>" class="text-000"><?php echo $item->title; ?></a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div><!-- /. Main Category row -->
                <?php
            }
            ?>
        </div><!-- /.container -->
    </section>

    <section class="products-grid container d-none">
        <h2 class="section-title text-uppercase text-center mb-1 mb-md-3 pb-xl-2 mb-xl-4">Our Trendy
            <strong>Products</strong></h2>

        <ul class="nav nav-tabs mb-3 text-uppercase justify-content-center" id="collections-tab" role="tablist">
            <?php
            if (empty($category)) {
                echo '<li><div class="alert alert-warning col-md-12">Sorry, Category not available!</div></li>';
            } else {
                foreach ($category as $key => $category_data) {
                    $url = base_url();
                    ?>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link nav-link_underscore <?php echo $key == 0 ? 'active' : ''; ?>"
                           id="collections-tab-<?php echo $category_data->category_id; ?>-trigger" data-bs-toggle="tab"
                           href="#collections-tab-<?php echo $category_data->category_id; ?>" role="tab"
                           aria-controls="collections-tab-<?php echo $category_data->category_id; ?>"
                           aria-selected="true">
                            <?php echo $category_data->title; ?>
                        </a>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>

        <div class="tab-content pt-2" id="collections-tab-content">
            <?php
            if (empty($category)) {
                echo '<li><div class="alert alert-warning col-md-12">Sorry, Category not available!</div></li>';
            } else {
                foreach ($category as $key => $category_data) {
                    $url = base_url();
                    ?>
                    <div class="tab-pane fade show <?php echo $key == 0 ? 'active' : ''; ?>"
                         id="collections-tab-<?php echo $category_data->category_id; ?>"
                         role="tabpanel"
                         aria-labelledby="collections-tab-<?php echo $category_data->category_id; ?>-trigger">

                        <div class="row">
                            <?php
                            $products = $this->md->select_limit_order('tbl_product', 12, 'product_id', 'desc', array('category_id' => $category_data->category_id, 'status' => 1));
                            if (empty($products)) {
                                echo '<div class="alert alert-warning col-md-12">Sorry, Product not available!</div>';
                            } else {
                                foreach ($products as $product) {
                                    $category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
                                    $photos = array_filter(explode(",", $product->photos));
                                    $url = base_url('product/' . $product->slug . "/" . $product->product_id);
                                    $whatsappUrl = "https://wa.me/" . $web_data->whatsapp;
                                    $tempPhoto = (file_exists('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') ? ('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') : FILENOTFOUND);
                                    ?>
                                    <div class="col-6 col-md-4 col-lg-3">
                                        <div class="product-card mb-3 mb-md-4 mb-xxl-5">
                                            <div class="pc__img-wrapper">
                                                <a href="<?php echo $url; ?>">
                                                    <img
                                                            loading="lazy"
                                                            width="330"
                                                            height="400"
                                                            class="pc__img"
                                                            src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : $tempPhoto) : $tempPhoto); ?>"
                                                            alt="<?php echo $product->title; ?>"
                                                            title="<?php echo $product->title; ?>">
                                                    <?php
                                                    if (array_key_exists(1, $photos)) {
                                                        ?>
                                                        <img
                                                                loading="lazy"
                                                                src="<?php echo base_url($photos ? ($photos[1] ? $photos[1] : $tempPhoto) : $tempPhoto); ?>"
                                                                alt="<?php echo $product->title; ?>"
                                                                title="<?php echo $product->title; ?>"
                                                                width="330"
                                                                height="400"
                                                                class="pc__img pc__img-second"
                                                        >
                                                        <?php
                                                    }
                                                    ?>
                                                </a>
                                                <form method="post" action="<?php echo base_url('Pages/addToCart'); ?>"
                                                      name="addToCartForm">
                                                    <?php
                                                    $wh['product_id'] = $product->product_id;
                                                    $wh['category_id'] = $product->category_id;
                                                    $wh['unique_id'] = $this->input->cookie('unique_id');
                                                    $exist = $this->md->select_where('tbl_cart', $wh);
                                                    ?>
                                                    <input type="hidden" name="quantity"
                                                           value="<?php echo $exist ? ($exist[0]->qty + 1) : 1; ?>"/>
                                                    <input type="hidden" name="productId"
                                                           value="<?php echo $product->product_id; ?>"/>
                                                    <input type="hidden" name="categoryId"
                                                           value="<?php echo $product->category_id; ?>"/>
                                                    <button
                                                            name="addToCart" value="addToCart" type="submit"
                                                            class="pc__atc btn anim_appear-bottom btn position-absolute border-0 text-uppercase fw-medium"
                                                            data-aside="cartDrawer" title="Add To Cart">Add To Cart
                                                    </button>
                                                </form>

                                            </div>

                                            <div class="pc__info position-relative">
                                                <p class="pc__category"><?php echo $category ? $category[0]->title : ''; ?></p>
                                                <h6 class="pc__title"><a
                                                            href="<?php echo $url; ?>"><?php echo $product->title; ?></a>
                                                </h6>
                                                <a
                                                        href="<?php echo $whatsappUrl; ?>"
                                                        target="_blank"
                                                        class="pc__btn-wl position-absolute top-0 end-0 bg-transparent border-0 js-add-wishlist"
                                                        title="Inquiry on Whatsapp">
                                                    <svg class="w-6 h-6 text-gray-800 dark:text-white"
                                                         aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                                                         width="24" height="24" fill="none" viewBox="0 0 24 24">
                                                        <path fill="currentColor" fill-rule="evenodd"
                                                              d="M12 4a8 8 0 0 0-6.895 12.06l.569.718-.697 2.359 2.32-.648.379.243A8 8 0 1 0 12 4ZM2 12C2 6.477 6.477 2 12 2s10 4.477 10 10-4.477 10-10 10a9.96 9.96 0 0 1-5.016-1.347l-4.948 1.382 1.426-4.829-.006-.007-.033-.055A9.958 9.958 0 0 1 2 12Z"
                                                              clip-rule="evenodd"/>
                                                        <path fill="currentColor"
                                                              d="M16.735 13.492c-.038-.018-1.497-.736-1.756-.83a1.008 1.008 0 0 0-.34-.075c-.196 0-.362.098-.49.291-.146.217-.587.732-.723.886-.018.02-.042.045-.057.045-.013 0-.239-.093-.307-.123-1.564-.68-2.751-2.313-2.914-2.589-.023-.04-.024-.057-.024-.057.005-.021.058-.074.085-.101.08-.079.166-.182.249-.283l.117-.14c.121-.14.175-.25.237-.375l.033-.066a.68.68 0 0 0-.02-.64c-.034-.069-.65-1.555-.715-1.711-.158-.377-.366-.552-.655-.552-.027 0 0 0-.112.005-.137.005-.883.104-1.213.311-.35.22-.94.924-.94 2.16 0 1.112.705 2.162 1.008 2.561l.041.06c1.161 1.695 2.608 2.951 4.074 3.537 1.412.564 2.081.63 2.461.63.16 0 .288-.013.4-.024l.072-.007c.488-.043 1.56-.599 1.804-1.276.192-.534.243-1.117.115-1.329-.088-.144-.239-.216-.43-.308Z"/>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div><!-- /.row -->
                        <?php
                        if (!empty($products)) {
                            ?>
                            <div class="text-center mt-2">
                                <a class="btn-link btn-link_lg default-underline text-uppercase fw-medium"
                                   href="<?php echo base_url('collection'); ?>">Discover
                                    More</a>
                            </div>
                            <?php
                        }
                        ?>
                    </div><!-- /.tab-pane fade show-->
                    <?php
                }
            }
            ?>

        </div><!-- /.tab-content pt-2 -->
    </section><!-- /.products-grid -->

    <div class="mb-3 mb-xl-5 pb-1 pb-xl-5"></div>

    <!--    Featured Product-->
    <section class="products-carousel container">
        <h2 class="section-title text-uppercase text-center mb-4 pb-xl-2 mb-xl-4">Featured <strong>Collection</strong>
        </h2>

        <div id="product_carousel" class="position-relative">
            <div class="swiper-container js-swiper-slider"
                 data-settings='{
            "autoplay": {
              "delay": 5000
            },
            "slidesPerView": 4,
            "slidesPerGroup": 4,
            "effect": "none",
            "loop": true,
            "pagination": {
              "el": "#product_carousel .products-pagination",
              "type": "bullets",
              "clickable": true
            },
            "navigation": {
              "nextEl": "#product_carousel .products-carousel__next",
              "prevEl": "#product_carousel .products-carousel__prev"
            },
            "breakpoints": {
              "320": {
                "slidesPerView": 2,
                "slidesPerGroup": 2,
                "spaceBetween": 14
              },
              "768": {
                "slidesPerView": 3,
                "slidesPerGroup": 3,
                "spaceBetween": 24
              },
              "992": {
                "slidesPerView": 4,
                "slidesPerGroup": 1,
                "spaceBetween": 30
              }
            }
          }'>
                <div class="swiper-wrapper">
                    <?php
                    if (empty($featuredProducts)) {
                        echo '<div class="alert alert-warning col-md-12">Sorry, Product not available!</div>';
                    } else {
                        foreach ($featuredProducts as $product) {
                            $category = $this->md->select_where('tbl_category', array('category_id' => $product->category_id));
                            $photos = array_filter(explode(",", $product->photos));
                            $url = base_url('product/' . $product->slug . "/" . $product->product_id);
                            $whatsappUrl = "https://wa.me/" . $web_data->whatsapp;
                            $tempPhoto = (file_exists('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') ? ('admin_asset/allPhotos/' . substr($product->title, 3) . '.jpg') : FILENOTFOUND);
                            ?>
                            <div class="swiper-slide product-card">
                                <div class="pc__img-wrapper">
                                    <a href="<?php echo $url; ?>">
                                        <img
                                                loading="lazy"
                                                width="330"
                                                height="400"
                                                class="pc__img"
                                                src="<?php echo base_url($photos ? ($photos[0] ? $photos[0] : $tempPhoto) : $tempPhoto); ?>"
                                                alt="<?php echo $product->title; ?>"
                                                title="<?php echo $product->title; ?>">
                                        <?php
                                        if (array_key_exists(1, $photos)) {
                                            ?>
                                            <img
                                                    loading="lazy"
                                                    src="<?php echo base_url($photos ? ($photos[1] ? $photos[1] : $tempPhoto) : $tempPhoto); ?>"
                                                    alt="<?php echo $product->title; ?>"
                                                    title="<?php echo $product->title; ?>"
                                                    width="330"
                                                    height="400"
                                                    class="pc__img pc__img-second"
                                            >
                                            <?php
                                        }
                                        ?>
                                    </a>
                                    <form method="post" action="<?php echo base_url('Pages/addToCart'); ?>"
                                          name="addToCartForm">
                                        <?php
                                        $wh['product_id'] = $product->product_id;
                                        $wh['category_id'] = $product->category_id;
                                        $wh['unique_id'] = $this->input->cookie('unique_id');
                                        $exist = $this->md->select_where('tbl_cart', $wh);
                                        ?>
                                        <input type="hidden" name="quantity"
                                               value="<?php echo $exist ? ($exist[0]->qty + 1) : 1; ?>"/>
                                        <input type="hidden" name="productId"
                                               value="<?php echo $product->product_id; ?>"/>
                                        <input type="hidden" name="categoryId"
                                               value="<?php echo $product->category_id; ?>"/>
                                        <button
                                                name="addToCart" value="addToCart" type="submit"
                                                class="pc__atc btn anim_appear-bottom btn position-absolute border-0 text-uppercase fw-medium"
                                                data-aside="cartDrawer" title="Add To Cart">Add To Cart
                                        </button>
                                    </form>

                                </div>

                                <div class="pc__info position-relative">
                                    <p class="pc__category"><?php echo $category ? $category[0]->title : ''; ?></p>
                                    <h6 class="pc__title"><a
                                                href="<?php echo $url; ?>"><?php echo $product->title; ?></a>
                                    </h6>
                                    <a
                                            href="<?php echo $whatsappUrl; ?>"
                                            target="_blank"
                                            class="pc__btn-wl position-absolute top-0 end-0 bg-transparent border-0 js-add-wishlist"
                                            title="Inquiry on Whatsapp">
                                        <svg class="w-6 h-6 text-gray-800 dark:text-white"
                                             aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                                             width="24" height="24" fill="none" viewBox="0 0 24 24">
                                            <path fill="currentColor" fill-rule="evenodd"
                                                  d="M12 4a8 8 0 0 0-6.895 12.06l.569.718-.697 2.359 2.32-.648.379.243A8 8 0 1 0 12 4ZM2 12C2 6.477 6.477 2 12 2s10 4.477 10 10-4.477 10-10 10a9.96 9.96 0 0 1-5.016-1.347l-4.948 1.382 1.426-4.829-.006-.007-.033-.055A9.958 9.958 0 0 1 2 12Z"
                                                  clip-rule="evenodd"/>
                                            <path fill="currentColor"
                                                  d="M16.735 13.492c-.038-.018-1.497-.736-1.756-.83a1.008 1.008 0 0 0-.34-.075c-.196 0-.362.098-.49.291-.146.217-.587.732-.723.886-.018.02-.042.045-.057.045-.013 0-.239-.093-.307-.123-1.564-.68-2.751-2.313-2.914-2.589-.023-.04-.024-.057-.024-.057.005-.021.058-.074.085-.101.08-.079.166-.182.249-.283l.117-.14c.121-.14.175-.25.237-.375l.033-.066a.68.68 0 0 0-.02-.64c-.034-.069-.65-1.555-.715-1.711-.158-.377-.366-.552-.655-.552-.027 0 0 0-.112.005-.137.005-.883.104-1.213.311-.35.22-.94.924-.94 2.16 0 1.112.705 2.162 1.008 2.561l.041.06c1.161 1.695 2.608 2.951 4.074 3.537 1.412.564 2.081.63 2.461.63.16 0 .288-.013.4-.024l.072-.007c.488-.043 1.56-.599 1.804-1.276.192-.534.243-1.117.115-1.329-.088-.144-.239-.216-.43-.308Z"/>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div><!-- /.swiper-wrapper -->
            </div><!-- /.swiper-container js-swiper-slider -->

            <div
                    class="products-carousel__prev position-absolute top-50 d-flex align-items-center justify-content-center">
                <svg width="25" height="25" viewBox="0 0 25 25" xmlns="http://www.w3.org/2000/svg">
                    <use href="#icon_prev_md"/>
                </svg>
            </div><!-- /.products-carousel__prev -->
            <div
                    class="products-carousel__next position-absolute top-50 d-flex align-items-center justify-content-center">
                <svg width="25" height="25" viewBox="0 0 25 25" xmlns="http://www.w3.org/2000/svg">
                    <use href="#icon_next_md"/>
                </svg>
            </div><!-- /.products-carousel__next -->

            <div class="products-pagination mt-4 mb-5 d-flex align-items-center justify-content-center"></div>
            <!-- /.products-pagination -->
        </div><!-- /.position-relative -->

    </section><!-- /.products-carousel container -->

    <div class="mb-4 pb-4 pb-xl-5 mb-xl-5"></div>

    <!--    Deal of the day-->
    <section class="deal-timer position-relative d-flex align-items-end overflow-hidden"
             style="background-color: #ebebeb;">
        <div class="background-img" style="background-image: url('images/deal1.jpg');"></div>

        <div class="deal-timer-wrapper container position-relative">
            <div class="deal-timer__content pb-2 mb-3 pb-xl-5 mb-xl-3 mb-xxl-5">
                <p class="text_dash text-uppercase text-red fw-medium">Deal of the week</p>
                <h3 class="h1 text-uppercase"><strong>Spring</strong> Collection</h3>
                <a href="<?php echo base_url('collection'); ?>"
                   class="btn-link default-underline text-uppercase fw-medium mt-3">Shop Now</a>
            </div>

            <div class="position-relative d-flex align-items-center text-center pt-xxl-4 js-countdown"
                 data-date="18-5-2025" data-time="06:50">
                <div class="day countdown-unit">
                    <span class="countdown-num d-block"></span>
                    <span class="countdown-word fw-bold text-uppercase text-secondary">Days</span>
                </div>

                <div class="hour countdown-unit">
                    <span class="countdown-num d-block"></span>
                    <span class="countdown-word fw-bold text-uppercase text-secondary">Hours</span>
                </div>

                <div class="min countdown-unit">
                    <span class="countdown-num d-block"></span>
                    <span class="countdown-word fw-bold text-uppercase text-secondary">Mins</span>
                </div>

                <div class="sec countdown-unit">
                    <span class="countdown-num d-block"></span>
                    <span class="countdown-word fw-bold text-uppercase text-secondary">Sec</span>
                </div>
            </div>
        </div><!-- /.deal-timer-wrapper -->
    </section><!-- /.deal-timer -->

    <div class="mb-3 mb-xl-5 pb-1 pb-xl-5"></div>

    <!--    Shipping & Other info-->
    <section class="service-promotion container mb-md-4 pb-md-4 mb-xl-5">
        <div class="row">
            <div class="col-md-4 text-center mb-5 mb-md-0">
                <div class="service-promotion__icon mb-4">
                    <svg width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use href="#icon_shipping"/>
                    </svg>
                </div>
                <h3 class="service-promotion__title h5 text-uppercase">Fast And Free Delivery</h3>
                <p class="service-promotion__content text-secondary">Free delivery for all orders over $140</p>
            </div><!-- /.col-md-4 text-center-->

            <div class="col-md-4 text-center mb-5 mb-md-0">
                <div class="service-promotion__icon mb-4">
                    <svg width="53" height="52" viewBox="0 0 53 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use href="#icon_headphone"/>
                    </svg>
                </div>
                <h3 class="service-promotion__title h5 text-uppercase">24/7 Customer Support</h3>
                <p class="service-promotion__content text-secondary">Friendly 24/7 customer support</p>
            </div><!-- /.col-md-4 text-center-->

            <div class="col-md-4 text-center mb-4 pb-1 mb-md-0">
                <div class="service-promotion__icon mb-4">
                    <svg width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use href="#icon_shield"/>
                    </svg>
                </div>
                <h3 class="service-promotion__title h5 text-uppercase">Money Back Guarantee</h3>
                <p class="service-promotion__content text-secondary">We return money within 30 days</p>
            </div><!-- /.col-md-4 text-center-->
        </div><!-- /.row -->
    </section><!-- /.service-promotion container -->
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>

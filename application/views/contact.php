<?php
echo $page_head;
$admin_data = $this->md->select('tbl_web_data')[0];
$web_data = ($web_data) ? $web_data[0] : '';
?>
<body>
<?php echo $page_header; ?>

<main>
    <div class="mb-4 pb-4"></div>
    <section class="contact-us container">
        <div class="mw-930">
            <h2 class="page-title">CONTACT US</h2>
        </div>
    </section>

    <section class="google-map mb-1">
        <div class="google-map__wrapper">
            <?php echo($admin_data->map); ?>
        </div>
    </section>

    <section class="">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <h3 class="mb-5">Get In Touch</h3>

                    <div class="d-flex justify-content-between mt-5 mb-5">
                        <div>
                            <b>
                                <svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true"
                                     xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none"
                                     viewBox="0 0 24 24">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="2" d="M12 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="2"
                                          d="M17.8 13.938h-.011a7 7 0 1 0-11.464.144h-.016l.14.171c.1.127.2.251.3.371L12 21l5.13-6.248c.194-.209.374-.429.54-.659l.13-.155Z"/>
                                </svg>
                                Address:</b>
                            <p class="mt-20"> <?php echo($admin_data->address); ?>
                            </p>
                        </div>
                        <div>
                            <b>
                                <svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true"
                                     xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none"
                                     viewBox="0 0 24 24">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="2"
                                          d="M21 8v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8m18 0-8.029-4.46a2 2 0 0 0-1.942 0L3 8m18 0-9 6.5L3 8"/>
                                </svg>
                                Email:</b>
                            <p class="mt-20">
                                <a
                                    href="mailto:"><?php echo($admin_data->email_address); ?></a>
                            </p>
                        </div>
                        <div>
                            <b>
                                <svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true"
                                     xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none"
                                     viewBox="0 0 24 24">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="2"
                                          d="M18.427 14.768 17.2 13.542a1.733 1.733 0 0 0-2.45 0l-.613.613a1.732 1.732 0 0 1-2.45 0l-1.838-1.84a1.735 1.735 0 0 1 0-2.452l.612-.613a1.735 1.735 0 0 0 0-2.452L9.237 5.572a1.6 1.6 0 0 0-2.45 0c-3.223 3.2-1.702 6.896 1.519 10.117 3.22 3.221 6.914 4.745 10.12 1.535a1.601 1.601 0 0 0 0-2.456Z"/>
                                </svg>
                                Phone:</b>
                            <p class="mt-20">
                                <a
                                    href="tel:<?php echo($admin_data->phone); ?>"><?php echo($admin_data->phone); ?></a>
                            </p>
                        </div>
                    </div>

                    <h2 class="mb-11 font-18">Message form:</h2>
                    <form class="contact-form" method="post">
                        <?php
                        if (isset($error)) {
                            ?>
                            <div class="alert alert-danger p-1">
                                <?php echo $error; ?>
                            </div>
                            <?php
                        }
                        if (isset($success)) {
                            ?>
                            <div class="alert alert-success p-1">
                                <?php echo $success; ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-md-6 col-12 mb-3">
                                <input type="text" class="form-control" placeholder="Enter Extra field"
                                       name="extra_field" style="display: none !important"/>
                                <input type="text" class="form-control input-focus" name="fname" placeholder="Name"
                                       value="<?php
                                       if (set_value('fname') && !isset($success)) {
                                           echo set_value('fname');
                                       }
                                       ?>">
                                <div class="error-text">
                                    <?php
                                    if (form_error('fname')) {
                                        echo form_error('fname');
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-12 mb-3">
                                <input type="email" class="form-control input-focus" placeholder="Email *"
                                       name="email"
                                       value="<?php
                                       if (set_value('email') && !isset($success)) {
                                           echo set_value('email');
                                       }
                                       ?>">
                                <div class="error-text">
                                    <?php
                                    if (form_error('email')) {
                                        echo form_error('email');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <input type="tel" class="form-control input-focus mb-4" placeholder="Phone number"
                               name="phone"
                               value="<?php
                               if (set_value('phone') && !isset($success)) {
                                   echo set_value('phone');
                               }
                               ?>">
                        <div class="error-text">
                            <?php
                            if (form_error('phone')) {
                                echo form_error('phone');
                            }
                            ?>
                        </div>
                        <textarea class="form-control mb-3 input-focus" placeholder="Comment" rows="7"
                                  name="message"><?php
                            if (set_value('message') && !isset($success)) {
                                echo set_value('message');
                            }
                            ?></textarea>
                        <div class="error-text">
                            <?php
                            if (form_error('message')) {
                                echo form_error('message');
                            }
                            ?>
                        </div>
                        <div class="col-md-12 mt-8 mb-15">
                            <?php
                            if ($admin_data->captcha_visibility) :
                                echo '<div class="g-recaptcha" data-sitekey="' . $admin_data->captcha_site_key . '"></div>';
                            endif;
                            ?>
                        </div>

                        <button type="submit" value="send" name="send" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>


<main class="main mt-100 mt-xs-50">

</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>
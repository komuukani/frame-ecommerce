<?php
echo $page_head;
?>
<body class="home">
<div class="page-wrapper">
    <?php echo $page_header; ?>
    <main class="main mt-100 mt-xs-50">
        <section class="pt-50 pb-50">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="text-left">
                            <h2 class="font-58 font-weight-bold">Shipping Policy</h2>
                        </div>
                        <div>
                            <p style="font-size: 16.8px">
                                1. infiniti is responsible to fulfil your order within its designated time-line of 9-10
                                days in Riyadh and/or 10-12 day anywhere outside Riyadh city, kingdom-wide
                                <br/><br/>
                                2. Customers shall be informed by e-mail and/or mobile once their order is ready for
                                delivery
                                <br/><br/>
                                3. infiniti's customer support shall communicate with the customer, prior the delivery,
                                to confirm the delivery appointment and get the customer's location map on Whatsapp
                                <br/><br/>
                                4. Customers remain responsible to provide accurate and correct information in
                                correlation to their delivery location
                                <br/><br/>
                                5. infiniti does not allow any incidents to delay and/or stretch a customer delivery
                                schedule, and the customer is responsible to be available for the delivery at his
                                location as per the schedule date/time.
                                <br/><br/>
                                6. In the event, where a customer does not show up on the scheduled date and time of
                                delivery; infiniti shall reschedule the delivery against a new additional shipment fee.
                                <br/><br/>
                                7. In the event, where a customer does not accept to receive his shipment, due to a
                                mistake in order and/or product fault; infiniti shall reschedule the delivery at the
                                earliest without any extra charges.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>
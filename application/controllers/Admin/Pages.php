<?php
/* * *
 * Project:    Enzo Admin Pro
 * Name:       Admin Side Pages
 * Package:    Pages.php
 * About:      A controller that handle backend of all pages
 * Copyright:  (C) 2022
 * Author:     Nishant Thummar
 * Website:    https://nishantthummar.in/
 * * */
defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends CI_Controller
{

    // Global Variables
    public $website, $developer, $web_link, $table_prefix, $web_data, $admin_data, $savepath = './admin_asset/';

    // Constructor
    public function __construct()
    {
        parent::__construct();
        $this->developer = DEVELOPER;
        $this->web_link = WEBLINK;
        $this->table_prefix = TABLE_PREFIX;     // SET TABLE PREFIX (TBL_) 
        // Check if database already connected or not [STATUS = PRE/POST] 
        $settings = INSTALLER_SETTING;
        if ($settings['status'] == "pre") {
            redirect('/install');
        }
        $this->web_data = $this->md->select('tbl_web_data');    // SELECT WEBSITE DATA
        if ($this->session->userdata('aemail') == USERNAME) {
            $this->admin_data = array((object)USERDATA);
        } else {
            $this->admin_data = $this->md->select_where('tbl_admin', array('email' => $this->session->userdata('aemail'))); // SELECT ADMIN DATA
        }
        date_default_timezone_set(($this->web_data) ? $this->md->getItemName('tbl_timezone', 'timezone_id', 'timezone', $this->web_data[0]->timezone) : DEFAULT_TIMEZONE);
    }

    // Check login security

    public function statusUpdate()
    {
        if (web_status()):
            $table = $this->input->post('tbl'); // table name
            $id = $this->input->post('id'); // item id
            $type = $this->input->post('type'); // active/inactive
//            $mail_status = $this->input->post('mail'); // if status is true then mail send, if false then no action.

//            $web_data = $this->web_data;    //  get all website data for email sending
//            $protocol = $web_data[0]->mail_protocol;    // Protocol
//            $host = $web_data[0]->mail_smtp_host;   // Host Email Service Provider
//            $sender = $web_data[0]->mail_email;     // Sender Email
//            $password = $web_data[0]->mail_password;    // Sender Password
//            $port = $web_data[0]->mail_port;    // PORT

            $this->md->update($this->table_prefix . $table, array('status' => ($type == "active") ? 1 : 0), array($table . '_id' => $id));
            // if ($mail_status):
            //     $item_data = $this->md->select_where($this->table_prefix . $table, array($table . '_id' => $id));
            //     if (!empty($item_data)):
            //         if ($type == 'active'):
            //             $subject = $web_data[0]->active_mail_subject;   // active mail subject from website data table
            //             $message = $web_data[0]->active_mail_message;    // active mail message from website data table
            //         else:
            //             $subject = $web_data[0]->inactive_mail_subject;  // inactive mail subject from website data table
            //             $message = $web_data[0]->inactive_mail_message;  // inactive mail message from website data table
            //         endif;
            //         $web_data && $this->md->sendMail($protocol, $host, $sender, $password, $port, $subject, $message, $item_data[0]->email); // Send mail
            //     endif;
            // endif;
            echo "true";
        else:
            echo "false";
        endif;
    }

    // Common Function for setting up global page variable and send it to page.

    public function banner()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-image font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                if (web_status()):
                    /* $this->form_validation->set_rules('title', 'Set Banner Title', 'required', array("required" => "Enter Banner Title!"));
                      $this->form_validation->set_rules('subtitle', 'Set Banner Sub Title', 'required', array("required" => "Enter Banner Sub Title!"));
                      if ($this->form_validation->run() == TRUE) { */
                    $dt['title'] = $this->input->post('title');
                    $dt['position'] = $this->input->post('position');
                    $dt['subtitle'] = $this->input->post('subtitle');
                    (!empty($_FILES['banner']['name']) && $dt['path'] = $this->md->uploadFile('banner')); // check if file is selected than only it will upload
                    $this->md->insert($this->table_prefix . $class, $dt);
                    $this->session->set_flashdata('success', 'Yeah, Banner added successfully!');
                    redirect($data['current_page'] . "/" . $data['page_type']);
                endif;
            }   // Add Data
            if ($this->input->post('update')) {
                if (web_status()):
                    // Check already photo is exist or not, if yes than it will remove and than upload.
                    $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                    $dt['title'] = $this->input->post('title');
                    $dt['position'] = $this->input->post('position');
                    $dt['subtitle'] = $this->input->post('subtitle');
                    $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('banner'));    // Upload Photo and return path from model
                    if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                        redirect('manage-' . $class . "/show");
                    }
                endif;
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Fetch Data from model, than it will return data to the function

    protected function setParameter($path, $page, $title, $page_breadcumb, $breadcrumb_icon, $datatables)
    {
        $this->security();
        $control_data = $this->md->select_where($this->table_prefix . 'control', array('control_id' => 1));
        if ($this->db->field_exists($page, $this->table_prefix . 'control') && !$control_data[0]->$page)
            redirect('authorize');
        $data = [];
        $data['page_title'] = ucwords($title);  // set page title
        $data['web_data'] = $this->web_data;    // get all Website data
        $data['admin_data'] = $this->admin_data;    // get all Admin data
        $data['website_title'] = ($this->web_data) ? $this->web_data[0]->project_name : 'Admin Panel'; // get project name from admin table
        $data['developer'] = $this->developer;  // get developer name
        $data['web_link'] = $this->web_link;    // get developer website link
        $data['page'] = $path . '/' . $page;   //  open page from admin folder
        $data['active_page'] = $page;   // get active page name
        $data['current_page'] = $this->uri->segment(1);     // get current page
        $data['page_type'] = ($this->uri->segment(2) ? $this->uri->segment(2) : '');  // Add/Edit/Show
        $data['header'] = true;
        $data['sidebar'] = true;
        $data['breadcrumb'] = $page_breadcumb;
        $data['breadcrumb_icon'] = $breadcrumb_icon;
        $data['controller'] = $this;
        $data['datatables'] = $datatables;
        if ($this->db->table_exists($this->table_prefix . $page))
            $data[$page] = $this->md->select($this->table_prefix . $page);
        return $data;
    }

    // Check User has permission to access page

    public function security()
    {
        if ($this->session->userdata('aemail') == "")
            redirect('authorize');
    }

    // Update status to active or inactive in common function

    public function check_permission($page)
    {
        $all = $read = $write = $edit = $delete = $status = false;
        if ($this->admin_data):
            if ($this->admin_data[0]->admin_id != 1):
                $admin_menu = $this->md->select_where('tbl_permission_assign', array('role_id' => $this->admin_data[0]->user_role_id, 'type' => 'page'));   // get current role's permisison
                $per_id = array();
                foreach ($admin_menu as $ext_per) {
                    $per = json_decode($ext_per->permission_id);
                    foreach ($per as $per_val):
                        $exist_prmsn = $this->md->select_where('tbl_permission', array('permission_id' => $per_val));
                        if ($exist_prmsn):
                            if ($exist_prmsn[0]->feature == $page):
                                if ($ext_per->all):
                                    $all = true;
                                endif;
                                if ($ext_per->read):
                                    $read = true;
                                endif;
                                if ($ext_per->write):
                                    $write = true;
                                endif;
                                if ($ext_per->edit):
                                    $edit = true;
                                endif;
                                if ($ext_per->delete):
                                    $delete = true;
                                endif;
                                if ($ext_per->status):
                                    $status = true;
                                endif;
                            endif;
                        endif;
                    endforeach;
                }
                $grant_permission = array('all' => $all, 'read' => $read, 'write' => $write, 'edit' => $edit, 'delete' => $delete, 'status' => $status); // other role
            else:
                $grant_permission = array('all' => true, 'read' => true, 'write' => true, 'edit' => true, 'delete' => true, 'status' => true);   // super admin
            endif;
        endif;
        return $grant_permission;
    }

    /*
      S I T E  -  P A G E S
     */

    // Banner - Sliders

    public function fetchData($class, $path)
    {
        $this->load->model('Admin/' . $path . '/' . ucfirst($class), $class);
        $postData = $this->input->post();  // get data from ajax call // POST data
        if ($postData):
            $data = $this->$class->getData($postData); // Get data
        endif;
        return json_encode($data);
    }

    // Blog

    public function blog()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-clipboard font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'Set Blog Title', 'required', array("required" => "Enter Blog Title!"));
                $this->form_validation->set_rules('about', 'Set Blog Description', 'required', array("required" => "Enter Blog Description!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['title'] = $this->input->post('title');
                        $dt['slug'] = $this->input->post('slug') ? $this->input->post('slug') : $this->md->generateSeoURL($this->input->post('title'));
                        (!empty($_FILES['blog']['name']) && $dt['path'] = $this->md->uploadFile('blog')); // check if file is selected than only it will upload
                        $dt['description'] = $this->input->post('about');
                        $dt['meta_title'] = $this->input->post('meta_title');
                        $dt['meta_keyword'] = $this->input->post('meta_keyword');
                        $dt['meta_desc'] = $this->input->post('meta_desc');
                        $dt['blogdate'] = date('Y-m-d H:i:s');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'Set Blog Title', 'required', array("required" => "Enter Blog Title!"));
                $this->form_validation->set_rules('about', 'Set Blog Description', 'required', array("required" => "Enter Blog Description!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['title'] = $this->input->post('title');
                        $dt['slug'] = $this->input->post('slug') ? $this->input->post('slug') : $this->md->generateSeoURL($this->input->post('title'));
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('blog'));    // Upload photo and return path from model
                        $dt['description'] = $this->input->post('about');
                        $dt['meta_title'] = $this->input->post('meta_title');
                        $dt['meta_keyword'] = $this->input->post('meta_keyword');
                        $dt['meta_desc'] = $this->input->post('meta_desc');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Client
    public function client()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-image font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'Set Client Title', 'required', array("required" => "Enter Client Title!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['title'] = $this->input->post('title');
                        (!empty($_FILES['client']['name']) && $dt['path'] = $this->md->uploadFile('client')); // check if file is selected than only it will upload
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, Client added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'Set Client Title', 'required', array("required" => "Enter Client Title!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['title'] = $this->input->post('title');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('client'));    // Upload Photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Contact
    public function contact()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage Contact Data", TRUE, "fas fa-headphones-alt font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }


    // Email Subscriber - Newsletter
    public function email()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage Email Subscriber", TRUE, "fas fa-envelope-open-text font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($this->input->post('send_mail')) {
                $this->form_validation->set_rules('subject', 'Subject', 'required', array("required" => "Enter Subject!"));
                $this->form_validation->set_rules('message', 'Message', 'required', array("required" => "Enter Message!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $emails = $this->input->post('email');
                        if (!empty($emails)):
                            foreach ($emails as $eml):
                                $subject = $this->input->post('subject');
                                $message = $this->input->post('message');
                                if ($data['web_data']):
                                    $protocol = $data['web_data'][0]->mail_protocol;    // Protocol
                                    $host = $data['web_data'][0]->mail_smtp_host;   // Host Email Service Provider
                                    $sender = $data['web_data'][0]->mail_email;  // Sender Email
                                    $password = $data['web_data'][0]->mail_password;    // Sender Password
                                    $port = $data['web_data'][0]->mail_port;    // PORT
                                    $this->md->sendMail($protocol, $host, $sender, $password, $port, $subject, $message, $eml); // Send mail
                                endif;
                            endforeach;
                            $this->session->set_flashdata('success', 'Yeah, mail sent successfully.');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        else:
                            $this->session->set_flashdata('error', 'Sorry, select atleast one email!');
                        endif;
                    endif;
                }
            }   // Add Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Feedback
    public function feedback()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-comment-dots font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Gallery - Sliders
    public function gallery()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-image font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                if (web_status()):
                    /* $this->form_validation->set_rules('title', 'Set Gallery Title', 'required', array("required" => "Enter Gallery Title!"));
                      if ($this->form_validation->run() == TRUE) { */
                    $dt['title'] = $this->input->post('title');
                    (!empty($_FILES['gallery']['name']) && $dt['path'] = $this->md->uploadFile('gallery')); // check if file is selected than only it will upload
                    $this->md->insert($this->table_prefix . $class, $dt);
                    $this->session->set_flashdata('success', 'Yeah, Gallery added successfully!');
                    redirect($data['current_page'] . "/" . $data['page_type']);
                endif;
            }   // Add Data
            if ($this->input->post('update')) {
                if (web_status()):
                    $dt['title'] = $this->input->post('title');
                    // Check already photo is exist or not, if yes than it will remove and than upload.
                    $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                    $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('gallery'));    // Upload Photo and return path from model
                    if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                        redirect('manage-' . $class . "/show");
                    }
                endif;
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Inquiry
    public function inquiry()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-comment-dots font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Careers
    public function careers()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fas fa-user-check font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Review
    public function review()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-comments font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('username', 'Set Review Title', 'required', array("required" => "Enter Username!"));
                $this->form_validation->set_rules('msg', 'Set Review', 'required', array("required" => "Enter Review!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['username'] = $this->input->post('username');
                        $dt['review'] = $this->input->post('msg');
                        (!empty($_FILES['review']['name']) && $dt['path'] = $this->md->uploadFile('review')); // check if file is selected than only it will upload
                        $dt['datetime'] = date('Y-m-d');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('username', 'Set Review Title', 'required', array("required" => "Enter Username!"));
                $this->form_validation->set_rules('msg', 'Set Review', 'required', array("required" => "Enter Review!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['username'] = $this->input->post('username');
                        $dt['datetime'] = $this->input->post('datetime');
                        $dt['review'] = $this->input->post('msg');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('review'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Faq
    public function faq()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fas fa-question-circle font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('question', 'Set Faq Question', 'required', array("required" => "Enter Question!"));
                $this->form_validation->set_rules('answer', 'Set Faq Answer', 'required', array("required" => "Enter Answer!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['question'] = $this->input->post('question');
                        $dt['answer'] = $this->input->post('answer');
                        $dt['entry_date'] = date('Y-m-d');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('question', 'Set Faq Question', 'required', array("required" => "Enter Question!"));
                $this->form_validation->set_rules('answer', 'Set Faq Answer', 'required', array("required" => "Enter Answer!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['question'] = $this->input->post('question');
                        $dt['answer'] = $this->input->post('answer');
                        $dt['modify_date'] = date('Y-m-d');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // SEO
    public function seo()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fas fa-angle-double-up font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('page', 'Select Page Title', 'required', array("required" => "Select Page Title!"));
                $this->form_validation->set_rules('meta_title', 'Set Meta Title', 'required', array("required" => "Enter Meta Title!"));
                $this->form_validation->set_rules('meta_keyword', 'Set Meta Keyword', 'required', array("required" => "Enter SEO Keyword!"));
                $this->form_validation->set_rules('meta_desc', 'Set Meta Description', 'required', array("required" => "Enter Meta Description!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['page'] = $this->input->post('page');
                        $dt['title'] = $this->input->post('meta_title');
                        $dt['description'] = $this->input->post('meta_desc');
                        $dt['keyword'] = $this->input->post('meta_keyword');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('page', 'Select Page Title', 'required', array("required" => "Select Page Title!"));
                $this->form_validation->set_rules('meta_title', 'Set Meta Title', 'required', array("required" => "Enter Meta Title!"));
                $this->form_validation->set_rules('meta_keyword', 'Set Meta Keyword', 'required', array("required" => "Enter SEO Keyword!"));
                $this->form_validation->set_rules('meta_desc', 'Set Meta Description', 'required', array("required" => "Enter Meta Description!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['page'] = $this->input->post('page');
                        $dt['title'] = $this->input->post('meta_title');
                        $dt['description'] = $this->input->post('meta_desc');
                        $dt['keyword'] = $this->input->post('meta_keyword');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Team
    public function team()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fas fa-users font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', '', 'required', array("required" => "Enter User FullName!"));
                $this->form_validation->set_rules('email', '', 'required', array("required" => "Enter Email!"));
                $this->form_validation->set_rules('phone', '', 'required', array("required" => "Enter Phone!"));
                /* $this->form_validation->set_rules('facebook', '', 'required', array("required" => "Enter Facebook Link!"));
                  $this->form_validation->set_rules('instagram', '', 'required', array("required" => "Enter Instagram Link!"));
                  $this->form_validation->set_rules('linkedin', '', 'required', array("required" => "Enter Linkedin Link!")); */
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['name'] = $this->input->post('title');
                        $dt['email'] = $this->input->post('email');
                        $dt['phone'] = $this->input->post('phone');
                        $dt['facebook'] = $this->input->post('facebook');
                        $dt['instagram'] = $this->input->post('instagram');
                        $dt['twitter'] = $this->input->post('twitter');
                        $dt['linkedin'] = $this->input->post('linkedin');
                        (!empty($_FILES['team']['name']) && $dt['path'] = $this->md->uploadFile('team')); // check if file is selected than only it will upload
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', '', 'required', array("required" => "Enter User FullName!"));
                $this->form_validation->set_rules('email', '', 'required', array("required" => "Enter Email!"));
                $this->form_validation->set_rules('phone', '', 'required', array("required" => "Enter Phone!"));
                /* $this->form_validation->set_rules('facebook', '', 'required', array("required" => "Enter Facebook Link!"));
                  $this->form_validation->set_rules('instagram', '', 'required', array("required" => "Enter Instagram Link!"));
                  $this->form_validation->set_rules('linkedin', '', 'required', array("required" => "Enter Linkedin Link!")); */
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['name'] = $this->input->post('title');
                        $dt['email'] = $this->input->post('email');
                        $dt['phone'] = $this->input->post('phone');
                        $dt['facebook'] = $this->input->post('facebook');
                        $dt['instagram'] = $this->input->post('instagram');
                        $dt['twitter'] = $this->input->post('twitter');
                        $dt['linkedin'] = $this->input->post('linkedin');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('team'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Video
    public function video()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fas fa-play-circle font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('brand_id', '', 'required', array("required" => "Select Brand!"));
                $this->form_validation->set_rules('title', 'Set Video Title', 'required', array("required" => "Enter Video Title!"));
                $this->form_validation->set_rules('video', 'Set Video Path', 'required', array("required" => "Enter Video Path!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['brand_id'] = $this->input->post('brand_id');
                        $dt['title'] = $this->input->post('title');
                        $dt['video'] = $this->input->post('video');
                        $dt['status'] = 1;
                        $dt['entry_date'] = date('Y-m-d');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('brand_id', '', 'required', array("required" => "Select Brand!"));
                $this->form_validation->set_rules('title', 'Set Video Title', 'required', array("required" => "Enter Video Title!"));
                $this->form_validation->set_rules('video', 'Set Video Path', 'required', array("required" => "Enter Video Path!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['brand_id'] = $this->input->post('brand_id');
                        $dt['title'] = $this->input->post('title');
                        $dt['video'] = $this->input->post('video');
                        $dt['modify_date'] = date('Y-m-d');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, enter proper data!');
                }
            }   // Update Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    /*
      I N F O  -  P A G E S
     */

    // About us
    public function aboutus()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/info", $class, "Manage " . $class, TRUE, "far fa-question-circle font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'info');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif;  // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('about', 'About Us', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['about'] = $this->input->post('about');
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, something went wrong!');
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, insert proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('about', 'About Us', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['about'] = $this->input->post('about');
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Update Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Policy
    public function policy()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/info", $class, "Manage Privacy & Policy", TRUE, "far fa-question-circle font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'info');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif;  // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('policy', 'Privacy & Policy', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['policy'] = $this->input->post('policy');
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, something went wrong!');
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, insert proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('policy', 'Privacy & Policy', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['policy'] = $this->input->post('policy');
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Update Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Terms
    public function terms()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/info", $class, "Manage Terms & Condition", TRUE, "far fa-question-circle font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'info');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif;  // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('terms', 'Terms & Condition', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['terms'] = $this->input->post('terms');
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, something went wrong!');
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, insert proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('terms', 'Terms & Condition', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['terms'] = $this->input->post('terms');
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Update Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    /*
      C A T E G O R I E S   -  P A G E S
     */

    // Category
    public function category()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/categories", $class, "Manage Category", TRUE, "far fa-list-alt font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'categories');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'Title', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['title'] = $this->input->post('title');
                        $dt['position'] = $this->input->post('position');
                        (!empty($_FILES['category']['name']) && $dt['path'] = $this->md->uploadFile('category')); // check if file is selected than only it will upload
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'Title', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['title'] = $this->input->post('title');
                        $dt['position'] = $this->input->post('position');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('category'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Edit Data
            if ($this->input->post('importData')) {
                $path = './admin_asset/importData/';
                require_once APPPATH . "/third_party/PHPExcel.php";

                $config['upload_path'] = $path;
                if (!is_dir($config['upload_path']))
                    mkdir($config['upload_path'], 0777, TRUE); // Make a folder if not exists
                $config['allowed_types'] = 'xlsx|xls|csv';
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('importedFile')) {
                    $this->session->set_flashdata('error', 'Something went wrong!');
                } else {
                    $excelData = array('upload_data' => $this->upload->data());
                }
                try {
                    if (empty($error)) {
                        if (!empty($excelData['upload_data']['file_name'])) {
                            $import_xls_file = $excelData['upload_data']['file_name'];
                        } else {
                            $import_xls_file = 0;
                        }
                        $inputFileName = $path . $import_xls_file;
                        try {
                            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                            $objPHPExcel = $objReader->load($inputFileName);
                            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                            $flag = true;
                            $i = 0;
                            foreach ($allDataInSheet as $value) {
                                if ($flag) {
                                    $flag = false;
                                    continue;
                                }
                                $inserdata[$i]['title'] = $value['A'];
                                $inserdata[$i]['position'] = $value['B'];
                                $i++;
                            }
                            $result = $this->md->insert_import('tbl_category', $inserdata);
                            if ($result) {
                                $this->session->set_flashdata('success', 'Yeah, Category imported successfully.');
                            } else {
                                $this->session->set_flashdata('error', 'Something went wrong!');
                            }
                        } catch (Exception $e) {
                            $this->session->set_flashdata('error', 'Something went wrong!');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Something went wrong!');
                    }
                } catch (Exception $e) {
                    $this->session->set_flashdata('error', 'Something went wrong!');
                }
            }   // Import Data From EXCEL / CSV
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Sub Category
    public function subcategory()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/categories", $class, "Manage Sub Category", TRUE, "far fa-list-alt font-20 mr-5 font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'categories');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('parent', 'Parent', 'required');
                $this->form_validation->set_rules('title', 'Title', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['parent'] = $this->input->post('parent');
                        $insert_data['title'] = $this->input->post('title');
//                        $insert_data['description'] = $this->input->post('description');
                        (!empty($_FILES['category']['name']) && $insert_data['path'] = $this->md->uploadFile('category')); // check if file is selected than only it will upload
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Data not insterted!');
                        }
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'title', 'required');
                if ($this->form_validation->run() == TRUE) :
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['parent'] = $this->input->post('parent');
                        $dt['title'] = $this->input->post('title');
//                        $dt['description'] = $this->input->post('description');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('category'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                endif;
            }   // Edit Data
            if ($this->input->post('importData')) {
                $path = './admin_asset/importData/';
                require_once APPPATH . "/third_party/PHPExcel.php";

                $config['upload_path'] = $path;
                if (!is_dir($config['upload_path']))
                    mkdir($config['upload_path'], 0777, TRUE); // Make a folder if not exists
                $config['allowed_types'] = 'xlsx|xls|csv';
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('importedFile')) {
                    $this->session->set_flashdata('error', 'Something went wrong!');
                } else {
                    $excelData = array('upload_data' => $this->upload->data());
                }
                try {
                    if (empty($error)) {
                        if (!empty($excelData['upload_data']['file_name'])) {
                            $import_xls_file = $excelData['upload_data']['file_name'];
                        } else {
                            $import_xls_file = 0;
                        }
                        $inputFileName = $path . $import_xls_file;
                        try {
                            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                            $objPHPExcel = $objReader->load($inputFileName);
                            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                            $flag = true;
                            $i = 0;
                            foreach ($allDataInSheet as $value) {
                                if ($flag) {
                                    $flag = false;
                                    continue;
                                }
                                $inserdata[$i]['title'] = $value['A'];
                                $inserdata[$i]['parent'] = $value['B'];
                                $i++;
                            }
                            $result = $this->md->insert_import('tbl_subcategory', $inserdata);
                            if ($result) {
                                $this->session->set_flashdata('success', 'Yeah, Sub Category imported successfully.');
                            } else {
                                $this->session->set_flashdata('error', 'Something went wrong!');
                            }
                        } catch (Exception $e) {
                            $this->session->set_flashdata('error', 'Something went wrong!');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Something went wrong!');
                    }
                } catch (Exception $e) {
                    $this->session->set_flashdata('error', 'Something went wrong!');
                }
            }   // Import Data From EXCEL / CSV
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Peta Category
    public function petacategory()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/categories", $class, "Manage Peta Category", TRUE, "far fa-list-alt font-20 mr-5 font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'categories');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('main', 'Main', 'required');
                $this->form_validation->set_rules('parent', 'parent', 'required');
                $this->form_validation->set_rules('title', 'Title', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['main'] = $this->input->post('main');
                        $insert_data['parent'] = $this->input->post('parent');
                        $insert_data['title'] = $this->input->post('title');
                        (!empty($_FILES['category']['name']) && $insert_data['path'] = $this->md->uploadFile('category')); // check if file is selected than only it will upload
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Data not insterted!');
                        }
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('main', 'Main', 'required');
                $this->form_validation->set_rules('parent', 'parent', 'required');
                $this->form_validation->set_rules('title', 'Title', 'required');
                if ($this->form_validation->run() == TRUE) :
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['main'] = $this->input->post('main');
                        $dt['parent'] = $this->input->post('parent');
                        $dt['title'] = $this->input->post('title');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('category'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                endif;
            }   // Edit Data
            if ($this->input->post('importData')) {
                $path = './admin_asset/importData/';
                require_once APPPATH . "/third_party/PHPExcel.php";

                $config['upload_path'] = $path;
                if (!is_dir($config['upload_path']))
                    mkdir($config['upload_path'], 0777, TRUE); // Make a folder if not exists
                $config['allowed_types'] = 'xlsx|xls|csv';
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('importedFile')) {
                    $this->session->set_flashdata('error', 'Something went wrong!');
                } else {
                    $excelData = array('upload_data' => $this->upload->data());
                }
                try {
                    if (empty($error)) {
                        if (!empty($excelData['upload_data']['file_name'])) {
                            $import_xls_file = $excelData['upload_data']['file_name'];
                        } else {
                            $import_xls_file = 0;
                        }
                        $inputFileName = $path . $import_xls_file;
                        try {
                            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                            $objPHPExcel = $objReader->load($inputFileName);
                            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                            $flag = true;
                            $i = 0;
                            foreach ($allDataInSheet as $value) {
                                if ($flag) {
                                    $flag = false;
                                    continue;
                                }
                                $inserdata[$i]['title'] = $value['A'];
                                $inserdata[$i]['main'] = $value['B'];
                                $inserdata[$i]['parent'] = $value['C'];
                                $i++;
                            }
                            $result = $this->md->insert_import('tbl_petacategory', $inserdata);
                            if ($result) {
                                $this->session->set_flashdata('success', 'Yeah, Peta Category imported successfully.');
                            } else {
                                $this->session->set_flashdata('error', 'Something went wrong!');
                            }
                        } catch (Exception $e) {
                            $this->session->set_flashdata('error', 'Something went wrong!');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Something went wrong!');
                    }
                } catch (Exception $e) {
                    $this->session->set_flashdata('error', 'Something went wrong!');
                }
            }   // Import Data From EXCEL / CSV
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Product
    public function product()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fab fa-product-hunt font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'Set Product Title', 'required', array("required" => "Enter Product Title!"));
                $this->form_validation->set_rules('petacategory_id', '', 'required', array("required" => "Select Category ID!"));
//                $this->form_validation->set_rules('stock', '', 'required', array("required" => "Enter Product stock!"));
                $this->form_validation->set_rules('description', '', 'required', array("required" => "Enter Description!"));
//                $this->form_validation->set_rules('additional_info', '', 'required', array("required" => "Enter Additional Info!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['category_id'] = $this->input->post('category_id');
                        $dt['subcategory_id'] = $this->input->post('subcategory_id');
                        $dt['petacategory_id'] = $this->input->post('petacategory_id');
                        $dt['sku'] = $this->input->post('sku');
                        $dt['title'] = $this->input->post('title');
                        $dt['slug'] = $this->input->post('title') ? $this->md->generateSeoURL($this->input->post('title')) : $this->input->post('title');
//                        $dt['stock'] = $this->input->post('stock');
//                        $dt['location'] = $this->input->post('location');
                        $dt['rate'] = $this->input->post('rate');
                        $dt['stick'] = $this->input->post('stick');
                        $dt['ft'] = $this->input->post('ft');
                        $dt['description'] = $this->input->post('description');
                        $dt['additional_info'] = $this->input->post('additional_info');
                        (strlen($_FILES['product_photos']['name'][0]) > 0) && $dt['photos'] = $this->md->uploadMultiFile('product_photos', count($_FILES['product_photos']['name'])); // check if file is selected than only it will upload
//                        $dt['meta_title'] = $this->input->post('meta_title');
//                        $dt['meta_keyword'] = $this->input->post('meta_keyword');
//                        $dt['meta_desc'] = $this->input->post('meta_desc');
                        $dt['featured'] = $this->input->post('featured') ? 1 : 0;
                        $dt['status'] = 1;
                        $dt['entry_date'] = date('Y-m-d');
                        $this->md->insert($this->table_prefix . $class, $dt);

                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'Set Product Title', 'required', array("required" => "Enter Product Title!"));
                $this->form_validation->set_rules('petacategory_id', '', 'required', array("required" => "Select Category ID!"));
//                $this->form_validation->set_rules('stock', '', 'required', array("required" => "Enter Product stock!"));
                $this->form_validation->set_rules('description', '', 'required', array("required" => "Enter Description!"));
//                $this->form_validation->set_rules('additional_info', '', 'required', array("required" => "Enter Additional Info!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['category_id'] = $this->input->post('category_id');
                        $dt['subcategory_id'] = $this->input->post('subcategory_id');
                        $dt['petacategory_id'] = $this->input->post('petacategory_id');
                        $dt['sku'] = $this->input->post('sku');
                        $dt['title'] = $this->input->post('title');
                        $dt['slug'] = $this->input->post('title') ? $this->md->generateSeoURL($this->input->post('title')) : $this->input->post('title');
//                        $dt['stock'] = $this->input->post('stock');
//                        $dt['location'] = $this->input->post('location');
                        $dt['rate'] = $this->input->post('rate');
                        $dt['stick'] = $this->input->post('stick');
                        $dt['ft'] = $this->input->post('ft');
                        $dt['description'] = $this->input->post('description');
                        $dt['additional_info'] = $this->input->post('additional_info');
                        (strlen($_FILES['product_photos']['name'][0]) > 0) && $photos = $this->md->uploadMultiFile('product_photos', count($_FILES['product_photos']['name'])); // check if file is selected than only it will upload
                        if ($photos) :
                            $dt['photos'] = $this->input->post('oldPath') ? $this->input->post('oldPath') . "," . $photos : $photos;
                        endif;
//                        $dt['meta_title'] = $this->input->post('meta_title');
//                        $dt['meta_keyword'] = $this->input->post('meta_keyword');
//                        $dt['meta_desc'] = $this->input->post('meta_desc');
                        $dt['featured'] = $this->input->post('featured') ? 1 : 0;
                        $dt['modify_date'] = date('Y-m-d');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            if ($this->input->post('importData')) {
                $path = './admin_asset/importData/';
                require_once APPPATH . "/third_party/PHPExcel.php";

                $config['upload_path'] = $path;
                if (!is_dir($config['upload_path']))
                    mkdir($config['upload_path'], 0777, TRUE); // Make a folder if not exists
                $config['allowed_types'] = 'xlsx|xls|csv';
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('importedFile')) {
                    $this->session->set_flashdata('error', 'Something went wrong!');
                } else {
                    $excelData = array('upload_data' => $this->upload->data());
                }
                try {
                    if (empty($error)) {
                        if (!empty($excelData['upload_data']['file_name'])) {
                            $import_xls_file = $excelData['upload_data']['file_name'];
                        } else {
                            $import_xls_file = 0;
                        }
                        $inputFileName = $path . $import_xls_file;
                        try {
                            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                            $objPHPExcel = $objReader->load($inputFileName);
                            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                            $flag = true;
                            $i = 0;
                            foreach ($allDataInSheet as $value) {
                                if ($flag) {
                                    $flag = false;
                                    continue;
                                }
                                $inserdata[$i]['title'] = $value['A'];
                                $inserdata[$i]['slug'] = $value['A'] ? $this->md->generateSeoURL($value['A']) : $value['A'];
                                $inserdata[$i]['category_id'] = $value['B'];
                                $inserdata[$i]['subcategory_id'] = $value['C'];
                                $inserdata[$i]['petacategory_id'] = $value['D'];
                                $inserdata[$i]['sku'] = $value['E'];
                                $inserdata[$i]['rate'] = $value['F'];
                                $inserdata[$i]['stick'] = $value['G'];
                                $inserdata[$i]['ft'] = $value['H'];
                                $inserdata[$i]['description'] = $value['I'];
                                $inserdata[$i]['additional_info'] = $value['J'];
                                $inserdata[$i]['featured'] = $value['K'] ? $value['K'] : 0;
                                $inserdata[$i]['status'] = $value['L'] ? $value['L'] : 1;
                                $inserdata[$i]['entry_date'] = date('Y-m-d');
                                $i++;
                            }
                            $result = $this->md->insert_import('tbl_product', $inserdata);
                            if ($result) {
                                $this->session->set_flashdata('success', 'Yeah, Products imported successfully.');
                            } else {
                                $this->session->set_flashdata('error', 'Something went wrong!');
                            }
                        } catch (Exception $e) {
                            $this->session->set_flashdata('error', 'Something went wrong!');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Something went wrong!');
                    }
                } catch (Exception $e) {
                    $this->session->set_flashdata('error', 'Something went wrong!');
                }
            }   // Import Data From EXCEL / CSV
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // add Order - Walking Customer
    public function addOrder()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Add Order - Walking Customers", TRUE, "fas fa-plus font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . 'bill', array('bill_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('fname', '', 'required', array("required" => "Enter Customer Name!"));
                $this->form_validation->set_rules('phone', '', 'required', array("required" => "Enter Customer Phone!"));
                $this->form_validation->set_rules('city', '', 'required', array("required" => "Enter city!"));
                $this->form_validation->set_rules('postcode', '', 'required', array("required" => "Enter postcode!"));
                $this->form_validation->set_rules('address', '', 'required', array("required" => "Enter Address!"));
                $this->form_validation->set_rules('booking_destination', '', 'required', array("required" => "Enter Booking Destination!"));
                $this->form_validation->set_rules('name_transport', '', 'required', array("required" => "Enter Name of Transport!"));
                $this->form_validation->set_rules('drop_location', '', 'required', array("required" => "Enter Drop Location!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $totalAmount = 0;
                        $products = $this->input->post('products');
                        $qty = $this->input->post('qty');
                        if ($products && is_array($products)) {
                            foreach ($products as $key => $product) {
                                $productData = $this->md->select_where('tbl_product', array('product_id' => $product));
                                if ($productData) {
                                    $totalAmount = $totalAmount + $qty[$key] * $productData[0]->stick * $productData[0]->ft * $productData[0]->rate;
                                }
                            }

                            $orderID = date('YmdHis');
                            $ins['order_id'] = $orderID;
                            $ins['fname'] = $this->input->post('fname');
                            $ins['phone'] = $this->input->post('phone');
                            $ins['email'] = $this->input->post('email');
                            $ins['city'] = $this->input->post('city');
                            $ins['address'] = $this->input->post('address');
                            $ins['postal_code'] = $this->input->post('postcode');
                            $ins['notes'] = $this->input->post('notes');
                            $ins['booking_destination'] = $this->input->post('booking_destination');
                            $ins['drop_location'] = $this->input->post('drop_location');
                            $ins['name_sales_person'] = $this->input->post('name_sales_person');
                            $ins['name_transport'] = $this->input->post('name_transport');
                            $ins['total'] = $totalAmount;
                            $ins['entry_date'] = date('Y-m-d');
                            $ins['status'] = 'Inquired';
                            $ins['is_walking'] = 1;
                            if ($this->md->insert('tbl_bill', $ins)) {
                                $billId = $this->db->insert_id();   // Fetch latest bill id

                                // Insert into transaction table
                                foreach ($products as $key => $product) {
                                    $productData = $this->md->select_where('tbl_product', array('product_id' => $product));
                                    if ($productData) {
                                        $amount = $qty[$key] * $productData[0]->stick * $productData[0]->ft * $productData[0]->rate;
                                        $insTra['bill_id'] = $billId;
                                        $insTra['product_id'] = $product;
                                        $insTra['category_id'] = $productData ? $productData[0]->category_id : '';
                                        $insTra['qty'] = $qty[$key];
                                        $insTra['box'] = $qty[$key];
                                        $insTra['stick'] = $productData ? $productData[0]->stick : '';
                                        $insTra['ft'] = $productData ? $productData[0]->ft : '';
                                        $insTra['rate'] = $productData ? $productData[0]->rate : '';
                                        $insTra['amount'] = $amount;
                                        $insTra['entry_date'] = date('Y-m-d H:i:s');

                                        $this->md->insert('tbl_transaction', $insTra);
                                    }
                                }

                                $this->session->set_flashdata('success', 'Yeah, Order added successfully!');
                                redirect($data['current_page'] . "/" . $data['page_type']);
                            }

                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Select products!');
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('fname', '', 'required', array("required" => "Enter Customer Name!"));
                $this->form_validation->set_rules('phone', '', 'required', array("required" => "Enter Customer Phone!"));
                $this->form_validation->set_rules('city', '', 'required', array("required" => "Enter city!"));
                $this->form_validation->set_rules('postcode', '', 'required', array("required" => "Enter postcode!"));
                $this->form_validation->set_rules('address', '', 'required', array("required" => "Enter Address!"));
                $this->form_validation->set_rules('booking_destination', '', 'required', array("required" => "Enter Booking Destination!"));
                $this->form_validation->set_rules('name_transport', '', 'required', array("required" => "Enter Name of Transport!"));
                $this->form_validation->set_rules('drop_location', '', 'required', array("required" => "Enter Drop Location!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $totalAmount = 0;
                        $products = $this->input->post('products');
                        $qty = $this->input->post('qty');
                        if ($products && is_array($products)) {
                            foreach ($products as $key => $product) {
                                $productData = $this->md->select_where('tbl_product', array('product_id' => $product));
                                if ($productData) {
                                    $totalAmount = $totalAmount + $qty[$key] * $productData[0]->stick * $productData[0]->ft * $productData[0]->rate;
                                }
                            }

                            $ins['fname'] = $this->input->post('fname');
                            $ins['phone'] = $this->input->post('phone');
                            $ins['email'] = $this->input->post('email');
                            $ins['city'] = $this->input->post('city');
                            $ins['address'] = $this->input->post('address');
                            $ins['postal_code'] = $this->input->post('postcode');
                            $ins['notes'] = $this->input->post('notes');
                            $ins['booking_destination'] = $this->input->post('booking_destination');
                            $ins['drop_location'] = $this->input->post('drop_location');
                            $ins['name_sales_person'] = $this->input->post('name_sales_person');
                            $ins['name_transport'] = $this->input->post('name_transport');
                            $ins['total'] = $totalAmount;
                            $ins['modify_date'] = date('Y-m-d');
                            $ins['status'] = 'Inquired';
                            $ins['is_walking'] = 1;
                            if ($this->md->update('tbl_bill', $ins, array('bill_id' => $id))) {
                                // Delete old transaction data
                                $this->md->delete('tbl_transaction', array('bill_id' => $id));

                                // Insert into transaction table
                                foreach ($products as $key => $product) {
                                    $productData = $this->md->select_where('tbl_product', array('product_id' => $product));
                                    if ($productData) {
                                        $amount = $qty[$key] * $productData[0]->stick * $productData[0]->ft * $productData[0]->rate;
                                        $insTra['bill_id'] = $id;
                                        $insTra['product_id'] = $product;
                                        $insTra['category_id'] = $productData ? $productData[0]->category_id : '';
                                        $insTra['qty'] = $qty[$key];
                                        $insTra['box'] = $qty[$key];
                                        $insTra['stick'] = $productData ? $productData[0]->stick : '';
                                        $insTra['ft'] = $productData ? $productData[0]->ft : '';
                                        $insTra['rate'] = $productData ? $productData[0]->rate : '';
                                        $insTra['amount'] = $amount;
                                        $insTra['entry_date'] = date('Y-m-d H:i:s');

                                        $this->md->insert('tbl_transaction', $insTra);
                                    }
                                }

                                $this->session->set_flashdata('success', 'Yeah, Order added successfully!');
                                redirect($data['current_page'] . "/" . $data['page_type']);
                            }

                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Select products!');
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Inventory

    public function delete()
    {
        if (web_status()):
            $table = $this->input->post('tbl'); // table name
            $id = $this->input->post('id'); // item id
            $path = $this->input->post('path') ? $this->input->post('path') : ''; // item photo field
            if ($this->db->table_exists($this->table_prefix . $table)):
                if ($path):
                    $data = $this->md->select_where($this->table_prefix . $table, array($table . '_id' => $id));
                    if (!empty($data)):
                        $photos = explode(",", $data[0]->$path);
                        foreach ($photos as $pic):
                            unlink("./" . $pic);
                        endforeach;
                    endif;
                endif;
                $this->md->delete($this->table_prefix . $table, array($table . '_id' => $id));
            endif;
            echo "true";
        else:
            echo "false";
        endif;
    }

    // Bill

    public function inventory()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fas fa-clipboard-check font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . 'product', array('product_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $table = $this->input->post('table');   // product / location
                $stocks = $this->input->post('stock');
                if (is_array($stocks) && !empty($stocks)) {
                    if ($table == 'product') {
                        // Product
                        $locations = $this->input->post('location');
                        if (is_array($locations) && !empty($locations)) {
                            if (count($locations) == count($stocks)) {

                                // Check product ID already exist in inventory table - if exist remove it and insert new records
                                $inventories = $this->md->select_where('tbl_inventory', array('product_id' => $this->input->post('product_id')));
                                if ($inventories) {
                                    // Delete existed records
                                    $this->md->delete($this->table_prefix . $class, array('product_id' => $this->input->post('product_id')));
                                }
                                // Insert new records
                                foreach ($locations as $key => $location) {
                                    $checkProductionLocation = $this->md->select_where('tbl_location', array('location_id' => $location));
                                    if ($checkProductionLocation) {
                                        $dt['product_id'] = $this->input->post('product_id');
                                        $dt['stock'] = $stocks[$key];
                                        $dt['location_id'] = $location;
//                                    $dt['production'] = $checkProductionLocation ? ($checkProductionLocation[0]->title == 'production' ? 1 : 0) : 0;
                                        $dt['status'] = 1;
                                        $dt['entry_date'] = date('Y-m-d H:i:s');
                                        $this->md->insert($this->table_prefix . $class, $dt);
                                    }
                                }
                                $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                                redirect($data['current_page'] . "/" . $data['page_type']);
                            }
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, You have to select Location!');
                        }
                    } else {
                        // Location
                        $products = $this->input->post('product');
                        if (is_array($products) && !empty($products)) {
                            if (count($products) == count($stocks)) {

                                // Check location ID already exist in inventory table - if exist remove it and insert new records
                                $inventories = $this->md->select_where('tbl_inventory', array('location_id' => $this->input->post('location_id')));
                                if ($inventories) {
                                    // Delete existed records
                                    $this->md->delete($this->table_prefix . $class, array('location_id' => $this->input->post('location_id')));
                                }
                                // Insert new records
                                foreach ($products as $key => $product) {
                                    $checkProductionLocation = $this->md->select_where('tbl_product', array('product_id' => $product));
                                    if ($checkProductionLocation) {
                                        $dt['location_id'] = $this->input->post('location_id');
                                        $dt['stock'] = $stocks[$key];
                                        $dt['product_id'] = $product;
//                                    $dt['production'] = $checkProductionLocation ? ($checkProductionLocation[0]->title == 'production' ? 1 : 0) : 0;
                                        $dt['status'] = 1;
                                        $dt['entry_date'] = date('Y-m-d H:i:s');
                                        $this->md->insert($this->table_prefix . $class, $dt);
                                    }
                                }
                                $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                                redirect($data['current_page'] . "/" . $data['page_type']);
                            }
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, You have to select products!');
                        }
                    }
                } else {
                    $this->session->set_flashdata('error', 'Sorry, You have to add stock!');
                }

            }   // Add Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Delivered

    public function bill()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage Bill Data", TRUE, "fas fa-file-invoice font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('bill_id', 'Bill ID', 'required', array("required" => "Select Bill Id!"));
                $this->form_validation->set_rules('status', 'status', 'required', array("required" => "Select Status!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $id = $this->input->post('bill_id');

                        // If status is confirmed than decrease the stock from product table
                        if ($this->input->post('status') == 'Estimate') {
                            $transactions = $this->md->select_where('tbl_transaction', array('bill_id' => $id));
                            if ($transactions) {
                                foreach ($transactions as $key => $transaction) {

                                    // Delete temporary table data and insert new record
                                    $delTemp['register_id'] = $transaction->register_id;
                                    $delTemp['bill_id'] = $transaction->bill_id;
                                    $delTemp['product_id'] = $transaction->product_id;

                                    $this->md->delete('tbl_temp', $delTemp);

                                    //$inventoryData = $this->md->select_where('tbl_inventory', array('production' => 0, 'product_id' => $transaction->product_id));
                                    $inventoryData = $this->md->select_where('tbl_inventory', array('product_id' => $transaction->product_id));
                                    if ($inventoryData) {
                                        foreach ($inventoryData as $stockKey => $inventoryDatum) {
                                            $locationInfo = $this->md->select_where('tbl_location', array('location_id' => $inventoryDatum->location_id));
                                            if ($locationInfo) {
//                                                if ($locationInfo[0]->title != "production") {
                                                $stocks = $this->input->post('location')[$transaction->transaction_id][$stockKey];


                                                /* Insert new stock entry in temporary table, after bill status - dispatch,
                                                 * we will update stock into inventory table from temporary table.
                                                 */

                                                $insertStock['register_id'] = $transaction->register_id;
                                                $insertStock['bill_id'] = $transaction->bill_id;
                                                $insertStock['product_id'] = $transaction->product_id;
                                                $insertStock['inventory_id'] = $inventoryDatum->inventory_id;
                                                $insertStock['location_id'] = $inventoryDatum->location_id;
                                                $insertStock['stock'] = $stocks;
                                                $insertStock['entry_date'] = date('Y-m-d');

                                                $this->md->insert('tbl_temp', $insertStock);

                                                //$currentStock = $inventoryDatum->stock;
                                                //$newStock = $currentStock - $stocks;

                                                // Update new stock into product table
                                                //$this->md->update('tbl_product', array('stock' => ($newStock)), array('inventory_id' => $inventoryDatum->inventory_id));
//                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } elseif ($this->input->post('status') == 'Dispatch') {

                            /* if order status dispatch we will upload the screenshot &
                             decrease the stock from temporary table to inventory table */
                            $tempStocks = $this->md->select_where('tbl_temp', array('bill_id' => $id));
                            if ($tempStocks) {
                                foreach ($tempStocks as $tempStock) {
                                    // Update new stocks
                                    $upStockInventory['inventory_id'] = $tempStock->inventory_id;
                                    $availableStockInventory = $this->md->select_where('tbl_inventory', $upStockInventory);
                                    if ($availableStockInventory) {
                                        $availableStockInventory = $availableStockInventory[0]->stock;
                                        $this->md->update('tbl_inventory', array('stock' => ($availableStockInventory - $tempStock->stock)), array('inventory_id' => $tempStock->inventory_id));
                                    }
                                }
                                // Delete temporary records
                                $this->md->delete('tbl_temp', array('bill_id' => $id));
                            }

                        }

                        $dt['cartageDefault'] = $this->input->post('cartageDefault');
                        $dt['cartage'] = $this->input->post('cartage');
                        $dt['gstStatus'] = $this->input->post('gstStatus');
                        $dt['gstDefaultAmount'] = $this->input->post('gstDefaultAmount');
                        $dt['gst'] = $this->input->post('gst');
                        $dt['total'] = $this->input->post('total');
                        $dt['neft_rtgs'] = $this->input->post('neft_rtgs');
                        $dt['cash'] = $this->input->post('cash');
                        $dt['booking_destination'] = $this->input->post('booking_destination');
                        $dt['name_transport'] = $this->input->post('name_transport');
                        $dt['name_sales_person'] = $this->input->post('name_sales_person');
                        $dt['drop_location'] = $this->input->post('drop_location');
                        $dt['status'] = $this->input->post('status');
                        if ($this->input->post('status') == 'Dispatch') {
                            // Check already photo is exist or not, if yes than it will remove and than upload.
                            $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                            //$this->input->post('updateStatus') && ($dt['shippedPhoto'] = $this->md->uploadFile('shippedPhoto'));    // Upload Photo and return path from model

                            (strlen($_FILES['shippedPhoto']['name'][0]) > 0) && $photos = $this->md->uploadMultiFile('shippedPhoto', count($_FILES['shippedPhoto']['name'])); // check if file is selected than only it will upload
                            if ($photos) :
                                $dt['shippedPhoto'] = $this->input->post('oldPath') ? $this->input->post('oldPath') . "," . $photos : $photos;
                            endif;

                            $dt['no_of_bundles'] = $this->input->post('no_of_bundles');
                            $dt['lr_no'] = $this->input->post('lr_no');
                            $dt['date_of_lr'] = $this->input->post('date_of_lr');

                            if ($this->input->post('no_of_bundles') && $this->input->post('lr_no') && $this->input->post('date_of_lr')) {
                                // if all three field filled then change bill status to dispatched
                                $dt['status'] = 'Dispatched';
                            }
                        }
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {

                            // Update transaction table
                            $transactionIds = $this->input->post('transaction_id');
                            if ($transactionIds) {
                                foreach ($transactionIds as $key => $transactionId) {
                                    $upTra['box'] = $this->input->post('box')[$key];
                                    $upTra['stick'] = $this->input->post('stick')[$key];
                                    $upTra['ft'] = $this->input->post('ft')[$key];
                                    $upTra['rate'] = $this->input->post('rate')[$key];
                                    $upTra['amount'] = $this->input->post('amount')[$key];
                                    $this->md->update('tbl_transaction', $upTra, array('transaction_id' => $transactionId));
                                }
                            }

                            $this->session->set_flashdata('success', 'Yeah, Bill status updated.');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, permission not granted!');
                }
            }   // Assign Permission Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Cancelled

    public function delivered()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage Dispatched Data", TRUE, "fas fa-file-invoice font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }


    // Register

    public function cancelled()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage Cancelled Data", TRUE, "fas fa-times-circle text-danger font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Location

    public function register()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage Register Data", TRUE, "fas fa-file-invoice font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Upi

    public function location()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage Location", TRUE, "far fa-list-alt font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'Title', 'required|is_unique[' . $this->table_prefix . $class . '.title]');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()) {
                        $dt['title'] = $this->input->post('title');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    }
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            } // Add Data
            if ($this->input->post('update')) {
                // Add unique validation rule, excluding the current record
                $this->form_validation->set_rules('title', 'Title', 'required|is_unique[' . $this->table_prefix . $class . '.title.' . $id . ']');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()) {
                        $dt['title'] = $this->input->post('title');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    }
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            } // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Coupons

    public function upi()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-comments font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('upi', 'UPI', 'required', array("required" => "Enter UPI!"));
                $this->form_validation->set_rules('bank_name', 'Bank Name', 'required', array("required" => "Enter Bank Name!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['upi'] = $this->input->post('upi');
                        $dt['bank_name'] = $this->input->post('bank_name');
                        $dt['account_name'] = $this->input->post('account_name');
                        $dt['account_number'] = $this->input->post('account_number');
                        $dt['ifsc_code'] = $this->input->post('ifsc_code');
                        $dt['branch'] = $this->input->post('branch');
                        $dt['entry_date'] = date('Y-m-d');
                        (!empty($_FILES['upi']['name']) && $dt['path'] = $this->md->uploadFile('upi')); // check if file is selected than only it will upload
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('upi', 'UPI', 'required', array("required" => "Enter UPI!"));
                $this->form_validation->set_rules('bank_name', 'Bank Name', 'required', array("required" => "Enter Bank Name!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['upi'] = $this->input->post('upi');
                        $dt['bank_name'] = $this->input->post('bank_name');
                        $dt['account_name'] = $this->input->post('account_name');
                        $dt['account_number'] = $this->input->post('account_number');
                        $dt['ifsc_code'] = $this->input->post('ifsc_code');
                        $dt['branch'] = $this->input->post('branch');
                        $dt['modify_date'] = date('Y-m-d');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('upi'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    /*
     PDF REPORT - INVOICE
    */
    // Generate PDF Report

    public function coupon()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fa fa-tag font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current product has permission or not to use this page
        if ($data['page_type'] == "getdata") :
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else :
            if ($data['page_type'] == "edit") :
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('coupon_code', '', 'required', array("required" => "Enter Coupon Code!"));
//                $this->form_validation->set_rules('coupon_percentage', '', 'required', array("required" => "Enter Coupon Percentage!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()) :
                        $dt['coupon_code'] = $this->input->post('coupon_code');
                        $dt['coupon_percentage'] = $this->input->post('coupon_percentage');
                        $dt['minimum_subtotal'] = ($this->input->post('minimum_subtotal') ? $this->input->post('minimum_subtotal') : '');
                        $dt['maximum_subtotal'] = ($this->input->post('maximum_subtotal') ? $this->input->post('maximum_subtotal') : '');
                        $dt['product'] = $this->input->post('product') ? json_encode($this->input->post('product')) : '';
//                        $dt['category'] = $this->input->post('category') ? json_encode($this->input->post('category')) : '';
                        $dt['expiry_date'] = $this->input->post('expiry_date');
                        $dt['coupon_description'] = ($this->input->post('coupon_description') ? $this->input->post('coupon_description') : '');
                        $dt['free_shipping'] = (($this->input->post('free_shipping') == "yes") ? "yes" : "no");
                        $dt['status'] = (($this->input->post('status') == 1) ? 1 : 0);
                        $dt['entry_date'] = date('Y-m-d');
                        $this->md->insert($this->table_prefix . $class, $dt);  // Add coupon
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('coupon_code', '', 'required', array("required" => "Enter Coupon Code!"));
//                $this->form_validation->set_rules('coupon_percentage', '', 'required', array("required" => "Enter Coupon Percentage!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()) :
                        $dt['coupon_code'] = $this->input->post('coupon_code');
                        $dt['coupon_percentage'] = $this->input->post('coupon_percentage');
                        $dt['minimum_subtotal'] = ($this->input->post('minimum_subtotal') ? $this->input->post('minimum_subtotal') : '');
                        $dt['maximum_subtotal'] = ($this->input->post('maximum_subtotal') ? $this->input->post('maximum_subtotal') : '');
                        $dt['product'] = $this->input->post('product') ? json_encode($this->input->post('product')) : '';
//                        $dt['category'] = $this->input->post('category') ? json_encode($this->input->post('category')) : '';
                        $dt['expiry_date'] = $this->input->post('expiry_date');
                        $dt['coupon_description'] = ($this->input->post('coupon_description') ? $this->input->post('coupon_description') : '');
                        $dt['status'] = (($this->input->post('status') == 1) ? 1 : 0);
                        $dt['free_shipping'] = (($this->input->post('free_shipping') == "yes") ? "yes" : "no");
                        $dt['modify_date'] = date('Y-m-d');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }


    /*
      U S E R  -  P A G E S
     */

    // User - Admin - Super Admin Manage

    public function generate_report()
    {
        $downloadStatus = $this->uri->segment(2);   // (view / download)
        $invoice_number = $this->uri->segment(3);   // Invoice Number
        $this->load->library('PDF');
        $invoiceData = $this->md->select_where('tbl_bill', array('order_id' => $invoice_number));
        $html = $this->load->view('admin/site-pages/report', array('invoiceData' => $invoiceData), true);
        $this->pdf->createPDF($html, "Invoice No. - " . $invoice_number, ($downloadStatus == 'download' ? TRUE : FALSE));
    }

    // User Role

    public function user()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/user", $class, "Manage " . $class, TRUE, "fas fa-users font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'user');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . 'admin', array('admin_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('username', 'Set User Fullname', 'required', array("required" => "Enter User Fullname!"));
                $this->form_validation->set_rules('email', 'Set User Email', 'required', array("required" => "Enter Email!"));
                $this->form_validation->set_rules('password', 'Set User Password', 'required', array("required" => "Enter Password!"));
                $this->form_validation->set_rules('role', 'Select User Role', 'required', array("required" => "Select User Role!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $wh['email'] = $this->input->post('email'); // where condition
                        $admin_data = $this->md->select_where($this->table_prefix . 'admin', $wh); // get admin data from where condition
                        if (empty($admin_data)):
                            $dt['admin_name'] = $this->input->post('username');
                            $dt['email'] = $this->input->post('email');
                            $dt['phone'] = ($this->input->post('phone') ? $this->input->post('phone') : '');
                            $dt['address'] = ($this->input->post('address') ? $this->input->post('address') : '');
                            $dt['user_role_id'] = ($this->input->post('role') ? $this->input->post('role') : '');
                            $dt['status'] = (($this->input->post('status') == 1) ? 1 : 0);
                            $dt['password'] = $this->encryption->encrypt($this->input->post('password'));
                            (!empty($_FILES['user']['name']) && $dt['path'] = $this->md->uploadFile('user')); // check if file is selected than only it will upload
                            $dt['register_date'] = date('Y-m-d');
                            $this->md->insert($this->table_prefix . 'admin', $dt);
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        else:
                            $this->session->set_flashdata('error', 'Sorry, Email already registered!');
                        endif;
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('username', 'Set User Fullname', 'required', array("required" => "Enter User Fullname!"));
                $this->form_validation->set_rules('password', 'Set User Password', 'required', array("required" => "Enter Password!"));
                $this->form_validation->set_rules('role', 'Select User Role', 'required', array("required" => "Select User Role!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo

                        $dt['admin_name'] = $this->input->post('username');
                        $dt['phone'] = ($this->input->post('phone') ? $this->input->post('phone') : '');
                        $dt['address'] = ($this->input->post('address') ? $this->input->post('address') : '');
                        $dt['user_role_id'] = ($this->input->post('role') ? $this->input->post('role') : '');
                        $dt['status'] = (($this->input->post('status') == 1) ? 1 : 0);
                        $dt['password'] = $this->encryption->encrypt($this->input->post('password'));
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('user'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . 'admin', $dt, array('admin_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // User Permission

    public function role()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/user", $class, "Manage User Role", TRUE, "fas fa-user-tag font-20 mr-5 font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'user');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'User Role', 'required|is_unique[tbl_role.title]', array('is_unique' => 'Sorry, Role already exist!'));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['title'] = $this->input->post('title');
                        $insert_data['remark'] = $this->input->post('remark');
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Data not insterted!');
                        }
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                if (web_status()):
                    $exist = $this->md->select_where($this->table_prefix . $class, array('title' => $this->input->post('title')));
                    if (empty($exist)) {
                        $dt['title'] = $this->input->post('title');
                        $dt['remark'] = $this->input->post('remark');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    } else {
                        if ($exist[0]->role_id == $id) {
                            $dt['title'] = $this->input->post('title');
                            $dt['remark'] = $this->input->post('remark');
                            if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                                $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                                redirect('manage-' . $class . "/show");
                            }
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Role Already exist!');
                        }
                    }
                endif;
            }   // Edit Data
            if ($this->input->post('assign')) {
                $this->form_validation->set_rules('role_id', 'Role ID', 'required', array("required" => "Select Role!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $this->md->delete($this->table_prefix . 'permission_assign', array('role_id' => $this->input->post('role_id')));  // Delete current role's permisison
                        // Feature Permission
                        $feature_all = ($this->input->post('feature_all_per') ? json_encode(array_keys($this->input->post('feature_all_per'), 1)) : '');
                        $feature_read = ($this->input->post('feature_read_per') ? json_encode(array_keys($this->input->post('feature_read_per'), 1)) : '');
                        $feature_edit = ($this->input->post('feature_edit_per') ? json_encode(array_keys($this->input->post('feature_edit_per'), 1)) : '');

                        // Pages Permission
                        $page_all = ($this->input->post('all_per') ? json_encode(array_keys($this->input->post('all_per'), 1)) : '');
                        $page_read = ($this->input->post('read_per') ? json_encode(array_keys($this->input->post('read_per'), 1)) : '');
                        $page_write = ($this->input->post('write_per') ? json_encode(array_keys($this->input->post('write_per'), 1)) : '');
                        $page_edit = ($this->input->post('edit_per') ? json_encode(array_keys($this->input->post('edit_per'), 1)) : '');
                        $page_delete = ($this->input->post('delete_per') ? json_encode(array_keys($this->input->post('delete_per'), 1)) : '');
                        $page_status = ($this->input->post('status_per') ? json_encode(array_keys($this->input->post('status_per'), 1)) : '');

                        $permissions = array('feature_all', 'feature_read', 'feature_edit', 'page_all', 'page_read', 'page_write', 'page_edit', 'page_delete', 'page_status');
                        if (!empty($permissions)) :
                            foreach ($permissions as $per_item):
                                $per_type = substr($per_item, (strrpos($per_item, '_') ?: -1) + 1);
                                $dt['role_id'] = $this->input->post('role_id'); // Role ID
                                $dt['permission_id'] = $$per_item;   // permission ID
                                $dt['type'] = strtok($per_item, '_');   // Get type from array value, before '_'
                                $dt['all'] = ($per_type == 'all' ? '1' : '0');   // All permission
                                $dt['read'] = ($per_type == 'all' ? '1' : ($per_type == 'read' ? '1' : '0'));   // Read permission
                                $dt['edit'] = ($per_type == 'all' ? '1' : ($per_type == 'edit' ? '1' : '0'));   // Edit permission
                                $dt['write'] = ($per_type == 'all' ? '1' : ($per_type == 'write' ? '1' : '0'));   // Write permission
                                $dt['delete'] = ($per_type == 'all' ? '1' : ($per_type == 'delete' ? '1' : '0'));   // Delete permission
                                $dt['status'] = ($per_type == 'all' ? '1' : ($per_type == 'status' ? '1' : '0'));   // Status permission
                                (!empty($$per_item) && $this->md->insert($this->table_prefix . 'permission_assign', $dt));
                            endforeach;
                        endif;

                        $this->session->set_flashdata('success', 'Yeah, Permission granted successfully.');
                        redirect('manage-' . $class . "/show");
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, permission not granted!');
                }
            }   // Assign Permission Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    /*
      M E N U  -  P A G E S
     */

    // User Mainmenu

    public function permission()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/user", $class, "Manage User Permission", TRUE, "fas fa-user-check font-20 mr-5 font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'user');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('type', 'Permission Type', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        if ($this->input->post('type') == "page"):
                            $options = $this->input->post('page_option');
                            $insert_data['feature'] = strtolower($this->input->post('page'));
                            $insert_data['menu'] = strtolower($this->input->post('menu'));
                        else:
                            $options = $this->input->post('feature_option');
                            $insert_data['feature'] = strtolower($this->input->post('feature'));
                        endif;
                        $insert_data['type'] = $this->input->post('type');

                        // Check data already exist or not
                        $exist = $this->md->select_where($this->table_prefix . $class, array('type' => $this->input->post('type'), 'feature' => $insert_data['feature']));
                        if (empty($exist)):
                            if ($options):
                                foreach ($options as $option_item):
                                    if ($option_item == 'all'):
                                        $insert_data['all'] = '1';
                                        $insert_data['read'] = '1';
                                        $insert_data['edit'] = '1';
                                        if ($this->input->post('type') == "page"):
                                            $insert_data['write'] = '1';
                                            $insert_data['delete'] = '1';
                                            $insert_data['status'] = '1';
                                        endif;
                                    else:
                                        $insert_data[$option_item] = '1';
                                    endif;
                                endforeach;
                                if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                                    $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                                } else {
                                    $this->session->set_flashdata('error', 'Sorry, Data not insterted!');
                                }
                            else:
                                $this->session->set_flashdata('error', 'Sorry, select atleast one permission!');
                            endif;
                        else:
                            $this->session->set_flashdata('error', 'Sorry, Data already exist!');
                        endif;
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                if (web_status()):
                    if ($this->input->post('type') == "page"):
                        $options = $this->input->post('page_option');
                        $insert_data['feature'] = strtolower($this->input->post('page'));
                        $insert_data['menu'] = strtolower($this->input->post('menu'));
                    else:
                        $options = $this->input->post('feature_option');
                        $insert_data['feature'] = strtolower($this->input->post('feature'));
                    endif;
                    $insert_data['type'] = $this->input->post('type');
                    $updata = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
                    $up_data['all'] = '0';
                    $up_data['read'] = '0';
                    $up_data['edit'] = '0';
                    $up_data['write'] = '0';
                    $up_data['delete'] = '0';
                    $up_data['status'] = '0';
                    if ($this->md->update($this->table_prefix . $class, $up_data, array($class . '_id' => $id))) {
                        $type = $updata ? $updata[0]->type : '';
                        $options = ($type == "page") ? $this->input->post('page_option') : $options = $this->input->post('feature_option');
                        if (!empty($options)):
                            foreach ($options as $option_item):
                                if ($option_item == 'all'):
                                    $insert_data['all'] = '1';
                                    $insert_data['read'] = '1';
                                    $insert_data['edit'] = '1';
                                    if ($type == "page"):
                                        $insert_data['write'] = '1';
                                        $insert_data['delete'] = '1';
                                        $insert_data['status'] = '1';
                                    endif;
                                else:
                                    $insert_data[$option_item] = '1';
                                endif;
                            endforeach;
                        endif;
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Data not updated!');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        }
                    }
                endif;
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // User Submenu

    public function mainmenu()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/menu", $class, "Manage Mainmenu", TRUE, "fas fa-ellipsis-h font-20 mr-5 font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'menu');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('controller', 'Mainmenu controller', 'required');
                $this->form_validation->set_rules('title', 'Mainmenu Title', 'required');
                $this->form_validation->set_rules('url', 'Menu URL', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['controller'] = $this->input->post('controller');
                        $insert_data['title'] = $this->input->post('title');
                        $insert_data['slug'] = $this->input->post('slug') ? strtolower(str_replace(" ", "-", $this->input->post('slug'))) : strtolower(str_replace(" ", "-", $this->input->post('title')));
                        $insert_data['position'] = $this->input->post('position');
                        $insert_data['icon'] = $this->input->post('icon');
                        $insert_data['url'] = $this->input->post('url');
                        $insert_data['status'] = $this->input->post('status') ? $this->input->post('status') : 0;
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Data not insterted!');
                        }
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('controller', 'Mainmenu controller', 'required');
                $this->form_validation->set_rules('title', 'Mainmenu Title', 'required');
                $this->form_validation->set_rules('url', 'Menu URL', 'required');
                if ($this->form_validation->run() == TRUE) :
                    if (web_status()):
                        $insert_data['controller'] = $this->input->post('controller');
                        $insert_data['title'] = $this->input->post('title');
                        $insert_data['slug'] = $this->input->post('slug') ? strtolower(str_replace(" ", "-", $this->input->post('slug'))) : strtolower(str_replace(" ", "-", $this->input->post('title')));
                        $insert_data['position'] = $this->input->post('position');
                        $insert_data['icon'] = $this->input->post('icon');
                        $insert_data['url'] = $this->input->post('url');
                        $insert_data['status'] = $this->input->post('status') ? $this->input->post('status') : 0;
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                endif;
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    /*
      A J A X  -  C A L L
     */

    // get title to slug AJAX Call

    public function submenu()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/menu", $class, "Manage Submenu", TRUE, "fas fa-ellipsis-v font-20 mr-5 font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'menu');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('controller', 'Mainmenu controller', 'required');
                $this->form_validation->set_rules('title', 'Submenu Title', 'required');
                $this->form_validation->set_rules('url', 'Menu URL', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['controller'] = $this->input->post('controller');
                        $insert_data['title'] = $this->input->post('title');
                        $insert_data['mainmenu_id'] = $this->input->post('parent');
                        $insert_data['slug'] = $this->input->post('slug') ? strtolower(str_replace(" ", "-", $this->input->post('slug'))) : strtolower(str_replace(" ", "-", $this->input->post('title')));
                        $insert_data['position'] = $this->input->post('position');
                        $insert_data['icon'] = $this->input->post('icon');
                        $insert_data['url'] = $this->input->post('url');
                        $insert_data['status'] = $this->input->post('status') ? $this->input->post('status') : 0;
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Data not insterted!');
                        }
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('controller', 'Mainmenu controller', 'required');
                $this->form_validation->set_rules('title', 'Submenu Title', 'required');
                $this->form_validation->set_rules('url', 'Menu URL', 'required');
                if ($this->form_validation->run() == TRUE) :
                    if (web_status()):
                        $insert_data['controller'] = $this->input->post('controller');
                        $insert_data['title'] = $this->input->post('title');
                        $insert_data['mainmenu_id'] = $this->input->post('parent');
                        $insert_data['slug'] = $this->input->post('slug') ? strtolower(str_replace(" ", "-", $this->input->post('slug'))) : strtolower(str_replace(" ", "-", $this->input->post('title')));
                        $insert_data['position'] = $this->input->post('position');
                        $insert_data['icon'] = $this->input->post('icon');
                        $insert_data['url'] = $this->input->post('url');
                        $insert_data['status'] = $this->input->post('status') ? $this->input->post('status') : 0;
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                endif;
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // get sub category AJAX Call

    public function getSlug()
    {
        $val = $this->input->post('str');
        echo $this->md->generateSeoURL($val);
    }

    // get peta category AJAX Call

    public function get_category()
    {
        $val = $this->input->post('val');
        echo '<option value="">Select Sub Category</option>';
        $subcategory = $this->md->select_where('tbl_subcategory', array('parent' => $val));
        if (!empty($subcategory)) {
            foreach ($subcategory as $subcategory_data) {
                echo '<option value="' . $subcategory_data->subcategory_id . '">' . $subcategory_data->title . '</option>';
            }
        }
    }

    // get both category data

    public function get_petacategory()
    {
        $val = $this->input->post('val');
        echo '<option value="">Select Peta Category</option>';
        $petacategory = $this->md->select_where('tbl_petacategory', array('parent' => $val));
        if (!empty($petacategory)) {
            foreach ($petacategory as $petacategory_data) {
                echo '<option value="' . $petacategory_data->petacategory_id . '">' . $petacategory_data->title . '</option>';
            }
        }
    }

    // get Submenu Position AJAX Call

    public function get_category_data()
    {
        $val = $this->input->post('val');   // Peta ID
        $petacategory = $this->md->select_where('tbl_petacategory', array('petacategory_id' => $val));
        if (!empty($petacategory)) {
            $subcategory = $this->md->select_where('tbl_subcategory', array('subcategory_id' => $petacategory[0]->parent));
            if (!empty($subcategory)) {
                $category = $this->md->select_where('tbl_category', array('category_id' => $subcategory[0]->parent));
                if (!empty($category)) {
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Sub
                                    Category</label>
                                <select name="subcategory_id"
                                        class="form-control select2 text-capitalize <?php if (form_error('subcategory_id')) { ?> is-invalid <?php } ?>">
                                    <option
                                            value="<?php echo $subcategory[0]->subcategory_id; ?>"><?php echo $subcategory[0]->title; ?></option>
                                </select>
                                <div class="error-text">
                                    <?php
                                    if (form_error('subcategory_id')) {
                                        echo form_error('subcategory_id');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Category</label>
                                <select name="category_id"
                                        class="form-control select2 text-capitalize <?php if (form_error('category_id')) { ?> is-invalid <?php } ?>">
                                    <option
                                            value="<?php echo $category[0]->category_id; ?>"><?php echo $category[0]->title; ?></option>
                                </select>
                                <div class="error-text">
                                    <?php
                                    if (form_error('category_id')) {
                                        echo form_error('category_id');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
        }
    }

    // get Role's Granted permission AJAX Call

    public function get_submenuPosition()
    {
        $mainMenuId = $this->input->post('mainMenuId');   // get mainmenu id
        $submenu = $this->md->select_where('tbl_submenu', array('mainmenu_id' => $mainMenuId));
        $submenu = count($submenu);   // Count data of submenu table
        if ($submenu != 0):
            for ($i = ($submenu + 1); $i >= 1; $i--):
                echo '<option value="' . $i . '">' . $i . '</option>';
            endfor;
        else:
            echo '<option value="1">1</option>';
        endif;
    }

    // get Role's Granted permission AJAX Call

    public function get_permission_table()
    {
        $roleid = $this->input->post('roleid');   // get role id
        $permission = $this->md->select('tbl_permission');  // get all permission
        $existing_permission = $this->md->select_where('tbl_permission_assign', array('role_id' => $roleid));   // get current role's permisison
        $per_id = array();
        foreach ($existing_permission as $ext_per) {
            $per = json_decode($ext_per->permission_id);
            foreach ($per as $per_val):
                $per_id[] = array('per_id' => $per_val, 'assign_id' => $ext_per->permission_assigned_id);
            endforeach;
        }
        echo '<input type="hidden" value="' . $roleid . '" name="role_id" />';
        echo '<table class="table table-bordered table-hover table-sm permission_table">';
        echo '<tr class="bg-secondary text-000">
                <th>Features/Pages</th> 
                <th>All</th>
                <th>Read</th>
                <th>Write</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>Status</th>
            </tr>';
        foreach ($permission as $fields_val):
            $exist = false;
            $exist_prmsn = array();
            if (!empty($per_id)):
                $key = array_search($fields_val->permission_id, array_column($per_id, 'per_id'));
                if (!empty($key) || $key === 0) {
                    $exist = true;
                    $exist_prmsn = $this->md->select_where('tbl_permission_assign', array('permission_assigned_id' => $per_id[$key]['assign_id']));
                }
            endif;
            $all = $read = $write = $edit = $delete = $status = 0;
            if ($exist):
                if (!empty($exist_prmsn)):
                    $all = $exist_prmsn[0]->all;
                    $read = $exist_prmsn[0]->read;
                    $write = $exist_prmsn[0]->write;
                    $edit = $exist_prmsn[0]->edit;
                    $delete = $exist_prmsn[0]->delete;
                    $status = $exist_prmsn[0]->status;
                endif;
            endif;
            if ($fields_val->type == 'page'):
                echo '<tr>';
                ?>
                <td class="font-weight-bold"><span
                            class="badge badge-dark zoom-08">Page</span> <?php echo ucfirst($fields_val->feature); ?>
                </td> <!-- Feature Name -->
                <td><?php if ($fields_val->all): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($all) ? 'checked' : ''; ?>
                               onchange="checkAllPermission(this)" value="1"
                               name="all_per[<?php echo $fields_val->permission_id; ?>]" class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << All Permission  -->
                <td><?php if ($fields_val->read): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($read) ? 'checked' : ''; ?>
                               name="read_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << Read Permission  -->
                <td><?php if ($fields_val->write): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($write) ? 'checked' : ''; ?>
                               name="write_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << Write Permission  -->
                <td><?php if ($fields_val->edit): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($edit) ? 'checked' : ''; ?>
                               name="edit_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << Edit Permission  -->
                <td><?php if ($fields_val->delete): ?><label class="custom-switch zoom-08 cursor-pointer ">
                        <input type="checkbox" <?php echo ($delete) ? 'checked' : ''; ?>
                               name="delete_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label><?php endif; ?>
                </td> <!-- << Delete Permission  -->
                <td><?php if ($fields_val->status): ?><label class="custom-switch zoom-08 cursor-pointer ">
                        <input type="checkbox" <?php echo ($status) ? 'checked' : ''; ?>
                               name="status_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label><?php endif; ?>
                </td> <!-- << Status Permission  -->
                <?php
                echo '</tr>';
            elseif ($fields_val->type == 'feature'):
                echo '<tr>';
                ?>
                <td class="font-weight-bold">
                    <span class="badge badge-info zoom-08">Feature</span> <?php echo ucfirst($fields_val->feature); ?>
                </td> <!-- Feature Name -->
                <td><?php if ($fields_val->all): ?> <label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($all) ? 'checked' : ''; ?>
                               onchange="checkAllPermission(this)" value="1"
                               name="feature_all_per[<?php echo $fields_val->permission_id; ?>]"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << All Permission  -->
                <td><?php if ($fields_val->read): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($read) ? 'checked' : ''; ?>
                               name="feature_read_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << Read Permission  -->
                <td>
                    <!-- Write Permission Not Require in Feature Module -->
                </td> <!-- << Write Permission  -->
                <td><?php if ($fields_val->edit): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($edit) ? 'checked' : ''; ?>
                               name="feature_edit_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << Edit Permission  -->
                <td>
                    <!-- Delete Permission Not Require in Feature Module -->
                </td> <!-- << Delete Permission  -->
                <?php
                echo '</tr>';
            endif;
        endforeach;
        echo '</table>';
    }

    // getProductStockData

    public function getBillData()
    {
        $role = $this->session->userdata('role');
        $billid = $this->input->post('billid');   // get Bill id
        if ($billid) {
            $billData = $this->md->select_where('tbl_bill', array('bill_id' => $billid));
            if ($billData) {
                $billStatus = $billData[0]->status;
                ?>
                <h5 class="text-info">Order ID: <?php echo $billData[0]->order_id; ?></h5>
                <div class="row mt-20">
                    <div class="col-md-12">
                        <input type="hidden" value="<?php echo $billData[0]->bill_id; ?>" name="bill_id">
                        <div class="form-group">
                            <label>Select Status</label>
                            <select class="form-control" name="status"
                                <?php echo $role != 'admin' ? 'disabled' : ''; ?>
                                    onchange="showFieldStatusWise(this.value);">
                                <option <?php echo $billStatus == 'Inquired' ? 'selected' : ''; ?> >Inquired
                                </option>
                                <option <?php echo $billStatus == 'Estimate' ? 'selected' : ''; ?> >
                                    Estimate
                                </option>
                                <option <?php echo $billStatus == 'On Hold' ? 'selected' : ''; ?> >On Hold
                                </option>
                                <option <?php echo $billStatus == 'Dispatch' ? 'selected' : ''; ?>
                                        value="Dispatch">
                                    Ready to Dispatch
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">

                        <div class="form-group mb-0" id="cancelOrderBtn"
                             style="display: <?php echo $billStatus == 'On Hold' ? 'block' : 'none'; ?>">
                            <a class="btn btn-danger"
                               href="<?php echo base_url('Admin/Pages/cancelOrder/' . $billData[0]->bill_id); ?>"
                               onclick="return confirm('Are you sure you want to Cancel this order?');"> <i
                                        class="fa fa-times-circle"></i> Cancel Order</a>
                        </div>
                        <div class="form-group mb-0" id="shippedFileUpload"
                             style="display: <?php echo $billStatus == 'Dispatch' ? 'block' : 'none'; ?>">
                            <label class="control-label">Select Shipped Photo
                                <span style="font-size: 12px"
                                      class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span>
                            </label>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <input type="file" id="file"
                                           multiple
                                           onchange="readURL(this, 'blah');$(this).next().val('yes');"
                                           name="shippedPhoto[]" accept="image/*">
                                    <input type="hidden" id="updateStatus" name="updateStatus"/>
                                    <input type="hidden" value="<?php echo $billData[0]->shippedPhoto; ?>"
                                           name="oldPath"/>
                                </div>
                            </div>
                            <div class="row">
                                <?php
                                $allphotos = $billData[0]->shippedPhoto ? explode(",", $billData[0]->shippedPhoto) : [];
                                if (!empty($allphotos)) :
                                    $pp = 0;    // photo index
                                    foreach ($allphotos as $photo) :
                                        ?>
                                        <div class="col-md-3">
                                            <img
                                                    src="<?php echo base_url($photo); ?>"
                                                    class="mt-20 center-block"
                                                    style="width: 100%;height: 100px;object-fit: contain"/>
                                        </div>
                                        <?php
                                        $pp++;
                                    endforeach;
                                endif;
                                ?>
                            </div>
                            <p class="error-text file-error" style="display: none">Select a valid photo!</p>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group mb-0" id="no_of_bundles"
                                     style="display: <?php echo $billStatus == 'Dispatch' ? 'block' : 'none'; ?>">
                                    <label class="control-label">Number of Bundles
                                    </label>
                                    <input type="text" class="form-control"
                                           value="<?php echo $billData[0]->no_of_bundles; ?>"
                                           name="no_of_bundles"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-0" id="lr_no"
                                     style="display: <?php echo $billStatus == 'Dispatch' ? 'block' : 'none'; ?>">
                                    <label class="control-label">L/R Number
                                    </label>
                                    <input type="text" class="form-control" value="<?php echo $billData[0]->lr_no; ?>"
                                           name="lr_no"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-0" id="date_of_lr"
                                     style="display: <?php echo $billStatus == 'Dispatch' ? 'block' : 'none'; ?>">
                                    <label class="control-label">Date of L/R
                                    </label>
                                    <input type="date" class="form-control"
                                           value="<?php echo $billData[0]->date_of_lr; ?>"
                                           name="date_of_lr"/>
                                </div>
                            </div>
                            <?php
                            if ($billData[0]->no_of_bundles && $billData[0]->lr_no && $billData[0]->date_of_lr) {
                                ?>
                                <div class="col-md-4 mt-15">
                                    <a class="btn btn-danger"
                                       href="<?php echo base_url('Admin/Pages/dispatchedOrder/' . $billData[0]->bill_id); ?>"
                                       onclick="return confirm('Are you sure you want to Dispatched this order?');"> <i
                                                class="fa fa-truck-moving"></i> Dispatched Order</a>
                                </div>
                                <?php
                            }
                            ?>

                        </div>
                    </div>
                    <div class="col-md-12" id="billInfo"
                         style="display: <?php echo $billStatus != 'Inquired' ? 'block' : 'none'; ?>">
                        <div class="row">
                            <div class="col-md-12 mb-30 table-responsive">
                                <table class="customTable mt-30">
                                    <thead>
                                    <tr>
                                        <th>Location</th>
                                        <th>Frame No</th>
                                        <th>Box</th>
                                        <th>Stick</th>
                                        <th>FT</th>
                                        <th>Rate</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody class="calculation">
                                    <?php
                                    $subTotal = $totalQty = $totalstick = $totalft = $totalrate = 0;
                                    $items = $this->md->select_where('tbl_transaction', array('bill_id' => $billid));
                                    if ($items) {
                                        foreach ($items as $item) {
                                            $product = $this->md->select_where('tbl_product', array('product_id' => $item->product_id));
                                            $amount = $item->qty * $item->stick * $item->ft * $item->rate;
                                            $subTotal = $subTotal + $amount;
                                            $totalQty = $totalQty + $item->qty;
                                            $totalstick = $totalstick + $item->stick;
                                            $totalft = $totalft + $item->ft;
                                            $totalrate = $totalrate + $item->rate;
                                            //$locations = $this->md->select_where('tbl_inventory', array('production' => 0, 'product_id' => $item->product_id));
                                            $locations = $this->md->select_where('tbl_inventory', array('product_id' => $item->product_id));
                                            ?>
                                            <tr class="text-center">
                                                <td>
                                                    <?php
                                                    if (!empty($locations)):
                                                        foreach ($locations as $key => $location):
                                                            $locationInfo = $this->md->my_query('SELECT * FROM `tbl_location` WHERE `location_id` = ' . $location->location_id)->result();
//                                                            if ($locationInfo[0]->title != "production") {

                                                            $checkExistStock['bill_id'] = $billid;
                                                            $checkExistStock['product_id'] = $item->product_id;
                                                            $checkExistStock['inventory_id'] = $location->inventory_id;
                                                            $checkExistStock['location_id'] = $location->location_id;

                                                            $stocksCounter = $this->md->select_where('tbl_temp', $checkExistStock);
                                                            ?>
                                                            <div
                                                                    class="display-flex <?php echo $key != 0 ? 'mt-10' : ''; ?>"
                                                                    style="align-items: center;justify-content: space-between;">
                                                                <label
                                                                        class="font-weight-bold mr-10"><?php echo ucfirst($locationInfo ? $locationInfo[0]->title : ''); ?>
                                                                    (<?php echo $location->stock ? $location->stock : 0; ?>
                                                                    )
                                                                </label>
                                                                <input type="number"
                                                                       name="location[<?php echo $item->transaction_id ?>][]"
                                                                       min="0"
                                                                       value="<?php echo $stocksCounter ? $stocksCounter[0]->stock : 0; ?>"
                                                                       max="<?php echo $location->stock ? $location->stock : 0; ?>"/>
                                                            </div>
                                                        <?php
//                                                            }
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                    <?php //echo($product ? $product[0]->location : '');
                                                    ?>
                                                </td>
                                                <td><?php echo($product ? $product[0]->title : ''); ?> </td>
                                                <td>
                                                    <input type="number"
                                                           min="1"
                                                        <?php echo $role != 'admin' ? 'readonly' : ''; ?>
                                                           step="any"
                                                           id="calBox"
                                                           name="box[]"
                                                           value="<?php echo $item->qty; ?>"/>
                                                </td>
                                                <td>
                                                    <input type="number"
                                                           min="1"
                                                           name="stick[]"
                                                           id="calStick"
                                                           step="any"
                                                           readonly
                                                           value="<?php echo $item->stick; ?>"/>
                                                </td>
                                                <td>
                                                    <input type="number"
                                                           name="ft[]"
                                                           id="calFt"
                                                           step="any"
                                                           readonly
                                                           value="<?php echo $item->ft; ?>"/>
                                                </td>
                                                <td>
                                                    <input type="number"
                                                           min="1"
                                                        <?php echo $role != 'admin' ? 'readonly' : ''; ?>
                                                           name="rate[]"
                                                           id="calRate"
                                                           step="any"
                                                           value="<?php echo $item->rate; ?>"/>
                                                </td>
                                                <td>
                                                    <input type="number"
                                                           min="1"
                                                           readonly
                                                           id="calAmount"
                                                           name="amount[]"
                                                           step="any"
                                                           value="<?php echo $amount; ?>"/>
                                                    <input type="hidden"
                                                           readonly
                                                           name="transaction_id[]"
                                                           value="<?php echo $item->transaction_id; ?>"/>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    $cartage = $totalQty * 100;
                                    $gst = ($subTotal * 18) / 100;
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="font-16 text-center font-weight-bold">
                                        <td colspan="2">Cartage</td>
                                        <td class="totalBox"><?php echo $totalQty; ?></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>
                                            <input type="number"
                                                   class="cartageDefaultAmount"
                                                   name="cartageDefault"
                                                   step="any"
                                                   value="<?php echo($billData[0]->cartageDefault ? ($billData[0]->cartageDefault != 0 ? $billData[0]->cartageDefault : 100) : 100); ?>"/>
                                        </td>
                                        <td>
                                            <input type="number"
                                                   class="cartageAmount"
                                                   name="cartage"
                                                   step="any"
                                                   value="<?php echo $cartage; ?>"/>
                                        </td>
                                    </tr>
                                    <tr class="font-16 text-center font-weight-bold">
                                        <td colspan="2">
                                            GST
                                            <select name="gstStatus" id="gstStatus">
                                                <option <?php echo($billData[0]->gstStatus ? ($billData[0]->gstStatus == "Percentage" ? "selected" : "") : ""); ?>>
                                                    Percentage
                                                </option>
                                                <option <?php echo($billData[0]->gstStatus ? ($billData[0]->gstStatus == "Rate" ? "selected" : "") : ""); ?>>
                                                    Rate
                                                </option>
                                            </select>
                                        </td>
                                        <td>
                                            <span class="gstPercentage"
                                                  style="display: <?php echo($billData[0]->gstStatus ? ($billData[0]->gstStatus == "Percentage" ? "block" : "none") : "block"); ?>">18%</span>
                                        </td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td><input type="number"
                                                   style="display: <?php echo($billData[0]->gstStatus ? ($billData[0]->gstStatus == "Rate" ? "block" : "none") : "none"); ?>"
                                                   class="gstDefaultAmount"
                                                   name="gstDefaultAmount"
                                                   step="any"
                                                   value="<?php echo($billData[0]->gstDefaultAmount ? ($billData[0]->gstDefaultAmount != 0 ? $billData[0]->gstDefaultAmount : 540) : 540); ?>"/>
                                        </td>
                                        <td>
                                            <input type="number"
                                                   step="any"
                                                   class="gstAmount"
                                                   name="gst"
                                                   value="<?php echo($billData[0]->gstStatus ? ($billData[0]->gstStatus == "Percentage" ? $billData[0]->gst : $billData[0]->gst) : $gst); ?>"/>
                                        </td>
                                    </tr>
                                    <tr class="font-16 text-center font-weight-bold">
                                        <td colspan="2">Total</td>
                                        <td class="totalBox"><?php echo $totalQty; ?></td>
                                        <td id="totalStick"><?php echo $totalstick; ?></td>
                                        <td id="totalFt"><?php echo $totalft; ?></td>
                                        <td id="totalRate"><?php echo $totalrate; ?></td>
                                        <td>
                                            <input type="number"
                                                   class="totalAmount"
                                                   step="any"
                                                   name="total"
                                                   value="<?php echo $subTotal + $gst + $cartage; ?>"/>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Enter Booking Destination</label>
                                    <input type="text" placeholder="Booking Destination" class="form-control"
                                           name="booking_destination"
                                           value="<?php echo $billData[0]->booking_destination; ?>"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Enter Name Transport</label>
                                    <input type="text" placeholder="Booking Name Transport" class="form-control"
                                           name="name_transport"
                                           value="<?php echo $billData[0]->name_transport; ?>"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Enter Name Sales Person</label>
                                    <input type="text" placeholder="Sales Person" class="form-control"
                                           name="name_sales_person"
                                           value="<?php echo $billData[0]->name_sales_person; ?>"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Enter Drop Location</label>
                                    <input type="text" placeholder="Drop Location" class="form-control"
                                           name="drop_location"
                                           value="<?php echo $billData[0]->drop_location; ?>"/>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Enter NEFT/RTGS Amount</label>
                                    <input type="number" placeholder="NEFT/RTGS Amount" class="form-control"
                                           step="any"
                                           id="neft_rtgs"
                                           min="1"
                                           name="neft_rtgs"
                                           value="<?php echo $billData[0]->neft_rtgs; ?>"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Enter Cash Amount</label>
                                    <input type="number" placeholder="Cash Amount" class="form-control"
                                           step="any"
                                           id="calCash"
                                           min="1"
                                           name="cash"
                                           value="<?php echo $billData[0]->cash; ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                echo "Sorry, Bill data not found!";
            }
        }
    }

    // Dispatched Order
    public function dispatchedOrder()
    {
        $bill_id = $this->uri->segment(4);   // get bill_id
        $billData = $this->md->select_where('tbl_bill', array('bill_id' => $bill_id));
        if ($billData) {
            $this->md->update('tbl_bill', ['status' => 'Dispatched'], ['bill_id' => $bill_id]);
            redirect('manage-bill');
        } else {
            echo "Sorry, bill ID not found!";
            exit;
        }
    }

    public function getProductStockData()
    {
        $proid = $this->input->post('proid');   // get pro id
        $type = $this->input->post('type');   // get type
        $table = $this->input->post('table');   // product / location
        if ($proid) {
            if ($table == 'product') {
                $proData = $this->md->select_where('tbl_product', array('product_id' => $proid));
                $locations = $this->md->select('tbl_location');    // get all location
                $inventories = $this->md->select_where('tbl_inventory', array('product_id' => $proid));
                $totalStock = $this->md->my_query('SELECT sum(stock) as `totalStock` FROM `tbl_inventory` WHERE `product_id` = ' . $proid)->result();
                if ($proData) {
                    if ($type == 'add') {
                        ?>
                        <form method="post" name="inventory_form" id="inventory_form">
                            <div class="">
                                <h5>SKU: <?php echo $proData ? $proData[0]->sku : ''; ?></h5>
                                <input type="hidden" value="product"
                                       name="table"/>
                                <input type="hidden" value="<?php echo $proData ? $proData[0]->product_id : ''; ?>"
                                       name="product_id"/>

                                <div class="addContent mt-20">
                                    <?php
                                    if (empty($inventories)) {
                                        ?>
                                        <div class="item row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="control-label">Product location </label>
                                                    <select name="location[]"
                                                            required
                                                            class="form-control select2 text-capitalize <?php if (form_error('location')) { ?> is-invalid <?php } ?>">
                                                        <?php
                                                        if (!empty($locations)):
                                                            foreach ($locations as $location):
                                                                ?>
                                                                <option
                                                                        value="<?php echo $location->location_id; ?>"><?php echo $location->title; ?></option>
                                                            <?php
                                                            endforeach;
                                                        endif;
                                                        ?>
                                                    </select>
                                                    <div class="error-text">
                                                        <?php
                                                        if (form_error('location')) {
                                                            echo form_error('location');
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="control-label">Product Stock </label>
                                                    <input type="number" step="any" name="stock[]"
                                                           required
                                                           value="<?php
                                                           if (set_value('stock') && !isset($success)) {
                                                               echo set_value('stock');
                                                           }
                                                           ?>" placeholder="Enter Product Stock"
                                                           class="form-control <?php if (form_error('stock')) { ?> is-invalid <?php } ?>">
                                                    <div class="error-text">
                                                        <?php
                                                        if (form_error('stock')) {
                                                            echo form_error('stock');
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <button title="Add Item" class="btn btn-md btn-primary addMore"
                                                    type="button"><i
                                                        class="fa fa-plus"></i></button>
                                        </div>
                                        <?php
                                    } else {
                                        foreach ($inventories as $key => $inventory) {
                                            ?>
                                            <div class="item row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="control-label">Product location </label>
                                                        <select name="location[]"
                                                                required
                                                                class="form-control select2 text-capitalize <?php if (form_error('location')) { ?> is-invalid <?php } ?>">
                                                            <option value="">Select Location</option>
                                                            <?php
                                                            if (!empty($locations)):
                                                                foreach ($locations as $location):
                                                                    ?>
                                                                    <option
                                                                        <?php echo $inventory->location_id == $location->location_id ? 'selected' : ''; ?>
                                                                            value="<?php echo $location->location_id; ?>"><?php echo $location->title; ?></option>
                                                                <?php
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('location')) {
                                                                echo form_error('location');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="control-label">Product Stock </label>
                                                        <input type="number" step="any" name="stock[]"
                                                               required
                                                               value="<?php
                                                               if (set_value('stock') && !isset($success)) {
                                                                   echo set_value('stock');
                                                               } else {
                                                                   echo $inventory->stock;
                                                               }
                                                               ?>" placeholder="Enter Product Stock"
                                                               class="form-control <?php if (form_error('stock')) { ?> is-invalid <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('stock')) {
                                                                echo form_error('stock');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                echo ($key > 0) ? '<button title="Remove Item" class="btn btn-md btn-danger removeLast" type="button"><i class="fa fa-minus"></i></button>' : '<button title="Add Item" class="btn btn-md btn-primary addMore" type="button"><i class="fa fa-plus"></i></button>';
                                                ?>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>

                                <button type="submit" name="add" value="add" class="btn btn-primary"
                                        form="inventory_form">Add Stock
                                </button>
                            </div>
                        </form>
                        <?php
                    } elseif ($type == 'check') {
                        if (!empty($inventories)) {
                            ?>
                            <div class="mb-30">
                                <h5>SKU: <?php echo $proData ? $proData[0]->sku : ''; ?></h5>
                                <h5>Available
                                    Stock: <?php echo($totalStock ? ($totalStock[0]->totalStock ? $totalStock[0]->totalStock : 0) : 0); ?></h5>
                            </div>
                            <?php
                            foreach ($inventories as $key => $inventory) {
                                ?>
                                <div class="row mt-10">
                                    <div class="col-md-6">
                                        <div class="form-group mb-0">
                                            <label class="control-label">Product Stock </label>
                                            <input type="number"
                                                   disabled
                                                   value="<?php
                                                   echo $inventory->stock;
                                                   ?>" placeholder="Enter Product Stock"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group mb-0">
                                            <label class="control-label">Product location </label>
                                            <select
                                                    disabled
                                                    class="form-control text-capitalize">
                                                <option value="">Select Location</option>
                                                <?php
                                                if (!empty($locations)):
                                                    foreach ($locations as $location):
                                                        ?>
                                                        <option
                                                            <?php echo $inventory->location_id == $location->location_id ? 'selected' : ''; ?>
                                                                value="<?php echo $location->location_id; ?>"><?php echo $location->title; ?></option>
                                                    <?php
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            echo "Sorry, Stock not available!";
                        }
                    }
                } else {
                    echo "Sorry, product not found!";
                }
            } elseif ($table == 'location') {
                $proData = $this->md->select_where('tbl_location', array('location_id' => $proid));
                $products = $this->md->select('tbl_product');    // get all product
                $inventories = $this->md->select_where('tbl_inventory', array('location_id' => $proid));
                $totalStock = $this->md->my_query('SELECT sum(stock) as `totalStock` FROM `tbl_inventory` WHERE `location_id` = ' . $proid)->result();
                if ($proData) {
                    if ($type == 'add') {
                        ?>
                        <form method="post" name="inventory_form" id="inventory_form">
                            <div class="">
                                <h5>Location: <?php echo $proData ? $proData[0]->title : ''; ?></h5>

                                <input type="hidden" value="location"
                                       name="table"/>
                                <input type="hidden" value="<?php echo $proData ? $proData[0]->location_id : ''; ?>"
                                       name="location_id"/>

                                <div class="addContent mt-20">
                                    <?php
                                    if (empty($inventories)) {
                                        ?>
                                        <div class="item row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="control-label">Product Stock </label>
                                                    <input type="number" step="any" name="stock[]"
                                                           required
                                                           value="<?php
                                                           if (set_value('stock') && !isset($success)) {
                                                               echo set_value('stock');
                                                           }
                                                           ?>" placeholder="Enter Product Stock"
                                                           class="form-control <?php if (form_error('stock')) { ?> is-invalid <?php } ?>">
                                                    <div class="error-text">
                                                        <?php
                                                        if (form_error('stock')) {
                                                            echo form_error('stock');
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="control-label">Select Product </label>
                                                    <select name="product[]"
                                                            required
                                                            class="form-control select2 text-capitalize <?php if (form_error('product')) { ?> is-invalid <?php } ?>">
                                                        <?php
                                                        if (!empty($products)):
                                                            foreach ($products as $product):
                                                                ?>
                                                                <option
                                                                        value="<?php echo $product->product_id; ?>"><?php echo $product->title; ?></option>
                                                            <?php
                                                            endforeach;
                                                        endif;
                                                        ?>
                                                    </select>
                                                    <div class="error-text">
                                                        <?php
                                                        if (form_error('product')) {
                                                            echo form_error('product');
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <button title="Add Item" class="btn btn-md btn-primary addMore"
                                                    type="button"><i
                                                        class="fa fa-plus"></i></button>
                                        </div>
                                        <?php
                                    } else {
                                        foreach ($inventories as $key => $inventory) {
                                            ?>
                                            <div class="item row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="control-label">Product Stock </label>
                                                        <input type="number" step="any" name="stock[]"
                                                               required
                                                               value="<?php
                                                               if (set_value('stock') && !isset($success)) {
                                                                   echo set_value('stock');
                                                               } else {
                                                                   echo $inventory->stock;
                                                               }
                                                               ?>" placeholder="Enter Product Stock"
                                                               class="form-control <?php if (form_error('stock')) { ?> is-invalid <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('stock')) {
                                                                echo form_error('stock');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="control-label">Product </label>
                                                        <select name="product[]"
                                                                required
                                                                class="form-control select2 text-capitalize <?php if (form_error('product')) { ?> is-invalid <?php } ?>">
                                                            <option value="">Select Product</option>
                                                            <?php
                                                            if (!empty($products)):
                                                                foreach ($products as $product):
                                                                    ?>
                                                                    <option
                                                                        <?php echo $inventory->product_id == $product->product_id ? 'selected' : ''; ?>
                                                                            value="<?php echo $product->product_id; ?>"><?php echo $product->title; ?></option>
                                                                <?php
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('product')) {
                                                                echo form_error('product');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                echo ($key > 0) ? '<button title="Remove Item" class="btn btn-md btn-danger removeLast" type="button"><i class="fa fa-minus"></i></button>' : '<button title="Add Item" class="btn btn-md btn-primary addMore" type="button"><i class="fa fa-plus"></i></button>';
                                                ?>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>

                                <button type="submit" name="add" value="add" class="btn btn-primary"
                                        form="inventory_form">Add Stock
                                </button>
                            </div>
                        </form>
                        <?php
                    } elseif ($type == 'check') {
                        if (!empty($inventories)) {
                            ?>
                            <div class="mb-30">
                                <h5>Location: <?php echo $proData ? $proData[0]->title : ''; ?></h5>
                                <h5>Available
                                    Stock: <?php echo($totalStock ? ($totalStock[0]->totalStock ? $totalStock[0]->totalStock : 0) : 0); ?></h5>
                            </div>
                            <?php
                            foreach ($inventories as $key => $inventory) {
                                ?>
                                <div class="row mt-10">
                                    <div class="col-md-6">
                                        <div class="form-group mb-0">
                                            <label class="control-label">Product Stock </label>
                                            <input type="number"
                                                   disabled
                                                   value="<?php
                                                   echo $inventory->stock;
                                                   ?>" placeholder="Enter Product Stock"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group mb-0">
                                            <label class="control-label">Select Product </label>
                                            <select
                                                    disabled
                                                    class="form-control text-capitalize">
                                                <option value="">Select Product</option>
                                                <?php
                                                if (!empty($products)):
                                                    foreach ($products as $product):
                                                        ?>
                                                        <option
                                                            <?php echo $inventory->product_id == $product->product_id ? 'selected' : ''; ?>
                                                                value="<?php echo $product->product_id; ?>"><?php echo $product->title; ?></option>
                                                    <?php
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            echo "Sorry, Stock not available!";
                        }
                    }
                } else {
                    echo "Sorry, product not found!";
                }
            }
        }
    }

    /*
      C O M M O N  -  D E L E T E
     */


    // Remove Product photo Data

    public function cancelOrder()
    {
        $billId = $this->uri->segment(4);
        if ($billId) {
            $billData = $this->md->select_where('tbl_bill', array('bill_id' => $billId));
            if ($billData) {
                // Delete temporary stock data from Bill ID
                $this->md->delete('tbl_temp', array('bill_id' => $billId));
                // Change bill Status to cancelled
                $this->md->update('tbl_bill', array('cancelStatus' => 1), array('bill_id' => $billId));
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    // Remove photo Data

    public function removeProductPhoto()
    {
        $table = 'product_attribute';
        $id = $this->uri->segment(4);
        $path = $this->uri->segment(5);
        if ($this->db->table_exists($this->table_prefix . $table)) :
            $data = $this->md->select_where($this->table_prefix . $table, array($table . '_id' => $id))[0]->product_photos;
            $photos = explode(",", $data);
            unlink($photos[$path]);
            unset($photos[$path]);
            $this->md->update($this->table_prefix . $table, array('product_photos' => implode(",", $photos)), array($table . '_id' => $id));
            redirect($_SERVER['HTTP_REFERER']);
        endif;
    }

    // Common Remove Data

    public function removePhoto()
    {
        $table = $this->uri->segment(4);
        $id = $this->uri->segment(5);
        $path = $this->uri->segment(6);
        if ($this->db->table_exists($this->table_prefix . $table)) :
            $data = $this->md->select_where($this->table_prefix . $table, array($table . '_id' => $id))[0]->photos;
            $photos = explode(",", $data);
            unlink($photos[$path]);
            unset($photos[$path]);
            $this->md->update($this->table_prefix . $table, array('photos' => implode(",", $photos)), array($table . '_id' => $id));
            redirect($_SERVER['HTTP_REFERER']);
        endif;
    }

}

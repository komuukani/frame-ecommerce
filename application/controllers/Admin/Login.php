<?php

/* * *
 * Project:    Enzo Admin Pro
 * Name:       Admin Login & Settings
 * Package:    Login.php
 * About:      A controller that handle admin login and other website changes of project
 * Copyright:  (C) 2022
 * Author:     Nishant Thummar
 * Website:    https://nishantthummar.in/
 * * */
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    // Global Variables
    public $website, $developer, $web_link, $table_prefix, $web_data, $admin_data, $savepath = './admin_asset/';

    // Constructor
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('cookie');
        $this->developer = DEVELOPER;
        $this->web_link = WEBLINK;
        $this->table_prefix = TABLE_PREFIX;     // SET TABLE PREFIX (TBL_) 
        // Check if database already connected or not [STATUS = PRE/POST] 
        $settings = INSTALLER_SETTING;
        if ($settings['status'] == "pre") {
            redirect('/install');
        }
        $this->web_data = $this->md->select('tbl_web_data');    // SELECT WEBSITE DATA
        if ($this->session->userdata('aemail') == USERNAME) {
            $this->admin_data = array((object)USERDATA);
        } else {
            $this->admin_data = $this->md->select_where('tbl_admin', array('email' => $this->session->userdata('aemail'))); // SELECT ADMIN DATA
        }
        date_default_timezone_set(($this->web_data) ? $this->md->getItemName('tbl_timezone', 'timezone_id', 'timezone', $this->web_data[0]->timezone) : DEFAULT_TIMEZONE);
    }

    // Check login security
    public function security()
    {
        if ($this->session->userdata('aemail') == "")
            redirect('authorize');
    }

    // Common Function for setting up global page variable and send it to page.
    protected function setParameter($path, $page, $title, $header, $sidebar, $page_breadcumb, $breadcrumb_icon, $datatables)
    {
        ($page != 'login' && $page != "forgot_password") ? $this->security() : '';  // check security for all pages accept login page
        $control_data = $this->md->select_where($this->table_prefix . 'control', array('control_id' => 1));
        if ($this->db->field_exists($page, $this->table_prefix . 'control') && !$control_data[0]->$page)
            redirect('authorize');
        $data = [];
        $data['page_title'] = ucwords($title);  // set page title
        $data['web_data'] = $this->web_data;    // get all Website data
        $data['admin_data'] = $this->admin_data;    // get all Admin data
        $data['website_title'] = ($this->web_data) ? $this->web_data[0]->project_name : 'Admin Panel'; // get project name from admin table
        $data['developer'] = $this->developer;  // get developer name from admin table
        $data['web_link'] = $this->web_link;    // get developer website link
        $data['page'] = $path . '/' . $page;   //  open page from admin folder
        $data['active_page'] = $page;   // get active page name
        $data['current_page'] = $this->uri->segment(1);     // get current page
        $data['header'] = $header;
        $data['sidebar'] = $sidebar;
        $data['breadcrumb'] = $page_breadcumb;
        $data['breadcrumb_icon'] = $breadcrumb_icon;
        $data['datatables'] = $datatables;
        if ($this->db->table_exists($this->table_prefix . $page))
            $data[$page] = $this->md->select($this->table_prefix . $page);
        return $data;
    }

    // Something went wrong 500
    public function page_500()
    {
        $this->load->view('admin/errors/500');
    }


    // Admin Login
    public function index()
    {
        $data = $this->setParameter("admin/auth", "login", "Admin Login", FALSE, FALSE, FALSE, "", FALSE);
        if ($this->input->post('send')) :
            $this->form_validation->set_rules('username', 'UserName', 'required', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('password', 'Password', 'required', array("required" => "Enter Password!"));
            if ($this->form_validation->run() == TRUE) :
                if (login($this->input->post('username'))) :
                    $this->session->set_userdata('aemail', $this->input->post('username')); // store admin email
                    $this->session->set_userdata('lastseen', date('Y-m-d h:i:s')); // store last login
                    $this->session->set_flashdata('success', 'Login successfully.');
                    redirect('admin-dashboard'); // redirect to dashboard after successfully login
                else:

                    $this->session->set_userdata('remember_me', $this->input->post('remember_me'));

                    $wh['email'] = $this->input->post('username'); // where condition
                    $admin_data = $this->md->select_where('tbl_admin', $wh); // get admin data from where condition
                    // check whether admin data is exist and both password matched ↴
                    if (!empty($admin_data) && $this->encryption->decrypt($admin_data[0]->password) == $this->input->post('password')) :

                        if ($this->input->post('remember_me')) {
                            $time = 60 * 25;
                            set_cookie('aemail', $this->input->post('username'), $time);
                            set_cookie('password', $this->encryption->decrypt($admin_data[0]->password), $time);

                        } else {

                            if ($this->input->cookie('aemail')) {
                                set_cookie('aemail', "", -10);
                                set_cookie('password', "", -10);
                            }
                        }
                        if ($admin_data[0]->status) :
                            $role = ($admin_data[0]->user_role_id == 12 ? 'manager' : ($admin_data[0]->user_role_id == 13 ? 'delivery' : 'admin'));
                            $this->session->set_userdata('aemail', $wh['email']); // store admin email
                            $this->session->set_userdata('lastseen', date('Y-m-d h:i:s')); // store last login
                            $this->session->set_flashdata('success', 'Login successfully.');
                            $this->session->set_flashdata('role', $role);
                            redirect('admin-dashboard'); // redirect to dashboard after successfully login
                        else :
                            $this->session->set_flashdata('error', 'Sorry, you are not an active user!');
                        endif;
                    else :
                        $this->session->set_flashdata('error', 'Sorry, Authentication Fail');
                    endif;
                endif;
            else :
                $this->session->set_flashdata('error', 'Sorry, Authentication Fail');
            endif;
        endif;
        $this->load->view('admin/common/master', $data);
    }

    // Admin Forget Password / Recover Password
    public function forgot_password()
    {
        $data = $this->setParameter("admin/auth", "forgot_password", "Forgot Password", FALSE, FALSE, FALSE, "", FALSE);
        if ($this->input->post('send')) {
            $this->form_validation->set_rules('username', 'UserName', 'required', array("required" => "Enter Email Address!"));
            if ($this->form_validation->run() == TRUE) {
                if (web_status()) :
                    $wh['email'] = $this->input->post('username');
                    $adminData = $this->md->select_where('tbl_admin', $wh);
                    if (!empty($adminData)) {
                        $password = $this->encryption->decrypt($adminData[0]->password);
                        $this->md->sendMail($this->admin_data[0]->mail_smtp_host, $this->admin_data[0]->mail_email, $this->admin_data[0]->mail_password, $this->admin_data[0]->mail_subject, $this->admin_data[0]->mail_message, $adminData[0]->email_address);  // Send Email
                        $this->session->set_flashdata('success', 'Password has been sent on your registered email address.');
                    } else {
                        $this->session->set_flashdata('error', 'Sorry, Email address not found!');
                    }
                endif;
            } else {
                $this->session->set_flashdata('error', 'Sorry, Authentication Fail');
            }
        }
        $this->load->view('admin/common/master', $data);
    }

    // Admin Dashboard
    public function dashboard()
    {
        $data = $this->setParameter("admin/auth", "dashboard", "Admin Dashboard", TRUE, TRUE, TRUE, "fa fa-rocket font-20 mr-5", FALSE);
        $this->load->view('admin/common/master', $data);
    }

    // Admin Profile Setting
    public function profile()
    {
        $data = $this->setParameter("admin/auth", "profile", "Admin Profile", TRUE, TRUE, TRUE, "fas fa-user font-20 mr-5", FALSE);
        $user = $this->session->userdata('aemail');
        if ($this->input->post('update')) {
            $this->form_validation->set_rules('admin_name', 'Admin Full Name', 'required', array("required" => "Enter Admin Fullname!"));
            $this->form_validation->set_rules('phone', 'phone', 'required', array("required" => "Enter phone!"));
            $this->form_validation->set_rules('email', 'email', 'required|valid_email', array("required" => "Enter email!"));
            if ($this->form_validation->run() == TRUE) {
                if (web_status()) :
                    // Check already Profile Photo data is exist or not, if yes than it will remove and than upload.
                    $this->input->post('updateProfileStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Profile Photo

                    $data1['admin_name'] = $this->input->post('admin_name');
                    $data1['phone'] = $this->input->post('phone');
                    $data1['email'] = $this->input->post('email');
                    $data1['address'] = $this->input->post('address');
                    $data1['bio'] = $this->input->post('bio');
                    $this->input->post('updateProfileStatus') && ($data1['path'] = $this->md->uploadFile('profile'));    // Upload Profile Photo and return path from model
                    $dt = $this->md->update('tbl_admin', $data1, array('email' => $user));
                    if (!empty($dt)) {
                        $this->session->set_userdata('aemail', $this->input->post('email')); // store admin email
                        $this->session->set_flashdata('success', 'Yeah, Data Updated Successfully.');
                        redirect('admin-profile');
                    } else {
                        $this->session->set_flashdata('error', "Sorry, something went wrong!");
                    }
                endif;
            } else {
                $this->session->set_flashdata('error', "Sorry, Enter proper field!");
            }
        } // change contact detail  [GENERAL CONTACT SETTINGS]
        $this->load->view('admin/common/master', $data);
    }

    // Feature Setting
    public function feature_setting()
    {
        $data = $this->setParameter("admin/auth", "feature_setting", "Admin Feature Setting", TRUE, TRUE, TRUE, "fas fa-cog font-20 mr-5", FALSE);
        $this->load->view('admin/common/master', $data);
    }

    // Control Panel - Admin whole setting page
    public function control_panel()
    {
        $type = $this->uri->segment(5);    // Page/Features Type
        if ($this->uri->segment(4)) :
            if (web_status()) :
                $action = $this->uri->segment(4);   // Page/Features Name
                $val = $this->input->post('val');   // boolean value [1/0]
                $this->md->update(($type == 'page' ? 'tbl_control' : 'tbl_control_feature'), array($action => $val), array(($type == 'page' ? 'control_id' : 'control_feature_id') => 1));
            endif;
        else :
            $data = $this->setParameter("admin/auth", "control_panel", "Admin Dashboard", TRUE, TRUE, TRUE, "fas fa-cogs font-20 mr-5", FALSE);
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Admin Setting
    public function setting()
    {
        $data = $this->setParameter("admin/auth", "setting", "Admin Setting", TRUE, TRUE, TRUE, "fas fa-cogs font-20 mr-5", FALSE);
        $user = $this->session->userdata('aemail');
        if ($this->input->post('send')) {
            $this->form_validation->set_rules('current', 'Current Password', 'required', array("required" => "Enter Current Password!"));
            $this->form_validation->set_rules('new', 'New Password', 'required', array("required" => "Enter New Password!"));
            $this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[new]', array("required" => "Enter Confirm Password!"));
            if ($this->form_validation->run() == TRUE) {
                if (web_status()) :
                    $current = $this->input->post('current');
                    $new = $this->input->post('new');
                    $dt = $this->md->select_where('tbl_admin', array('email' => $user));
                    if (!empty($dt)) {
                        $nn = $this->encryption->decrypt($dt[0]->password);
                        if ($nn == $current) {
                            $this->md->update('tbl_admin', array('password' => $this->encryption->encrypt($new)), array('email' => $user));
                            $this->session->set_flashdata('success', 'Yeah, Password Changed Successfully.');
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Current Password not matched!');
                        }
                    }
                endif;
            }
        } // Change Password    [SECURITY SETTINGS]
        if ($this->input->post('social')) {
            if (web_status()) :
                $data1['meta_keyword'] = $this->input->post('meta_keyword');
                $data1['meta_desc'] = $this->input->post('meta_desc');
                $data1['facebook'] = $this->input->post('facebook');
                $data1['instagram'] = $this->input->post('instagram');
                $data1['twitter'] = $this->input->post('twitter');
                $data1['linkedin'] = $this->input->post('linkedin');
                $data1['youtube'] = $this->input->post('youtube');
                $data1['whatsapp'] = $this->input->post('whatsapp');
                $dt = $this->md->update('tbl_web_data', $data1, array('web_data_id' => 1));
                if (!empty($dt)) {
                    $this->session->set_flashdata('success', 'Yeah, Social Data Updated Successfully.');
                } else {
                    $this->session->set_flashdata('error', "Sorry, something went wrong!");
                }
            endif;
        } // Change social links    [SEO & SOCIAL MEDIA SETTINGS]
        if ($this->input->post('contact')) {
            if (web_status()) :
                $data1['office'] = $this->input->post('office');
                $data1['phone'] = implode(",", $this->input->post('phone'));
                $data1['email_address'] = implode(",", $this->input->post('email'));
                $data1['address'] = $this->input->post('address');
                $data1['map'] = $this->input->post('map');
                $data1['footer'] = $this->input->post('footer');
                $dt = $this->md->update('tbl_web_data', $data1, array('web_data_id' => 1));
                if (!empty($dt)) {
                    $this->session->set_flashdata('success', 'Yeah, Contact Data Updated Successfully.');
                } else {
                    $this->session->set_flashdata('error', "Sorry, something went wrong!");
                }
            endif;
        } // change contact detail  [GENERAL CONTACT SETTINGS]
        if ($this->input->post('update')) {
            if (web_status()) :
                // Check already logo or favicon data is exist or not, if yes than it will remove and than upload.
                $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Logo  
                $this->input->post('updateStatusfavicon') && (($this->input->post('oldPathfavicon') != FILENOTFOUND) ? unlink($this->input->post('oldPathfavicon')) : '');  // Remove Old Favicon

                $dt['project_name'] = $this->input->post('project_name');
                $this->input->post('updateStatus') && ($dt['logo'] = $this->md->uploadFile('logo'));    // Upload Logo and return path from model
                $this->input->post('updateStatusfavicon') && ($dt['favicon'] = $this->md->uploadFile('favicon'));    // Upload Favicon and return path from model
                if ($this->md->update('tbl_web_data', $dt, array('web_data_id' => 1))) {
                    $this->session->set_flashdata('success', 'Yeah, Data Updated Successfully.');
                }
            endif;
        } // change logo & favicon & Name detail    [LOGO & FAVICON SETTINGS]
        if ($this->input->post('general_setting')) {
            $this->form_validation->set_rules('free_shipping_india', 'Free Shipping Over - India', 'required', array("required" => "Enter Free Shipping Over - India!"));
            $this->form_validation->set_rules('shipping_charge_india', 'Shipping Charge - India', 'required', array("required" => "Enter Shipping Charge - India!"));
            $this->form_validation->set_rules('free_shipping_usa', 'Free Shipping Over - USA', 'required', array("required" => "Enter Free Shipping Over - USA!"));
            $this->form_validation->set_rules('shipping_charge_usa', 'Shipping Charge - USA', 'required', array("required" => "Enter Shipping Charge - USA!"));
            if ($this->form_validation->run() == TRUE) {
                if (web_status()) :
                    $data1['free_shipping_india'] = $this->input->post('free_shipping_india');
                    $data1['shipping_charge_india'] = $this->input->post('shipping_charge_india');
                    $data1['free_shipping_usa'] = $this->input->post('free_shipping_usa');
                    $data1['shipping_charge_usa'] = $this->input->post('shipping_charge_usa');
                    $dt = $this->md->update('tbl_web_data', $data1, array('web_data_id' => 1));
                    if (!empty($dt)) {
                        $this->session->set_flashdata('success', 'Yeah, Data Updated Successfully.');
                    } else {
                        $this->session->set_flashdata('error', "Sorry, something went wrong!");
                    }
                endif;
            } else {
                $this->session->set_flashdata('error', validation_errors());
            }
        } // SHIPPING CHARGE      [GENERAL WEBSITE SETTING]
        if ($this->input->post('mail_save')) {
            $this->form_validation->set_rules('smtp_host', 'SMTP Host', 'required', array("required" => "Enter SMTP Host!"));
            $this->form_validation->set_rules('email_address', 'Email Address', 'required|valid_email', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('password', 'Password', 'required', array("required" => "Enter Password!"));
            $this->form_validation->set_rules('mail_protocol', 'Mail Protocol', 'required', array("required" => "Enter Mail Protocol!"));
            $this->form_validation->set_rules('mail_port', 'Mail Port', 'required', array("required" => "Enter Mail Port!"));
            // $this->form_validation->set_rules('subject', 'Subject', 'required', array("required" => "Enter Subject!"));
            // $this->form_validation->set_rules('message', 'Message', 'required', array("required" => "Enter Message!"));
            if ($this->form_validation->run() == TRUE) {
                if (web_status()) :
                    $data1['mail_smtp_host'] = $this->input->post('smtp_host');
                    $data1['mail_email'] = $this->input->post('email_address');
                    $data1['mail_password'] = $this->input->post('password');
                    $data1['mail_protocol'] = $this->input->post('mail_protocol');
                    $data1['mail_port'] = $this->input->post('mail_port');
                    $data1['forgot_mail_subject'] = $this->input->post('forgot_mail_subject');
                    $data1['forgot_mail_message'] = $this->input->post('forgot_mail_message');
                    $data1['welcome_mail_subject'] = $this->input->post('welcome_mail_subject');
                    $data1['welcome_mail_message'] = $this->input->post('welcome_mail_message');
                    $data1['inquiry_mail_subject'] = $this->input->post('inquiry_mail_subject');
                    $data1['inquiry_mail_message'] = $this->input->post('inquiry_mail_message');
                    $data1['active_mail_subject'] = $this->input->post('active_mail_subject');
                    $data1['active_mail_message'] = $this->input->post('active_mail_message');
                    $data1['inactive_mail_subject'] = $this->input->post('inactive_mail_subject');
                    $data1['inactive_mail_message'] = $this->input->post('inactive_mail_message');
                    $dt = $this->md->update('tbl_web_data', $data1, array('web_data_id' => 1));
                    if (!empty($dt)) {
                        $this->session->set_flashdata('success', 'Yeah, Email Data Updated Successfully.');
                    } else {
                        $this->session->set_flashdata('error', "Sorry, something went wrong!");
                    }
                endif;
            } else {
                $this->session->set_flashdata('error', validation_errors());
            }
        } // Mail Templates & Configuration     [EMAIL SETTINGS]
        if ($this->input->post('captcha')) {
            if (web_status()) :
                $data1['captcha_site_key'] = $this->input->post('site');
                $data1['captcha_secret_key'] = $this->input->post('secret');
                $data1['captcha_visibility'] = $this->input->post('visibility');
                $dt = $this->md->update('tbl_web_data', $data1, array('web_data_id' => 1));
                if (!empty($dt)) {
                    $this->session->set_flashdata('success', 'Yeah, Captcha Data Updated Successfully.');
                } else {
                    $this->session->set_flashdata('error', "Sorry, something went wrong!");
                }
            endif;
        } // change captcha detail  [GOOGLE CAPTCHA]
        if ($this->input->post('system')) {
            if (web_status()) :
                $data1['date_format'] = $this->input->post('date_format');
                $data1['time_format'] = $this->input->post('time_format');
                $data1['timezone'] = $this->input->post('timezone');
                $dt = $this->md->update('tbl_web_data', $data1, array('web_data_id' => 1));
                if (!empty($dt)) {
                    $this->session->set_flashdata('success', 'Yeah, System Info Changed Successfully.');
                } else {
                    $this->session->set_flashdata('error', "Sorry, something went wrong!");
                }
            endif;
        } // system detail  [DATE/TIME/TIMEZONE]
        if ($this->input->post('theme_btn')) {
            if (web_status()) :
                $data1['theme'] = $this->input->post('theme');
                $dt = $this->md->update('tbl_admin', $data1, array('email' => $user));
                if (!empty($dt)) {
                    $this->session->set_flashdata('success', 'Yeah, Theme Changed Successfully.');
                } else {
                    $this->session->set_flashdata('error', "Sorry, something went wrong!");
                }
            endif;
        } // change theme [THEME]
        if ($this->input->post('backup_btn')) {
            if (web_status()) :
                $export = $this->input->post('export');    // gzip, zip, txt
                $type = $this->input->post('type');     // all / selected table
                $table = $this->input->post('table');   // selected table
                $this->load->dbutil();
                $prefs = array(
                    'tables' => ($table) ? $table : array(),
                    'format' => $export, // gzip, zip, txt
                    'filename' => 'db_backup.sql'
                );
                $backup = &$this->dbutil->backup($prefs);
                $db_name = 'backup-on-' . date("Y-m-d-H-i-s") . '.' . $export;
                $save = 'pathtobkfolder/' . $db_name;

                $this->load->helper('file');
                write_file($save, $backup);

                $this->load->helper('download');
                force_download($db_name, $backup);
                $this->session->set_flashdata('success', 'Yeah, Database downloaded.');
            endif;
        } // DB Backup
        //$data['admin_data'] = $this->md->select_where('tbl_admin', array('email' => $user));
        $data['web_data'] = $this->md->select('tbl_web_data');
        $this->load->view('admin/common/master', $data);
    }

    // Admin Logout
    public function logout()
    {
        $where = array('email' => $this->session->userdata('aemail'));
        $data = array('lastseen' => $this->session->userdata('lastseen'));

        $this->md->update('tbl_admin', $data, $where);

        $this->session->unset_userdata('aemail');
        $this->session->unset_userdata('lastseen');
        $this->session->unset_userdata('role');
        redirect('authorize');  // Back to login page
    }
}

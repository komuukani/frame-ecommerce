<?php
/* * *
 * Project:    T-Build
 * Name:       Admin Side User
 * Package:    User.php
 * About:      A controller that handle backend of all pages
 * Copyright:  (C) 2022
 * Author:     Nishant Thummar
 * Website:    https://nishantthummar.tech/
 * * */
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    // Global Variables
    public $website, $developer, $web_link, $table_prefix, $web_data, $admin_data, $savepath = './admin_asset/';

    // Constructor
    public function __construct()
    {
        parent::__construct();

        $this->developer = DEVELOPER;
        $this->web_link = WEBLINK;
        $this->table_prefix = TABLE_PREFIX;     // SET TABLE PREFIX (TBL_) 
        // Check if database already connected or not [STATUS = PRE/POST] 
        $settings = INSTALLER_SETTING;
        if ($settings['status'] == "pre") {
            redirect('/install');
        }
        $this->web_data = $this->md->select('tbl_web_data');    // SELECT WEBSITE DATA
        $this->admin_data = $this->md->select_where('tbl_admin', array('email' => $this->session->userdata('aemail'))); // SELECT ADMIN DATA
        date_default_timezone_set(($this->web_data) ? $this->md->getItemName('tbl_timezone', 'timezone_id', 'timezone', $this->web_data[0]->timezone) : DEFAULT_TIMEZONE);
        $this->load->helper('cookie');
    }

    /*
      P A G E S
     */

    // Security for without login do not access page
    public function security()
    {
        if ($this->session->userdata('email') == "") {
            redirect('user-login');
        }
    }

    // Get ID Address or Unique ID of user
    public function getIPAddress()
    {
        return $this->input->cookie('unique_id');
    }

    // Page not found 404
    public function page_not_found()
    {
        $data = $this->setParameter(__FUNCTION__, "Page Not Found", false);
        $this->load->view('master', $data);
    }

    protected function setParameter($page, $title, $page_breadcumb)
    {
        // generate unique_id
        if (empty($this->input->cookie('unique_id'))) {
            $uid = $this->input->cookie('ci_session');
            set_cookie('unique_id', $uid, 60 * 60 * 24 * 365);
        }
        $data = [];
        $data['page_title'] = $title;
        $data['web_data'] = $this->web_data;    // get all Website data
        $data['admin_data'] = $this->admin_data;    // get all Admin data
        $data['website_title'] = $this->website;
        $data['developer'] = $this->developer;
        $data['web_link'] = $this->web_link;
        $data['page_breadcumb'] = $page_breadcumb;
        $data['page'] = 'user/' . $page;
        if ($this->db->table_exists($this->table_prefix . $page))
            $data[$page] = $this->md->select($this->table_prefix . $page);
        return $data;
    }

    // Login
    public function login()
    {
        $data = $this->setParameter(__FUNCTION__, "Login to Account", true);
        if ($this->input->post('login')) {
            $this->form_validation->set_rules('username', '', 'required', array("required" => "Enter Email or Phone!"));
            $this->form_validation->set_rules('password', 'Password', 'required', array("required" => "Enter Password!"));
            if ($this->form_validation->run() == TRUE) {
                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha
                if ($captchaStatus) {
                    $t['email'] = $this->input->post('username');
                    $r = $this->md->select_where('tbl_register', $t);
                    if (!empty($r) && ($this->encryption->decrypt($r[0]->password) == $this->input->post('password'))) {
                        $this->session->set_userdata('userType', 'regular');
                        $this->session->set_userdata('email', $t['email']);
                        //$this->md->update('tbl_wishlist', array('email' => $t['email'], 'unique_id' => ''), array('unique_id' => $this->getIPAddress()));
                        $this->md->update('tbl_cart', array('register_id' => $r[0]->register_id), array('unique_id' => $this->getIPAddress()));
                        redirect('welcome');
                    } else {
                        $data['error'] = 'Sorry, Authentication Fail';
                    }
                } else {
                    $data['error'] = 'Oops, Validate google reCaptcha!';
                }
            } else {
                $data['error'] = 'Sorry, Fillout email and password!';
            }
        }
        $this->load->view('master', $data);
    }

    // Register
    public function register()
    {
        $data = $this->setParameter(__FUNCTION__, "Register", true);
        if ($this->input->post('register')) {
            $this->form_validation->set_rules('fname', 'Full Name', 'required|regex_match[/^[a-zA-Z ]+$/]', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[tbl_register.email]', array("required" => "Enter Email Address!", "is_unique" => "Oops, Email Already Exists!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]|is_unique[tbl_register.phone]', array("required" => "Enter Phone Number!", "is_unique" => "Oops, Phone Already Exists!"));
            $this->form_validation->set_rules('password', 'Password', 'required', array("required" => "Enter Password!"));
            if ($this->form_validation->run() == TRUE) {
                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha
                if ($captchaStatus) {
                    $insert_data['fname'] = $this->input->post('fname');
                    $insert_data['email'] = $this->input->post('email');
                    $insert_data['phone'] = $this->input->post('phone');
                    $insert_data['password'] = $this->encryption->encrypt($this->input->post('password'));
                    $insert_data['status'] = 1;
                    $insert_data['register_date'] = date('Y-m-d');
                    if ($this->md->insert('tbl_register', $insert_data)) {
                        $registerId = $this->db->insert_id();
                        //                    $body = "<p><b>Name : </b>" . $this->input->post('fname') . "</p>";
                        //                    $body .= "<p><b>Email : </b>" . $this->input->post('email') . "</p>";
                        //                    $body .= "<p><b>Phone : </b>" . $this->input->post('phone') . "</p>";
                        //                    $body .= "<p><b>Password : </b>" . $this->input->post('password') . "</p>";
                        //                    $body .= "<p><b>Register Date : </b>" . date('Y-m-d H:i:s') . "</p>";
                        //                    $this->md->my_mailer($this->input->post('email'), 'register', $body);
                        $data['success'] = 'Registration successfully.';
                        $this->session->set_userdata('userType', 'regular');
                        $this->session->set_userdata('email', $this->input->post('email'));
                        //$this->md->update('tbl_wishlist', array('email' => $this->input->post('email'), 'unique_id' => ''), array('unique_id' => $this->getIPAddress()));
                        $this->md->update('tbl_cart', array('register_id' => $registerId), array('unique_id' => $this->getIPAddress()));
                        redirect('profile-success');
                    } else {
                        $data['error'] = 'Sorry, Registration Fail!';
                    }
                } else {
                    $data['error'] = 'Oops, Validate google reCaptcha!';
                }
            } else {
                $data['error'] = 'Sorry, Fillout all the fields!';
            }
        }
        $this->load->view('master', $data);
    }

    // Forgot Password
    public function forgot_password()
    {
        $data = $this->setParameter(__FUNCTION__, "Forgot Password", true);
        if ($this->input->post('forgot')) {
            $this->form_validation->set_rules('username', '', 'required', array("required" => "Enter Email or Phone!"));
            if ($this->form_validation->run() == TRUE) {
                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha
                if ($captchaStatus) {
                    $username = $this->input->post('username');
                    $userExisted = $this->md->my_query('SELECT * FROM `tbl_register` WHERE (`phone` = "' . $username . '" OR `email`="' . $username . '") AND delete_status = 0')->result();
                    if (!empty($userExisted)) {
                        if ($userExisted[0]->status) {
                            $username = $userExisted[0]->phone;
                            $otp = rand(100000, 999999); // OTP
                            $this->md->sendTextSMS($username, $otp);    // Send Text SMS on Mobile
                            $this->session->set_userdata('email', $userExisted[0]->email);
                            $this->session->set_userdata('show_menu', false);
                            $this->session->set_userdata('otp', $otp);
                            $this->session->set_userdata('page', "forgot");
                            redirect('user-verify');
                        } else {
                            $data['error'] = 'Sorry, Your profile has been deactivated!';
                        }
                    } else {
                        $data['error'] = 'Sorry, Authentication Fail!';
                    }
                } else {
                    $data['error'] = 'Oops, Validate google reCaptcha!';
                }
            } else {
                $data['error'] = 'Sorry, Fillout email and password!';
            }
        }
        $this->load->view('master', $data);
    }

    // Verify
    public function verify()
    {
        $data = $this->setParameter(__FUNCTION__, "Verify", true);
        $user = $this->session->userdata('email');
        $userData = $this->md->select_where('tbl_register', array('email' => $user));
        if ($this->input->post('verify')) {
            $this->form_validation->set_rules('otp', 'OTP', 'required|numeric|exact_length[6]', array("required" => "Enter Verification OTP!"));
            if ($this->form_validation->run() == TRUE) {
                //                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha 
                //                if ($captchaStatus) {
                $otp = $this->session->userdata('otp');
                if ($otp == $this->input->post('otp')) {
                    if ($this->md->update('tbl_register', array('status' => 1), array('email' => $user))) {
                        $type = $this->session->userdata('page');
                        if ($type == "forgot") {
                            redirect('update-password');
                        } else if ($type == "register") {
                            $this->session->set_userdata('show_menu', true);
                            redirect('pricing');
                        } else {
                            redirect('404');
                        }
                    } else {
                        $data['error'] = 'Sorry, verification not completed!';
                    }
                } else {
                    $data['error'] = 'Sorry, Invalid OTP entered!';
                }
                //                } else {
                //                    $data['error'] = 'Oops, Validate google reCaptcha!';
                //                }
            } else {
                $data['error'] = 'Sorry, Enter Valid OTP!';
            }
        }
        if ($this->input->post('resend')) {
            if (!empty($userData)) {
                $otp = rand(100000, 999999); // OTP 
                $this->session->set_userdata('otp', $otp);
                if ($userData[0]->phone != '') {
                    $this->md->sendTextSMS($userData[0]->phone, $otp);    // Send Text SMS on Mobile 
                    $data['success'] = "OTP has been sent successfully!";
                } else {
                    $data['error'] = "Sorry, You have not registered any phone number!";
                }
            } else {
                $data['error'] = 'Sorry, Unauthorized access!';
            }
        }
        $this->load->view('master', $data);
    }

    // Cart
    public function cart()
    {
        $data = $this->setParameter(__FUNCTION__, "Your Shopping Cart", true);
        $this->load->view('master', $data);
    }

    // checkout
    public function checkout()
    {
        $orderPlaced = $this->session->userdata('orderPlaced');
        if ($orderPlaced == 1) {
            redirect('order-placed');
        }
        $data = $this->setParameter(__FUNCTION__, "Checkout", true);

        $cart = $this->md->select_where('tbl_cart', array('unique_id' => $this->getIPAddress()));
        if (empty($cart)) {
            redirect('home');
        } else {

            if ($this->input->post('login')) {
                $this->form_validation->set_rules('username', '', 'required', array("required" => "Enter Email or Phone!"));
                $this->form_validation->set_rules('password', 'Password', 'required', array("required" => "Enter Password!"));
                if ($this->form_validation->run() == TRUE) {
                    $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha
                    if ($captchaStatus) {
                        $t['email'] = $this->input->post('username');
                        $r = $this->md->select_where('tbl_register', $t);
                        if (!empty($r) && ($this->encryption->decrypt($r[0]->password) == $this->input->post('password'))) {
                            $this->session->set_userdata('userType', 'regular');
                            $this->session->set_userdata('email', $t['email']);
                            $this->md->update('tbl_cart', array('register_id' => $r[0]->register_id), array('unique_id' => $this->getIPAddress()));
                            redirect('checkout');
                        } else {
                            $data['error'] = 'Sorry, Authentication Fail';
                        }
                    } else {
                        $data['error'] = 'Oops, Validate google reCaptcha!';
                    }
                } else {
                    $data['error'] = 'Sorry, Fillout email and password!';
                }
            }

        }
        $this->load->view('master', $data);
    }

    // Order Success Page
    public function success()
    {
        $data = $this->setParameter(__FUNCTION__, "Order Placed Successfully", true);
        $this->session->unset_userdata('orderPlaced');
        $this->load->view('master', $data);
    }

    // My order
    public function myorder()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "My Order", true);
        $user = $this->session->userdata('email');
        $user_id = $this->md->select_where('tbl_register', array('email' => $user));
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $data['orders'] = $this->md->select_where('tbl_bill', array('register_id' => $user_id[0]->register_id));
        $this->load->view('master', $data);
    }

    // User Dashboard
    public function welcome()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Welcome User", true);
        $user = $this->session->userdata('email');
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $this->load->view('master', $data);
    }

    // Change Password
    public function change_password()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Change Password", true);
        $user = $this->session->userdata('email');
        if ($this->input->post('update')) {
            $this->form_validation->set_rules('current_password', 'Current Password', 'required', array("required" => "Enter Current Password!"));
            $this->form_validation->set_rules('new_password', 'New Password', 'required', array("required" => "Enter New Password!"));
            $this->form_validation->set_rules('con_password', 'Confirm Password', 'required|matches[new_password]', array("required" => "Enter Confirm Password!"));
            if ($this->form_validation->run() == TRUE) {
                $current = $this->input->post('current_password');
                $new = $this->input->post('new_password');
                $user = $this->session->userdata('email');
                $dt = $this->md->select_where('tbl_register', array('email' => $user));
                if (!empty($dt)) {
                    $nn = $this->encryption->decrypt($dt[0]->password);
                    if ($nn == $current) {
                        $this->md->update('tbl_register', array('password' => $this->encryption->encrypt($new)), array('email' => $user));
                        $data['success'] = 'Password Changed Successfully.';
                    } else {
                        $data['error'] = 'Sorry, Current Password not matched!';
                    }
                } else {
                    $data['error'] = 'Sorry, Unauthorized access!';
                }
            } else {
                $data['error'] = 'Sorry, fillout all the field properly!';
            }
        }
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $this->load->view('master', $data);
    }

    // Edit Profile
    public function edit_profile()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Edit Profile", true);
        $user = $this->session->userdata('email');
        if ($this->input->post('save')) {
            $this->form_validation->set_rules('fname', 'Full Name', 'required', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]', array("required" => "Enter Phone Number!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('state', '', 'required', array("required" => "Enter State!"));
            $this->form_validation->set_rules('country', '', 'required', array("required" => "Enter Country!"));
            $this->form_validation->set_rules('city', '', 'required', array("required" => "Enter City!"));
            $this->form_validation->set_rules('address', '', 'required', array("required" => "Enter Address!"));
            $this->form_validation->set_rules('postal_code', 'Postal Code', 'required', array("required" => "Enter Postal Code!"));
            if ($this->form_validation->run() == TRUE) {
                $data1['fname'] = $this->input->post('fname');
                $data1['phone'] = $this->input->post('phone');
                $data1['email'] = $this->input->post('email');
                $data1['gender'] = $this->input->post('gender');
                $data1['country'] = $this->input->post('country');
                $data1['state'] = $this->input->post('state');
                $data1['city'] = $this->input->post('city');
                $data1['address'] = $this->input->post('address');
                $data1['postal_code'] = $this->input->post('postal_code');
                // Check already photo is exist or not, if yes than it will remove and than upload.
                $this->input->post('updateStatus') && ($this->input->post('oldPath') ? (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '') : '');  // Remove Old Photo
                $this->input->post('updateStatus') && ($data1['path'] = $this->md->uploadFile('customer'));    // Upload Photo and return path from model
                $this->md->update('tbl_register', $data1, array('email' => $user));
                $this->session->set_userdata('email', $this->input->post('email'));
                $data['success'] = 'Data Updated successfully.';
                redirect('edit-profile');
            } else {
                $data['error'] = 'Sorry, fillout all the field properly!';
            }
        }
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $this->load->view('master', $data);
    }

    // Profile Success
    public function profile_success()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Profile Created Successfully", true);
        $this->load->view('master', $data);
    }

    // Customer/ User Logout
    public function logout()
    {
        $this->security();
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('otp');
        $this->session->unset_userdata('show_menu');
        $this->session->unset_userdata('page');
        redirect('user-login');
    }


    // get city
    public function get_city()
    {
        $countryId = $this->input->post('countryId');
        $query = "SELECT * FROM `tbl_city` WHERE `state_id` in (SELECT `id` FROM `tbl_state` WHERE `country_id` = $countryId)";
        $cities = $this->md->my_query($query)->result();
        if ($cities) {
            echo '<option value="">City</option>';
            foreach ($cities as $city) {
                echo '<option value="' . ($city->id) . '">' . ($city->name) . '</option>';
            }
        }
    }

    // Add Order
    public function add_order()
    {
        $user = $this->session->userdata('email');
        $userdata = $cartProduct = array();
        if (isset($user)) :
            $userdata = $this->md->select_where('tbl_register', array('email' => $user));
            $cartProduct = $this->md->select_where('tbl_cart', array('register_id' => ($userdata ? $userdata[0]->register_id : '')));
        else:
            $cartProduct = $this->md->select_where('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));
        endif;

        $city = $this->input->post('city');
        $postal_code = $this->input->post('postal_code');
        $address = $this->input->post('address');
        $notes = $this->input->post('notes');
        $booking_destination = $this->input->post('booking_destination');
        $name_sales_person = $this->input->post('name_sales_person');
        $drop_location = $this->input->post('drop_location');
        $name_transport = $this->input->post('name_transport');

        if ($city != "" && $postal_code != "" && $address != "") {

            // Update latest shipping info into register table
            $up['city'] = $city;
            $up['address'] = $address;
            $up['postal_code'] = $postal_code;

            $this->md->update('tbl_register', $up, array('register_id' => ($userdata ? $userdata[0]->register_id : 0)));

            $totalAmount = 0;
            if ($cartProduct) {
                foreach ($cartProduct as $cartItem) {
                    $product_id = $cartItem->product_id;
                    $productData = $this->md->select_where('tbl_product', array('product_id' => $product_id));
                    if ($productData) {
                        $totalAmount = $totalAmount + $cartItem->qty * $productData[0]->stick * $productData[0]->ft * $productData[0]->rate;
                    }
                }
            }


            // Add order data into bill table
            $orderID = date('YmdHis');
            $ins['order_id'] = $orderID;
            $ins['unique_id'] = $this->input->cookie('unique_id');
            $ins['register_id'] = $userdata ? $userdata[0]->register_id : 0;
            $ins['fname'] = $userdata ? $userdata[0]->fname : '';
            $ins['phone'] = $userdata ? $userdata[0]->phone : '';
            $ins['email'] = $userdata ? $userdata[0]->email : '';
            $ins['city'] = $city;
            $ins['address'] = $address;
            $ins['postal_code'] = $postal_code;
            $ins['notes'] = $notes;
            $ins['booking_destination'] = $booking_destination;
            $ins['drop_location'] = $drop_location;
            $ins['name_sales_person'] = $name_sales_person;
            $ins['name_transport'] = $name_transport;
            $ins['total'] = $totalAmount;
            $ins['entry_date'] = date('Y-m-d');
            $ins['status'] = 'Inquired';

            if ($this->md->insert('tbl_bill', $ins)) {
                $billId = $this->db->insert_id();   // Fetch latest bill id
                $productGroups = '';
                // Insert into transaction table
                if ($cartProduct) {
                    foreach ($cartProduct as $cartItem) {
                        $product_id = $cartItem->product_id;
                        $productData = $this->md->select_where('tbl_product', array('product_id' => $product_id));
                        if ($productData) {
                            $amount = $cartItem->qty * $productData[0]->stick * $productData[0]->ft * $productData[0]->rate;
                            $insTra['bill_id'] = $billId;
                            $insTra['register_id'] = $cartItem->register_id;
                            $insTra['unique_id'] = $cartItem->unique_id;
                            $insTra['product_id'] = $cartItem->product_id;
                            $insTra['category_id'] = $cartItem->category_id;
                            $insTra['qty'] = $cartItem->qty;
                            $insTra['box'] = $cartItem->qty;
                            $insTra['stick'] = $productData ? $productData[0]->stick : '';
                            $insTra['ft'] = $productData ? $productData[0]->ft : '';
                            $insTra['rate'] = $productData ? $productData[0]->rate : '';
                            $insTra['amount'] = $amount;
                            $insTra['entry_date'] = date('Y-m-d H:i:s');

                            $this->md->insert('tbl_transaction', $insTra);

                            $productGroups .= $productData ? ($productData[0]->title . ", ") : '';
                        }
                    }
                }
                $productGroups = substr($productGroups, 0, -2);

                // Delete cart data
                $this->md->delete('tbl_cart', array('unique_id' => $this->input->cookie('unique_id')));

                $this->session->set_userdata('orderPlaced', true);  // Page redirect to success page

                $msg = "Hello, %0a%0aCustomer Name: *" . ($userdata ? ucfirst($userdata[0]->fname) : '') . "* %0aCustomer Phone: *" . ($userdata ? $userdata[0]->phone : '') . "* %0aOrder ID: *" . ($orderID) . "* %0aOrder Date: *" . (date('Y-m-d')) . "* %0a%0aI am interested in following products: %0a*" . $productGroups . "*  %0a%0aPlease send more information about it. %0a%0aThanks!";
                $url = 'https://api.whatsapp.com/send?phone=918460124263&text=' . $msg;
                echo $url; // Order added & send it to whatsapp

            } else {
                echo 0; // Something went wrong during bill insertion
            }

        } else {
            echo 2; // Enter Shipping info
        }

    }

}
